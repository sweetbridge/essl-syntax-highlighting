# Essl Extension for VSCode

## Features

- Syntax highlighting for .essl, .esslsln, and .esslproj files within VS Code.
- Commands to ease the development process of the ESSL language
    - Compile the current project and dependencies (hammer button in top right title bar)
    - Compile services
    - Compare ESSL projects
    - Generate services
    - Generate puml artifacts (png, svg, pdf)
    - Generate types (c#, kotlin, c# newtonsoft json, IMS DataScape)
    - Copy to project
    - Copy from project
- Automatic syntax checking and error highlighting
- Find usages (Right click on an identifier, 'Find all references')
- Code hovering tooltips: shows the file, documentation etc. of the type
- Go to definition command
    - Also 'Go back' and 'Go forward' buttons like in other IDEs. These are the arrow buttons in the top right title bar.
- Code completion
    - Enables code completion for types and decorators

## Notes
- The extension is only active when there is a **workspace or folder** open. Not just a file. This is necessary because the extension parses all the open files in the folder to enable autocompletion and commands.
- The workspace that is open must be in a subdirectory of a directory containing the .esslsln file. A warning will be shown if this file cannot be found. Many features, such as commands, will not work if there is no found .esslsln file.
- Adjust this .esslsln file's "root" variable to the location of the project directories to enable many commands dependent on the project files.

## Changing extension preferences/settings
- Go to the extensions tab in the left side of the VS Code window.
- Find "ESSL" - right click, "Extension Settings"
- There are a few config options you can change here based on user preference.

## Installation of the ESSL VSCode Extension
Windows/Mac:
1. Download the .zip and place the directory inside it into your ~/.vscode/extensions folder, and name the folder "essl-extension" or something similar.
2. In a terminal window, cd into the extension directory and run "npm install" to ensure you have the required packages.
3. Restart VS Code, and the extension should be operational.
4. If you don't see ESSL in the extensions folder, from the VSCode menu, select View --> Command Palette...  Then, search
for "Developer: Install Extension from Location..." and select it.  Navigate
to your ~/.vscode/extensions folder and select the folder you unzipped into.  After selecting, ESSL should show as an 
installed extension.

## Installation of PlantUML VSCode Extension
The VSCode 
<a href="https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml" target="_blank">PlantUML Extension</a> 
is also recommended for ESSL development. This will let you preview the plant uml diagrams that you generate via ```essl-extension/generate-artifacts``` while within VS Code.


To install the PlantUML extension, go into VS Code and click on extensions in the navigation bar in the left. Search for "PlantUML". Look for the one published by jebbs; it
should be at the top of the list. Click on it, and click install.


Now, while you have a .puml file open, you can right click and select "Preview Current Diagram" to display a live preview of the .puml diagram you're making. Alternatively, press alt+d.