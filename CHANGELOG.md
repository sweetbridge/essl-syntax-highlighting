# Change Log

All notable changes to the ESSL extension will be documented in this file.

## [1.5.0]
### Added
- @authPolicy decorator command/query to fields
- @public decorator command/query to fields

## [1.2.0]
### Added
- @question decorator to fields

## [1.1.0]
### Added
- Four configuration settings
- Dialog to show why the folder picker is being opened in gen artifacts

## [1.0.0]
- Initial release