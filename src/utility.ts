import * as vscode from 'vscode';
import * as path from 'path';
import * as fileSystem from 'fs';
import * as extension from './extension';

export var savedSolutionPath:string;

/*
Run the saveAll VS Code command. This method is generally called
when the user runs a command
*/
export function runSaveAll()
{
    vscode.workspace.saveAll();
}

/*
Get the color of the opened theme and terminal in VS Code, then return
the appropriate color
*/
export function getReadableColor()
{
    var color = vscode.window.activeColorTheme.kind;

    switch (color)
    {
        case 1: // light
            return " -c light";
        case 2: // dark
            return " -c dark";
    }

    return "";
}

/*
Read the directory of the file and a number of its parent directories to
find the .esslsln file.
*/
export function getSolutionPath(filePath:string) : string
{
    var dirPath:string = fileSystem.lstatSync(filePath).isDirectory() ? filePath : path.resolve(path.dirname(filePath));
    var rootPath:string = filePath.split(path.sep)[0] + path.sep;
    var solutionPath:string = "";
    while (true)
    {
        fileSystem.readdirSync(dirPath).forEach(file =>
        {
            var ext:string = path.extname(file);
            if (path.extname(file) === ".esslsln")
            {
                solutionPath = path.join(dirPath, file);
                return;
            }
        });

        if (solutionPath !== "")
        {
            savedSolutionPath = solutionPath;
            return solutionPath;
        }

        if (dirPath === rootPath)
        {
            break;
        }
        dirPath = path.resolve(path.dirname(dirPath));
    }

    // If no .esslsln is found, return ""
    savedSolutionPath = "";
    return "";
}

export function getSolutionPathQuoted(filePath:string) : string
{
    var solutionPath = getSolutionPath(filePath);
    if (solutionPath.includes(" "))
    {
        solutionPath = "\"" + solutionPath + "\"";
    }

    return solutionPath;
}

/*
Get the service name of the current project. Reads the json of the .esslproj file.
*/
export function getServiceName(filePath:string, getParent:boolean) : string
{
    var dirPath:string = filePath;

    // Get the absolute path of the folder than contains the file
    if (getParent)
    {
        dirPath = path.resolve(path.dirname(filePath));
    }

    // Loop through the folder's files until it finds .esslproj
    var serviceName:string = "";
    var foundSln:boolean = false;
    fileSystem.readdirSync(dirPath).forEach(file => {
        if (path.extname(file) === ".esslproj")
        {
            // Parse the .esslproj file's json for serviceName
            var esslprojPath:string = path.join(dirPath, file);
            var data:string = fileSystem.readFileSync(esslprojPath, "utf8");
            var json:any = JSON.parse(data);
            serviceName = json.serviceName;
        }
        else if (path.extname(file) === ".esslsln")
        {
            foundSln = true;
        }
    });

    if (serviceName === "" && !foundSln)
    {
        return getServiceName(dirPath, true);
    }

    // If nothing is found and the current dir contains the .esslsln, return ""
    return serviceName;
    
}

/*
Returns a new, valid terminal with the name provided.
*/
export function enableTerminal(name:string) : vscode.Terminal
{
    // Get the open terminal. If it exists, close it.
    let terminal:(vscode.Terminal | undefined) = vscode.window.activeTerminal;
    if (terminal !== undefined)
    {
        terminal.dispose();
    }

    // Initialize a new terminal and return it.
    terminal = vscode.window.createTerminal(name);
    terminal.show();
    return terminal;
}

/*
Opens a dropdown to select which services to use.
Returns the names of these services.
*/
export async function genServicePicker(filePath:string): Promise<string[]>
{
    // Get every service name to be put in the picker
    var folderNames:string[] = getFolderNames(filePath);

    // If the array is empty, none were found.
    if (folderNames.length === 0)
    {
        vscode.window.showErrorMessage("Could not find any service names");
        return [];
    }

    // If there is only one project, no need to show it to the user to select.  Just return it.
    if (folderNames.length === 1)
    {
        return folderNames;
    }

    // Get the name of the service to preselect via the .esslproj of the directory that contains the currently opened file.
    var preselectedService:string = getServiceName(filePath, true);

    // Create the array of the items for the quickPicker.
    // This is needed because of the preselect feature.
    var quickPickItems:vscode.QuickPickItem[] = [];
    for (var i=0; i<folderNames.length; i++)
    {
        quickPickItems.push({label: folderNames[i], picked: folderNames[i] === preselectedService});
    }

    // Show the quickPicker dropdown and await the user's input.
    var selectedFolders:string[] = [];
    await vscode.window.showQuickPick(quickPickItems,
    {
        canPickMany: true,
        title: "Choose services"
    }).then((selection) =>
    {
        // If selection is undefined, leave the array to return empty.
        // If not, copy the selection's labels as strings into the array.
        if (selection !== undefined)
        {
            for (var i:number = 0; i<selection!.length; i++)
            {
                var item:string = selection![i].label;
                selectedFolders.push(item);
            }
        }
    });

    // Return the selected folder names.
    // This is either empty or has values, never undefined or null.
    return selectedFolders;
}

/*
Opens a single-choice dropdown for the user to choose from one of the provided options
Returns the name of the selected option
*/
export async function genPicker(...items: string[]): Promise<string>
{
    // Show the quickPicker dropdown and await the user's input.
    var selectedItem:string = "";
    await vscode.window.showQuickPick(items,
    {
        title: "Choose an option"
    }).then((selection) =>
    {
        if (selection !== undefined)
        {
            selectedItem = selection;
        }
    });

    // Return the selection
    return selectedItem;
}

/*
Used for genPicker.
Gets every serviceName by looping through every directory to find the .esslproj files.
Returns an array of strings of these names.
*/
function getFolderNames(filePath:string) : string[]
{
    // Get the absolute path of the .esslsln file
    var solutionPath:string = getSolutionPath(filePath);

    // Get the absolute path of the directory the solution file is in
    var directoryPath:string = path.resolve(path.dirname(solutionPath));

    var serviceNames:string[] = [];

    // Loop through the directory that contains the .esslsln file
    fileSystem.readdirSync(directoryPath).forEach(file => {
        var absPath:string = path.join(directoryPath, file);
        if (fileSystem.lstatSync(absPath).isDirectory())
        {
            // If the file is a directory, loop through it to find the .esslproj file
            var serviceName:string = getServiceName(absPath, false);
            if (serviceName !== "" && !serviceNames.includes(serviceName))
            {
                serviceNames.push(serviceName);
            }
        }
    });

    // Return the array of service names. Will be length 0 if none were found.
    return serviceNames;
}