import * as vscode from 'vscode';

export enum Type {
    type,
    enum,
    dictionary,
    default,
    entity
}

export function getTypeFromStr(str: string) : Type
{
    switch(str)
    {
        case "entity":
            return Type.entity;
        case "type":
            return Type.type;
        case "enum":
            return Type.enum;
        case "dictionary":
            return Type.dictionary;
    }

    return Type.default;
}
