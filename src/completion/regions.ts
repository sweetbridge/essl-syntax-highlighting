import { ParserRuleContext } from 'antlr4ts';
import * as vscode from 'vscode';

export class RegionType
{
    public static regionTypes:RegionType[] = [];

    public regions:Region[];
    public type:string;

    constructor(_regions:Region[], _type:string)
    {
        this.regions = _regions;
        this.type = _type;
    }

    static getRegionTypeFromType(type: string) : RegionType
    {
        for (var i=0; i<this.regionTypes.length; i++)
        {
            if (RegionType.regionTypes[i].type === type)
            {
                return RegionType.regionTypes[i];
            }
        }

        var newRegionType = new RegionType([], type);
        RegionType.regionTypes.push(newRegionType);
        return newRegionType;
    }
}

export class Region
{
    range:vscode.Range;

    private constructor(_range:vscode.Range)
    {
        this.range = _range;
    }

    public static genRegion(context:ParserRuleContext, _type:string) : void
    {
        var range:vscode.Range = new vscode.Range(
            new vscode.Position(context._start.line-1, context._start.charPositionInLine),
            new vscode.Position(context._stop!.line-1, context._stop!.charPositionInLine + context.text.length)
        );

        var region:Region = new Region(range);
        var regionType:RegionType = RegionType.getRegionTypeFromType(_type);
        regionType.regions.push(region);

    }
}