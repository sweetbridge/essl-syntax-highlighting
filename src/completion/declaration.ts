import * as vscode from "vscode";
import { declarations } from "./completion";
import * as g4type from "./type";
import * as path from "path";

export const primitives:string[] = ["bool", "date", "datetime", "decimal", "float", "int", "string", "uuid"];

// Instance of a 'type xyz {' saved
// Vars:
// type = type, enum, dictionary, etc
// name = 'xyz' for above example
// fileName = name of the essl file
// projectName = name of the project the essl file is in
// position = where in the file the declaration occurs (used for go to definition)
// documentation = the document provided in the .essl file, in the lines before (documentation is ///)
// usages = instances of this declaration being used elsewhere
export class Declaration
{
    public isEqual(dec: Declaration) : boolean
    {
        return this.name === dec.name &&
            this.fsPath.toString() === dec.fsPath.toString();
    }

    constructor(_type:g4type.Type, _name:string, _fsPath:string, _fileName:string, _projectName:string, position:vscode.Position, _documentation:string)
    {
        this.type = _type;
        this.name = _name;
        this.fileName = _fileName;
        this.projectName = _projectName;
        this.position = position;
        this.documentation = _documentation;
        this.fsPath = vscode.Uri.file(_fsPath);

        this.usages = [];
    }

    public type:g4type.Type;
    public name:string;
    public fsPath:vscode.Uri;
    public fileName:string;
    public projectName:string;
    public position:vscode.Position;
    public documentation:string;

    public usages:DeclarationUsage[];
    
    // Generates the CompletionList using the given list of declarations, and optionally includes the primitive types (string, etc.)
    static toCompletionList(declarations:Declaration[], includePrimitives:boolean): vscode.ProviderResult<vscode.CompletionItem[] | vscode.CompletionList<vscode.CompletionItem>>
    {
        // Declare new CompletionList
        var list:vscode.CompletionList = new vscode.CompletionList();

        // Loop through every given declaration, and create a new CompletionItem for each
        for (var i=0; i<declarations.length; i++)
        {
            var dec:Declaration = declarations[i];
            var item:vscode.CompletionItem = new vscode.CompletionItem(dec.name);
            item.kind = Declaration.getCompletionItemKind(dec.type);
            item.detail = ` ${g4type.Type[dec.type]}\n ${dec.projectName}\n ${dec.fileName}`;
            if (dec.documentation !== "")
            {
                item.documentation = dec.documentation;
            }

            list.items.push(item);
        }

        // Optionally includes the primitive types in the CompletionList
        if (includePrimitives)
        {
            for (var i=0; i<primitives.length; i++)
            {
                var primitive = primitives[i];
                var item:vscode.CompletionItem = new vscode.CompletionItem(primitive);
                item.kind = vscode.CompletionItemKind.Keyword;
                item.detail = "Primitive";
                list.items.push(item);
            }
        }

        return list;
    }

    // Gets the corresponding CompletionItemKind (icon in completion list) for types
    static getCompletionItemKind(type:g4type.Type) : vscode.CompletionItemKind
    {
        switch (type)
        {
            case g4type.Type.type:
                return vscode.CompletionItemKind.TypeParameter;
            case g4type.Type.enum:
                return vscode.CompletionItemKind.Enum;
            case g4type.Type.dictionary:
                return vscode.CompletionItemKind.Class;
        }

        return vscode.CompletionItemKind.Field;
    }

    static getDeclarationFromName(text: string): Declaration | undefined
    {
        for (var i=0; i<declarations.length; i++)
        {
            if (declarations[i].name === text)
            {
                return declarations[i];
            }
        }

        return undefined;
    }

    // Gets the corresponding declaration from a given name
    static getDeclaration(text: string, uri: vscode.Uri, projPath: string): Declaration | undefined
    {
        // Get possible declarations
        var possibleDecs:Declaration[] = [];
        for (var i=0; i<declarations.length; i++)
        {
            if (declarations[i].name === text)
            {
                possibleDecs.push(declarations[i]);
            }
        }

        // If just one, return that one
        if (possibleDecs.length === 1)
        {
            return possibleDecs[0];
        }

        // Get the most likely declaration via file name
        for (var i=0; i<possibleDecs.length; i++)
        {
            var declaration:Declaration = possibleDecs[i];
            if (declaration.fsPath === uri)
            {
                return declaration;
            }
        }

        var splitProjPath = projPath.split(path.sep);
        // If that doesn't work, get it via project name
        for (var i=0; i<possibleDecs.length; i++)
        {
            var declaration:Declaration = possibleDecs[i];
            if (splitProjPath.indexOf(declaration.projectName.split("/")[0]) !== -1)
            {
                return declaration;
            }
        }

        if (possibleDecs.length >= 1)
        {
            return possibleDecs[0];
        }

        return undefined;
    }
}

// Stores the usages of the Declarations
export class DeclarationUsage
{
    public fileName:string;
    public projectName:string;
    public position:vscode.Position;

    constructor(_position:vscode.Position, _fileName: string, _projectName:string)
    {
        this.fileName = _fileName;
        this.projectName = _projectName;
        this.position = _position;
    }
}