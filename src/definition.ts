import * as vscode from 'vscode';
import * as hover from './hover';
import { declarations } from './completion/completion';
import { Declaration } from './completion/declaration';

export class EsslDefinitionProvider implements vscode.DefinitionProvider
{
    provideDefinition(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken): vscode.ProviderResult<vscode.Definition | vscode.LocationLink[]>
    {
        // Adjust the given position to be the start of the identifier.
        // If undefined, user is not hovering on an identifier, so return.
        var adjustedChar:number | undefined = hover.getPositionOfIdentifierStart(document, position);
        if (adjustedChar === undefined)
        {
            return;
        }

        // Declare the new position of the hovered location
        var adjustedPosition:vscode.Position = new vscode.Position(position.line, adjustedChar!);

        // Loop through all predefined declarations to find the corresponding one
        for (var i=0; i<declarations.length; i++)
        {
            var declaration:Declaration = declarations[i];
            if (declaration.usages.length > 0)
            {
                for (var j=0; j<declaration.usages.length; j++)
                {
                    var usage = declaration.usages[j];
                    if (usage.fileName === document.uri.fsPath && usage.position.isEqual(adjustedPosition))
                    {
                        var definition = new vscode.Location(declaration.fsPath, declaration.position);
                        return definition;
                    }
                }
            }
            
        }

        return undefined;
    }
}