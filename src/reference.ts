import * as vscode from 'vscode';
import * as hover from './hover';
import { declarations } from './completion/completion';
import { Declaration, DeclarationUsage } from './completion/declaration';

export class EsslReferenceProvider implements vscode.ReferenceProvider
{
    // Method implemented from vscode.ReferenceProvider
    provideReferences(document: vscode.TextDocument, position: vscode.Position, context: vscode.ReferenceContext, token: vscode.CancellationToken): vscode.ProviderResult<vscode.Location[]>
    {
        // Adjust the given position to be the start of the identifier.
        // If undefined, user is not hovering on an identifier, so return.
        var adjustedChar:number | undefined = hover.getPositionOfIdentifierStart(document, position);
        if (adjustedChar === undefined)
        {
            return;
        }

        // Declare the new position of the hovered location
        var adjustedPosition:vscode.Position = new vscode.Position(position.line, adjustedChar!);

        // Loop through all known declarations to find the currently selected one
        for (var i=0; i<declarations.length; i++)
        {
            var declaration:Declaration = declarations[i];
            if (declaration.fsPath.toString() === document.uri.toString())
            {
                var declarationAdjustedPosition = new vscode.Position(declaration.position.line, declaration.position.character - declaration.name.length);

                // If true, this is the correct declaration.
                if (declarationAdjustedPosition.isEqual(adjustedPosition))
                {
                    // Convert the array of DeclarationUsages to an array of locations to return
                    var locations:vscode.Location[] = [];
                    for (var i=0; i<declaration.usages.length; i++)
                    {
                        var usage:DeclarationUsage = declaration.usages[i];
                        locations.push(new vscode.Location(
                            vscode.Uri.file(usage.fileName),
                            usage.position
                        ));
                    }

                    return locations;
                }
            }

            // Check the usages of the declaration too
            // Doesn't have to be in the same file
            for (var j=0; j<declaration.usages.length; j++)
            {
                var usage:DeclarationUsage = declaration.usages[j];

                if (usage.fileName !== document.uri.fsPath)
                {
                    continue;
                }

                // If the cursor is selecting a usage of the current declaration,
                // return the result of the method called via the parent declaration's position.
                if (usage.position.isEqual(adjustedPosition))
                {
                    var locations:vscode.Location[] = [];
                    for (var i=0; i<declaration.usages.length; i++)
                    {
                        var usage:DeclarationUsage = declaration.usages[i];
                        locations.push(new vscode.Location(
                            vscode.Uri.file(usage.fileName),
                            usage.position
                        ));
                    }

                    return locations;
                }
            }
        }

        return;
    }
}