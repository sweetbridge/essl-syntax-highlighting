import * as vscode from 'vscode';
import * as utility from './utility';
import * as systemPath from 'path';
import * as fileSystem from 'fs';

/*
Generate artifacts command
Shows a dropdown of which services to generate for.
*/
export async function generatePlantUMLImages(uri: vscode.Uri, format:string)
{
	var path:string = uri.fsPath;
    utility.runSaveAll();

    // Search for the absolute path to the .esslsln file
    var solutionPath:string = utility.getSolutionPath(path);

    // If the path is "", no .esslsln was found.
    if (solutionPath === "")
    {
        vscode.window.showErrorMessage("Could not find .esslsln file");
        return;
    }

    // Verify the folder exists
    var content:string = fileSystem.readFileSync(solutionPath).toString();

    // Strip content of comments
    // Regex expression from https://stackoverflow.com/questions/33483667/
    content = content.replace(/\\"|"(?:\\"|[^"])*"|(\/\/.*|\/\*[\s\S]*?\*\/)/g, (m, g) => g ? "" : m);
    var json = JSON.parse(content);
    var rootPath:string = json["root"];

    // Show a dropdown for the user to choose services to generate
    var selectedFoldersArray:string[] = await utility.genServicePicker(path);

    // If the selected folders array is empty, none were chosen.
    // This can also be empty if no .esslproj files were found.
    // There should not be a message because if none were chosen,
    // it is self explanatory and if an error occurred, a warning was
    // already shown.
    if (selectedFoldersArray.length === 0)
    {
        return;
    }

    // Convert the array of selected folders into a string to be concat'd
    // into the terminal command
    var selectedFoldersString:string = selectedFoldersArray.join(" ");

    var dirnamePath:string = systemPath.dirname(solutionPath);
    var selectedFile:string = await getDirectoryToGenerateInto(dirnamePath);

    // If no directory is selected, cancel
    if (selectedFile === "")
    {
        return;
    }

    vscode.window.showInformationMessage("Generating into " + selectedFile);

    if (solutionPath.includes(" "))
    {
        solutionPath = "\"" + solutionPath + "\"";
    }

    // Open the terminal and run the command
    let terminal:vscode.Terminal = utility.enableTerminal("Generate");
    terminal.sendText("essl.generator genArtifacts " + solutionPath + " -r -q -f " + format + " -s " + selectedFoldersString + utility.getReadableColor() + " -o " + selectedFile);

    // If the artifacts directory exists, open it with explorer/finder
    var artifactsPath:string = systemPath.resolve(selectedFile);
    if (fileSystem.existsSync(artifactsPath))
    {
        let os:string = process.platform;

        switch (os)
        {
            case "darwin":
                require('child_process').exec(`open "${artifactsPath}"`);
                break;
            default:
                require('child_process').exec(`start "" "${artifactsPath}"`);
                break;
        }
    }
}

/*
Generate types command
Shows a dropdown of which services to generate for.
*/
export async function generateTypes(uri: vscode.Uri, format:string)
{
	var path:string = uri.fsPath;
    utility.runSaveAll();

    // Search for the absolute path to the .esslsln file
    var solutionPath:string = utility.getSolutionPathQuoted(path);

    // If the path is "", no .esslsln was found.
    if (solutionPath === "")
    {
        vscode.window.showErrorMessage("Could not find .esslsln file");
        return;
    }

    // Show a dropdown for the user to choose services to generate
    var selectedFoldersArray:string[] = await utility.genServicePicker(path);

    // If the selected folders array is empty, none were chosen.
    // This can also be empty if no .esslproj files were found.
    // There should not be a message because if none were chosen,
    // it is self explanatory and if an error occurred, a warning was
    // already shown.
    if (selectedFoldersArray.length === 0)
    {
        return;
    }

    // Convert the array of selected folders into a string to be concat'd
    // into the terminal command
    var selectedFoldersString:string = selectedFoldersArray.join(" ");

    var dirnamePath:string = systemPath.dirname(solutionPath);
    var selectedFile:string = await getDirectoryToGenerateInto(dirnamePath);

    // If no directory is selected, cancel
    if (selectedFile === "")
    {
        return;
    }

    vscode.window.showInformationMessage("Generating into " + selectedFile);

    // Open the terminal and run the command
    let terminal:vscode.Terminal = utility.enableTerminal("Generate");
    terminal.sendText("essl.generator genTypes " + solutionPath + " -q -g " + format + " -s " + selectedFoldersString + utility.getReadableColor() + " -o " + selectedFile);

    // If the artifacts directory exists, open it with explorer/finder
    var artifactsPath:string = systemPath.resolve(selectedFile);
    if (fileSystem.existsSync(artifactsPath))
    {
        let os:string = process.platform;

        switch (os)
        {
            case "darwin":
                require('child_process').exec(`open "${artifactsPath}"`);
                break;
            default:
                require('child_process').exec(`start "" "${artifactsPath}"`);
                break;
        }
    }
}

// Open a dialog menu for the user to pick a menu
async function getDirectoryToGenerateInto(dirnamePath:string) : Promise<string>
{
    // Get the config'd path
    const config = vscode.workspace.getConfiguration('ESSL');
    var defPath:(string|undefined) = config.get("DefaultArtifactsPath");

    // If the config'd path is valid, return it.
    if (defPath && defPath !== "")
    {
        if (fileSystem.existsSync(defPath))
        {
            return defPath;
        }

        vscode.window.showErrorMessage("Default path to artifacts is configured, but the configured path is invalid.");
    }

    const downloadsPath = systemPath.resolve(systemPath.join(process.env.HOMEPATH?? systemPath.resolve(dirnamePath), "Downloads"));
    // Open a picker to display where to generate the artifacts
    var uri:vscode.Uri = vscode.Uri.file(downloadsPath);
    
    var options:vscode.OpenDialogOptions = {
        canSelectFiles: false,
        canSelectFolders: true,
        canSelectMany: false,
        defaultUri: uri,
        openLabel: "Generate into this directory"
    };
    var selectedFile:string = "";
    await vscode.window.showOpenDialog(options).then(fileUri =>
    {
        if (fileUri && fileUri[0])
        {
            selectedFile = fileUri[0].fsPath;
        }
    });

    return selectedFile;

}
