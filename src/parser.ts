import * as essl_listener from './grammar/ESSL_Listener';
import * as essl_parser from './grammar/ESSL_Parser';
import { ANTLRErrorListener, ParserRuleContext, RecognitionException, Recognizer } from 'antlr4ts';
import * as vscode from 'vscode';
import * as antlr4ts from "antlr4ts";
import { Diagnostic } from "vscode";
import * as semantics from './semantics';
import * as reg from './completion/regions';
import * as decl from './completion/declaration';
import * as path from 'path';
import * as utility from './utility';

const attributeRegex = /\b(bool|date|datetime|decimal|float|int|string|uuid|null)\b/;

// ParseListener used for semantics. Detects enter/exit actions by the parser when
// delving into the generated code from ESSL_.g4

export class ParseListener implements essl_listener.ESSL_Listener
{
    // Utility method. Used by most listener functions below
    // Using the context, pushes the new semantic token structure into the builder
    push(context:ParserRuleContext, tokenType:string)
    {
        semantics.builder.push(
            new vscode.Range(
                new vscode.Position(context._start.line-1, context._start.charPositionInLine),
                new vscode.Position(context._stop!.line-1, context._stop!.charPositionInLine + context.text.length)
            ),
            tokenType,
            ["declaration"]
        );
    }

    // #region Semantic building
    exitTypeRefName(context: essl_parser.TypeRefNameContext)
    {
        if (!context.text.match(attributeRegex))
        {
            this.push(context, "property");
        }
        else
        {
            return;
        }

        var text = context.text;

        var projPath = semantics.currentFile.fsPath.replace(path.basename(semantics.currentFile.fsPath), "");
        var relProjPath:string = path.relative(path.dirname(utility.savedSolutionPath), projPath).replace(path.sep, "/");;

        var declaration = decl.Declaration.getDeclaration(text, semantics.currentFile, projPath);

        if (declaration === undefined)
        {
            return;
        }

        declaration.usages.push(new decl.DeclarationUsage(
            new vscode.Position(context.start.line-1, context.start.charPositionInLine),
            semantics.currentFile.fsPath,
            relProjPath
        ));
    }

    exitLineLabel(context: essl_parser.LineLabelContext)
    {
        this.push(context, "string");
    }

    exitEventRef(context: essl_parser.EventRefContext)
    {
        this.push(context, "property");
    }

    exitEnumValue(context: essl_parser.EnumValueContext)
    {
        this.push(context, "enumMember");
    }

    exitEventName(context: essl_parser.EventNameContext)
    {
        this.push(context, "enumMember");
    }

    exitCreatedEvent(context: essl_parser.CreatedEventContext)
    {
        this.push(context, "enumMember");
    }

    exitName(context: essl_parser.NameContext)
    {
        switch (context.parent!.constructor.name)
        {
            case "CommandContext":
            case "QueryContext":
                this.push(context, "function");
                break;
            case "ValueTypeContext":
            case "EnumDeclContext":
            case "InterfaceTypeContext":
            case "DictionaryContext":
                this.push(context, "class");
                break;
        }
    }

    exitDictDefault(context: essl_parser.DictDefaultContext)
    {
        this.push(context, "property");
    }

    exitEntityName(context: essl_parser.EntityNameContext)
    {
        this.push(context, "class");
    }

    exitDottedId(context: essl_parser.DottedIdContext)
    {
        this.push(context, "variable");
    }

    exitBaseName(context: essl_parser.BaseNameContext)
    {
        this.push(context, "class");

        var text = context.text;

        var projPath = semantics.currentFile.fsPath.replace(path.basename(semantics.currentFile.fsPath), "");
        var relProjPath:string = path.relative(path.dirname(utility.savedSolutionPath), projPath).replace(path.sep, "/");;

        var declaration = decl.Declaration.getDeclaration(text, semantics.currentFile, projPath);
        if (declaration === undefined)
        {
            return;
        }

        declaration.usages.push(new decl.DeclarationUsage(
            new vscode.Position(context.start.line-1, context.start.charPositionInLine),
            semantics.currentFile.fsPath,
            relProjPath
        ));
    }
    // #endregion

    // #region Utility
    exitTypeBlock(context: essl_parser.TypeBlockContext)
    {
        reg.Region.genRegion(context, "type");
    }

    exitCommandParams(context: essl_parser.CommandParamsContext)
    {
        reg.Region.genRegion(context, "param");
    }
    // #endregion

    // #region Decorators for code completion
    exitServiceDecor(context: essl_parser.ServiceDecorContext)
    {
        reg.Region.genRegion(context, "serviceDecor");
    }

    exitEntityTypeDecor(context: essl_parser.EntityTypeDecorContext)
    {
        reg.Region.genRegion(context, "entityTypeDecor");
    }

    exitIndexAttrDecor(context: essl_parser.IndexAttrDecorContext)
    {
        reg.Region.genRegion(context, "indexAttrDecor");
    }

    exitValueTypeDecor(context: essl_parser.ValueTypeDecorContext)
    {
        reg.Region.genRegion(context, "valueTypeDecor");
    }

    exitInterfaceTypeDecor(context: essl_parser.InterfaceTypeDecorContext)
    {
        reg.Region.genRegion(context, "interfaceTypeDecor");
    }

    exitEnumDecor(context: essl_parser.EnumDecorContext)
    {
        reg.Region.genRegion(context, "enumDecor");
    }

    exitDictionaryDecor(context: essl_parser.DictionaryDecorContext)
    {
        reg.Region.genRegion(context, "dictionaryDecor");
    }

    exitEventsDecor(context: essl_parser.EventsDecorContext)
    {
        reg.Region.genRegion(context, "eventsDecor");
    }

    exitEventNameDecor(context: essl_parser.EventNameDecorContext)
    {
        reg.Region.genRegion(context, "eventNameDecor");
    }

    exitFieldDecorator(context: essl_parser.FieldDecoratorContext)
    {
        reg.Region.genRegion(context, "fieldDecorator");
    }

    exitCommandsBlockDecorator(context: essl_parser.CommandsBlockDecoratorContext)
    {
        reg.Region.genRegion(context, "commandsBlockDecorator");
    }

    exitParamDecorator(context: essl_parser.ParamDecoratorContext)
    {
        reg.Region.genRegion(context, "paramDecorator");
    }

    exitQueriesBlockDecorator(context: essl_parser.QueriesBlockDecoratorContext)
    {
        reg.Region.genRegion(context, "queriesBlockDecorator");
    }

    exitQueryDecorator(context: essl_parser.QueryDecoratorContext)
    {
        reg.Region.genRegion(context, "queryDecorator");
    }

    exitSubscriptionsBlockDecorator(context: essl_parser.SubscriptionsBlockDecoratorContext)
    {
        reg.Region.genRegion(context, "subscriptionsBlockDecorator");
    }

    exitSubscriptionDecorator(context: essl_parser.SubscriptionDecoratorContext)
    {
        reg.Region.genRegion(context, "subscriptionDecorator");
    }

    exitCommandDecorator(context: essl_parser.CommandDecoratorContext)
    {
        reg.Region.genRegion(context, "commandDecorator");
    }
    // #endregion

    // #region DeclarationUsages
    
    // See exitTypeRefName above

    // #endregion
}

// ErrorListener used for diagnostics. Detects syntax errors
// Listener for both lexer and parser
export class ErrorParserListener implements ANTLRErrorListener<any>
{
    // This method is called by the client when a syntax error is detected
    syntaxError<T extends any>(recognizer: antlr4ts.Recognizer<T, any>, offendingSymbol: T | undefined, line: number, charPositionInLine: number, msg: string, e: antlr4ts.RecognitionException | undefined): void
    {
        // Declare the diagnostic via the params of this method
        var diagnostic:ESSLDiagnostic = new ESSLDiagnostic
        (
            new vscode.Range
            (
                new vscode.Position(line-1, charPositionInLine),
                new vscode.Position(line-1, charPositionInLine + 1)
            ),
            msg
        );

        // Check if diagnostic already exists
        // Diagnostic will already exist if multiple syntax errors occur on the same line
        for(var i:number = 0; i<semantics.diagnostics.length; i++)
        {
            let thatDiagnostic = semantics.diagnostics[i];
            if (diagnostic.equals(thatDiagnostic))
            {
                return;
            }
        }

        // Push the new diagnostic to the list
        semantics.diagnostics.push(diagnostic);
    }
}

// Subclass of vscode.Diagnostic for ESSL files
export class ESSLDiagnostic extends Diagnostic
{
    // Equals method
    // Used so there are not multiple of the same diagnostic on the same line
    public equals(that:Diagnostic) : boolean
    {
        return this.range.isEqual(that.range) &&
            this.message === that.message;
    }
}