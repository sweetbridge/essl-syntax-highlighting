// Generated from syntaxes/grammar/ESSL_.g4 by ANTLR 4.9.0-SNAPSHOT


import { ParseTreeListener } from "antlr4ts/tree/ParseTreeListener";

import { TopContext } from "./ESSL_Parser";
import { EntityContext } from "./ESSL_Parser";
import { EntityNameContext } from "./ESSL_Parser";
import { EntityBlockContext } from "./ESSL_Parser";
import { VirtualEntityContext } from "./ESSL_Parser";
import { ExtendsEntityContext } from "./ESSL_Parser";
import { BaseEntityNameContext } from "./ESSL_Parser";
import { VirtualEntityBlockContext } from "./ESSL_Parser";
import { NamespaceContext } from "./ESSL_Parser";
import { NamespaceNameContext } from "./ESSL_Parser";
import { DependsOnContext } from "./ESSL_Parser";
import { DependsOnNameSpaceContext } from "./ESSL_Parser";
import { ServiceContext } from "./ESSL_Parser";
import { ServiceDecorsContext } from "./ESSL_Parser";
import { ServiceDecorContext } from "./ESSL_Parser";
import { AllApiAuthPolicyContext } from "./ESSL_Parser";
import { AllCommandAuthPolicyContext } from "./ESSL_Parser";
import { AllQueryAuthPolicyContext } from "./ESSL_Parser";
import { AllSubscriptionAuthPolicyContext } from "./ESSL_Parser";
import { AllAdminAuthPolicyContext } from "./ESSL_Parser";
import { AuthPolicyNamesContext } from "./ESSL_Parser";
import { EcosystemPoliciesMethodContext } from "./ESSL_Parser";
import { EcosystemPoliciesMethodNameContext } from "./ESSL_Parser";
import { EcosystemSecretsMethodContext } from "./ESSL_Parser";
import { EcosystemSecretsMethodNameContext } from "./ESSL_Parser";
import { UmlGroupStrReplDecorContext } from "./ESSL_Parser";
import { UmlGroupStrReplParamsContext } from "./ESSL_Parser";
import { UmlGroupTitleStrReplContext } from "./ESSL_Parser";
import { UmlStrReplOldValueContext } from "./ESSL_Parser";
import { UmlStrReplNewValueContext } from "./ESSL_Parser";
import { UmlImageDecorContext } from "./ESSL_Parser";
import { UmlImageParamsContext } from "./ESSL_Parser";
import { UmlGroupImageNameContext } from "./ESSL_Parser";
import { UmlImageFormatContext } from "./ESSL_Parser";
import { UmlImageRemoveTitleContext } from "./ESSL_Parser";
import { UmlImageSubFolderContext } from "./ESSL_Parser";
import { EntityTypeContext } from "./ESSL_Parser";
import { ValueTypeContext } from "./ESSL_Parser";
import { InterfaceTypeContext } from "./ESSL_Parser";
import { NameContext } from "./ESSL_Parser";
import { ExtendsListContext } from "./ESSL_Parser";
import { BaseNameContext } from "./ESSL_Parser";
import { TypeBlockContext } from "./ESSL_Parser";
import { PossiblyEmptyTypeBlockContext } from "./ESSL_Parser";
import { EntityTypeDecorsContext } from "./ESSL_Parser";
import { EntityTypeDecorContext } from "./ESSL_Parser";
import { PartialCommandContext } from "./ESSL_Parser";
import { TransCoordinatorContext } from "./ESSL_Parser";
import { DefaultConstructorContext } from "./ESSL_Parser";
import { IndexTypeDecorContext } from "./ESSL_Parser";
import { UniqueIndexTypeDecorContext } from "./ESSL_Parser";
import { IndexTypeAttrsContext } from "./ESSL_Parser";
import { IndexKeyAttrsContext } from "./ESSL_Parser";
import { IndexKeyAttrContext } from "./ESSL_Parser";
import { IndexedFieldContext } from "./ESSL_Parser";
import { QueryPartitionDecorContext } from "./ESSL_Parser";
import { QueryPartitionAttrsContext } from "./ESSL_Parser";
import { PartitionIndexContext } from "./ESSL_Parser";
import { PartitionTypeUniqueIndexContext } from "./ESSL_Parser";
import { CrossPartitionDecorContext } from "./ESSL_Parser";
import { UmlGroupDecorContext } from "./ESSL_Parser";
import { UmlGroupContext } from "./ESSL_Parser";
import { UmlGroupTitleContext } from "./ESSL_Parser";
import { UmlSubGroupContext } from "./ESSL_Parser";
import { HashLookupDecorContext } from "./ESSL_Parser";
import { BeforeEventPersistContext } from "./ESSL_Parser";
import { AuthPolicyNameContext } from "./ESSL_Parser";
import { AuthPolicyDecorContext } from "./ESSL_Parser";
import { PublicDecorContext } from "./ESSL_Parser";
import { PluralNameDecorContext } from "./ESSL_Parser";
import { PluralNameContext } from "./ESSL_Parser";
import { TableNameDecorContext } from "./ESSL_Parser";
import { TableNameContext } from "./ESSL_Parser";
import { GraphQLNameDecorContext } from "./ESSL_Parser";
import { GraphQLNameContext } from "./ESSL_Parser";
import { GraphQLPluralNameDecorContext } from "./ESSL_Parser";
import { GraphQLPluralNameContext } from "./ESSL_Parser";
import { GraphQLCamelCaseDecorContext } from "./ESSL_Parser";
import { GraphQLCamelCaseContext } from "./ESSL_Parser";
import { GraphQLPluralCamelCaseDecorContext } from "./ESSL_Parser";
import { GraphQLPluralCamelCaseContext } from "./ESSL_Parser";
import { Base64RefIdsDecorContext } from "./ESSL_Parser";
import { InterfaceDecorContext } from "./ESSL_Parser";
import { InterfaceNameContext } from "./ESSL_Parser";
import { CamelCaseDecorContext } from "./ESSL_Parser";
import { CamelCaseNameContext } from "./ESSL_Parser";
import { GenerateIfDecorContext } from "./ESSL_Parser";
import { GenerateIfNameContext } from "./ESSL_Parser";
import { DimensionNameDecorContext } from "./ESSL_Parser";
import { StyleDecorContext } from "./ESSL_Parser";
import { EventsContext } from "./ESSL_Parser";
import { SlowlyChangingContext } from "./ESSL_Parser";
import { LedgerContext } from "./ESSL_Parser";
import { FactLedgerContext } from "./ESSL_Parser";
import { IndexAttrDecorsContext } from "./ESSL_Parser";
import { IndexAttrDecorContext } from "./ESSL_Parser";
import { CacheModelContext } from "./ESSL_Parser";
import { NoCacheModelContext } from "./ESSL_Parser";
import { AllowTemporalContext } from "./ESSL_Parser";
import { IndexNameDecorContext } from "./ESSL_Parser";
import { IndexNameContext } from "./ESSL_Parser";
import { ValueTypeDecorsContext } from "./ESSL_Parser";
import { ValueTypeDecorContext } from "./ESSL_Parser";
import { PartialContext } from "./ESSL_Parser";
import { JsonConstructorContext } from "./ESSL_Parser";
import { WebValueStringContext } from "./ESSL_Parser";
import { WebValueMethodContext } from "./ESSL_Parser";
import { DiscriminatedByContext } from "./ESSL_Parser";
import { DiscriminatedByValueContext } from "./ESSL_Parser";
import { OutputTypeContext } from "./ESSL_Parser";
import { VoidPrimaryFactContext } from "./ESSL_Parser";
import { InterfaceTypeDecorsContext } from "./ESSL_Parser";
import { InterfaceTypeDecorContext } from "./ESSL_Parser";
import { EnumDeclContext } from "./ESSL_Parser";
import { EnumBlockContext } from "./ESSL_Parser";
import { EnumItemContext } from "./ESSL_Parser";
import { EnumValueContext } from "./ESSL_Parser";
import { EnumExplicitValueContext } from "./ESSL_Parser";
import { EnumDecorsContext } from "./ESSL_Parser";
import { EnumDecorContext } from "./ESSL_Parser";
import { SuppressDecorContext } from "./ESSL_Parser";
import { SuppressOptionContext } from "./ESSL_Parser";
import { ModelsuppressOptionContext } from "./ESSL_Parser";
import { GraphQlsuppressOptionContext } from "./ESSL_Parser";
import { InputTypeOptionContext } from "./ESSL_Parser";
import { FlagsDecorContext } from "./ESSL_Parser";
import { DictionaryContext } from "./ESSL_Parser";
import { DictionaryBlockContext } from "./ESSL_Parser";
import { DictPartitionedContext } from "./ESSL_Parser";
import { DictValueTypeDeclContext } from "./ESSL_Parser";
import { DictValueTypeContext } from "./ESSL_Parser";
import { DictDefaultDeclContext } from "./ESSL_Parser";
import { DictDefaultContext } from "./ESSL_Parser";
import { DictEntriesContext } from "./ESSL_Parser";
import { DictionaryDecorsContext } from "./ESSL_Parser";
import { DictionaryDecorContext } from "./ESSL_Parser";
import { FieldDefContext } from "./ESSL_Parser";
import { TypeRefContext } from "./ESSL_Parser";
import { TypeRefNameContext } from "./ESSL_Parser";
import { PrimitiveTypeContext } from "./ESSL_Parser";
import { ValueTypeRefContext } from "./ESSL_Parser";
import { ListIndicatorContext } from "./ESSL_Parser";
import { NullableIndicatorContext } from "./ESSL_Parser";
import { FieldInitializerContext } from "./ESSL_Parser";
import { NullValueContext } from "./ESSL_Parser";
import { BoolValueContext } from "./ESSL_Parser";
import { NewInstanceContext } from "./ESSL_Parser";
import { InitialEnumValContext } from "./ESSL_Parser";
import { NumValueContext } from "./ESSL_Parser";
import { IntegerContext } from "./ESSL_Parser";
import { DecimalContext } from "./ESSL_Parser";
import { StringValueContext } from "./ESSL_Parser";
import { FlagsEnumSetContext } from "./ESSL_Parser";
import { FlagsEnumValueContext } from "./ESSL_Parser";
import { FieldDecoratorsContext } from "./ESSL_Parser";
import { FieldDecoratorContext } from "./ESSL_Parser";
import { IndexDecorContext } from "./ESSL_Parser";
import { UniqueIndexDecorContext } from "./ESSL_Parser";
import { IndexAttrsContext } from "./ESSL_Parser";
import { CaseInsensitiveContext } from "./ESSL_Parser";
import { NullHandlingContext } from "./ESSL_Parser";
import { InlineEnumDecorContext } from "./ESSL_Parser";
import { InlineEnumValueContext } from "./ESSL_Parser";
import { ComposedDecorContext } from "./ESSL_Parser";
import { RequiredDecorContext } from "./ESSL_Parser";
import { ReadOnlyDecorContext } from "./ESSL_Parser";
import { HiddenDecorContext } from "./ESSL_Parser";
import { CalculatedDecorContext } from "./ESSL_Parser";
import { ImmutableDecorContext } from "./ESSL_Parser";
import { ClonePartitionerDecorContext } from "./ESSL_Parser";
import { CloneIdAsIsDecorContext } from "./ESSL_Parser";
import { FieldUmlDecorContext } from "./ESSL_Parser";
import { FieldUmlGroupTitleContext } from "./ESSL_Parser";
import { DirectionContext } from "./ESSL_Parser";
import { HorizontalVerticalContext } from "./ESSL_Parser";
import { LineLengthContext } from "./ESSL_Parser";
import { HideRelationshipContext } from "./ESSL_Parser";
import { LineLabelContext } from "./ESSL_Parser";
import { SectionDecorContext } from "./ESSL_Parser";
import { BreakTypeContext } from "./ESSL_Parser";
import { AsValueTypeDecorContext } from "./ESSL_Parser";
import { SameAsDecorContext } from "./ESSL_Parser";
import { SameAsPersistContext } from "./ESSL_Parser";
import { ConstantDecorContext } from "./ESSL_Parser";
import { NotPersistedDecorContext } from "./ESSL_Parser";
import { HandCodedDecorContext } from "./ESSL_Parser";
import { DictDecorContext } from "./ESSL_Parser";
import { TypeDiscriminatorContext } from "./ESSL_Parser";
import { MultiLineDecorContext } from "./ESSL_Parser";
import { QuestionDecorContext } from "./ESSL_Parser";
import { LabelDecorContext } from "./ESSL_Parser";
import { LabelContext } from "./ESSL_Parser";
import { AutoFillDecorContext } from "./ESSL_Parser";
import { AutoFillTypeContext } from "./ESSL_Parser";
import { AttributeDecorContext } from "./ESSL_Parser";
import { AttributePairContext } from "./ESSL_Parser";
import { AttributeKeyContext } from "./ESSL_Parser";
import { AttributeValueContext } from "./ESSL_Parser";
import { FactDimensionDecorContext } from "./ESSL_Parser";
import { DimPrimaryContext } from "./ESSL_Parser";
import { DimInxContext } from "./ESSL_Parser";
import { DimensionKeyDecorContext } from "./ESSL_Parser";
import { JournalEntriesDecorContext } from "./ESSL_Parser";
import { EntityEventsContext } from "./ESSL_Parser";
import { EventsBlockContext } from "./ESSL_Parser";
import { CreatedEventDefContext } from "./ESSL_Parser";
import { CreatedEventContext } from "./ESSL_Parser";
import { EventDefContext } from "./ESSL_Parser";
import { EventNameContext } from "./ESSL_Parser";
import { EventsDecorsContext } from "./ESSL_Parser";
import { EventsDecorContext } from "./ESSL_Parser";
import { EventNameDecorsContext } from "./ESSL_Parser";
import { EventNameDecorContext } from "./ESSL_Parser";
import { EntityCommandsContext } from "./ESSL_Parser";
import { CommandsBlockContext } from "./ESSL_Parser";
import { CommandsBlockDecoratorContext } from "./ESSL_Parser";
import { CommandsBlockDecoratorsContext } from "./ESSL_Parser";
import { CommandContext } from "./ESSL_Parser";
import { CommandParamsContext } from "./ESSL_Parser";
import { ParamContext } from "./ESSL_Parser";
import { ParamDeclContext } from "./ESSL_Parser";
import { OptionalTypeRefContext } from "./ESSL_Parser";
import { ParamNameModifiersContext } from "./ESSL_Parser";
import { ParamInitializerContext } from "./ESSL_Parser";
import { CommandResultsInContext } from "./ESSL_Parser";
import { YieldsContext } from "./ESSL_Parser";
import { EventListContext } from "./ESSL_Parser";
import { EventRefContext } from "./ESSL_Parser";
import { ReturnsTypeContext } from "./ESSL_Parser";
import { ReturnTypeRefContext } from "./ESSL_Parser";
import { ParamDecoratorContext } from "./ESSL_Parser";
import { TypeQualifierDecorContext } from "./ESSL_Parser";
import { ParamDecoratorsContext } from "./ESSL_Parser";
import { TypeQualifierNameContext } from "./ESSL_Parser";
import { IndexQualifierContext } from "./ESSL_Parser";
import { FilterExprContext } from "./ESSL_Parser";
import { ByValueDecorContext } from "./ESSL_Parser";
import { IdDecorContext } from "./ESSL_Parser";
import { CommandDecoratorContext } from "./ESSL_Parser";
import { CommandDecoratorsContext } from "./ESSL_Parser";
import { ModelDecorContext } from "./ESSL_Parser";
import { AsyncDecorContext } from "./ESSL_Parser";
import { AsyncSpecContext } from "./ESSL_Parser";
import { AsyncParamValidationContext } from "./ESSL_Parser";
import { AsyncModelValidationContext } from "./ESSL_Parser";
import { AsyncBusinessLogicContext } from "./ESSL_Parser";
import { SyncDecorContext } from "./ESSL_Parser";
import { TransDecorContext } from "./ESSL_Parser";
import { TransAttrContext } from "./ESSL_Parser";
import { ContinuationDecorContext } from "./ESSL_Parser";
import { EffectiveDateDecorContext } from "./ESSL_Parser";
import { ExplicitDecorContext } from "./ESSL_Parser";
import { InternalDecorContext } from "./ESSL_Parser";
import { MessageSourceDecorContext } from "./ESSL_Parser";
import { CreateDecorContext } from "./ESSL_Parser";
import { DeleteDecorContext } from "./ESSL_Parser";
import { PartialErrorDecorContext } from "./ESSL_Parser";
import { GenerateDecorContext } from "./ESSL_Parser";
import { GeneratorOptionContext } from "./ESSL_Parser";
import { GenSetFieldContext } from "./ESSL_Parser";
import { GenAssignmentsContext } from "./ESSL_Parser";
import { AssignmentListContext } from "./ESSL_Parser";
import { AssignmentContext } from "./ESSL_Parser";
import { ValueExpressionContext } from "./ESSL_Parser";
import { GenAddToSetContext } from "./ESSL_Parser";
import { GenRemoveFromSetContext } from "./ESSL_Parser";
import { GenClearSetContext } from "./ESSL_Parser";
import { GenUpdateContext } from "./ESSL_Parser";
import { JsonKeyContext } from "./ESSL_Parser";
import { RefFieldContext } from "./ESSL_Parser";
import { EntityQueriesContext } from "./ESSL_Parser";
import { QueriesBlockContext } from "./ESSL_Parser";
import { QueriesBlockDecoratorContext } from "./ESSL_Parser";
import { QueriesBlockDecoratorsContext } from "./ESSL_Parser";
import { QueryContext } from "./ESSL_Parser";
import { QueryDecoratorContext } from "./ESSL_Parser";
import { QueryDecoratorsContext } from "./ESSL_Parser";
import { TemporalDecorContext } from "./ESSL_Parser";
import { ProvideUserContextDecorContext } from "./ESSL_Parser";
import { ProvideGraphQLSchemaDecorContext } from "./ESSL_Parser";
import { EntitySubscriptionsContext } from "./ESSL_Parser";
import { SubscriptionsBlockContext } from "./ESSL_Parser";
import { SubscriptionsBlockDecoratorContext } from "./ESSL_Parser";
import { SubscriptionsBlockDecoratorsContext } from "./ESSL_Parser";
import { SubscriptionContext } from "./ESSL_Parser";
import { SubscriptionDecoratorContext } from "./ESSL_Parser";
import { SubscriptionDecoratorsContext } from "./ESSL_Parser";
import { DottedIdContext } from "./ESSL_Parser";
import { CommentContext } from "./ESSL_Parser";
import { BeforeCommentContext } from "./ESSL_Parser";
import { AfterCommentContext } from "./ESSL_Parser";
import { EsslCommentContext } from "./ESSL_Parser";
import { DomBeforeLineCommentContext } from "./ESSL_Parser";
import { DomBlockCommentContext } from "./ESSL_Parser";
import { EsslBlockCommentContext } from "./ESSL_Parser";
import { EsslLineCommentContext } from "./ESSL_Parser";


/**
 * This interface defines a complete listener for a parse tree produced by
 * `ESSL_Parser`.
 */
export interface ESSL_Listener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by `ESSL_Parser.top`.
	 * @param ctx the parse tree
	 */
	enterTop?: (ctx: TopContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.top`.
	 * @param ctx the parse tree
	 */
	exitTop?: (ctx: TopContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.entity`.
	 * @param ctx the parse tree
	 */
	enterEntity?: (ctx: EntityContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.entity`.
	 * @param ctx the parse tree
	 */
	exitEntity?: (ctx: EntityContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.entityName`.
	 * @param ctx the parse tree
	 */
	enterEntityName?: (ctx: EntityNameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.entityName`.
	 * @param ctx the parse tree
	 */
	exitEntityName?: (ctx: EntityNameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.entityBlock`.
	 * @param ctx the parse tree
	 */
	enterEntityBlock?: (ctx: EntityBlockContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.entityBlock`.
	 * @param ctx the parse tree
	 */
	exitEntityBlock?: (ctx: EntityBlockContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.virtualEntity`.
	 * @param ctx the parse tree
	 */
	enterVirtualEntity?: (ctx: VirtualEntityContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.virtualEntity`.
	 * @param ctx the parse tree
	 */
	exitVirtualEntity?: (ctx: VirtualEntityContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.extendsEntity`.
	 * @param ctx the parse tree
	 */
	enterExtendsEntity?: (ctx: ExtendsEntityContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.extendsEntity`.
	 * @param ctx the parse tree
	 */
	exitExtendsEntity?: (ctx: ExtendsEntityContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.baseEntityName`.
	 * @param ctx the parse tree
	 */
	enterBaseEntityName?: (ctx: BaseEntityNameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.baseEntityName`.
	 * @param ctx the parse tree
	 */
	exitBaseEntityName?: (ctx: BaseEntityNameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.virtualEntityBlock`.
	 * @param ctx the parse tree
	 */
	enterVirtualEntityBlock?: (ctx: VirtualEntityBlockContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.virtualEntityBlock`.
	 * @param ctx the parse tree
	 */
	exitVirtualEntityBlock?: (ctx: VirtualEntityBlockContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.namespace`.
	 * @param ctx the parse tree
	 */
	enterNamespace?: (ctx: NamespaceContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.namespace`.
	 * @param ctx the parse tree
	 */
	exitNamespace?: (ctx: NamespaceContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.namespaceName`.
	 * @param ctx the parse tree
	 */
	enterNamespaceName?: (ctx: NamespaceNameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.namespaceName`.
	 * @param ctx the parse tree
	 */
	exitNamespaceName?: (ctx: NamespaceNameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.dependsOn`.
	 * @param ctx the parse tree
	 */
	enterDependsOn?: (ctx: DependsOnContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.dependsOn`.
	 * @param ctx the parse tree
	 */
	exitDependsOn?: (ctx: DependsOnContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.dependsOnNameSpace`.
	 * @param ctx the parse tree
	 */
	enterDependsOnNameSpace?: (ctx: DependsOnNameSpaceContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.dependsOnNameSpace`.
	 * @param ctx the parse tree
	 */
	exitDependsOnNameSpace?: (ctx: DependsOnNameSpaceContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.service`.
	 * @param ctx the parse tree
	 */
	enterService?: (ctx: ServiceContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.service`.
	 * @param ctx the parse tree
	 */
	exitService?: (ctx: ServiceContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.serviceDecors`.
	 * @param ctx the parse tree
	 */
	enterServiceDecors?: (ctx: ServiceDecorsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.serviceDecors`.
	 * @param ctx the parse tree
	 */
	exitServiceDecors?: (ctx: ServiceDecorsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.serviceDecor`.
	 * @param ctx the parse tree
	 */
	enterServiceDecor?: (ctx: ServiceDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.serviceDecor`.
	 * @param ctx the parse tree
	 */
	exitServiceDecor?: (ctx: ServiceDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.allApiAuthPolicy`.
	 * @param ctx the parse tree
	 */
	enterAllApiAuthPolicy?: (ctx: AllApiAuthPolicyContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.allApiAuthPolicy`.
	 * @param ctx the parse tree
	 */
	exitAllApiAuthPolicy?: (ctx: AllApiAuthPolicyContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.allCommandAuthPolicy`.
	 * @param ctx the parse tree
	 */
	enterAllCommandAuthPolicy?: (ctx: AllCommandAuthPolicyContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.allCommandAuthPolicy`.
	 * @param ctx the parse tree
	 */
	exitAllCommandAuthPolicy?: (ctx: AllCommandAuthPolicyContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.allQueryAuthPolicy`.
	 * @param ctx the parse tree
	 */
	enterAllQueryAuthPolicy?: (ctx: AllQueryAuthPolicyContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.allQueryAuthPolicy`.
	 * @param ctx the parse tree
	 */
	exitAllQueryAuthPolicy?: (ctx: AllQueryAuthPolicyContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.allSubscriptionAuthPolicy`.
	 * @param ctx the parse tree
	 */
	enterAllSubscriptionAuthPolicy?: (ctx: AllSubscriptionAuthPolicyContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.allSubscriptionAuthPolicy`.
	 * @param ctx the parse tree
	 */
	exitAllSubscriptionAuthPolicy?: (ctx: AllSubscriptionAuthPolicyContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.allAdminAuthPolicy`.
	 * @param ctx the parse tree
	 */
	enterAllAdminAuthPolicy?: (ctx: AllAdminAuthPolicyContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.allAdminAuthPolicy`.
	 * @param ctx the parse tree
	 */
	exitAllAdminAuthPolicy?: (ctx: AllAdminAuthPolicyContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.authPolicyNames`.
	 * @param ctx the parse tree
	 */
	enterAuthPolicyNames?: (ctx: AuthPolicyNamesContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.authPolicyNames`.
	 * @param ctx the parse tree
	 */
	exitAuthPolicyNames?: (ctx: AuthPolicyNamesContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.ecosystemPoliciesMethod`.
	 * @param ctx the parse tree
	 */
	enterEcosystemPoliciesMethod?: (ctx: EcosystemPoliciesMethodContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.ecosystemPoliciesMethod`.
	 * @param ctx the parse tree
	 */
	exitEcosystemPoliciesMethod?: (ctx: EcosystemPoliciesMethodContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.ecosystemPoliciesMethodName`.
	 * @param ctx the parse tree
	 */
	enterEcosystemPoliciesMethodName?: (ctx: EcosystemPoliciesMethodNameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.ecosystemPoliciesMethodName`.
	 * @param ctx the parse tree
	 */
	exitEcosystemPoliciesMethodName?: (ctx: EcosystemPoliciesMethodNameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.ecosystemSecretsMethod`.
	 * @param ctx the parse tree
	 */
	enterEcosystemSecretsMethod?: (ctx: EcosystemSecretsMethodContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.ecosystemSecretsMethod`.
	 * @param ctx the parse tree
	 */
	exitEcosystemSecretsMethod?: (ctx: EcosystemSecretsMethodContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.ecosystemSecretsMethodName`.
	 * @param ctx the parse tree
	 */
	enterEcosystemSecretsMethodName?: (ctx: EcosystemSecretsMethodNameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.ecosystemSecretsMethodName`.
	 * @param ctx the parse tree
	 */
	exitEcosystemSecretsMethodName?: (ctx: EcosystemSecretsMethodNameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.umlGroupStrReplDecor`.
	 * @param ctx the parse tree
	 */
	enterUmlGroupStrReplDecor?: (ctx: UmlGroupStrReplDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.umlGroupStrReplDecor`.
	 * @param ctx the parse tree
	 */
	exitUmlGroupStrReplDecor?: (ctx: UmlGroupStrReplDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.umlGroupStrReplParams`.
	 * @param ctx the parse tree
	 */
	enterUmlGroupStrReplParams?: (ctx: UmlGroupStrReplParamsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.umlGroupStrReplParams`.
	 * @param ctx the parse tree
	 */
	exitUmlGroupStrReplParams?: (ctx: UmlGroupStrReplParamsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.umlGroupTitleStrRepl`.
	 * @param ctx the parse tree
	 */
	enterUmlGroupTitleStrRepl?: (ctx: UmlGroupTitleStrReplContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.umlGroupTitleStrRepl`.
	 * @param ctx the parse tree
	 */
	exitUmlGroupTitleStrRepl?: (ctx: UmlGroupTitleStrReplContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.umlStrReplOldValue`.
	 * @param ctx the parse tree
	 */
	enterUmlStrReplOldValue?: (ctx: UmlStrReplOldValueContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.umlStrReplOldValue`.
	 * @param ctx the parse tree
	 */
	exitUmlStrReplOldValue?: (ctx: UmlStrReplOldValueContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.umlStrReplNewValue`.
	 * @param ctx the parse tree
	 */
	enterUmlStrReplNewValue?: (ctx: UmlStrReplNewValueContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.umlStrReplNewValue`.
	 * @param ctx the parse tree
	 */
	exitUmlStrReplNewValue?: (ctx: UmlStrReplNewValueContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.umlImageDecor`.
	 * @param ctx the parse tree
	 */
	enterUmlImageDecor?: (ctx: UmlImageDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.umlImageDecor`.
	 * @param ctx the parse tree
	 */
	exitUmlImageDecor?: (ctx: UmlImageDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.umlImageParams`.
	 * @param ctx the parse tree
	 */
	enterUmlImageParams?: (ctx: UmlImageParamsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.umlImageParams`.
	 * @param ctx the parse tree
	 */
	exitUmlImageParams?: (ctx: UmlImageParamsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.umlGroupImageName`.
	 * @param ctx the parse tree
	 */
	enterUmlGroupImageName?: (ctx: UmlGroupImageNameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.umlGroupImageName`.
	 * @param ctx the parse tree
	 */
	exitUmlGroupImageName?: (ctx: UmlGroupImageNameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.umlImageFormat`.
	 * @param ctx the parse tree
	 */
	enterUmlImageFormat?: (ctx: UmlImageFormatContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.umlImageFormat`.
	 * @param ctx the parse tree
	 */
	exitUmlImageFormat?: (ctx: UmlImageFormatContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.umlImageRemoveTitle`.
	 * @param ctx the parse tree
	 */
	enterUmlImageRemoveTitle?: (ctx: UmlImageRemoveTitleContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.umlImageRemoveTitle`.
	 * @param ctx the parse tree
	 */
	exitUmlImageRemoveTitle?: (ctx: UmlImageRemoveTitleContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.umlImageSubFolder`.
	 * @param ctx the parse tree
	 */
	enterUmlImageSubFolder?: (ctx: UmlImageSubFolderContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.umlImageSubFolder`.
	 * @param ctx the parse tree
	 */
	exitUmlImageSubFolder?: (ctx: UmlImageSubFolderContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.entityType`.
	 * @param ctx the parse tree
	 */
	enterEntityType?: (ctx: EntityTypeContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.entityType`.
	 * @param ctx the parse tree
	 */
	exitEntityType?: (ctx: EntityTypeContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.valueType`.
	 * @param ctx the parse tree
	 */
	enterValueType?: (ctx: ValueTypeContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.valueType`.
	 * @param ctx the parse tree
	 */
	exitValueType?: (ctx: ValueTypeContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.interfaceType`.
	 * @param ctx the parse tree
	 */
	enterInterfaceType?: (ctx: InterfaceTypeContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.interfaceType`.
	 * @param ctx the parse tree
	 */
	exitInterfaceType?: (ctx: InterfaceTypeContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.name`.
	 * @param ctx the parse tree
	 */
	enterName?: (ctx: NameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.name`.
	 * @param ctx the parse tree
	 */
	exitName?: (ctx: NameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.extendsList`.
	 * @param ctx the parse tree
	 */
	enterExtendsList?: (ctx: ExtendsListContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.extendsList`.
	 * @param ctx the parse tree
	 */
	exitExtendsList?: (ctx: ExtendsListContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.baseName`.
	 * @param ctx the parse tree
	 */
	enterBaseName?: (ctx: BaseNameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.baseName`.
	 * @param ctx the parse tree
	 */
	exitBaseName?: (ctx: BaseNameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.typeBlock`.
	 * @param ctx the parse tree
	 */
	enterTypeBlock?: (ctx: TypeBlockContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.typeBlock`.
	 * @param ctx the parse tree
	 */
	exitTypeBlock?: (ctx: TypeBlockContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.possiblyEmptyTypeBlock`.
	 * @param ctx the parse tree
	 */
	enterPossiblyEmptyTypeBlock?: (ctx: PossiblyEmptyTypeBlockContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.possiblyEmptyTypeBlock`.
	 * @param ctx the parse tree
	 */
	exitPossiblyEmptyTypeBlock?: (ctx: PossiblyEmptyTypeBlockContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.entityTypeDecors`.
	 * @param ctx the parse tree
	 */
	enterEntityTypeDecors?: (ctx: EntityTypeDecorsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.entityTypeDecors`.
	 * @param ctx the parse tree
	 */
	exitEntityTypeDecors?: (ctx: EntityTypeDecorsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.entityTypeDecor`.
	 * @param ctx the parse tree
	 */
	enterEntityTypeDecor?: (ctx: EntityTypeDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.entityTypeDecor`.
	 * @param ctx the parse tree
	 */
	exitEntityTypeDecor?: (ctx: EntityTypeDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.partialCommand`.
	 * @param ctx the parse tree
	 */
	enterPartialCommand?: (ctx: PartialCommandContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.partialCommand`.
	 * @param ctx the parse tree
	 */
	exitPartialCommand?: (ctx: PartialCommandContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.transCoordinator`.
	 * @param ctx the parse tree
	 */
	enterTransCoordinator?: (ctx: TransCoordinatorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.transCoordinator`.
	 * @param ctx the parse tree
	 */
	exitTransCoordinator?: (ctx: TransCoordinatorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.defaultConstructor`.
	 * @param ctx the parse tree
	 */
	enterDefaultConstructor?: (ctx: DefaultConstructorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.defaultConstructor`.
	 * @param ctx the parse tree
	 */
	exitDefaultConstructor?: (ctx: DefaultConstructorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.indexTypeDecor`.
	 * @param ctx the parse tree
	 */
	enterIndexTypeDecor?: (ctx: IndexTypeDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.indexTypeDecor`.
	 * @param ctx the parse tree
	 */
	exitIndexTypeDecor?: (ctx: IndexTypeDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.uniqueIndexTypeDecor`.
	 * @param ctx the parse tree
	 */
	enterUniqueIndexTypeDecor?: (ctx: UniqueIndexTypeDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.uniqueIndexTypeDecor`.
	 * @param ctx the parse tree
	 */
	exitUniqueIndexTypeDecor?: (ctx: UniqueIndexTypeDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.indexTypeAttrs`.
	 * @param ctx the parse tree
	 */
	enterIndexTypeAttrs?: (ctx: IndexTypeAttrsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.indexTypeAttrs`.
	 * @param ctx the parse tree
	 */
	exitIndexTypeAttrs?: (ctx: IndexTypeAttrsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.indexKeyAttrs`.
	 * @param ctx the parse tree
	 */
	enterIndexKeyAttrs?: (ctx: IndexKeyAttrsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.indexKeyAttrs`.
	 * @param ctx the parse tree
	 */
	exitIndexKeyAttrs?: (ctx: IndexKeyAttrsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.indexKeyAttr`.
	 * @param ctx the parse tree
	 */
	enterIndexKeyAttr?: (ctx: IndexKeyAttrContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.indexKeyAttr`.
	 * @param ctx the parse tree
	 */
	exitIndexKeyAttr?: (ctx: IndexKeyAttrContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.indexedField`.
	 * @param ctx the parse tree
	 */
	enterIndexedField?: (ctx: IndexedFieldContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.indexedField`.
	 * @param ctx the parse tree
	 */
	exitIndexedField?: (ctx: IndexedFieldContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.queryPartitionDecor`.
	 * @param ctx the parse tree
	 */
	enterQueryPartitionDecor?: (ctx: QueryPartitionDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.queryPartitionDecor`.
	 * @param ctx the parse tree
	 */
	exitQueryPartitionDecor?: (ctx: QueryPartitionDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.queryPartitionAttrs`.
	 * @param ctx the parse tree
	 */
	enterQueryPartitionAttrs?: (ctx: QueryPartitionAttrsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.queryPartitionAttrs`.
	 * @param ctx the parse tree
	 */
	exitQueryPartitionAttrs?: (ctx: QueryPartitionAttrsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.partitionIndex`.
	 * @param ctx the parse tree
	 */
	enterPartitionIndex?: (ctx: PartitionIndexContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.partitionIndex`.
	 * @param ctx the parse tree
	 */
	exitPartitionIndex?: (ctx: PartitionIndexContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.partitionTypeUniqueIndex`.
	 * @param ctx the parse tree
	 */
	enterPartitionTypeUniqueIndex?: (ctx: PartitionTypeUniqueIndexContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.partitionTypeUniqueIndex`.
	 * @param ctx the parse tree
	 */
	exitPartitionTypeUniqueIndex?: (ctx: PartitionTypeUniqueIndexContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.crossPartitionDecor`.
	 * @param ctx the parse tree
	 */
	enterCrossPartitionDecor?: (ctx: CrossPartitionDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.crossPartitionDecor`.
	 * @param ctx the parse tree
	 */
	exitCrossPartitionDecor?: (ctx: CrossPartitionDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.umlGroupDecor`.
	 * @param ctx the parse tree
	 */
	enterUmlGroupDecor?: (ctx: UmlGroupDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.umlGroupDecor`.
	 * @param ctx the parse tree
	 */
	exitUmlGroupDecor?: (ctx: UmlGroupDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.umlGroup`.
	 * @param ctx the parse tree
	 */
	enterUmlGroup?: (ctx: UmlGroupContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.umlGroup`.
	 * @param ctx the parse tree
	 */
	exitUmlGroup?: (ctx: UmlGroupContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.umlGroupTitle`.
	 * @param ctx the parse tree
	 */
	enterUmlGroupTitle?: (ctx: UmlGroupTitleContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.umlGroupTitle`.
	 * @param ctx the parse tree
	 */
	exitUmlGroupTitle?: (ctx: UmlGroupTitleContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.umlSubGroup`.
	 * @param ctx the parse tree
	 */
	enterUmlSubGroup?: (ctx: UmlSubGroupContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.umlSubGroup`.
	 * @param ctx the parse tree
	 */
	exitUmlSubGroup?: (ctx: UmlSubGroupContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.hashLookupDecor`.
	 * @param ctx the parse tree
	 */
	enterHashLookupDecor?: (ctx: HashLookupDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.hashLookupDecor`.
	 * @param ctx the parse tree
	 */
	exitHashLookupDecor?: (ctx: HashLookupDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.beforeEventPersist`.
	 * @param ctx the parse tree
	 */
	enterBeforeEventPersist?: (ctx: BeforeEventPersistContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.beforeEventPersist`.
	 * @param ctx the parse tree
	 */
	exitBeforeEventPersist?: (ctx: BeforeEventPersistContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.authPolicyName`.
	 * @param ctx the parse tree
	 */
	enterAuthPolicyName?: (ctx: AuthPolicyNameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.authPolicyName`.
	 * @param ctx the parse tree
	 */
	exitAuthPolicyName?: (ctx: AuthPolicyNameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.authPolicyDecor`.
	 * @param ctx the parse tree
	 */
	enterAuthPolicyDecor?: (ctx: AuthPolicyDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.authPolicyDecor`.
	 * @param ctx the parse tree
	 */
	exitAuthPolicyDecor?: (ctx: AuthPolicyDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.publicDecor`.
	 * @param ctx the parse tree
	 */
	enterPublicDecor?: (ctx: PublicDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.publicDecor`.
	 * @param ctx the parse tree
	 */
	exitPublicDecor?: (ctx: PublicDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.pluralNameDecor`.
	 * @param ctx the parse tree
	 */
	enterPluralNameDecor?: (ctx: PluralNameDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.pluralNameDecor`.
	 * @param ctx the parse tree
	 */
	exitPluralNameDecor?: (ctx: PluralNameDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.pluralName`.
	 * @param ctx the parse tree
	 */
	enterPluralName?: (ctx: PluralNameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.pluralName`.
	 * @param ctx the parse tree
	 */
	exitPluralName?: (ctx: PluralNameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.tableNameDecor`.
	 * @param ctx the parse tree
	 */
	enterTableNameDecor?: (ctx: TableNameDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.tableNameDecor`.
	 * @param ctx the parse tree
	 */
	exitTableNameDecor?: (ctx: TableNameDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.tableName`.
	 * @param ctx the parse tree
	 */
	enterTableName?: (ctx: TableNameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.tableName`.
	 * @param ctx the parse tree
	 */
	exitTableName?: (ctx: TableNameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.graphQLNameDecor`.
	 * @param ctx the parse tree
	 */
	enterGraphQLNameDecor?: (ctx: GraphQLNameDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.graphQLNameDecor`.
	 * @param ctx the parse tree
	 */
	exitGraphQLNameDecor?: (ctx: GraphQLNameDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.graphQLName`.
	 * @param ctx the parse tree
	 */
	enterGraphQLName?: (ctx: GraphQLNameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.graphQLName`.
	 * @param ctx the parse tree
	 */
	exitGraphQLName?: (ctx: GraphQLNameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.graphQLPluralNameDecor`.
	 * @param ctx the parse tree
	 */
	enterGraphQLPluralNameDecor?: (ctx: GraphQLPluralNameDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.graphQLPluralNameDecor`.
	 * @param ctx the parse tree
	 */
	exitGraphQLPluralNameDecor?: (ctx: GraphQLPluralNameDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.graphQLPluralName`.
	 * @param ctx the parse tree
	 */
	enterGraphQLPluralName?: (ctx: GraphQLPluralNameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.graphQLPluralName`.
	 * @param ctx the parse tree
	 */
	exitGraphQLPluralName?: (ctx: GraphQLPluralNameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.graphQLCamelCaseDecor`.
	 * @param ctx the parse tree
	 */
	enterGraphQLCamelCaseDecor?: (ctx: GraphQLCamelCaseDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.graphQLCamelCaseDecor`.
	 * @param ctx the parse tree
	 */
	exitGraphQLCamelCaseDecor?: (ctx: GraphQLCamelCaseDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.graphQLCamelCase`.
	 * @param ctx the parse tree
	 */
	enterGraphQLCamelCase?: (ctx: GraphQLCamelCaseContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.graphQLCamelCase`.
	 * @param ctx the parse tree
	 */
	exitGraphQLCamelCase?: (ctx: GraphQLCamelCaseContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.graphQLPluralCamelCaseDecor`.
	 * @param ctx the parse tree
	 */
	enterGraphQLPluralCamelCaseDecor?: (ctx: GraphQLPluralCamelCaseDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.graphQLPluralCamelCaseDecor`.
	 * @param ctx the parse tree
	 */
	exitGraphQLPluralCamelCaseDecor?: (ctx: GraphQLPluralCamelCaseDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.graphQLPluralCamelCase`.
	 * @param ctx the parse tree
	 */
	enterGraphQLPluralCamelCase?: (ctx: GraphQLPluralCamelCaseContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.graphQLPluralCamelCase`.
	 * @param ctx the parse tree
	 */
	exitGraphQLPluralCamelCase?: (ctx: GraphQLPluralCamelCaseContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.base64RefIdsDecor`.
	 * @param ctx the parse tree
	 */
	enterBase64RefIdsDecor?: (ctx: Base64RefIdsDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.base64RefIdsDecor`.
	 * @param ctx the parse tree
	 */
	exitBase64RefIdsDecor?: (ctx: Base64RefIdsDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.interfaceDecor`.
	 * @param ctx the parse tree
	 */
	enterInterfaceDecor?: (ctx: InterfaceDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.interfaceDecor`.
	 * @param ctx the parse tree
	 */
	exitInterfaceDecor?: (ctx: InterfaceDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.interfaceName`.
	 * @param ctx the parse tree
	 */
	enterInterfaceName?: (ctx: InterfaceNameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.interfaceName`.
	 * @param ctx the parse tree
	 */
	exitInterfaceName?: (ctx: InterfaceNameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.camelCaseDecor`.
	 * @param ctx the parse tree
	 */
	enterCamelCaseDecor?: (ctx: CamelCaseDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.camelCaseDecor`.
	 * @param ctx the parse tree
	 */
	exitCamelCaseDecor?: (ctx: CamelCaseDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.camelCaseName`.
	 * @param ctx the parse tree
	 */
	enterCamelCaseName?: (ctx: CamelCaseNameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.camelCaseName`.
	 * @param ctx the parse tree
	 */
	exitCamelCaseName?: (ctx: CamelCaseNameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.generateIfDecor`.
	 * @param ctx the parse tree
	 */
	enterGenerateIfDecor?: (ctx: GenerateIfDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.generateIfDecor`.
	 * @param ctx the parse tree
	 */
	exitGenerateIfDecor?: (ctx: GenerateIfDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.generateIfName`.
	 * @param ctx the parse tree
	 */
	enterGenerateIfName?: (ctx: GenerateIfNameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.generateIfName`.
	 * @param ctx the parse tree
	 */
	exitGenerateIfName?: (ctx: GenerateIfNameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.dimensionNameDecor`.
	 * @param ctx the parse tree
	 */
	enterDimensionNameDecor?: (ctx: DimensionNameDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.dimensionNameDecor`.
	 * @param ctx the parse tree
	 */
	exitDimensionNameDecor?: (ctx: DimensionNameDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.styleDecor`.
	 * @param ctx the parse tree
	 */
	enterStyleDecor?: (ctx: StyleDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.styleDecor`.
	 * @param ctx the parse tree
	 */
	exitStyleDecor?: (ctx: StyleDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.events`.
	 * @param ctx the parse tree
	 */
	enterEvents?: (ctx: EventsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.events`.
	 * @param ctx the parse tree
	 */
	exitEvents?: (ctx: EventsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.slowlyChanging`.
	 * @param ctx the parse tree
	 */
	enterSlowlyChanging?: (ctx: SlowlyChangingContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.slowlyChanging`.
	 * @param ctx the parse tree
	 */
	exitSlowlyChanging?: (ctx: SlowlyChangingContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.ledger`.
	 * @param ctx the parse tree
	 */
	enterLedger?: (ctx: LedgerContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.ledger`.
	 * @param ctx the parse tree
	 */
	exitLedger?: (ctx: LedgerContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.factLedger`.
	 * @param ctx the parse tree
	 */
	enterFactLedger?: (ctx: FactLedgerContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.factLedger`.
	 * @param ctx the parse tree
	 */
	exitFactLedger?: (ctx: FactLedgerContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.indexAttrDecors`.
	 * @param ctx the parse tree
	 */
	enterIndexAttrDecors?: (ctx: IndexAttrDecorsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.indexAttrDecors`.
	 * @param ctx the parse tree
	 */
	exitIndexAttrDecors?: (ctx: IndexAttrDecorsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.indexAttrDecor`.
	 * @param ctx the parse tree
	 */
	enterIndexAttrDecor?: (ctx: IndexAttrDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.indexAttrDecor`.
	 * @param ctx the parse tree
	 */
	exitIndexAttrDecor?: (ctx: IndexAttrDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.cacheModel`.
	 * @param ctx the parse tree
	 */
	enterCacheModel?: (ctx: CacheModelContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.cacheModel`.
	 * @param ctx the parse tree
	 */
	exitCacheModel?: (ctx: CacheModelContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.noCacheModel`.
	 * @param ctx the parse tree
	 */
	enterNoCacheModel?: (ctx: NoCacheModelContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.noCacheModel`.
	 * @param ctx the parse tree
	 */
	exitNoCacheModel?: (ctx: NoCacheModelContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.allowTemporal`.
	 * @param ctx the parse tree
	 */
	enterAllowTemporal?: (ctx: AllowTemporalContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.allowTemporal`.
	 * @param ctx the parse tree
	 */
	exitAllowTemporal?: (ctx: AllowTemporalContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.indexNameDecor`.
	 * @param ctx the parse tree
	 */
	enterIndexNameDecor?: (ctx: IndexNameDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.indexNameDecor`.
	 * @param ctx the parse tree
	 */
	exitIndexNameDecor?: (ctx: IndexNameDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.indexName`.
	 * @param ctx the parse tree
	 */
	enterIndexName?: (ctx: IndexNameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.indexName`.
	 * @param ctx the parse tree
	 */
	exitIndexName?: (ctx: IndexNameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.valueTypeDecors`.
	 * @param ctx the parse tree
	 */
	enterValueTypeDecors?: (ctx: ValueTypeDecorsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.valueTypeDecors`.
	 * @param ctx the parse tree
	 */
	exitValueTypeDecors?: (ctx: ValueTypeDecorsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.valueTypeDecor`.
	 * @param ctx the parse tree
	 */
	enterValueTypeDecor?: (ctx: ValueTypeDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.valueTypeDecor`.
	 * @param ctx the parse tree
	 */
	exitValueTypeDecor?: (ctx: ValueTypeDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.partial`.
	 * @param ctx the parse tree
	 */
	enterPartial?: (ctx: PartialContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.partial`.
	 * @param ctx the parse tree
	 */
	exitPartial?: (ctx: PartialContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.jsonConstructor`.
	 * @param ctx the parse tree
	 */
	enterJsonConstructor?: (ctx: JsonConstructorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.jsonConstructor`.
	 * @param ctx the parse tree
	 */
	exitJsonConstructor?: (ctx: JsonConstructorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.webValueString`.
	 * @param ctx the parse tree
	 */
	enterWebValueString?: (ctx: WebValueStringContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.webValueString`.
	 * @param ctx the parse tree
	 */
	exitWebValueString?: (ctx: WebValueStringContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.webValueMethod`.
	 * @param ctx the parse tree
	 */
	enterWebValueMethod?: (ctx: WebValueMethodContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.webValueMethod`.
	 * @param ctx the parse tree
	 */
	exitWebValueMethod?: (ctx: WebValueMethodContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.discriminatedBy`.
	 * @param ctx the parse tree
	 */
	enterDiscriminatedBy?: (ctx: DiscriminatedByContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.discriminatedBy`.
	 * @param ctx the parse tree
	 */
	exitDiscriminatedBy?: (ctx: DiscriminatedByContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.discriminatedByValue`.
	 * @param ctx the parse tree
	 */
	enterDiscriminatedByValue?: (ctx: DiscriminatedByValueContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.discriminatedByValue`.
	 * @param ctx the parse tree
	 */
	exitDiscriminatedByValue?: (ctx: DiscriminatedByValueContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.outputType`.
	 * @param ctx the parse tree
	 */
	enterOutputType?: (ctx: OutputTypeContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.outputType`.
	 * @param ctx the parse tree
	 */
	exitOutputType?: (ctx: OutputTypeContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.voidPrimaryFact`.
	 * @param ctx the parse tree
	 */
	enterVoidPrimaryFact?: (ctx: VoidPrimaryFactContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.voidPrimaryFact`.
	 * @param ctx the parse tree
	 */
	exitVoidPrimaryFact?: (ctx: VoidPrimaryFactContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.interfaceTypeDecors`.
	 * @param ctx the parse tree
	 */
	enterInterfaceTypeDecors?: (ctx: InterfaceTypeDecorsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.interfaceTypeDecors`.
	 * @param ctx the parse tree
	 */
	exitInterfaceTypeDecors?: (ctx: InterfaceTypeDecorsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.interfaceTypeDecor`.
	 * @param ctx the parse tree
	 */
	enterInterfaceTypeDecor?: (ctx: InterfaceTypeDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.interfaceTypeDecor`.
	 * @param ctx the parse tree
	 */
	exitInterfaceTypeDecor?: (ctx: InterfaceTypeDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.enumDecl`.
	 * @param ctx the parse tree
	 */
	enterEnumDecl?: (ctx: EnumDeclContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.enumDecl`.
	 * @param ctx the parse tree
	 */
	exitEnumDecl?: (ctx: EnumDeclContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.enumBlock`.
	 * @param ctx the parse tree
	 */
	enterEnumBlock?: (ctx: EnumBlockContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.enumBlock`.
	 * @param ctx the parse tree
	 */
	exitEnumBlock?: (ctx: EnumBlockContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.enumItem`.
	 * @param ctx the parse tree
	 */
	enterEnumItem?: (ctx: EnumItemContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.enumItem`.
	 * @param ctx the parse tree
	 */
	exitEnumItem?: (ctx: EnumItemContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.enumValue`.
	 * @param ctx the parse tree
	 */
	enterEnumValue?: (ctx: EnumValueContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.enumValue`.
	 * @param ctx the parse tree
	 */
	exitEnumValue?: (ctx: EnumValueContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.enumExplicitValue`.
	 * @param ctx the parse tree
	 */
	enterEnumExplicitValue?: (ctx: EnumExplicitValueContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.enumExplicitValue`.
	 * @param ctx the parse tree
	 */
	exitEnumExplicitValue?: (ctx: EnumExplicitValueContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.enumDecors`.
	 * @param ctx the parse tree
	 */
	enterEnumDecors?: (ctx: EnumDecorsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.enumDecors`.
	 * @param ctx the parse tree
	 */
	exitEnumDecors?: (ctx: EnumDecorsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.enumDecor`.
	 * @param ctx the parse tree
	 */
	enterEnumDecor?: (ctx: EnumDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.enumDecor`.
	 * @param ctx the parse tree
	 */
	exitEnumDecor?: (ctx: EnumDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.suppressDecor`.
	 * @param ctx the parse tree
	 */
	enterSuppressDecor?: (ctx: SuppressDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.suppressDecor`.
	 * @param ctx the parse tree
	 */
	exitSuppressDecor?: (ctx: SuppressDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.suppressOption`.
	 * @param ctx the parse tree
	 */
	enterSuppressOption?: (ctx: SuppressOptionContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.suppressOption`.
	 * @param ctx the parse tree
	 */
	exitSuppressOption?: (ctx: SuppressOptionContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.modelsuppressOption`.
	 * @param ctx the parse tree
	 */
	enterModelsuppressOption?: (ctx: ModelsuppressOptionContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.modelsuppressOption`.
	 * @param ctx the parse tree
	 */
	exitModelsuppressOption?: (ctx: ModelsuppressOptionContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.graphQlsuppressOption`.
	 * @param ctx the parse tree
	 */
	enterGraphQlsuppressOption?: (ctx: GraphQlsuppressOptionContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.graphQlsuppressOption`.
	 * @param ctx the parse tree
	 */
	exitGraphQlsuppressOption?: (ctx: GraphQlsuppressOptionContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.inputTypeOption`.
	 * @param ctx the parse tree
	 */
	enterInputTypeOption?: (ctx: InputTypeOptionContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.inputTypeOption`.
	 * @param ctx the parse tree
	 */
	exitInputTypeOption?: (ctx: InputTypeOptionContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.flagsDecor`.
	 * @param ctx the parse tree
	 */
	enterFlagsDecor?: (ctx: FlagsDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.flagsDecor`.
	 * @param ctx the parse tree
	 */
	exitFlagsDecor?: (ctx: FlagsDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.dictionary`.
	 * @param ctx the parse tree
	 */
	enterDictionary?: (ctx: DictionaryContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.dictionary`.
	 * @param ctx the parse tree
	 */
	exitDictionary?: (ctx: DictionaryContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.dictionaryBlock`.
	 * @param ctx the parse tree
	 */
	enterDictionaryBlock?: (ctx: DictionaryBlockContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.dictionaryBlock`.
	 * @param ctx the parse tree
	 */
	exitDictionaryBlock?: (ctx: DictionaryBlockContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.dictPartitioned`.
	 * @param ctx the parse tree
	 */
	enterDictPartitioned?: (ctx: DictPartitionedContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.dictPartitioned`.
	 * @param ctx the parse tree
	 */
	exitDictPartitioned?: (ctx: DictPartitionedContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.dictValueTypeDecl`.
	 * @param ctx the parse tree
	 */
	enterDictValueTypeDecl?: (ctx: DictValueTypeDeclContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.dictValueTypeDecl`.
	 * @param ctx the parse tree
	 */
	exitDictValueTypeDecl?: (ctx: DictValueTypeDeclContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.dictValueType`.
	 * @param ctx the parse tree
	 */
	enterDictValueType?: (ctx: DictValueTypeContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.dictValueType`.
	 * @param ctx the parse tree
	 */
	exitDictValueType?: (ctx: DictValueTypeContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.dictDefaultDecl`.
	 * @param ctx the parse tree
	 */
	enterDictDefaultDecl?: (ctx: DictDefaultDeclContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.dictDefaultDecl`.
	 * @param ctx the parse tree
	 */
	exitDictDefaultDecl?: (ctx: DictDefaultDeclContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.dictDefault`.
	 * @param ctx the parse tree
	 */
	enterDictDefault?: (ctx: DictDefaultContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.dictDefault`.
	 * @param ctx the parse tree
	 */
	exitDictDefault?: (ctx: DictDefaultContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.dictEntries`.
	 * @param ctx the parse tree
	 */
	enterDictEntries?: (ctx: DictEntriesContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.dictEntries`.
	 * @param ctx the parse tree
	 */
	exitDictEntries?: (ctx: DictEntriesContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.dictionaryDecors`.
	 * @param ctx the parse tree
	 */
	enterDictionaryDecors?: (ctx: DictionaryDecorsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.dictionaryDecors`.
	 * @param ctx the parse tree
	 */
	exitDictionaryDecors?: (ctx: DictionaryDecorsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.dictionaryDecor`.
	 * @param ctx the parse tree
	 */
	enterDictionaryDecor?: (ctx: DictionaryDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.dictionaryDecor`.
	 * @param ctx the parse tree
	 */
	exitDictionaryDecor?: (ctx: DictionaryDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.fieldDef`.
	 * @param ctx the parse tree
	 */
	enterFieldDef?: (ctx: FieldDefContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.fieldDef`.
	 * @param ctx the parse tree
	 */
	exitFieldDef?: (ctx: FieldDefContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.typeRef`.
	 * @param ctx the parse tree
	 */
	enterTypeRef?: (ctx: TypeRefContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.typeRef`.
	 * @param ctx the parse tree
	 */
	exitTypeRef?: (ctx: TypeRefContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.typeRefName`.
	 * @param ctx the parse tree
	 */
	enterTypeRefName?: (ctx: TypeRefNameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.typeRefName`.
	 * @param ctx the parse tree
	 */
	exitTypeRefName?: (ctx: TypeRefNameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.primitiveType`.
	 * @param ctx the parse tree
	 */
	enterPrimitiveType?: (ctx: PrimitiveTypeContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.primitiveType`.
	 * @param ctx the parse tree
	 */
	exitPrimitiveType?: (ctx: PrimitiveTypeContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.valueTypeRef`.
	 * @param ctx the parse tree
	 */
	enterValueTypeRef?: (ctx: ValueTypeRefContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.valueTypeRef`.
	 * @param ctx the parse tree
	 */
	exitValueTypeRef?: (ctx: ValueTypeRefContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.listIndicator`.
	 * @param ctx the parse tree
	 */
	enterListIndicator?: (ctx: ListIndicatorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.listIndicator`.
	 * @param ctx the parse tree
	 */
	exitListIndicator?: (ctx: ListIndicatorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.nullableIndicator`.
	 * @param ctx the parse tree
	 */
	enterNullableIndicator?: (ctx: NullableIndicatorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.nullableIndicator`.
	 * @param ctx the parse tree
	 */
	exitNullableIndicator?: (ctx: NullableIndicatorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.fieldInitializer`.
	 * @param ctx the parse tree
	 */
	enterFieldInitializer?: (ctx: FieldInitializerContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.fieldInitializer`.
	 * @param ctx the parse tree
	 */
	exitFieldInitializer?: (ctx: FieldInitializerContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.nullValue`.
	 * @param ctx the parse tree
	 */
	enterNullValue?: (ctx: NullValueContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.nullValue`.
	 * @param ctx the parse tree
	 */
	exitNullValue?: (ctx: NullValueContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.boolValue`.
	 * @param ctx the parse tree
	 */
	enterBoolValue?: (ctx: BoolValueContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.boolValue`.
	 * @param ctx the parse tree
	 */
	exitBoolValue?: (ctx: BoolValueContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.newInstance`.
	 * @param ctx the parse tree
	 */
	enterNewInstance?: (ctx: NewInstanceContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.newInstance`.
	 * @param ctx the parse tree
	 */
	exitNewInstance?: (ctx: NewInstanceContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.initialEnumVal`.
	 * @param ctx the parse tree
	 */
	enterInitialEnumVal?: (ctx: InitialEnumValContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.initialEnumVal`.
	 * @param ctx the parse tree
	 */
	exitInitialEnumVal?: (ctx: InitialEnumValContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.numValue`.
	 * @param ctx the parse tree
	 */
	enterNumValue?: (ctx: NumValueContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.numValue`.
	 * @param ctx the parse tree
	 */
	exitNumValue?: (ctx: NumValueContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.integer`.
	 * @param ctx the parse tree
	 */
	enterInteger?: (ctx: IntegerContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.integer`.
	 * @param ctx the parse tree
	 */
	exitInteger?: (ctx: IntegerContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.decimal`.
	 * @param ctx the parse tree
	 */
	enterDecimal?: (ctx: DecimalContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.decimal`.
	 * @param ctx the parse tree
	 */
	exitDecimal?: (ctx: DecimalContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.stringValue`.
	 * @param ctx the parse tree
	 */
	enterStringValue?: (ctx: StringValueContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.stringValue`.
	 * @param ctx the parse tree
	 */
	exitStringValue?: (ctx: StringValueContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.flagsEnumSet`.
	 * @param ctx the parse tree
	 */
	enterFlagsEnumSet?: (ctx: FlagsEnumSetContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.flagsEnumSet`.
	 * @param ctx the parse tree
	 */
	exitFlagsEnumSet?: (ctx: FlagsEnumSetContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.flagsEnumValue`.
	 * @param ctx the parse tree
	 */
	enterFlagsEnumValue?: (ctx: FlagsEnumValueContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.flagsEnumValue`.
	 * @param ctx the parse tree
	 */
	exitFlagsEnumValue?: (ctx: FlagsEnumValueContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.fieldDecorators`.
	 * @param ctx the parse tree
	 */
	enterFieldDecorators?: (ctx: FieldDecoratorsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.fieldDecorators`.
	 * @param ctx the parse tree
	 */
	exitFieldDecorators?: (ctx: FieldDecoratorsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.fieldDecorator`.
	 * @param ctx the parse tree
	 */
	enterFieldDecorator?: (ctx: FieldDecoratorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.fieldDecorator`.
	 * @param ctx the parse tree
	 */
	exitFieldDecorator?: (ctx: FieldDecoratorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.indexDecor`.
	 * @param ctx the parse tree
	 */
	enterIndexDecor?: (ctx: IndexDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.indexDecor`.
	 * @param ctx the parse tree
	 */
	exitIndexDecor?: (ctx: IndexDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.uniqueIndexDecor`.
	 * @param ctx the parse tree
	 */
	enterUniqueIndexDecor?: (ctx: UniqueIndexDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.uniqueIndexDecor`.
	 * @param ctx the parse tree
	 */
	exitUniqueIndexDecor?: (ctx: UniqueIndexDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.indexAttrs`.
	 * @param ctx the parse tree
	 */
	enterIndexAttrs?: (ctx: IndexAttrsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.indexAttrs`.
	 * @param ctx the parse tree
	 */
	exitIndexAttrs?: (ctx: IndexAttrsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.caseInsensitive`.
	 * @param ctx the parse tree
	 */
	enterCaseInsensitive?: (ctx: CaseInsensitiveContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.caseInsensitive`.
	 * @param ctx the parse tree
	 */
	exitCaseInsensitive?: (ctx: CaseInsensitiveContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.nullHandling`.
	 * @param ctx the parse tree
	 */
	enterNullHandling?: (ctx: NullHandlingContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.nullHandling`.
	 * @param ctx the parse tree
	 */
	exitNullHandling?: (ctx: NullHandlingContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.inlineEnumDecor`.
	 * @param ctx the parse tree
	 */
	enterInlineEnumDecor?: (ctx: InlineEnumDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.inlineEnumDecor`.
	 * @param ctx the parse tree
	 */
	exitInlineEnumDecor?: (ctx: InlineEnumDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.inlineEnumValue`.
	 * @param ctx the parse tree
	 */
	enterInlineEnumValue?: (ctx: InlineEnumValueContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.inlineEnumValue`.
	 * @param ctx the parse tree
	 */
	exitInlineEnumValue?: (ctx: InlineEnumValueContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.composedDecor`.
	 * @param ctx the parse tree
	 */
	enterComposedDecor?: (ctx: ComposedDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.composedDecor`.
	 * @param ctx the parse tree
	 */
	exitComposedDecor?: (ctx: ComposedDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.requiredDecor`.
	 * @param ctx the parse tree
	 */
	enterRequiredDecor?: (ctx: RequiredDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.requiredDecor`.
	 * @param ctx the parse tree
	 */
	exitRequiredDecor?: (ctx: RequiredDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.readOnlyDecor`.
	 * @param ctx the parse tree
	 */
	enterReadOnlyDecor?: (ctx: ReadOnlyDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.readOnlyDecor`.
	 * @param ctx the parse tree
	 */
	exitReadOnlyDecor?: (ctx: ReadOnlyDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.hiddenDecor`.
	 * @param ctx the parse tree
	 */
	enterHiddenDecor?: (ctx: HiddenDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.hiddenDecor`.
	 * @param ctx the parse tree
	 */
	exitHiddenDecor?: (ctx: HiddenDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.calculatedDecor`.
	 * @param ctx the parse tree
	 */
	enterCalculatedDecor?: (ctx: CalculatedDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.calculatedDecor`.
	 * @param ctx the parse tree
	 */
	exitCalculatedDecor?: (ctx: CalculatedDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.immutableDecor`.
	 * @param ctx the parse tree
	 */
	enterImmutableDecor?: (ctx: ImmutableDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.immutableDecor`.
	 * @param ctx the parse tree
	 */
	exitImmutableDecor?: (ctx: ImmutableDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.clonePartitionerDecor`.
	 * @param ctx the parse tree
	 */
	enterClonePartitionerDecor?: (ctx: ClonePartitionerDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.clonePartitionerDecor`.
	 * @param ctx the parse tree
	 */
	exitClonePartitionerDecor?: (ctx: ClonePartitionerDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.cloneIdAsIsDecor`.
	 * @param ctx the parse tree
	 */
	enterCloneIdAsIsDecor?: (ctx: CloneIdAsIsDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.cloneIdAsIsDecor`.
	 * @param ctx the parse tree
	 */
	exitCloneIdAsIsDecor?: (ctx: CloneIdAsIsDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.fieldUmlDecor`.
	 * @param ctx the parse tree
	 */
	enterFieldUmlDecor?: (ctx: FieldUmlDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.fieldUmlDecor`.
	 * @param ctx the parse tree
	 */
	exitFieldUmlDecor?: (ctx: FieldUmlDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.fieldUmlGroupTitle`.
	 * @param ctx the parse tree
	 */
	enterFieldUmlGroupTitle?: (ctx: FieldUmlGroupTitleContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.fieldUmlGroupTitle`.
	 * @param ctx the parse tree
	 */
	exitFieldUmlGroupTitle?: (ctx: FieldUmlGroupTitleContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.direction`.
	 * @param ctx the parse tree
	 */
	enterDirection?: (ctx: DirectionContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.direction`.
	 * @param ctx the parse tree
	 */
	exitDirection?: (ctx: DirectionContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.horizontalVertical`.
	 * @param ctx the parse tree
	 */
	enterHorizontalVertical?: (ctx: HorizontalVerticalContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.horizontalVertical`.
	 * @param ctx the parse tree
	 */
	exitHorizontalVertical?: (ctx: HorizontalVerticalContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.lineLength`.
	 * @param ctx the parse tree
	 */
	enterLineLength?: (ctx: LineLengthContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.lineLength`.
	 * @param ctx the parse tree
	 */
	exitLineLength?: (ctx: LineLengthContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.hideRelationship`.
	 * @param ctx the parse tree
	 */
	enterHideRelationship?: (ctx: HideRelationshipContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.hideRelationship`.
	 * @param ctx the parse tree
	 */
	exitHideRelationship?: (ctx: HideRelationshipContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.lineLabel`.
	 * @param ctx the parse tree
	 */
	enterLineLabel?: (ctx: LineLabelContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.lineLabel`.
	 * @param ctx the parse tree
	 */
	exitLineLabel?: (ctx: LineLabelContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.sectionDecor`.
	 * @param ctx the parse tree
	 */
	enterSectionDecor?: (ctx: SectionDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.sectionDecor`.
	 * @param ctx the parse tree
	 */
	exitSectionDecor?: (ctx: SectionDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.breakType`.
	 * @param ctx the parse tree
	 */
	enterBreakType?: (ctx: BreakTypeContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.breakType`.
	 * @param ctx the parse tree
	 */
	exitBreakType?: (ctx: BreakTypeContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.asValueTypeDecor`.
	 * @param ctx the parse tree
	 */
	enterAsValueTypeDecor?: (ctx: AsValueTypeDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.asValueTypeDecor`.
	 * @param ctx the parse tree
	 */
	exitAsValueTypeDecor?: (ctx: AsValueTypeDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.sameAsDecor`.
	 * @param ctx the parse tree
	 */
	enterSameAsDecor?: (ctx: SameAsDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.sameAsDecor`.
	 * @param ctx the parse tree
	 */
	exitSameAsDecor?: (ctx: SameAsDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.sameAsPersist`.
	 * @param ctx the parse tree
	 */
	enterSameAsPersist?: (ctx: SameAsPersistContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.sameAsPersist`.
	 * @param ctx the parse tree
	 */
	exitSameAsPersist?: (ctx: SameAsPersistContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.constantDecor`.
	 * @param ctx the parse tree
	 */
	enterConstantDecor?: (ctx: ConstantDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.constantDecor`.
	 * @param ctx the parse tree
	 */
	exitConstantDecor?: (ctx: ConstantDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.notPersistedDecor`.
	 * @param ctx the parse tree
	 */
	enterNotPersistedDecor?: (ctx: NotPersistedDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.notPersistedDecor`.
	 * @param ctx the parse tree
	 */
	exitNotPersistedDecor?: (ctx: NotPersistedDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.handCodedDecor`.
	 * @param ctx the parse tree
	 */
	enterHandCodedDecor?: (ctx: HandCodedDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.handCodedDecor`.
	 * @param ctx the parse tree
	 */
	exitHandCodedDecor?: (ctx: HandCodedDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.dictDecor`.
	 * @param ctx the parse tree
	 */
	enterDictDecor?: (ctx: DictDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.dictDecor`.
	 * @param ctx the parse tree
	 */
	exitDictDecor?: (ctx: DictDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.typeDiscriminator`.
	 * @param ctx the parse tree
	 */
	enterTypeDiscriminator?: (ctx: TypeDiscriminatorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.typeDiscriminator`.
	 * @param ctx the parse tree
	 */
	exitTypeDiscriminator?: (ctx: TypeDiscriminatorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.multiLineDecor`.
	 * @param ctx the parse tree
	 */
	enterMultiLineDecor?: (ctx: MultiLineDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.multiLineDecor`.
	 * @param ctx the parse tree
	 */
	exitMultiLineDecor?: (ctx: MultiLineDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.questionDecor`.
	 * @param ctx the parse tree
	 */
	enterQuestionDecor?: (ctx: QuestionDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.questionDecor`.
	 * @param ctx the parse tree
	 */
	exitQuestionDecor?: (ctx: QuestionDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.labelDecor`.
	 * @param ctx the parse tree
	 */
	enterLabelDecor?: (ctx: LabelDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.labelDecor`.
	 * @param ctx the parse tree
	 */
	exitLabelDecor?: (ctx: LabelDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.label`.
	 * @param ctx the parse tree
	 */
	enterLabel?: (ctx: LabelContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.label`.
	 * @param ctx the parse tree
	 */
	exitLabel?: (ctx: LabelContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.autoFillDecor`.
	 * @param ctx the parse tree
	 */
	enterAutoFillDecor?: (ctx: AutoFillDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.autoFillDecor`.
	 * @param ctx the parse tree
	 */
	exitAutoFillDecor?: (ctx: AutoFillDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.autoFillType`.
	 * @param ctx the parse tree
	 */
	enterAutoFillType?: (ctx: AutoFillTypeContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.autoFillType`.
	 * @param ctx the parse tree
	 */
	exitAutoFillType?: (ctx: AutoFillTypeContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.attributeDecor`.
	 * @param ctx the parse tree
	 */
	enterAttributeDecor?: (ctx: AttributeDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.attributeDecor`.
	 * @param ctx the parse tree
	 */
	exitAttributeDecor?: (ctx: AttributeDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.attributePair`.
	 * @param ctx the parse tree
	 */
	enterAttributePair?: (ctx: AttributePairContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.attributePair`.
	 * @param ctx the parse tree
	 */
	exitAttributePair?: (ctx: AttributePairContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.attributeKey`.
	 * @param ctx the parse tree
	 */
	enterAttributeKey?: (ctx: AttributeKeyContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.attributeKey`.
	 * @param ctx the parse tree
	 */
	exitAttributeKey?: (ctx: AttributeKeyContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.attributeValue`.
	 * @param ctx the parse tree
	 */
	enterAttributeValue?: (ctx: AttributeValueContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.attributeValue`.
	 * @param ctx the parse tree
	 */
	exitAttributeValue?: (ctx: AttributeValueContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.factDimensionDecor`.
	 * @param ctx the parse tree
	 */
	enterFactDimensionDecor?: (ctx: FactDimensionDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.factDimensionDecor`.
	 * @param ctx the parse tree
	 */
	exitFactDimensionDecor?: (ctx: FactDimensionDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.dimPrimary`.
	 * @param ctx the parse tree
	 */
	enterDimPrimary?: (ctx: DimPrimaryContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.dimPrimary`.
	 * @param ctx the parse tree
	 */
	exitDimPrimary?: (ctx: DimPrimaryContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.dimInx`.
	 * @param ctx the parse tree
	 */
	enterDimInx?: (ctx: DimInxContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.dimInx`.
	 * @param ctx the parse tree
	 */
	exitDimInx?: (ctx: DimInxContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.dimensionKeyDecor`.
	 * @param ctx the parse tree
	 */
	enterDimensionKeyDecor?: (ctx: DimensionKeyDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.dimensionKeyDecor`.
	 * @param ctx the parse tree
	 */
	exitDimensionKeyDecor?: (ctx: DimensionKeyDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.journalEntriesDecor`.
	 * @param ctx the parse tree
	 */
	enterJournalEntriesDecor?: (ctx: JournalEntriesDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.journalEntriesDecor`.
	 * @param ctx the parse tree
	 */
	exitJournalEntriesDecor?: (ctx: JournalEntriesDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.entityEvents`.
	 * @param ctx the parse tree
	 */
	enterEntityEvents?: (ctx: EntityEventsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.entityEvents`.
	 * @param ctx the parse tree
	 */
	exitEntityEvents?: (ctx: EntityEventsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.eventsBlock`.
	 * @param ctx the parse tree
	 */
	enterEventsBlock?: (ctx: EventsBlockContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.eventsBlock`.
	 * @param ctx the parse tree
	 */
	exitEventsBlock?: (ctx: EventsBlockContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.createdEventDef`.
	 * @param ctx the parse tree
	 */
	enterCreatedEventDef?: (ctx: CreatedEventDefContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.createdEventDef`.
	 * @param ctx the parse tree
	 */
	exitCreatedEventDef?: (ctx: CreatedEventDefContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.createdEvent`.
	 * @param ctx the parse tree
	 */
	enterCreatedEvent?: (ctx: CreatedEventContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.createdEvent`.
	 * @param ctx the parse tree
	 */
	exitCreatedEvent?: (ctx: CreatedEventContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.eventDef`.
	 * @param ctx the parse tree
	 */
	enterEventDef?: (ctx: EventDefContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.eventDef`.
	 * @param ctx the parse tree
	 */
	exitEventDef?: (ctx: EventDefContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.eventName`.
	 * @param ctx the parse tree
	 */
	enterEventName?: (ctx: EventNameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.eventName`.
	 * @param ctx the parse tree
	 */
	exitEventName?: (ctx: EventNameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.eventsDecors`.
	 * @param ctx the parse tree
	 */
	enterEventsDecors?: (ctx: EventsDecorsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.eventsDecors`.
	 * @param ctx the parse tree
	 */
	exitEventsDecors?: (ctx: EventsDecorsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.eventsDecor`.
	 * @param ctx the parse tree
	 */
	enterEventsDecor?: (ctx: EventsDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.eventsDecor`.
	 * @param ctx the parse tree
	 */
	exitEventsDecor?: (ctx: EventsDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.eventNameDecors`.
	 * @param ctx the parse tree
	 */
	enterEventNameDecors?: (ctx: EventNameDecorsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.eventNameDecors`.
	 * @param ctx the parse tree
	 */
	exitEventNameDecors?: (ctx: EventNameDecorsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.eventNameDecor`.
	 * @param ctx the parse tree
	 */
	enterEventNameDecor?: (ctx: EventNameDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.eventNameDecor`.
	 * @param ctx the parse tree
	 */
	exitEventNameDecor?: (ctx: EventNameDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.entityCommands`.
	 * @param ctx the parse tree
	 */
	enterEntityCommands?: (ctx: EntityCommandsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.entityCommands`.
	 * @param ctx the parse tree
	 */
	exitEntityCommands?: (ctx: EntityCommandsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.commandsBlock`.
	 * @param ctx the parse tree
	 */
	enterCommandsBlock?: (ctx: CommandsBlockContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.commandsBlock`.
	 * @param ctx the parse tree
	 */
	exitCommandsBlock?: (ctx: CommandsBlockContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.commandsBlockDecorator`.
	 * @param ctx the parse tree
	 */
	enterCommandsBlockDecorator?: (ctx: CommandsBlockDecoratorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.commandsBlockDecorator`.
	 * @param ctx the parse tree
	 */
	exitCommandsBlockDecorator?: (ctx: CommandsBlockDecoratorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.commandsBlockDecorators`.
	 * @param ctx the parse tree
	 */
	enterCommandsBlockDecorators?: (ctx: CommandsBlockDecoratorsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.commandsBlockDecorators`.
	 * @param ctx the parse tree
	 */
	exitCommandsBlockDecorators?: (ctx: CommandsBlockDecoratorsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.command`.
	 * @param ctx the parse tree
	 */
	enterCommand?: (ctx: CommandContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.command`.
	 * @param ctx the parse tree
	 */
	exitCommand?: (ctx: CommandContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.commandParams`.
	 * @param ctx the parse tree
	 */
	enterCommandParams?: (ctx: CommandParamsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.commandParams`.
	 * @param ctx the parse tree
	 */
	exitCommandParams?: (ctx: CommandParamsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.param`.
	 * @param ctx the parse tree
	 */
	enterParam?: (ctx: ParamContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.param`.
	 * @param ctx the parse tree
	 */
	exitParam?: (ctx: ParamContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.paramDecl`.
	 * @param ctx the parse tree
	 */
	enterParamDecl?: (ctx: ParamDeclContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.paramDecl`.
	 * @param ctx the parse tree
	 */
	exitParamDecl?: (ctx: ParamDeclContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.optionalTypeRef`.
	 * @param ctx the parse tree
	 */
	enterOptionalTypeRef?: (ctx: OptionalTypeRefContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.optionalTypeRef`.
	 * @param ctx the parse tree
	 */
	exitOptionalTypeRef?: (ctx: OptionalTypeRefContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.paramNameModifiers`.
	 * @param ctx the parse tree
	 */
	enterParamNameModifiers?: (ctx: ParamNameModifiersContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.paramNameModifiers`.
	 * @param ctx the parse tree
	 */
	exitParamNameModifiers?: (ctx: ParamNameModifiersContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.paramInitializer`.
	 * @param ctx the parse tree
	 */
	enterParamInitializer?: (ctx: ParamInitializerContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.paramInitializer`.
	 * @param ctx the parse tree
	 */
	exitParamInitializer?: (ctx: ParamInitializerContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.commandResultsIn`.
	 * @param ctx the parse tree
	 */
	enterCommandResultsIn?: (ctx: CommandResultsInContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.commandResultsIn`.
	 * @param ctx the parse tree
	 */
	exitCommandResultsIn?: (ctx: CommandResultsInContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.yields`.
	 * @param ctx the parse tree
	 */
	enterYields?: (ctx: YieldsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.yields`.
	 * @param ctx the parse tree
	 */
	exitYields?: (ctx: YieldsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.eventList`.
	 * @param ctx the parse tree
	 */
	enterEventList?: (ctx: EventListContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.eventList`.
	 * @param ctx the parse tree
	 */
	exitEventList?: (ctx: EventListContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.eventRef`.
	 * @param ctx the parse tree
	 */
	enterEventRef?: (ctx: EventRefContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.eventRef`.
	 * @param ctx the parse tree
	 */
	exitEventRef?: (ctx: EventRefContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.returnsType`.
	 * @param ctx the parse tree
	 */
	enterReturnsType?: (ctx: ReturnsTypeContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.returnsType`.
	 * @param ctx the parse tree
	 */
	exitReturnsType?: (ctx: ReturnsTypeContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.returnTypeRef`.
	 * @param ctx the parse tree
	 */
	enterReturnTypeRef?: (ctx: ReturnTypeRefContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.returnTypeRef`.
	 * @param ctx the parse tree
	 */
	exitReturnTypeRef?: (ctx: ReturnTypeRefContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.paramDecorator`.
	 * @param ctx the parse tree
	 */
	enterParamDecorator?: (ctx: ParamDecoratorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.paramDecorator`.
	 * @param ctx the parse tree
	 */
	exitParamDecorator?: (ctx: ParamDecoratorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.typeQualifierDecor`.
	 * @param ctx the parse tree
	 */
	enterTypeQualifierDecor?: (ctx: TypeQualifierDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.typeQualifierDecor`.
	 * @param ctx the parse tree
	 */
	exitTypeQualifierDecor?: (ctx: TypeQualifierDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.paramDecorators`.
	 * @param ctx the parse tree
	 */
	enterParamDecorators?: (ctx: ParamDecoratorsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.paramDecorators`.
	 * @param ctx the parse tree
	 */
	exitParamDecorators?: (ctx: ParamDecoratorsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.typeQualifierName`.
	 * @param ctx the parse tree
	 */
	enterTypeQualifierName?: (ctx: TypeQualifierNameContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.typeQualifierName`.
	 * @param ctx the parse tree
	 */
	exitTypeQualifierName?: (ctx: TypeQualifierNameContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.indexQualifier`.
	 * @param ctx the parse tree
	 */
	enterIndexQualifier?: (ctx: IndexQualifierContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.indexQualifier`.
	 * @param ctx the parse tree
	 */
	exitIndexQualifier?: (ctx: IndexQualifierContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.filterExpr`.
	 * @param ctx the parse tree
	 */
	enterFilterExpr?: (ctx: FilterExprContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.filterExpr`.
	 * @param ctx the parse tree
	 */
	exitFilterExpr?: (ctx: FilterExprContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.byValueDecor`.
	 * @param ctx the parse tree
	 */
	enterByValueDecor?: (ctx: ByValueDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.byValueDecor`.
	 * @param ctx the parse tree
	 */
	exitByValueDecor?: (ctx: ByValueDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.idDecor`.
	 * @param ctx the parse tree
	 */
	enterIdDecor?: (ctx: IdDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.idDecor`.
	 * @param ctx the parse tree
	 */
	exitIdDecor?: (ctx: IdDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.commandDecorator`.
	 * @param ctx the parse tree
	 */
	enterCommandDecorator?: (ctx: CommandDecoratorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.commandDecorator`.
	 * @param ctx the parse tree
	 */
	exitCommandDecorator?: (ctx: CommandDecoratorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.commandDecorators`.
	 * @param ctx the parse tree
	 */
	enterCommandDecorators?: (ctx: CommandDecoratorsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.commandDecorators`.
	 * @param ctx the parse tree
	 */
	exitCommandDecorators?: (ctx: CommandDecoratorsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.modelDecor`.
	 * @param ctx the parse tree
	 */
	enterModelDecor?: (ctx: ModelDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.modelDecor`.
	 * @param ctx the parse tree
	 */
	exitModelDecor?: (ctx: ModelDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.asyncDecor`.
	 * @param ctx the parse tree
	 */
	enterAsyncDecor?: (ctx: AsyncDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.asyncDecor`.
	 * @param ctx the parse tree
	 */
	exitAsyncDecor?: (ctx: AsyncDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.asyncSpec`.
	 * @param ctx the parse tree
	 */
	enterAsyncSpec?: (ctx: AsyncSpecContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.asyncSpec`.
	 * @param ctx the parse tree
	 */
	exitAsyncSpec?: (ctx: AsyncSpecContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.asyncParamValidation`.
	 * @param ctx the parse tree
	 */
	enterAsyncParamValidation?: (ctx: AsyncParamValidationContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.asyncParamValidation`.
	 * @param ctx the parse tree
	 */
	exitAsyncParamValidation?: (ctx: AsyncParamValidationContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.asyncModelValidation`.
	 * @param ctx the parse tree
	 */
	enterAsyncModelValidation?: (ctx: AsyncModelValidationContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.asyncModelValidation`.
	 * @param ctx the parse tree
	 */
	exitAsyncModelValidation?: (ctx: AsyncModelValidationContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.asyncBusinessLogic`.
	 * @param ctx the parse tree
	 */
	enterAsyncBusinessLogic?: (ctx: AsyncBusinessLogicContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.asyncBusinessLogic`.
	 * @param ctx the parse tree
	 */
	exitAsyncBusinessLogic?: (ctx: AsyncBusinessLogicContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.syncDecor`.
	 * @param ctx the parse tree
	 */
	enterSyncDecor?: (ctx: SyncDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.syncDecor`.
	 * @param ctx the parse tree
	 */
	exitSyncDecor?: (ctx: SyncDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.transDecor`.
	 * @param ctx the parse tree
	 */
	enterTransDecor?: (ctx: TransDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.transDecor`.
	 * @param ctx the parse tree
	 */
	exitTransDecor?: (ctx: TransDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.transAttr`.
	 * @param ctx the parse tree
	 */
	enterTransAttr?: (ctx: TransAttrContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.transAttr`.
	 * @param ctx the parse tree
	 */
	exitTransAttr?: (ctx: TransAttrContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.continuationDecor`.
	 * @param ctx the parse tree
	 */
	enterContinuationDecor?: (ctx: ContinuationDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.continuationDecor`.
	 * @param ctx the parse tree
	 */
	exitContinuationDecor?: (ctx: ContinuationDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.effectiveDateDecor`.
	 * @param ctx the parse tree
	 */
	enterEffectiveDateDecor?: (ctx: EffectiveDateDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.effectiveDateDecor`.
	 * @param ctx the parse tree
	 */
	exitEffectiveDateDecor?: (ctx: EffectiveDateDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.explicitDecor`.
	 * @param ctx the parse tree
	 */
	enterExplicitDecor?: (ctx: ExplicitDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.explicitDecor`.
	 * @param ctx the parse tree
	 */
	exitExplicitDecor?: (ctx: ExplicitDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.internalDecor`.
	 * @param ctx the parse tree
	 */
	enterInternalDecor?: (ctx: InternalDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.internalDecor`.
	 * @param ctx the parse tree
	 */
	exitInternalDecor?: (ctx: InternalDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.messageSourceDecor`.
	 * @param ctx the parse tree
	 */
	enterMessageSourceDecor?: (ctx: MessageSourceDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.messageSourceDecor`.
	 * @param ctx the parse tree
	 */
	exitMessageSourceDecor?: (ctx: MessageSourceDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.createDecor`.
	 * @param ctx the parse tree
	 */
	enterCreateDecor?: (ctx: CreateDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.createDecor`.
	 * @param ctx the parse tree
	 */
	exitCreateDecor?: (ctx: CreateDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.deleteDecor`.
	 * @param ctx the parse tree
	 */
	enterDeleteDecor?: (ctx: DeleteDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.deleteDecor`.
	 * @param ctx the parse tree
	 */
	exitDeleteDecor?: (ctx: DeleteDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.partialErrorDecor`.
	 * @param ctx the parse tree
	 */
	enterPartialErrorDecor?: (ctx: PartialErrorDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.partialErrorDecor`.
	 * @param ctx the parse tree
	 */
	exitPartialErrorDecor?: (ctx: PartialErrorDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.generateDecor`.
	 * @param ctx the parse tree
	 */
	enterGenerateDecor?: (ctx: GenerateDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.generateDecor`.
	 * @param ctx the parse tree
	 */
	exitGenerateDecor?: (ctx: GenerateDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.generatorOption`.
	 * @param ctx the parse tree
	 */
	enterGeneratorOption?: (ctx: GeneratorOptionContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.generatorOption`.
	 * @param ctx the parse tree
	 */
	exitGeneratorOption?: (ctx: GeneratorOptionContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.genSetField`.
	 * @param ctx the parse tree
	 */
	enterGenSetField?: (ctx: GenSetFieldContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.genSetField`.
	 * @param ctx the parse tree
	 */
	exitGenSetField?: (ctx: GenSetFieldContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.genAssignments`.
	 * @param ctx the parse tree
	 */
	enterGenAssignments?: (ctx: GenAssignmentsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.genAssignments`.
	 * @param ctx the parse tree
	 */
	exitGenAssignments?: (ctx: GenAssignmentsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.assignmentList`.
	 * @param ctx the parse tree
	 */
	enterAssignmentList?: (ctx: AssignmentListContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.assignmentList`.
	 * @param ctx the parse tree
	 */
	exitAssignmentList?: (ctx: AssignmentListContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.assignment`.
	 * @param ctx the parse tree
	 */
	enterAssignment?: (ctx: AssignmentContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.assignment`.
	 * @param ctx the parse tree
	 */
	exitAssignment?: (ctx: AssignmentContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.valueExpression`.
	 * @param ctx the parse tree
	 */
	enterValueExpression?: (ctx: ValueExpressionContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.valueExpression`.
	 * @param ctx the parse tree
	 */
	exitValueExpression?: (ctx: ValueExpressionContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.genAddToSet`.
	 * @param ctx the parse tree
	 */
	enterGenAddToSet?: (ctx: GenAddToSetContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.genAddToSet`.
	 * @param ctx the parse tree
	 */
	exitGenAddToSet?: (ctx: GenAddToSetContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.genRemoveFromSet`.
	 * @param ctx the parse tree
	 */
	enterGenRemoveFromSet?: (ctx: GenRemoveFromSetContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.genRemoveFromSet`.
	 * @param ctx the parse tree
	 */
	exitGenRemoveFromSet?: (ctx: GenRemoveFromSetContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.genClearSet`.
	 * @param ctx the parse tree
	 */
	enterGenClearSet?: (ctx: GenClearSetContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.genClearSet`.
	 * @param ctx the parse tree
	 */
	exitGenClearSet?: (ctx: GenClearSetContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.genUpdate`.
	 * @param ctx the parse tree
	 */
	enterGenUpdate?: (ctx: GenUpdateContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.genUpdate`.
	 * @param ctx the parse tree
	 */
	exitGenUpdate?: (ctx: GenUpdateContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.jsonKey`.
	 * @param ctx the parse tree
	 */
	enterJsonKey?: (ctx: JsonKeyContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.jsonKey`.
	 * @param ctx the parse tree
	 */
	exitJsonKey?: (ctx: JsonKeyContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.refField`.
	 * @param ctx the parse tree
	 */
	enterRefField?: (ctx: RefFieldContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.refField`.
	 * @param ctx the parse tree
	 */
	exitRefField?: (ctx: RefFieldContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.entityQueries`.
	 * @param ctx the parse tree
	 */
	enterEntityQueries?: (ctx: EntityQueriesContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.entityQueries`.
	 * @param ctx the parse tree
	 */
	exitEntityQueries?: (ctx: EntityQueriesContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.queriesBlock`.
	 * @param ctx the parse tree
	 */
	enterQueriesBlock?: (ctx: QueriesBlockContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.queriesBlock`.
	 * @param ctx the parse tree
	 */
	exitQueriesBlock?: (ctx: QueriesBlockContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.queriesBlockDecorator`.
	 * @param ctx the parse tree
	 */
	enterQueriesBlockDecorator?: (ctx: QueriesBlockDecoratorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.queriesBlockDecorator`.
	 * @param ctx the parse tree
	 */
	exitQueriesBlockDecorator?: (ctx: QueriesBlockDecoratorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.queriesBlockDecorators`.
	 * @param ctx the parse tree
	 */
	enterQueriesBlockDecorators?: (ctx: QueriesBlockDecoratorsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.queriesBlockDecorators`.
	 * @param ctx the parse tree
	 */
	exitQueriesBlockDecorators?: (ctx: QueriesBlockDecoratorsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.query`.
	 * @param ctx the parse tree
	 */
	enterQuery?: (ctx: QueryContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.query`.
	 * @param ctx the parse tree
	 */
	exitQuery?: (ctx: QueryContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.queryDecorator`.
	 * @param ctx the parse tree
	 */
	enterQueryDecorator?: (ctx: QueryDecoratorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.queryDecorator`.
	 * @param ctx the parse tree
	 */
	exitQueryDecorator?: (ctx: QueryDecoratorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.queryDecorators`.
	 * @param ctx the parse tree
	 */
	enterQueryDecorators?: (ctx: QueryDecoratorsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.queryDecorators`.
	 * @param ctx the parse tree
	 */
	exitQueryDecorators?: (ctx: QueryDecoratorsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.temporalDecor`.
	 * @param ctx the parse tree
	 */
	enterTemporalDecor?: (ctx: TemporalDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.temporalDecor`.
	 * @param ctx the parse tree
	 */
	exitTemporalDecor?: (ctx: TemporalDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.provideUserContextDecor`.
	 * @param ctx the parse tree
	 */
	enterProvideUserContextDecor?: (ctx: ProvideUserContextDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.provideUserContextDecor`.
	 * @param ctx the parse tree
	 */
	exitProvideUserContextDecor?: (ctx: ProvideUserContextDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.provideGraphQLSchemaDecor`.
	 * @param ctx the parse tree
	 */
	enterProvideGraphQLSchemaDecor?: (ctx: ProvideGraphQLSchemaDecorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.provideGraphQLSchemaDecor`.
	 * @param ctx the parse tree
	 */
	exitProvideGraphQLSchemaDecor?: (ctx: ProvideGraphQLSchemaDecorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.entitySubscriptions`.
	 * @param ctx the parse tree
	 */
	enterEntitySubscriptions?: (ctx: EntitySubscriptionsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.entitySubscriptions`.
	 * @param ctx the parse tree
	 */
	exitEntitySubscriptions?: (ctx: EntitySubscriptionsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.subscriptionsBlock`.
	 * @param ctx the parse tree
	 */
	enterSubscriptionsBlock?: (ctx: SubscriptionsBlockContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.subscriptionsBlock`.
	 * @param ctx the parse tree
	 */
	exitSubscriptionsBlock?: (ctx: SubscriptionsBlockContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.subscriptionsBlockDecorator`.
	 * @param ctx the parse tree
	 */
	enterSubscriptionsBlockDecorator?: (ctx: SubscriptionsBlockDecoratorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.subscriptionsBlockDecorator`.
	 * @param ctx the parse tree
	 */
	exitSubscriptionsBlockDecorator?: (ctx: SubscriptionsBlockDecoratorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.subscriptionsBlockDecorators`.
	 * @param ctx the parse tree
	 */
	enterSubscriptionsBlockDecorators?: (ctx: SubscriptionsBlockDecoratorsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.subscriptionsBlockDecorators`.
	 * @param ctx the parse tree
	 */
	exitSubscriptionsBlockDecorators?: (ctx: SubscriptionsBlockDecoratorsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.subscription`.
	 * @param ctx the parse tree
	 */
	enterSubscription?: (ctx: SubscriptionContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.subscription`.
	 * @param ctx the parse tree
	 */
	exitSubscription?: (ctx: SubscriptionContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.subscriptionDecorator`.
	 * @param ctx the parse tree
	 */
	enterSubscriptionDecorator?: (ctx: SubscriptionDecoratorContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.subscriptionDecorator`.
	 * @param ctx the parse tree
	 */
	exitSubscriptionDecorator?: (ctx: SubscriptionDecoratorContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.subscriptionDecorators`.
	 * @param ctx the parse tree
	 */
	enterSubscriptionDecorators?: (ctx: SubscriptionDecoratorsContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.subscriptionDecorators`.
	 * @param ctx the parse tree
	 */
	exitSubscriptionDecorators?: (ctx: SubscriptionDecoratorsContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.dottedId`.
	 * @param ctx the parse tree
	 */
	enterDottedId?: (ctx: DottedIdContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.dottedId`.
	 * @param ctx the parse tree
	 */
	exitDottedId?: (ctx: DottedIdContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.comment`.
	 * @param ctx the parse tree
	 */
	enterComment?: (ctx: CommentContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.comment`.
	 * @param ctx the parse tree
	 */
	exitComment?: (ctx: CommentContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.beforeComment`.
	 * @param ctx the parse tree
	 */
	enterBeforeComment?: (ctx: BeforeCommentContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.beforeComment`.
	 * @param ctx the parse tree
	 */
	exitBeforeComment?: (ctx: BeforeCommentContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.afterComment`.
	 * @param ctx the parse tree
	 */
	enterAfterComment?: (ctx: AfterCommentContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.afterComment`.
	 * @param ctx the parse tree
	 */
	exitAfterComment?: (ctx: AfterCommentContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.esslComment`.
	 * @param ctx the parse tree
	 */
	enterEsslComment?: (ctx: EsslCommentContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.esslComment`.
	 * @param ctx the parse tree
	 */
	exitEsslComment?: (ctx: EsslCommentContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.domBeforeLineComment`.
	 * @param ctx the parse tree
	 */
	enterDomBeforeLineComment?: (ctx: DomBeforeLineCommentContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.domBeforeLineComment`.
	 * @param ctx the parse tree
	 */
	exitDomBeforeLineComment?: (ctx: DomBeforeLineCommentContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.domBlockComment`.
	 * @param ctx the parse tree
	 */
	enterDomBlockComment?: (ctx: DomBlockCommentContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.domBlockComment`.
	 * @param ctx the parse tree
	 */
	exitDomBlockComment?: (ctx: DomBlockCommentContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.esslBlockComment`.
	 * @param ctx the parse tree
	 */
	enterEsslBlockComment?: (ctx: EsslBlockCommentContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.esslBlockComment`.
	 * @param ctx the parse tree
	 */
	exitEsslBlockComment?: (ctx: EsslBlockCommentContext) => void;

	/**
	 * Enter a parse tree produced by `ESSL_Parser.esslLineComment`.
	 * @param ctx the parse tree
	 */
	enterEsslLineComment?: (ctx: EsslLineCommentContext) => void;
	/**
	 * Exit a parse tree produced by `ESSL_Parser.esslLineComment`.
	 * @param ctx the parse tree
	 */
	exitEsslLineComment?: (ctx: EsslLineCommentContext) => void;
}

