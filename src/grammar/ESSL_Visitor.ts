// Generated from syntaxes/grammar/ESSL_.g4 by ANTLR 4.9.0-SNAPSHOT


import { ParseTreeVisitor } from "antlr4ts/tree/ParseTreeVisitor";

import { TopContext } from "./ESSL_Parser";
import { EntityContext } from "./ESSL_Parser";
import { EntityNameContext } from "./ESSL_Parser";
import { EntityBlockContext } from "./ESSL_Parser";
import { VirtualEntityContext } from "./ESSL_Parser";
import { ExtendsEntityContext } from "./ESSL_Parser";
import { BaseEntityNameContext } from "./ESSL_Parser";
import { VirtualEntityBlockContext } from "./ESSL_Parser";
import { NamespaceContext } from "./ESSL_Parser";
import { NamespaceNameContext } from "./ESSL_Parser";
import { DependsOnContext } from "./ESSL_Parser";
import { DependsOnNameSpaceContext } from "./ESSL_Parser";
import { ServiceContext } from "./ESSL_Parser";
import { ServiceDecorsContext } from "./ESSL_Parser";
import { ServiceDecorContext } from "./ESSL_Parser";
import { AllApiAuthPolicyContext } from "./ESSL_Parser";
import { AllCommandAuthPolicyContext } from "./ESSL_Parser";
import { AllQueryAuthPolicyContext } from "./ESSL_Parser";
import { AllSubscriptionAuthPolicyContext } from "./ESSL_Parser";
import { AllAdminAuthPolicyContext } from "./ESSL_Parser";
import { AuthPolicyNamesContext } from "./ESSL_Parser";
import { EcosystemPoliciesMethodContext } from "./ESSL_Parser";
import { EcosystemPoliciesMethodNameContext } from "./ESSL_Parser";
import { EcosystemSecretsMethodContext } from "./ESSL_Parser";
import { EcosystemSecretsMethodNameContext } from "./ESSL_Parser";
import { UmlGroupStrReplDecorContext } from "./ESSL_Parser";
import { UmlGroupStrReplParamsContext } from "./ESSL_Parser";
import { UmlGroupTitleStrReplContext } from "./ESSL_Parser";
import { UmlStrReplOldValueContext } from "./ESSL_Parser";
import { UmlStrReplNewValueContext } from "./ESSL_Parser";
import { UmlImageDecorContext } from "./ESSL_Parser";
import { UmlImageParamsContext } from "./ESSL_Parser";
import { UmlGroupImageNameContext } from "./ESSL_Parser";
import { UmlImageFormatContext } from "./ESSL_Parser";
import { UmlImageRemoveTitleContext } from "./ESSL_Parser";
import { UmlImageSubFolderContext } from "./ESSL_Parser";
import { EntityTypeContext } from "./ESSL_Parser";
import { ValueTypeContext } from "./ESSL_Parser";
import { InterfaceTypeContext } from "./ESSL_Parser";
import { NameContext } from "./ESSL_Parser";
import { ExtendsListContext } from "./ESSL_Parser";
import { BaseNameContext } from "./ESSL_Parser";
import { TypeBlockContext } from "./ESSL_Parser";
import { PossiblyEmptyTypeBlockContext } from "./ESSL_Parser";
import { EntityTypeDecorsContext } from "./ESSL_Parser";
import { EntityTypeDecorContext } from "./ESSL_Parser";
import { PartialCommandContext } from "./ESSL_Parser";
import { TransCoordinatorContext } from "./ESSL_Parser";
import { DefaultConstructorContext } from "./ESSL_Parser";
import { IndexTypeDecorContext } from "./ESSL_Parser";
import { UniqueIndexTypeDecorContext } from "./ESSL_Parser";
import { IndexTypeAttrsContext } from "./ESSL_Parser";
import { IndexKeyAttrsContext } from "./ESSL_Parser";
import { IndexKeyAttrContext } from "./ESSL_Parser";
import { IndexedFieldContext } from "./ESSL_Parser";
import { QueryPartitionDecorContext } from "./ESSL_Parser";
import { QueryPartitionAttrsContext } from "./ESSL_Parser";
import { PartitionIndexContext } from "./ESSL_Parser";
import { PartitionTypeUniqueIndexContext } from "./ESSL_Parser";
import { CrossPartitionDecorContext } from "./ESSL_Parser";
import { UmlGroupDecorContext } from "./ESSL_Parser";
import { UmlGroupContext } from "./ESSL_Parser";
import { UmlGroupTitleContext } from "./ESSL_Parser";
import { UmlSubGroupContext } from "./ESSL_Parser";
import { HashLookupDecorContext } from "./ESSL_Parser";
import { BeforeEventPersistContext } from "./ESSL_Parser";
import { AuthPolicyNameContext } from "./ESSL_Parser";
import { AuthPolicyDecorContext } from "./ESSL_Parser";
import { PublicDecorContext } from "./ESSL_Parser";
import { PluralNameDecorContext } from "./ESSL_Parser";
import { PluralNameContext } from "./ESSL_Parser";
import { TableNameDecorContext } from "./ESSL_Parser";
import { TableNameContext } from "./ESSL_Parser";
import { GraphQLNameDecorContext } from "./ESSL_Parser";
import { GraphQLNameContext } from "./ESSL_Parser";
import { GraphQLPluralNameDecorContext } from "./ESSL_Parser";
import { GraphQLPluralNameContext } from "./ESSL_Parser";
import { GraphQLCamelCaseDecorContext } from "./ESSL_Parser";
import { GraphQLCamelCaseContext } from "./ESSL_Parser";
import { GraphQLPluralCamelCaseDecorContext } from "./ESSL_Parser";
import { GraphQLPluralCamelCaseContext } from "./ESSL_Parser";
import { Base64RefIdsDecorContext } from "./ESSL_Parser";
import { InterfaceDecorContext } from "./ESSL_Parser";
import { InterfaceNameContext } from "./ESSL_Parser";
import { CamelCaseDecorContext } from "./ESSL_Parser";
import { CamelCaseNameContext } from "./ESSL_Parser";
import { GenerateIfDecorContext } from "./ESSL_Parser";
import { GenerateIfNameContext } from "./ESSL_Parser";
import { DimensionNameDecorContext } from "./ESSL_Parser";
import { StyleDecorContext } from "./ESSL_Parser";
import { EventsContext } from "./ESSL_Parser";
import { SlowlyChangingContext } from "./ESSL_Parser";
import { LedgerContext } from "./ESSL_Parser";
import { FactLedgerContext } from "./ESSL_Parser";
import { IndexAttrDecorsContext } from "./ESSL_Parser";
import { IndexAttrDecorContext } from "./ESSL_Parser";
import { CacheModelContext } from "./ESSL_Parser";
import { NoCacheModelContext } from "./ESSL_Parser";
import { AllowTemporalContext } from "./ESSL_Parser";
import { IndexNameDecorContext } from "./ESSL_Parser";
import { IndexNameContext } from "./ESSL_Parser";
import { ValueTypeDecorsContext } from "./ESSL_Parser";
import { ValueTypeDecorContext } from "./ESSL_Parser";
import { PartialContext } from "./ESSL_Parser";
import { JsonConstructorContext } from "./ESSL_Parser";
import { WebValueStringContext } from "./ESSL_Parser";
import { WebValueMethodContext } from "./ESSL_Parser";
import { DiscriminatedByContext } from "./ESSL_Parser";
import { DiscriminatedByValueContext } from "./ESSL_Parser";
import { OutputTypeContext } from "./ESSL_Parser";
import { VoidPrimaryFactContext } from "./ESSL_Parser";
import { InterfaceTypeDecorsContext } from "./ESSL_Parser";
import { InterfaceTypeDecorContext } from "./ESSL_Parser";
import { EnumDeclContext } from "./ESSL_Parser";
import { EnumBlockContext } from "./ESSL_Parser";
import { EnumItemContext } from "./ESSL_Parser";
import { EnumValueContext } from "./ESSL_Parser";
import { EnumExplicitValueContext } from "./ESSL_Parser";
import { EnumDecorsContext } from "./ESSL_Parser";
import { EnumDecorContext } from "./ESSL_Parser";
import { SuppressDecorContext } from "./ESSL_Parser";
import { SuppressOptionContext } from "./ESSL_Parser";
import { ModelsuppressOptionContext } from "./ESSL_Parser";
import { GraphQlsuppressOptionContext } from "./ESSL_Parser";
import { InputTypeOptionContext } from "./ESSL_Parser";
import { FlagsDecorContext } from "./ESSL_Parser";
import { DictionaryContext } from "./ESSL_Parser";
import { DictionaryBlockContext } from "./ESSL_Parser";
import { DictPartitionedContext } from "./ESSL_Parser";
import { DictValueTypeDeclContext } from "./ESSL_Parser";
import { DictValueTypeContext } from "./ESSL_Parser";
import { DictDefaultDeclContext } from "./ESSL_Parser";
import { DictDefaultContext } from "./ESSL_Parser";
import { DictEntriesContext } from "./ESSL_Parser";
import { DictionaryDecorsContext } from "./ESSL_Parser";
import { DictionaryDecorContext } from "./ESSL_Parser";
import { FieldDefContext } from "./ESSL_Parser";
import { TypeRefContext } from "./ESSL_Parser";
import { TypeRefNameContext } from "./ESSL_Parser";
import { PrimitiveTypeContext } from "./ESSL_Parser";
import { ValueTypeRefContext } from "./ESSL_Parser";
import { ListIndicatorContext } from "./ESSL_Parser";
import { NullableIndicatorContext } from "./ESSL_Parser";
import { FieldInitializerContext } from "./ESSL_Parser";
import { NullValueContext } from "./ESSL_Parser";
import { BoolValueContext } from "./ESSL_Parser";
import { NewInstanceContext } from "./ESSL_Parser";
import { InitialEnumValContext } from "./ESSL_Parser";
import { NumValueContext } from "./ESSL_Parser";
import { IntegerContext } from "./ESSL_Parser";
import { DecimalContext } from "./ESSL_Parser";
import { StringValueContext } from "./ESSL_Parser";
import { FlagsEnumSetContext } from "./ESSL_Parser";
import { FlagsEnumValueContext } from "./ESSL_Parser";
import { FieldDecoratorsContext } from "./ESSL_Parser";
import { FieldDecoratorContext } from "./ESSL_Parser";
import { IndexDecorContext } from "./ESSL_Parser";
import { UniqueIndexDecorContext } from "./ESSL_Parser";
import { IndexAttrsContext } from "./ESSL_Parser";
import { CaseInsensitiveContext } from "./ESSL_Parser";
import { NullHandlingContext } from "./ESSL_Parser";
import { InlineEnumDecorContext } from "./ESSL_Parser";
import { InlineEnumValueContext } from "./ESSL_Parser";
import { ComposedDecorContext } from "./ESSL_Parser";
import { RequiredDecorContext } from "./ESSL_Parser";
import { ReadOnlyDecorContext } from "./ESSL_Parser";
import { HiddenDecorContext } from "./ESSL_Parser";
import { CalculatedDecorContext } from "./ESSL_Parser";
import { ImmutableDecorContext } from "./ESSL_Parser";
import { ClonePartitionerDecorContext } from "./ESSL_Parser";
import { CloneIdAsIsDecorContext } from "./ESSL_Parser";
import { FieldUmlDecorContext } from "./ESSL_Parser";
import { FieldUmlGroupTitleContext } from "./ESSL_Parser";
import { DirectionContext } from "./ESSL_Parser";
import { HorizontalVerticalContext } from "./ESSL_Parser";
import { LineLengthContext } from "./ESSL_Parser";
import { HideRelationshipContext } from "./ESSL_Parser";
import { LineLabelContext } from "./ESSL_Parser";
import { SectionDecorContext } from "./ESSL_Parser";
import { BreakTypeContext } from "./ESSL_Parser";
import { AsValueTypeDecorContext } from "./ESSL_Parser";
import { SameAsDecorContext } from "./ESSL_Parser";
import { SameAsPersistContext } from "./ESSL_Parser";
import { ConstantDecorContext } from "./ESSL_Parser";
import { NotPersistedDecorContext } from "./ESSL_Parser";
import { HandCodedDecorContext } from "./ESSL_Parser";
import { DictDecorContext } from "./ESSL_Parser";
import { TypeDiscriminatorContext } from "./ESSL_Parser";
import { MultiLineDecorContext } from "./ESSL_Parser";
import { QuestionDecorContext } from "./ESSL_Parser";
import { LabelDecorContext } from "./ESSL_Parser";
import { LabelContext } from "./ESSL_Parser";
import { AutoFillDecorContext } from "./ESSL_Parser";
import { AutoFillTypeContext } from "./ESSL_Parser";
import { AttributeDecorContext } from "./ESSL_Parser";
import { AttributePairContext } from "./ESSL_Parser";
import { AttributeKeyContext } from "./ESSL_Parser";
import { AttributeValueContext } from "./ESSL_Parser";
import { FactDimensionDecorContext } from "./ESSL_Parser";
import { DimPrimaryContext } from "./ESSL_Parser";
import { DimInxContext } from "./ESSL_Parser";
import { DimensionKeyDecorContext } from "./ESSL_Parser";
import { JournalEntriesDecorContext } from "./ESSL_Parser";
import { EntityEventsContext } from "./ESSL_Parser";
import { EventsBlockContext } from "./ESSL_Parser";
import { CreatedEventDefContext } from "./ESSL_Parser";
import { CreatedEventContext } from "./ESSL_Parser";
import { EventDefContext } from "./ESSL_Parser";
import { EventNameContext } from "./ESSL_Parser";
import { EventsDecorsContext } from "./ESSL_Parser";
import { EventsDecorContext } from "./ESSL_Parser";
import { EventNameDecorsContext } from "./ESSL_Parser";
import { EventNameDecorContext } from "./ESSL_Parser";
import { EntityCommandsContext } from "./ESSL_Parser";
import { CommandsBlockContext } from "./ESSL_Parser";
import { CommandsBlockDecoratorContext } from "./ESSL_Parser";
import { CommandsBlockDecoratorsContext } from "./ESSL_Parser";
import { CommandContext } from "./ESSL_Parser";
import { CommandParamsContext } from "./ESSL_Parser";
import { ParamContext } from "./ESSL_Parser";
import { ParamDeclContext } from "./ESSL_Parser";
import { OptionalTypeRefContext } from "./ESSL_Parser";
import { ParamNameModifiersContext } from "./ESSL_Parser";
import { ParamInitializerContext } from "./ESSL_Parser";
import { CommandResultsInContext } from "./ESSL_Parser";
import { YieldsContext } from "./ESSL_Parser";
import { EventListContext } from "./ESSL_Parser";
import { EventRefContext } from "./ESSL_Parser";
import { ReturnsTypeContext } from "./ESSL_Parser";
import { ReturnTypeRefContext } from "./ESSL_Parser";
import { ParamDecoratorContext } from "./ESSL_Parser";
import { TypeQualifierDecorContext } from "./ESSL_Parser";
import { ParamDecoratorsContext } from "./ESSL_Parser";
import { TypeQualifierNameContext } from "./ESSL_Parser";
import { IndexQualifierContext } from "./ESSL_Parser";
import { FilterExprContext } from "./ESSL_Parser";
import { ByValueDecorContext } from "./ESSL_Parser";
import { IdDecorContext } from "./ESSL_Parser";
import { CommandDecoratorContext } from "./ESSL_Parser";
import { CommandDecoratorsContext } from "./ESSL_Parser";
import { ModelDecorContext } from "./ESSL_Parser";
import { AsyncDecorContext } from "./ESSL_Parser";
import { AsyncSpecContext } from "./ESSL_Parser";
import { AsyncParamValidationContext } from "./ESSL_Parser";
import { AsyncModelValidationContext } from "./ESSL_Parser";
import { AsyncBusinessLogicContext } from "./ESSL_Parser";
import { SyncDecorContext } from "./ESSL_Parser";
import { TransDecorContext } from "./ESSL_Parser";
import { TransAttrContext } from "./ESSL_Parser";
import { ContinuationDecorContext } from "./ESSL_Parser";
import { EffectiveDateDecorContext } from "./ESSL_Parser";
import { ExplicitDecorContext } from "./ESSL_Parser";
import { InternalDecorContext } from "./ESSL_Parser";
import { MessageSourceDecorContext } from "./ESSL_Parser";
import { CreateDecorContext } from "./ESSL_Parser";
import { DeleteDecorContext } from "./ESSL_Parser";
import { PartialErrorDecorContext } from "./ESSL_Parser";
import { GenerateDecorContext } from "./ESSL_Parser";
import { GeneratorOptionContext } from "./ESSL_Parser";
import { GenSetFieldContext } from "./ESSL_Parser";
import { GenAssignmentsContext } from "./ESSL_Parser";
import { AssignmentListContext } from "./ESSL_Parser";
import { AssignmentContext } from "./ESSL_Parser";
import { ValueExpressionContext } from "./ESSL_Parser";
import { GenAddToSetContext } from "./ESSL_Parser";
import { GenRemoveFromSetContext } from "./ESSL_Parser";
import { GenClearSetContext } from "./ESSL_Parser";
import { GenUpdateContext } from "./ESSL_Parser";
import { JsonKeyContext } from "./ESSL_Parser";
import { RefFieldContext } from "./ESSL_Parser";
import { EntityQueriesContext } from "./ESSL_Parser";
import { QueriesBlockContext } from "./ESSL_Parser";
import { QueriesBlockDecoratorContext } from "./ESSL_Parser";
import { QueriesBlockDecoratorsContext } from "./ESSL_Parser";
import { QueryContext } from "./ESSL_Parser";
import { QueryDecoratorContext } from "./ESSL_Parser";
import { QueryDecoratorsContext } from "./ESSL_Parser";
import { TemporalDecorContext } from "./ESSL_Parser";
import { ProvideUserContextDecorContext } from "./ESSL_Parser";
import { ProvideGraphQLSchemaDecorContext } from "./ESSL_Parser";
import { EntitySubscriptionsContext } from "./ESSL_Parser";
import { SubscriptionsBlockContext } from "./ESSL_Parser";
import { SubscriptionsBlockDecoratorContext } from "./ESSL_Parser";
import { SubscriptionsBlockDecoratorsContext } from "./ESSL_Parser";
import { SubscriptionContext } from "./ESSL_Parser";
import { SubscriptionDecoratorContext } from "./ESSL_Parser";
import { SubscriptionDecoratorsContext } from "./ESSL_Parser";
import { DottedIdContext } from "./ESSL_Parser";
import { CommentContext } from "./ESSL_Parser";
import { BeforeCommentContext } from "./ESSL_Parser";
import { AfterCommentContext } from "./ESSL_Parser";
import { EsslCommentContext } from "./ESSL_Parser";
import { DomBeforeLineCommentContext } from "./ESSL_Parser";
import { DomBlockCommentContext } from "./ESSL_Parser";
import { EsslBlockCommentContext } from "./ESSL_Parser";
import { EsslLineCommentContext } from "./ESSL_Parser";


/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by `ESSL_Parser`.
 *
 * @param <Result> The return type of the visit operation. Use `void` for
 * operations with no return type.
 */
export interface ESSL_Visitor<Result> extends ParseTreeVisitor<Result> {
	/**
	 * Visit a parse tree produced by `ESSL_Parser.top`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTop?: (ctx: TopContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.entity`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEntity?: (ctx: EntityContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.entityName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEntityName?: (ctx: EntityNameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.entityBlock`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEntityBlock?: (ctx: EntityBlockContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.virtualEntity`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitVirtualEntity?: (ctx: VirtualEntityContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.extendsEntity`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitExtendsEntity?: (ctx: ExtendsEntityContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.baseEntityName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitBaseEntityName?: (ctx: BaseEntityNameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.virtualEntityBlock`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitVirtualEntityBlock?: (ctx: VirtualEntityBlockContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.namespace`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitNamespace?: (ctx: NamespaceContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.namespaceName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitNamespaceName?: (ctx: NamespaceNameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.dependsOn`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDependsOn?: (ctx: DependsOnContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.dependsOnNameSpace`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDependsOnNameSpace?: (ctx: DependsOnNameSpaceContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.service`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitService?: (ctx: ServiceContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.serviceDecors`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitServiceDecors?: (ctx: ServiceDecorsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.serviceDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitServiceDecor?: (ctx: ServiceDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.allApiAuthPolicy`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAllApiAuthPolicy?: (ctx: AllApiAuthPolicyContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.allCommandAuthPolicy`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAllCommandAuthPolicy?: (ctx: AllCommandAuthPolicyContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.allQueryAuthPolicy`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAllQueryAuthPolicy?: (ctx: AllQueryAuthPolicyContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.allSubscriptionAuthPolicy`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAllSubscriptionAuthPolicy?: (ctx: AllSubscriptionAuthPolicyContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.allAdminAuthPolicy`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAllAdminAuthPolicy?: (ctx: AllAdminAuthPolicyContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.authPolicyNames`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAuthPolicyNames?: (ctx: AuthPolicyNamesContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.ecosystemPoliciesMethod`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEcosystemPoliciesMethod?: (ctx: EcosystemPoliciesMethodContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.ecosystemPoliciesMethodName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEcosystemPoliciesMethodName?: (ctx: EcosystemPoliciesMethodNameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.ecosystemSecretsMethod`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEcosystemSecretsMethod?: (ctx: EcosystemSecretsMethodContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.ecosystemSecretsMethodName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEcosystemSecretsMethodName?: (ctx: EcosystemSecretsMethodNameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.umlGroupStrReplDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUmlGroupStrReplDecor?: (ctx: UmlGroupStrReplDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.umlGroupStrReplParams`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUmlGroupStrReplParams?: (ctx: UmlGroupStrReplParamsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.umlGroupTitleStrRepl`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUmlGroupTitleStrRepl?: (ctx: UmlGroupTitleStrReplContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.umlStrReplOldValue`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUmlStrReplOldValue?: (ctx: UmlStrReplOldValueContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.umlStrReplNewValue`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUmlStrReplNewValue?: (ctx: UmlStrReplNewValueContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.umlImageDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUmlImageDecor?: (ctx: UmlImageDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.umlImageParams`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUmlImageParams?: (ctx: UmlImageParamsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.umlGroupImageName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUmlGroupImageName?: (ctx: UmlGroupImageNameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.umlImageFormat`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUmlImageFormat?: (ctx: UmlImageFormatContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.umlImageRemoveTitle`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUmlImageRemoveTitle?: (ctx: UmlImageRemoveTitleContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.umlImageSubFolder`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUmlImageSubFolder?: (ctx: UmlImageSubFolderContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.entityType`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEntityType?: (ctx: EntityTypeContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.valueType`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitValueType?: (ctx: ValueTypeContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.interfaceType`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitInterfaceType?: (ctx: InterfaceTypeContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.name`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitName?: (ctx: NameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.extendsList`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitExtendsList?: (ctx: ExtendsListContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.baseName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitBaseName?: (ctx: BaseNameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.typeBlock`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTypeBlock?: (ctx: TypeBlockContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.possiblyEmptyTypeBlock`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPossiblyEmptyTypeBlock?: (ctx: PossiblyEmptyTypeBlockContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.entityTypeDecors`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEntityTypeDecors?: (ctx: EntityTypeDecorsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.entityTypeDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEntityTypeDecor?: (ctx: EntityTypeDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.partialCommand`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPartialCommand?: (ctx: PartialCommandContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.transCoordinator`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTransCoordinator?: (ctx: TransCoordinatorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.defaultConstructor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDefaultConstructor?: (ctx: DefaultConstructorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.indexTypeDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitIndexTypeDecor?: (ctx: IndexTypeDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.uniqueIndexTypeDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUniqueIndexTypeDecor?: (ctx: UniqueIndexTypeDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.indexTypeAttrs`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitIndexTypeAttrs?: (ctx: IndexTypeAttrsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.indexKeyAttrs`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitIndexKeyAttrs?: (ctx: IndexKeyAttrsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.indexKeyAttr`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitIndexKeyAttr?: (ctx: IndexKeyAttrContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.indexedField`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitIndexedField?: (ctx: IndexedFieldContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.queryPartitionDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitQueryPartitionDecor?: (ctx: QueryPartitionDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.queryPartitionAttrs`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitQueryPartitionAttrs?: (ctx: QueryPartitionAttrsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.partitionIndex`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPartitionIndex?: (ctx: PartitionIndexContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.partitionTypeUniqueIndex`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPartitionTypeUniqueIndex?: (ctx: PartitionTypeUniqueIndexContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.crossPartitionDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCrossPartitionDecor?: (ctx: CrossPartitionDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.umlGroupDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUmlGroupDecor?: (ctx: UmlGroupDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.umlGroup`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUmlGroup?: (ctx: UmlGroupContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.umlGroupTitle`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUmlGroupTitle?: (ctx: UmlGroupTitleContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.umlSubGroup`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUmlSubGroup?: (ctx: UmlSubGroupContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.hashLookupDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitHashLookupDecor?: (ctx: HashLookupDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.beforeEventPersist`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitBeforeEventPersist?: (ctx: BeforeEventPersistContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.authPolicyName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAuthPolicyName?: (ctx: AuthPolicyNameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.authPolicyDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAuthPolicyDecor?: (ctx: AuthPolicyDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.publicDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPublicDecor?: (ctx: PublicDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.pluralNameDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPluralNameDecor?: (ctx: PluralNameDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.pluralName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPluralName?: (ctx: PluralNameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.tableNameDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTableNameDecor?: (ctx: TableNameDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.tableName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTableName?: (ctx: TableNameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.graphQLNameDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGraphQLNameDecor?: (ctx: GraphQLNameDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.graphQLName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGraphQLName?: (ctx: GraphQLNameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.graphQLPluralNameDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGraphQLPluralNameDecor?: (ctx: GraphQLPluralNameDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.graphQLPluralName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGraphQLPluralName?: (ctx: GraphQLPluralNameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.graphQLCamelCaseDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGraphQLCamelCaseDecor?: (ctx: GraphQLCamelCaseDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.graphQLCamelCase`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGraphQLCamelCase?: (ctx: GraphQLCamelCaseContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.graphQLPluralCamelCaseDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGraphQLPluralCamelCaseDecor?: (ctx: GraphQLPluralCamelCaseDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.graphQLPluralCamelCase`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGraphQLPluralCamelCase?: (ctx: GraphQLPluralCamelCaseContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.base64RefIdsDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitBase64RefIdsDecor?: (ctx: Base64RefIdsDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.interfaceDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitInterfaceDecor?: (ctx: InterfaceDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.interfaceName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitInterfaceName?: (ctx: InterfaceNameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.camelCaseDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCamelCaseDecor?: (ctx: CamelCaseDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.camelCaseName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCamelCaseName?: (ctx: CamelCaseNameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.generateIfDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGenerateIfDecor?: (ctx: GenerateIfDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.generateIfName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGenerateIfName?: (ctx: GenerateIfNameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.dimensionNameDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDimensionNameDecor?: (ctx: DimensionNameDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.styleDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitStyleDecor?: (ctx: StyleDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.events`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEvents?: (ctx: EventsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.slowlyChanging`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSlowlyChanging?: (ctx: SlowlyChangingContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.ledger`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitLedger?: (ctx: LedgerContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.factLedger`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFactLedger?: (ctx: FactLedgerContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.indexAttrDecors`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitIndexAttrDecors?: (ctx: IndexAttrDecorsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.indexAttrDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitIndexAttrDecor?: (ctx: IndexAttrDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.cacheModel`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCacheModel?: (ctx: CacheModelContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.noCacheModel`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitNoCacheModel?: (ctx: NoCacheModelContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.allowTemporal`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAllowTemporal?: (ctx: AllowTemporalContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.indexNameDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitIndexNameDecor?: (ctx: IndexNameDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.indexName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitIndexName?: (ctx: IndexNameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.valueTypeDecors`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitValueTypeDecors?: (ctx: ValueTypeDecorsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.valueTypeDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitValueTypeDecor?: (ctx: ValueTypeDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.partial`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPartial?: (ctx: PartialContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.jsonConstructor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitJsonConstructor?: (ctx: JsonConstructorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.webValueString`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitWebValueString?: (ctx: WebValueStringContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.webValueMethod`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitWebValueMethod?: (ctx: WebValueMethodContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.discriminatedBy`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDiscriminatedBy?: (ctx: DiscriminatedByContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.discriminatedByValue`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDiscriminatedByValue?: (ctx: DiscriminatedByValueContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.outputType`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitOutputType?: (ctx: OutputTypeContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.voidPrimaryFact`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitVoidPrimaryFact?: (ctx: VoidPrimaryFactContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.interfaceTypeDecors`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitInterfaceTypeDecors?: (ctx: InterfaceTypeDecorsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.interfaceTypeDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitInterfaceTypeDecor?: (ctx: InterfaceTypeDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.enumDecl`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEnumDecl?: (ctx: EnumDeclContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.enumBlock`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEnumBlock?: (ctx: EnumBlockContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.enumItem`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEnumItem?: (ctx: EnumItemContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.enumValue`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEnumValue?: (ctx: EnumValueContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.enumExplicitValue`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEnumExplicitValue?: (ctx: EnumExplicitValueContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.enumDecors`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEnumDecors?: (ctx: EnumDecorsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.enumDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEnumDecor?: (ctx: EnumDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.suppressDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSuppressDecor?: (ctx: SuppressDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.suppressOption`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSuppressOption?: (ctx: SuppressOptionContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.modelsuppressOption`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitModelsuppressOption?: (ctx: ModelsuppressOptionContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.graphQlsuppressOption`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGraphQlsuppressOption?: (ctx: GraphQlsuppressOptionContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.inputTypeOption`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitInputTypeOption?: (ctx: InputTypeOptionContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.flagsDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFlagsDecor?: (ctx: FlagsDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.dictionary`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDictionary?: (ctx: DictionaryContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.dictionaryBlock`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDictionaryBlock?: (ctx: DictionaryBlockContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.dictPartitioned`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDictPartitioned?: (ctx: DictPartitionedContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.dictValueTypeDecl`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDictValueTypeDecl?: (ctx: DictValueTypeDeclContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.dictValueType`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDictValueType?: (ctx: DictValueTypeContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.dictDefaultDecl`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDictDefaultDecl?: (ctx: DictDefaultDeclContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.dictDefault`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDictDefault?: (ctx: DictDefaultContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.dictEntries`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDictEntries?: (ctx: DictEntriesContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.dictionaryDecors`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDictionaryDecors?: (ctx: DictionaryDecorsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.dictionaryDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDictionaryDecor?: (ctx: DictionaryDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.fieldDef`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFieldDef?: (ctx: FieldDefContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.typeRef`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTypeRef?: (ctx: TypeRefContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.typeRefName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTypeRefName?: (ctx: TypeRefNameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.primitiveType`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPrimitiveType?: (ctx: PrimitiveTypeContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.valueTypeRef`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitValueTypeRef?: (ctx: ValueTypeRefContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.listIndicator`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitListIndicator?: (ctx: ListIndicatorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.nullableIndicator`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitNullableIndicator?: (ctx: NullableIndicatorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.fieldInitializer`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFieldInitializer?: (ctx: FieldInitializerContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.nullValue`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitNullValue?: (ctx: NullValueContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.boolValue`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitBoolValue?: (ctx: BoolValueContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.newInstance`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitNewInstance?: (ctx: NewInstanceContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.initialEnumVal`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitInitialEnumVal?: (ctx: InitialEnumValContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.numValue`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitNumValue?: (ctx: NumValueContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.integer`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitInteger?: (ctx: IntegerContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.decimal`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDecimal?: (ctx: DecimalContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.stringValue`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitStringValue?: (ctx: StringValueContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.flagsEnumSet`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFlagsEnumSet?: (ctx: FlagsEnumSetContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.flagsEnumValue`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFlagsEnumValue?: (ctx: FlagsEnumValueContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.fieldDecorators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFieldDecorators?: (ctx: FieldDecoratorsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.fieldDecorator`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFieldDecorator?: (ctx: FieldDecoratorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.indexDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitIndexDecor?: (ctx: IndexDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.uniqueIndexDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitUniqueIndexDecor?: (ctx: UniqueIndexDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.indexAttrs`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitIndexAttrs?: (ctx: IndexAttrsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.caseInsensitive`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCaseInsensitive?: (ctx: CaseInsensitiveContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.nullHandling`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitNullHandling?: (ctx: NullHandlingContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.inlineEnumDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitInlineEnumDecor?: (ctx: InlineEnumDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.inlineEnumValue`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitInlineEnumValue?: (ctx: InlineEnumValueContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.composedDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitComposedDecor?: (ctx: ComposedDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.requiredDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitRequiredDecor?: (ctx: RequiredDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.readOnlyDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitReadOnlyDecor?: (ctx: ReadOnlyDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.hiddenDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitHiddenDecor?: (ctx: HiddenDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.calculatedDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCalculatedDecor?: (ctx: CalculatedDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.immutableDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitImmutableDecor?: (ctx: ImmutableDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.clonePartitionerDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitClonePartitionerDecor?: (ctx: ClonePartitionerDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.cloneIdAsIsDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCloneIdAsIsDecor?: (ctx: CloneIdAsIsDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.fieldUmlDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFieldUmlDecor?: (ctx: FieldUmlDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.fieldUmlGroupTitle`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFieldUmlGroupTitle?: (ctx: FieldUmlGroupTitleContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.direction`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDirection?: (ctx: DirectionContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.horizontalVertical`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitHorizontalVertical?: (ctx: HorizontalVerticalContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.lineLength`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitLineLength?: (ctx: LineLengthContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.hideRelationship`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitHideRelationship?: (ctx: HideRelationshipContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.lineLabel`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitLineLabel?: (ctx: LineLabelContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.sectionDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSectionDecor?: (ctx: SectionDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.breakType`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitBreakType?: (ctx: BreakTypeContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.asValueTypeDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAsValueTypeDecor?: (ctx: AsValueTypeDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.sameAsDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSameAsDecor?: (ctx: SameAsDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.sameAsPersist`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSameAsPersist?: (ctx: SameAsPersistContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.constantDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitConstantDecor?: (ctx: ConstantDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.notPersistedDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitNotPersistedDecor?: (ctx: NotPersistedDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.handCodedDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitHandCodedDecor?: (ctx: HandCodedDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.dictDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDictDecor?: (ctx: DictDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.typeDiscriminator`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTypeDiscriminator?: (ctx: TypeDiscriminatorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.multiLineDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitMultiLineDecor?: (ctx: MultiLineDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.questionDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitQuestionDecor?: (ctx: QuestionDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.labelDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitLabelDecor?: (ctx: LabelDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.label`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitLabel?: (ctx: LabelContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.autoFillDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAutoFillDecor?: (ctx: AutoFillDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.autoFillType`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAutoFillType?: (ctx: AutoFillTypeContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.attributeDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAttributeDecor?: (ctx: AttributeDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.attributePair`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAttributePair?: (ctx: AttributePairContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.attributeKey`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAttributeKey?: (ctx: AttributeKeyContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.attributeValue`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAttributeValue?: (ctx: AttributeValueContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.factDimensionDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFactDimensionDecor?: (ctx: FactDimensionDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.dimPrimary`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDimPrimary?: (ctx: DimPrimaryContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.dimInx`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDimInx?: (ctx: DimInxContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.dimensionKeyDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDimensionKeyDecor?: (ctx: DimensionKeyDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.journalEntriesDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitJournalEntriesDecor?: (ctx: JournalEntriesDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.entityEvents`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEntityEvents?: (ctx: EntityEventsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.eventsBlock`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEventsBlock?: (ctx: EventsBlockContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.createdEventDef`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCreatedEventDef?: (ctx: CreatedEventDefContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.createdEvent`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCreatedEvent?: (ctx: CreatedEventContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.eventDef`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEventDef?: (ctx: EventDefContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.eventName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEventName?: (ctx: EventNameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.eventsDecors`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEventsDecors?: (ctx: EventsDecorsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.eventsDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEventsDecor?: (ctx: EventsDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.eventNameDecors`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEventNameDecors?: (ctx: EventNameDecorsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.eventNameDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEventNameDecor?: (ctx: EventNameDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.entityCommands`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEntityCommands?: (ctx: EntityCommandsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.commandsBlock`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCommandsBlock?: (ctx: CommandsBlockContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.commandsBlockDecorator`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCommandsBlockDecorator?: (ctx: CommandsBlockDecoratorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.commandsBlockDecorators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCommandsBlockDecorators?: (ctx: CommandsBlockDecoratorsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.command`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCommand?: (ctx: CommandContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.commandParams`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCommandParams?: (ctx: CommandParamsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.param`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitParam?: (ctx: ParamContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.paramDecl`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitParamDecl?: (ctx: ParamDeclContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.optionalTypeRef`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitOptionalTypeRef?: (ctx: OptionalTypeRefContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.paramNameModifiers`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitParamNameModifiers?: (ctx: ParamNameModifiersContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.paramInitializer`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitParamInitializer?: (ctx: ParamInitializerContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.commandResultsIn`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCommandResultsIn?: (ctx: CommandResultsInContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.yields`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitYields?: (ctx: YieldsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.eventList`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEventList?: (ctx: EventListContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.eventRef`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEventRef?: (ctx: EventRefContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.returnsType`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitReturnsType?: (ctx: ReturnsTypeContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.returnTypeRef`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitReturnTypeRef?: (ctx: ReturnTypeRefContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.paramDecorator`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitParamDecorator?: (ctx: ParamDecoratorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.typeQualifierDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTypeQualifierDecor?: (ctx: TypeQualifierDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.paramDecorators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitParamDecorators?: (ctx: ParamDecoratorsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.typeQualifierName`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTypeQualifierName?: (ctx: TypeQualifierNameContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.indexQualifier`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitIndexQualifier?: (ctx: IndexQualifierContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.filterExpr`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitFilterExpr?: (ctx: FilterExprContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.byValueDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitByValueDecor?: (ctx: ByValueDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.idDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitIdDecor?: (ctx: IdDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.commandDecorator`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCommandDecorator?: (ctx: CommandDecoratorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.commandDecorators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCommandDecorators?: (ctx: CommandDecoratorsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.modelDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitModelDecor?: (ctx: ModelDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.asyncDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAsyncDecor?: (ctx: AsyncDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.asyncSpec`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAsyncSpec?: (ctx: AsyncSpecContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.asyncParamValidation`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAsyncParamValidation?: (ctx: AsyncParamValidationContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.asyncModelValidation`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAsyncModelValidation?: (ctx: AsyncModelValidationContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.asyncBusinessLogic`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAsyncBusinessLogic?: (ctx: AsyncBusinessLogicContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.syncDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSyncDecor?: (ctx: SyncDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.transDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTransDecor?: (ctx: TransDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.transAttr`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTransAttr?: (ctx: TransAttrContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.continuationDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitContinuationDecor?: (ctx: ContinuationDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.effectiveDateDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEffectiveDateDecor?: (ctx: EffectiveDateDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.explicitDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitExplicitDecor?: (ctx: ExplicitDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.internalDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitInternalDecor?: (ctx: InternalDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.messageSourceDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitMessageSourceDecor?: (ctx: MessageSourceDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.createDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitCreateDecor?: (ctx: CreateDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.deleteDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDeleteDecor?: (ctx: DeleteDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.partialErrorDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitPartialErrorDecor?: (ctx: PartialErrorDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.generateDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGenerateDecor?: (ctx: GenerateDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.generatorOption`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGeneratorOption?: (ctx: GeneratorOptionContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.genSetField`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGenSetField?: (ctx: GenSetFieldContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.genAssignments`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGenAssignments?: (ctx: GenAssignmentsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.assignmentList`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAssignmentList?: (ctx: AssignmentListContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.assignment`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAssignment?: (ctx: AssignmentContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.valueExpression`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitValueExpression?: (ctx: ValueExpressionContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.genAddToSet`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGenAddToSet?: (ctx: GenAddToSetContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.genRemoveFromSet`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGenRemoveFromSet?: (ctx: GenRemoveFromSetContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.genClearSet`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGenClearSet?: (ctx: GenClearSetContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.genUpdate`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitGenUpdate?: (ctx: GenUpdateContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.jsonKey`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitJsonKey?: (ctx: JsonKeyContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.refField`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitRefField?: (ctx: RefFieldContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.entityQueries`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEntityQueries?: (ctx: EntityQueriesContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.queriesBlock`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitQueriesBlock?: (ctx: QueriesBlockContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.queriesBlockDecorator`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitQueriesBlockDecorator?: (ctx: QueriesBlockDecoratorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.queriesBlockDecorators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitQueriesBlockDecorators?: (ctx: QueriesBlockDecoratorsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.query`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitQuery?: (ctx: QueryContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.queryDecorator`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitQueryDecorator?: (ctx: QueryDecoratorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.queryDecorators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitQueryDecorators?: (ctx: QueryDecoratorsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.temporalDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitTemporalDecor?: (ctx: TemporalDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.provideUserContextDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitProvideUserContextDecor?: (ctx: ProvideUserContextDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.provideGraphQLSchemaDecor`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitProvideGraphQLSchemaDecor?: (ctx: ProvideGraphQLSchemaDecorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.entitySubscriptions`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEntitySubscriptions?: (ctx: EntitySubscriptionsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.subscriptionsBlock`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSubscriptionsBlock?: (ctx: SubscriptionsBlockContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.subscriptionsBlockDecorator`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSubscriptionsBlockDecorator?: (ctx: SubscriptionsBlockDecoratorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.subscriptionsBlockDecorators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSubscriptionsBlockDecorators?: (ctx: SubscriptionsBlockDecoratorsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.subscription`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSubscription?: (ctx: SubscriptionContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.subscriptionDecorator`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSubscriptionDecorator?: (ctx: SubscriptionDecoratorContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.subscriptionDecorators`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitSubscriptionDecorators?: (ctx: SubscriptionDecoratorsContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.dottedId`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDottedId?: (ctx: DottedIdContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.comment`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitComment?: (ctx: CommentContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.beforeComment`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitBeforeComment?: (ctx: BeforeCommentContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.afterComment`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitAfterComment?: (ctx: AfterCommentContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.esslComment`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEsslComment?: (ctx: EsslCommentContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.domBeforeLineComment`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDomBeforeLineComment?: (ctx: DomBeforeLineCommentContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.domBlockComment`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitDomBlockComment?: (ctx: DomBlockCommentContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.esslBlockComment`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEsslBlockComment?: (ctx: EsslBlockCommentContext) => Result;

	/**
	 * Visit a parse tree produced by `ESSL_Parser.esslLineComment`.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	visitEsslLineComment?: (ctx: EsslLineCommentContext) => Result;
}

