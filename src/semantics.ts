import * as vscode from 'vscode';
import * as extension from './extension';
import * as parser from './parser';
import * as completion from './completion/completion';
import { ANTLRInputStream, CommonTokenStream } from 'antlr4ts';
import * as essl_lexer from './grammar/ESSL_Lexer';
import * as essl_listener from './grammar/ESSL_Listener';
import * as essl_parser from './grammar/ESSL_Parser';
import * as reg from './completion/regions';

// #region Create legend for semantics
// List of colors for default dark+ https://github.com/microsoft/vscode/blob/main/extensions/theme-defaults/themes/dark_plus.json
const tokenTypesLegend = [
    'class', 'variable', 'property', 'enumMember', 'function', 'string'
];
const tokenModifiersLegend = [
    'declaration'
];
export const legend = new vscode.SemanticTokensLegend(tokenTypesLegend, tokenModifiersLegend);
// #endregion

// #region Declare variables to be used by the parser
export var builder = new vscode.SemanticTokensBuilder(legend);
export var diagnostics:parser.ESSLDiagnostic[] = [];
export var currentFile:vscode.Uri;
const parseListener:essl_listener.ESSL_Listener = new parser.ParseListener;
const errorParserListener:parser.ErrorParserListener = new parser.ErrorParserListener;
// #endregion

export class EsslSemanticTokensProvider implements vscode.DocumentSemanticTokensProvider 
{
    // This method is called by the vscode client when the user loads a file, edits a file, etc.
    provideDocumentSemanticTokens(document: vscode.TextDocument, token: vscode.CancellationToken): vscode.ProviderResult<vscode.SemanticTokens>
    {
        EsslSemanticTokensProvider.performDiagnostics(document.getText(), document.uri);
        return builder.build();
    }

    public static performDiagnostics(document: string, documentPath:vscode.Uri) : void
    {
        currentFile = documentPath;

        // Build autocompletion hierarchy (declarations)
        completion.buildCompletion();

        // Reset the semantics builder
        builder = new vscode.SemanticTokensBuilder(legend);

        // Parse the doc and provide semantics
        // Also update the list of regions for code completion
        EsslSemanticTokensProvider.updateRegions(document);

        // By this point, the listeners have finished collecting info.
        // Send the client diagnostics, and return the finished builder.
        extension.diagnosticCollection.set(documentPath, diagnostics);
    }

    // Parse the doc and provide semantics
    // Also update the list of regions for code completion
    public static updateRegions(text: string) : void
    {
        // Declare the ESSL Parser
        let inputStream = new ANTLRInputStream(text!);
        let esslLexer = new essl_lexer.ESSL_Lexer(inputStream);
        let tokenStream = new CommonTokenStream(esslLexer);
        let esslParser = new essl_parser.ESSL_Parser(tokenStream);

        // Remove the predefined console listeners
        esslParser.removeParseListeners();
        esslParser.removeErrorListeners();
        esslLexer.removeErrorListeners();

        // Add the listeners to the parser
        // ParseListener is in charge of syntax highlighting, and uses the exported builder variable
        // ErrorListener is in charge of diagnostics, and uses the exported diagnostics variable
        esslLexer.addErrorListener(errorParserListener);
        esslParser.addParseListener(parseListener);
        esslParser.addErrorListener(errorParserListener);

        // Reset the list of diagnostic errors
        diagnostics = [];
        reg.RegionType.regionTypes = [];

        // Parse the ESSL file, starting at top (declared in ESSL_.g4 as the start)
        esslParser.top();
    }
}

