import * as utility from './utility';
import * as vscode from 'vscode';
import * as fileSystem from 'fs';

export function checkSolutionPath(): string
{
    // Try to find .esslsln file
    if (vscode.workspace.workspaceFolders)
    {
        var workspacePath = vscode.workspace.workspaceFolders![0].uri.fsPath;
        var solutionPath:string = utility.getSolutionPath(workspacePath);

        // If the path is "", no .esslsln was found. Warn the user.
        if (solutionPath === "")
        {
            vscode.window.showWarningMessage("Could not find .esslsln file. Without one, commands will not work.");
            return "warning";
        }

        return solutionPath;
    }
    else
    {
        vscode.window.showErrorMessage("Loading essl extension failed. Please open a workspace");
        return "";
    }
}
