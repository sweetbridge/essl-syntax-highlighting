import * as vscode from 'vscode';
import * as utility from './utility';
import * as systemPath from 'path';
import * as fileSystem from 'fs';

/*
Local compile command.
This displays only on .essl and .esslproj files in the title headbar.
Compiles the service that the open file is from and that service's dependencies.
*/
export function compile(uri:vscode.Uri)
{
    var path:string = uri.fsPath;
    utility.runSaveAll();

    // Search for the absolute path to the .esslsln file
    var solutionPath:string = utility.getSolutionPathQuoted(path);

    // If the path is "", no .esslsln was found.
    if (solutionPath === "")
    {
        vscode.window.showErrorMessage("Could not find .esslsln file");
        return;
    }

    // Get the current service name from the .esslproj file
    // Necessary because the directory's name is sometimes different than the serviceName.
    var serviceName:string = utility.getServiceName(path, true);

    // If the name is "", no .esslproj file was found
    if (serviceName === "")
    {
        vscode.window.showErrorMessage("Could not find the .esslsproj file");
        return;
    }

    // Open the terminal and run the command
    let terminal:vscode.Terminal = utility.enableTerminal("Compile");
    terminal.sendText("essl.generator compile " + solutionPath + " -s " + serviceName + utility.getReadableColor());

}

/*
Service compile command.
Shows a dropdown of which services to compile.
Will compile those services and their dependencies.
*/
export async function compileChoose(uri: vscode.Uri) {
	var path:string = uri.fsPath;
    utility.runSaveAll();

    // Search for the absolute path to the .esslsln file
    var solutionPath:string = utility.getSolutionPathQuoted(path);

    // If the path is "", no .esslsln was found.
    if (solutionPath === "")
    {
        vscode.window.showErrorMessage("Could not find .esslsln file");
        return;
    }

    // Show a dropdown for the user to choose services to compile
    var selectedFoldersArray:string[] = await utility.genServicePicker(path);

    // If the selected folders array is empty, none were chosen.
    // This can also be empty if no .esslproj files were found.
    // There should not be a message because if none were chosen,
    // it is self explanatory and if an error occurred, a warning was
    // already shown.
    if (selectedFoldersArray.length === 0)
    {
        return;
    }

    // Convert the array of selected folders into a string to be concat'd
    // into the terminal command
    var selectedFoldersString:string = selectedFoldersArray.join(" ");

    // Open the terminal and run the command
    let terminal:vscode.Terminal = utility.enableTerminal("Compile");
    terminal.sendText("essl.generator compile " + solutionPath + " -s " + selectedFoldersString + utility.getReadableColor());
}

/*
Compare services command
Compares all .essl files locally to the project files
*/
export function compare(uri: vscode.Uri)
{
	var path:string = uri.fsPath;
    utility.runSaveAll();

    // Search for the absolute path to the .esslsln file
    var solutionPath:string = utility.getSolutionPathQuoted(path);

    // If the path is "", no .esslsln was found.
    if (solutionPath === "")
    {
        vscode.window.showErrorMessage("Could not find .esslsln file");
        return;
    }

    // Open the terminal and run the command
    let terminal:vscode.Terminal = utility.enableTerminal("Compare");
    terminal.sendText("essl.generator compare " + solutionPath + utility.getReadableColor());
}

/*
Generate plant uml files command
Shows a dropdown of which services to generate for.
*/
export async function generatePlantuml(uri: vscode.Uri)
{
    var path:string = uri.fsPath;
    utility.runSaveAll();

    // Search for the absolute path to the .esslsln file
    var solutionPath:string = utility.getSolutionPathQuoted(path);

    // If the path is "", no .esslsln was found.
    if (solutionPath === "")
    {
        vscode.window.showErrorMessage("Could not find .esslsln file");
        return;
    }

    // Show a dropdown for the user to choose services to generate
    var selectedFoldersArray:string[] = await utility.genServicePicker(path);

    // If the selected folders array is empty, none were chosen.
    // This can also be empty if no .esslproj files were found.
    // There should not be a message because if none were chosen,
    // it is self explanatory and if an error occurred, a warning was
    // already shown.
    if (selectedFoldersArray.length === 0)
    {
        return;
    }

    // Convert the array of selected folders into a string to be concat'd
    // into the terminal command
    var selectedFoldersString:string = selectedFoldersArray.join(" ");

    // Open the terminal and run the command
    let terminal:vscode.Terminal = utility.enableTerminal("Generate");
    terminal.sendText("essl.generator genPlantuml " + solutionPath + " -s " + selectedFoldersString + utility.getReadableColor());
}

/*
Generate services command
Shows a dropdown of which services to generate.
*/
export async function generate(uri: vscode.Uri)
{
    var path:string = uri.fsPath;
    utility.runSaveAll();

    // Search for the absolute path to the .esslsln file
    var solutionPath:string = utility.getSolutionPathQuoted(path);

    // If the path is "", no .esslsln was found.
    if (solutionPath === "")
    {
        vscode.window.showErrorMessage("Could not find .esslsln file");
        return;
    }

    // Show a dropdown for the user to choose services to generate
    var selectedFoldersArray:string[] = await utility.genServicePicker(path);

    // If the selected folders array is empty, none were chosen.
    // This can also be empty if no .esslproj files were found.
    // There should not be a message because if none were chosen,
    // it is self explanatory and if an error occurred, a warning was
    // already shown.
    if (selectedFoldersArray.length === 0)
    {
        return;
    }

    // Convert the array of selected folders into a string to be concat'd
    // into the terminal command
    var selectedFoldersString:string = selectedFoldersArray.join(" ");

    // Open the terminal and run the command
    let terminal:vscode.Terminal = utility.enableTerminal("Generate");
    terminal.sendText("essl.generator genService " + solutionPath + " -s " + selectedFoldersString + utility.getReadableColor());
}

/*
Command to copy the .essl files from local to project directory
Project path is hardcoded in .esslsln file
*/
export async function copyToProject(uri: vscode.Uri)
{
    var path:string = uri.fsPath;
    utility.runSaveAll();

    // Search for the absolute path to the .esslsln file
    var solutionPath:string = utility.getSolutionPathQuoted(path);

    // If the path is "", no .esslsln was found.
    if (solutionPath === "")
    {
        vscode.window.showErrorMessage("Could not find .esslsln file");
        return;
    }

    // Show a dropdown for the user to choose services to copy
    var selectedFoldersArray:string[] = await utility.genServicePicker(path);

    // If the selected folders array is empty, none were chosen.
    // This can also be empty if no .esslproj files were found.
    // There should not be a message because if none were chosen,
    // it is self explanatory and if an error occurred, a warning was
    // already shown.
    if (selectedFoldersArray.length === 0)
    {
        return;
    }

    // Convert the array of selected folders into a string to be concat'd
    // into the terminal command
    var selectedFoldersString:string = selectedFoldersArray.join(" ");

    // Open the terminal and run the command
    let terminal:vscode.Terminal = utility.enableTerminal("Copy");
    terminal.sendText("essl.generator copy " + solutionPath + " -d ToProject -s " + selectedFoldersString + utility.getReadableColor());
}

/*
Command to copy the .essl files from project directory to local
Project path is hardcoded in .esslsln file
*/
export async function copyFromProject(uri: vscode.Uri)
{
    var path:string = uri.fsPath;
    utility.runSaveAll();

    // Search for the absolute path to the .esslsln file
    var solutionPath:string = utility.getSolutionPathQuoted(path);

    // If the path is "", no .esslsln was found.
    if (solutionPath === "")
    {
        vscode.window.showErrorMessage("Could not find .esslsln file");
        return;
    }

    // Show a dropdown for the user to choose services to copy
    var selectedFoldersArray:string[] = await utility.genServicePicker(path);

    // If the selected folders array is empty, none were chosen.
    // This can also be empty if no .esslproj files were found.
    // There should not be a message because if none were chosen,
    // it is self explanatory and if an error occurred, a warning was
    // already shown.
    if (selectedFoldersArray.length === 0)
    {
        return;
    }

    // Convert the array of selected folders into a string to be concat'd
    // into the terminal command
    var selectedFoldersString:string = selectedFoldersArray.join(" ");

    // Open the terminal and run the command
    let terminal:vscode.Terminal = utility.enableTerminal("Copy");
    terminal.sendText("essl.generator copy " + solutionPath + " -d FromProject -s " + selectedFoldersString + utility.getReadableColor());
}

export function parse(uri: vscode.Uri)
{
    utility.runSaveAll();

    
}

