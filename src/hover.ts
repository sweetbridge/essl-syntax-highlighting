import * as vscode from 'vscode';
import { declarations } from './completion/completion';
import { Declaration } from './completion/declaration';

export class EsslHoverProvider implements vscode.HoverProvider
{
    provideHover(document: vscode.TextDocument, position: vscode.Position, token: vscode.CancellationToken): vscode.ProviderResult<vscode.Hover>
    {
        // Get the config for whether to provide hover dialog
        const config = vscode.workspace.getConfiguration('ESSL');
        var enable:(boolean|undefined) = config.get("EnableHoveringPreview");
        if (!enable)
        {
            return;
        }

        // Adjust the given position to be the start of the identifier.
        // If undefined, user is not hovering on an identifier, so return.
        var adjustedChar:number | undefined = getPositionOfIdentifierStart(document, position);
        if (adjustedChar === undefined)
        {
            return;
        }

        // Declare the new position of the hovered location
        var adjustedPosition:vscode.Position = new vscode.Position(position.line, adjustedChar!);

        // Loop through all predefined declarations to find the corresponding one
        for (var i=0; i<declarations.length; i++)
        {
            var declaration:Declaration = declarations[i];
            if (declaration.usages.length > 0)
            {
                for (var j=0; j<declaration.usages.length; j++)
                {
                    var usage = declaration.usages[j];
                    if (usage.fileName === document.uri.fsPath && usage.position.isEqual(adjustedPosition))
                    {
                        // Declare the text to be shown in a tooltip above the cursor
                        var mkString:vscode.MarkdownString = new vscode.MarkdownString
                        (
                            `**${declaration.name}** *${declaration.projectName}/${declaration.fileName}*\n\n${declaration.documentation}`
                        );
                        return new vscode.Hover(mkString);
                    }
                }
            }
            
        }

        return undefined;
    }
}

// Adjust the given position to be the start of the identifier.
// Return undefined if the index is not an identifier.
export function getPositionOfIdentifierStart(document: vscode.TextDocument, position:vscode.Position) : number | undefined
{
    var line = document.getText().split("\n")[position.line];

    var index = position.character;

    while (index >= 0)
    {
        if (!line.charAt(index).match(/[a-zA-Z0-9_]/))
        {
            return index === position.character ? undefined : index+1;
        }

        index-=1;
    }

    return undefined;
}