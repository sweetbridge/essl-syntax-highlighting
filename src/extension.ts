import * as vscode from 'vscode';
import * as commands from './commands';
import * as semantics from './semantics';
import * as completion from './completion/completion';
import * as hover from './hover';
import * as definition from './definition';
import * as fileSystem from 'fs';
import * as reference from './reference';
import * as artifacts from './artifacts';
import * as checker from './checker';

// Publicly visible list of diagnostics for the client to use
// This provides the user with error signals, red squigglies, etc.
export var diagnosticCollection: vscode.DiagnosticCollection;

// this method is called when your extension is deactivated
export function deactivate() {}

export function activate(context: vscode.ExtensionContext)
{
	// #region Check configuration of the workspace for errors.

	// If problems are found, quit the extension activation.
	var solutionPath:string = checker.checkSolutionPath();
	if (!solutionPath)
	{
		return;
	}
	// #endregion

	// #region Register commands for the extension
	context.subscriptions.push(vscode.commands.registerCommand('essl-extension.compile', (path:vscode.Uri) => {
		commands.compile(path);
	}));

	context.subscriptions.push(vscode.commands.registerCommand('essl-extension.compile-choose', async (path:vscode.Uri) => {
		await commands.compileChoose(path);
	}));

	context.subscriptions.push(vscode.commands.registerCommand('essl-extension.compare', (path:vscode.Uri) => {
		commands.compare(path);
	}));

	context.subscriptions.push(vscode.commands.registerCommand('essl-extension.generate-plantuml', async (path:vscode.Uri) => {
		await commands.generatePlantuml(path);
	}));

	context.subscriptions.push(vscode.commands.registerCommand('essl-extension.generate', async (path:vscode.Uri) => {
		await commands.generate(path);
	}));

	context.subscriptions.push(vscode.commands.registerCommand('essl-extension.generate-puml-image-png', async (path:vscode.Uri) => {
		await artifacts.generatePlantUMLImages(path, "png");
	}));

	context.subscriptions.push(vscode.commands.registerCommand('essl-extension.generate-puml-image-svg', async (path:vscode.Uri) => {
		await artifacts.generatePlantUMLImages(path, "svg");
	}));

	context.subscriptions.push(vscode.commands.registerCommand('essl-extension.generate-puml-image-pdf', async (path:vscode.Uri) => {
		await artifacts.generatePlantUMLImages(path, "pdf");
	}));

	context.subscriptions.push(vscode.commands.registerCommand('essl-extension.generate-csharp', async (path:vscode.Uri) => {
		await artifacts.generateTypes(path, "CSharp");
	}));

	context.subscriptions.push(vscode.commands.registerCommand('essl-extension.generate-csharpnewtonsoftjson', async (path:vscode.Uri) => {
		await artifacts.generateTypes(path, "CSharpNewtonSoftJson");
	}));

	context.subscriptions.push(vscode.commands.registerCommand('essl-extension.generate-kotlin', async (path:vscode.Uri) => {
		await artifacts.generateTypes(path, "Kotlin");
	}));

	context.subscriptions.push(vscode.commands.registerCommand('essl-extension.generate-ims-datascape', async (path:vscode.Uri) => {
		await artifacts.generateTypes(path, "IMSDataScapeTemplate");
	}));

	context.subscriptions.push(vscode.commands.registerCommand('essl-extension.copy-to-project', async (path:vscode.Uri) => {
		await commands.copyToProject(path);
	}));

	context.subscriptions.push(vscode.commands.registerCommand('essl-extension.copy-from-project', async (path:vscode.Uri) => {
		await commands.copyFromProject(path);
	}));

	context.subscriptions.push(vscode.commands.registerCommand('essl-extension.back', (path:vscode.Uri) => {
		vscode.commands.executeCommand("workbench.action.navigateBack");
	}));

	context.subscriptions.push(vscode.commands.registerCommand('essl-extension.forward', (path:vscode.Uri) => {
		vscode.commands.executeCommand("workbench.action.navigateForward");
	}));
	// #endregion

	// #region Register document semantics
	const selector = {language: 'essl', scheme: 'file'};
	vscode.languages.registerDocumentSemanticTokensProvider(selector, new semantics.EsslSemanticTokensProvider(), semantics.legend);
	// #endregion

	// #region Register diagnostics
	diagnosticCollection = vscode.languages.createDiagnosticCollection('essl');
	context.subscriptions.push(diagnosticCollection);
	// #endregion

	// #region Register code completion
	// Include characters at the end that represent needed code completion
	var completionProvider = vscode.languages.registerCompletionItemProvider(selector, new completion.EsslCompletionItemProvider(), '.', '\"', '@');
	context.subscriptions.push(completionProvider);
	//#endregion

	// #region Register code hovering
	var hoverProvider = vscode.languages.registerHoverProvider(selector, new hover.EsslHoverProvider());
	context.subscriptions.push(hoverProvider);
	// #endregion

	// #region Register definition provider (go to definition)
	var definitionProvider = vscode.languages.registerDefinitionProvider(selector, new definition.EsslDefinitionProvider());
	context.subscriptions.push(definitionProvider);
	// #endregion

	// #region Register references provider (find usages)
	var referenceProvider = vscode.languages.registerReferenceProvider(selector, new reference.EsslReferenceProvider());
	context.subscriptions.push(referenceProvider);
	//#endregion

	// #region Perform diagnostics on all .essl files in the open workspace
	var openWorkspaceFolders = vscode.workspace.workspaceFolders;
	if (openWorkspaceFolders)
	{
		var workspace:vscode.WorkspaceFolder = openWorkspaceFolders[0];
		var files:string[] = completion.getEsslFiles(workspace.uri.fsPath);
		for (var i=0; i<files.length; i++)
		{
            var content:string = fileSystem.readFileSync(files[i], "utf8");
			var uri:vscode.Uri = vscode.Uri.file(files[i]);
			semantics.EsslSemanticTokensProvider.performDiagnostics(content, uri);
		}
	}
	// #endregion

	// Get the config for whether to display success dialog
	const config = vscode.workspace.getConfiguration('ESSL');
	var enable:(boolean|undefined) = config.get("EnableLoadSuccessfulDialog");
	if (enable)
	{
		vscode.window.showInformationMessage("Essl extension activation was successful.");
	}
}