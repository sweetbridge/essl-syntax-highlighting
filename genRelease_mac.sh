cd ~
cd .vscode/extensions/essl-syntax-highlighting
rm essl.syntax-1.5.0.zip
rm -r -f release
mkdir release
cd release
cp ../README.md .
cp ../package.json .
cp ../tsconfig.json .
cp ../package-lock.json .
cp ../CHANGELOG.md .
cp ../.eslintrc.json .
cp -R ../out ./out
cp -R ../src ./src
cp -R ../images ./images
cp -R ../syntaxes ./syntaxes
cp -R ../node_modules ./node_modules

# Zip the freshly created release into a zip in the essl-syntax-highlighting folder
zip -r ../essl.syntax-1.5.0.zip .

# Open Finder then copy to https://drive.google.com/drive/u/1/folders/1CmJKIP1kAPnjCwOrFEzO7jJSAUrb0B1N
open ./