# Modifying ESSL_.g4 and regenerating TS files

In order to apply updated ESSL grammar and get TypeScript real-time parsing, perform these steps:

1. Copy the ESSL_.g4 file from Protocols.ESSL/ESSL.Parser/ESSL_.g4 to syntaxes/grammar/ESSL_.g4
   * Note that the files in the .antlr folder automatically regenerated.  These should be removed.
2. Open package.json.
   * Scroll to the bottom, and find the "scripts" section.
   * Right click on the key "antl4ts" and select "Run Script".
     * They generate as siblings to ESSL_.g4 and should appear as "new" files to source code control.
     * __Move__ the files to src/grammar, replacing the 8 files that were previously there.
3. Test and commit the changes.
4. To release, follow the instructions in GenRelease.cmd

5. If for some reason the essl extension does not function properly, try opening the extension folder directly
   in vscode and use it in the testing environment.
   * Click on run and debug in the vscode sidebar of this project, and then click run extension (green hollow arrow at the top).
   * It should open a new VS Code window requesting you to open a project.  Open your ESSL project.
   * At this point, autocompletion (new decorators for instance) should work, but it may still report them as compilation errors.
   * In the ESSL project, use Command Palette to do a "Developer: ReLoad Window".  Hopefully the dynamic compilation errors go away.
6. After the testing environment window opens, open the .essl file you were testing with in the testing window.
