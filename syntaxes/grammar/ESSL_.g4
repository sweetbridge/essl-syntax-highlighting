grammar ESSL_;

// Top level rule
//top: headerDesc=comment entity* EOF;
top: (entity | virtualEntity) EOF;

/*-----------
entity
-----------*/

entity: beforeComment 'entity' entityName entityBlock;
entityName: IDENTIFIER;
entityBlock: esslComment '{' namespace dependsOn* service? entityType (enumDecl | dictionary | valueType | interfaceType)* entityEvents entityCommands? entityQueries? entitySubscriptions? '}';

virtualEntity: beforeComment 'virtual' 'entity' entityName extendsEntity virtualEntityBlock;
extendsEntity: ( ':' baseEntityName )?;
baseEntityName : IDENTIFIER;
virtualEntityBlock: '{' namespace dependsOn* service? (enumDecl | dictionary | valueType | interfaceType)* entityCommands? entityQueries? entitySubscriptions? '}';

namespace: esslComment 'namespace' namespaceName;
namespaceName: dottedId;

dependsOn: esslComment 'dependsOn' dependsOnNameSpace;
dependsOnNameSpace: dottedId;

service: 'service' '{' '}' serviceDecors;
serviceDecors: (serviceDecor esslComment)*;
serviceDecor: '@'
    ( allApiAuthPolicy          // Auth Policy(S) which should be applied to all APIs in the service and dependent services
    | allCommandAuthPolicy      // Auth Policy(s) which should be applied to all Command APIs in the service and dependent services
    | allQueryAuthPolicy        // Auth Policy(s) which should be applied to all Query APIs in the service and dependent services
    | allSubscriptionAuthPolicy // Auth Policy(s) which should be applied to all Subscription APIs in the service and dependent services
    | allAdminAuthPolicy        // Auth Policy(s) which should be applied to all Administrative APIs in the service and dependent services
    | ecosystemPoliciesMethod   // The fully qualified name of a method which adds authentication policies shared by all members of the ecosystem
    | ecosystemSecretsMethod    // The fully qualified name of a method which populates a secrets dictionary with secrets shared by all members of the ecosystem 
    | umlGroupStrReplDecor      // Allows customization of generated PlantUml files by replacing substring(s) within the generated file
    | umlImageDecor             // Marks a PlantUml image to be generated, its format (either .png or .svg) and optionally a subfolder location
    );
    
allApiAuthPolicy: 'allApiAuthPolicy' authPolicyNames;
allCommandAuthPolicy: 'allCommandAuthPolicy' authPolicyNames;
allQueryAuthPolicy: 'allQueryAuthPolicy' authPolicyNames;
allSubscriptionAuthPolicy: 'allSubscriptionAuthPolicy' authPolicyNames;
allAdminAuthPolicy: 'allAdminAuthPolicy' authPolicyNames;
authPolicyNames: '(' authPolicyName (',' authPolicyName)* ')';
ecosystemPoliciesMethod: 'ecosystemPoliciesMethod' '(' ecosystemPoliciesMethodName ')';
ecosystemPoliciesMethodName: dottedId;
ecosystemSecretsMethod: 'ecosystemSecretsMethod' '(' ecosystemSecretsMethodName ')';
ecosystemSecretsMethodName: dottedId;
umlGroupStrReplDecor: 'umlGroupStrRepl' umlGroupStrReplParams;
umlGroupStrReplParams: '(' umlGroupTitleStrRepl ',' umlStrReplOldValue ',' umlStrReplNewValue ')';
umlGroupTitleStrRepl: IDENTIFIER;
umlStrReplOldValue: QUOTED_STR;
umlStrReplNewValue: QUOTED_STR;
umlImageDecor: 'umlImage' umlImageParams;
umlImageParams: '(' umlGroupImageName (',' umlImageFormat)? (',' umlImageRemoveTitle)? (',' umlImageSubFolder)? ')';
umlGroupImageName: IDENTIFIER;
umlImageFormat: ('svg'|'png');
umlImageRemoveTitle: boolValue;
umlImageSubFolder: QUOTED_STR;

/*-----------
Types, ValueTypes, InterfaceTypes
-----------*/

entityType: beforeComment 'type' extendsList typeBlock entityTypeDecors;
valueType: beforeComment 'type' name extendsList typeBlock valueTypeDecors;
interfaceType: beforeComment 'interface' name extendsList possiblyEmptyTypeBlock interfaceTypeDecors;
name: IDENTIFIER;
extendsList: ( ':' baseName ( ',' baseName )* )?;
baseName: IDENTIFIER;
typeBlock: '{' fieldDef+ afterComment '}';
possiblyEmptyTypeBlock: '{' fieldDef* afterComment '}';
// TODO:  Type decorators, either cached or projected to tell the store to keep the current version at the end of the item.

// entity Type decorators
entityTypeDecors: (entityTypeDecor esslComment)*;
entityTypeDecor: '@'
    ( partial                     // Allow for manual type extensions
    | partialCommand              // Allow for manual commandType extensions
    | transCoordinator            // The type is used to orchestrate saga-pattern transactions
    | defaultConstructor          // Include a public default constructor
    | indexTypeDecor              // non-unique index definition on one or more fields which should be indexed for quick retrieval
    | uniqueIndexTypeDecor        // unique index definition on one or more fields which should be indexed for quick retrieval
    | queryPartitionDecor         // Generates additional subset lookups based on a partitioning index and its keys
    | crossPartitionDecor         // Type specifically designed to have instances shared across all partitions
    | umlGroupDecor               // Indicates what UmlGroup diagrams the entity should be part of
    | webValueString              // Specifies the name of a method which is used to format the type as a readable string
    | hashLookupDecor             // Indicates that all entity events should include lookups by hash
    | beforeEventPersist          // Entity wants to participate in a before persistence call to allow standardized to the persisted body, such as for user hash chains
    | authPolicyDecor             // Allows one or more authorization policies to be applied to this type
    | styleDecor                  // Indicates persistence should be one of "Events" (the default), "SlowlyChanging", "Ledger", and "FactLedger"
    | discriminatedBy             // Indicates the value used by the type discriminator which references this type
    | pluralNameDecor             // Specifies how the entity name should be pluralized
    | tableNameDecor              // Allows a specific base table name to be set, rather than the just the plural version of the entity name.
    | graphQLNameDecor            // Enables GraphQL command and queries and type to be generated with a different name than the actual entity name.
    | graphQLPluralNameDecor      // Enables GraphQL command and queries and type to be generated with a different plural name than the actual entity name.
    | graphQLCamelCaseDecor       // Enables GraphQL command and queries and type to be generated with a different camelCase name than the actual entity name.
    | graphQLPluralCamelCaseDecor // Enables GraphQL command and queries and type to be generated with a different plural camelCase name than the actual entity name.
    | base64RefIdsDecor           // Specifies that entity references (Ids) should be stored in Base64 form rather than as UUIDs 
    | interfaceDecor              // Indicates that a type-equivalent interface with the specified name should be automatically generated
    | camelCaseDecor              // Allows the specification of the name in camelCase.  Used when simply lowercasing first character is undesirable
    | generateIfDecor             // Template selected for generation when it matches the identifier
    | attributeDecor              // Allows for the addition of arbitrary name/value pair attributes that can be accessed from the DOM
    );

partialCommand: 'partialCommand';
transCoordinator: 'transCoordinator';
defaultConstructor: 'defCtor';
indexTypeDecor: 'index' indexTypeAttrs;
uniqueIndexTypeDecor: 'uniqueIndex' indexTypeAttrs;
indexTypeAttrs: '(' name (',' indexedField ('(' indexKeyAttrs ')')? )+ (indexAttrDecors)? ')';
indexKeyAttrs: ( indexKeyAttr ( ',' indexKeyAttr )? );
indexKeyAttr: ( caseInsensitive | nullHandling | dimensionNameDecor | dimensionKeyDecor );
indexedField: IDENTIFIER;
queryPartitionDecor: 'queryPartition' queryPartitionAttrs;
queryPartitionAttrs: '(' partitionIndex ',' partitionTypeUniqueIndex ')';
partitionIndex: IDENTIFIER;
partitionTypeUniqueIndex: IDENTIFIER;
crossPartitionDecor: 'crossPartition';
umlGroupDecor: 'umlGroup' umlGroup;
umlGroup: '(' umlGroupTitle (',' umlSubGroup)? ')';
umlGroupTitle: IDENTIFIER;
umlSubGroup: IDENTIFIER;
hashLookupDecor: 'hashLookup';
beforeEventPersist: 'beforeEventPersist';
authPolicyName: IDENTIFIER;
authPolicyDecor: 'authPolicy' authPolicyNames;
publicDecor: 'public';
pluralNameDecor: 'pluralName' '(' pluralName ')';
pluralName: IDENTIFIER;
tableNameDecor: 'tableName' '(' tableName ')';
tableName: dottedId;
graphQLNameDecor: 'graphQLName' '(' graphQLName ')';
graphQLName: IDENTIFIER;
graphQLPluralNameDecor: 'graphQLPluralName' '(' graphQLPluralName ')';
graphQLPluralName: IDENTIFIER;
graphQLCamelCaseDecor: 'graphQLCamelCase' '(' graphQLCamelCase ')';
graphQLCamelCase: IDENTIFIER;
graphQLPluralCamelCaseDecor: 'graphQLPluralCamelCase' '(' graphQLPluralCamelCase ')';
graphQLPluralCamelCase: IDENTIFIER;
base64RefIdsDecor: 'base64RefIds';
interfaceDecor: 'interface' '(' interfaceName extendsList (',' umlGroup)? ')';
interfaceName: IDENTIFIER;
camelCaseDecor: 'camelCase' '(' camelCaseName ')';
camelCaseName: CAMELCASE_IDENTIFIER;
generateIfDecor: 'generateIf' '(' generateIfName ')';
generateIfName: IDENTIFIER | CAMELCASE_IDENTIFIER;
dimensionNameDecor: 'factDimension'; 

styleDecor: 'style' '(' (events | slowlyChanging | ledger | factLedger) ')';
events: 'events';                   // Persistence should be optimized as an EventStore (sequence of events).  This is the default behavior although slowlyChanging or ledger are typically desired.
slowlyChanging: 'slowlyChanging';   // Allows persistence logic to make assumptions on best way to implement updates/lookups (such as index projections)
ledger: 'ledger';                   // Persistence should be optimized as a ledger
factLedger: 'factLedger';           // Persistence should support fact-ledgers

// index attribute decorators
indexAttrDecors: ',' (indexAttrDecor)*;
indexAttrDecor: '@'
    ( cacheModel            // Current model should be cached in index (for Slowly Changing objects)
    | noCacheModel          // Current model should not be cached in index (for Slowly Changing object)
    | allowTemporal         // Temporal updates are allowed to the index (explicitly declared to be safe) 
    | indexNameDecor        // Allows a specific database index name to be set, rather than the just the declared index name.  
    );
    
cacheModel: 'cacheModel';
noCacheModel: 'noCacheModel';
allowTemporal: 'allowTemporal';
indexNameDecor: 'indexName' '(' indexName ')';
indexName: dottedId;

// ValueType decorators
valueTypeDecors: (valueTypeDecor esslComment)*;
valueTypeDecor: '@'
    ( partial            // Allow for manual type extensions
    | defaultConstructor // Include a public default constructor
    | jsonConstructor    // Generate a private default constructor for use when deserializing
    | webValueString     // Specifies the name of a method which is used to format the type as a readable string
    | umlGroupDecor      // Indicates what UmlGroup diagrams the value type should be part of
    | suppressDecor      // Indicates that certain aspects of generated code should be suppressed, typically replaced by hand-coded versions
    | authPolicyDecor    // Allows one or more authorization policies to be applied to this type
    | discriminatedBy    // Indicates the value used by the type discriminator which references this type
    | base64RefIdsDecor  // Specifies that entity references (Ids) should be stored in Base64 form rather than as UUIDs
    | outputType         // Indicates the type is only used for returning results and that corresponding input types are not necessary 
    | interfaceDecor     // Indicates that a type-equivalent interface with the specified name should be automatically generated
    | camelCaseDecor     // Allows the specification of the name in camelCase.  Used when simply lowercasing first character is undesirable
    | generateIfDecor    // Template selected for generation when it matches the identifier
    | attributeDecor     // Allows for the addition of arbitrary name/value pair attributes that can be accessed from the DOM
    | factDimensionDecor // Indicates the structure holds data for a fact dimension
    | voidPrimaryFact    // Indicates that while the type contains fact dimension entries, it purposely does not contain a primary fact entry
    );

partial: 'partial';
jsonConstructor: 'jCtor';
webValueString: 'webString' '(' webValueMethod ')';
webValueMethod: IDENTIFIER;
discriminatedBy: 'discriminatedBy' '(' discriminatedByValue ')';
discriminatedByValue: dottedId;
outputType: 'outputType';
voidPrimaryFact: 'voidPrimaryFactDimension';

// InterfaceType Decorators
interfaceTypeDecors: (interfaceTypeDecor esslComment)*;
interfaceTypeDecor: '@'
    ( partial            // Allow for manual type extensions
    | internalDecor      // The interface is internal and should not be exposed externally
    | umlGroupDecor      // Indicates what UmlGroup diagrams the value type should be part of
    );

/*-----------
Enums
-----------*/

enumDecl: beforeComment 'enum' name enumBlock enumDecors;
enumBlock: '{' enumItem (','? enumItem)* '}'; 
enumItem: beforeComment enumValue ('=' enumExplicitValue)? comment; 
enumValue: IDENTIFIER;
enumExplicitValue: integer;

enumDecors: enumDecor*;
enumDecor: '@'
    ( umlGroupDecor      // Indicates what UmlGroup diagrams the enum should be part of
    | suppressDecor      // Indicates that certain aspects of generated code should be suppressed, typically replaced by hand-coded versions
    | flagsDecor         // Indicates that enum values are treated as bit flags (and likely explicitly set to powers of 2)
    );

suppressDecor: 'suppress' '(' suppressOption (','? suppressOption)* ')';
suppressOption: ( modelsuppressOption | graphQlsuppressOption | inputTypeOption );
modelsuppressOption:   'model';
graphQlsuppressOption: 'graphQl';
inputTypeOption: 'inputType';
flagsDecor: 'flags';

/*-----------
Dictionary
-----------*/

dictionary: beforeComment 'dictionary' name dictionaryBlock dictionaryDecors;
dictionaryBlock: '{' (caseInsensitive | dictPartitioned | dictValueTypeDecl | dictDefaultDecl)* (dictEntries)? '}';
dictPartitioned: 'partitioned';
dictValueTypeDecl: 'valueType' '(' dictValueType ')';
dictValueType: typeRef;
dictDefaultDecl: 'default' '(' dictDefault ')';
dictDefault: IDENTIFIER | CAMELCASE_IDENTIFIER;
dictEntries: DICTENTRIES;
DICTENTRIES: '<Entries>' .*? '</Entries>';

dictionaryDecors: dictionaryDecor*;
dictionaryDecor: '@'
    ( umlGroupDecor      // Indicates what UmlGroup diagrams the dictionary should be part of
    );
    
/*-----------
Type Fields
-----------*/

fieldDef: beforeComment typeRef name fieldInitializer fieldDecorators comment;
typeRef: typeRefName (listIndicator)? (nullableIndicator)?;
typeRefName: (primitiveType | valueTypeRef);
primitiveType: 'bool' | 'date' | 'datetime' | 'decimal' | 'float' | 'int' | 'string' | 'uuid';
valueTypeRef: IDENTIFIER;
listIndicator: '[' ']';
nullableIndicator: '?';

fieldInitializer: ('=' ( nullValue | initialEnumVal | boolValue | stringValue | numValue | newInstance | flagsEnumSet ))?;
nullValue: 'null';
boolValue: ('true'|'false');
newInstance: 'new';
initialEnumVal: IDENTIFIER;
numValue: (integer | decimal);
integer: '-'? POS_INT;
decimal: '-'? POS_DEC;
stringValue: QUOTED_STR;    // Ideally this should be a more intelligent string literal, but QUOTED_STR does the trick for now.
// TODO:  Add addition field initialization mechanism (string literals and numbers) as needed
flagsEnumSet: '[' (flagsEnumValue (',' flagsEnumValue)* )? ']';
flagsEnumValue: IDENTIFIER;

// field decoratorsReduced confusion by eliminating use ObjectRef base class in favor of strongly named references in conjunction with schema.
fieldDecorators: (fieldDecorator esslComment)*;
fieldDecorator: '@'
    ( indexDecor            // field should be indexed (non-unique) for quick retrieval
    | uniqueIndexDecor      // field should be indexed (unique) to ensure uniqueness of an external key and for quick retrieval
    | inlineEnumDecor       // field is limited to a specific set of values
    | composedDecor         // field is a list which contains the entries as reconstructed through the Apply methods
                            // and does not necessarily contain all entries.  A cached should not hold the complete history.
                            // This is inline with the concept of a EventStore item being a ledger of its own.
    | requiredDecor         // field must have a value (initially for for GraphQL input)                     
    | readOnlyDecor         // field is read only (does not allow GraphQL DTO input)
    | hiddenDecor           // The field is hidden (not available in GraphQL DTOs)
    | calculatedDecor       // field is calculated, which implies non-persisted, and read only (does not allow GraphQL DTO input)
    | constantDecor         // The field is a constant (likely used in conjunction with @notPersisted)
    | notPersistedDecor     // The field is not persisted (and therefore is likely calculated or a constant)
    | immutableDecor        // The field may not be changed once the object is created
    | clonePartitionerDecor // field is the basis of the partition around which cloning is done.
    | cloneIdAsIsDecor      // field contains an Id that should be cloned without updating it.
    | fieldUmlDecor         // uml attributes about the field, such as which direction the relationship arrow should point
    | sectionDecor          // Indicates that this field starts a new logical section, along with a break type indicator
    | asValueTypeDecor      // Indicates the field should be generated as a value type rather than by reference.
    | authPolicyDecor       // Allows one or more authorization policies to be applied to this field
    | idDecor               // The string field should be treated as an Id
    | sameAsDecor           // A identifier (optionally dotted) which indicates the value that this read only calculated field should be the same as.
                            // By default these read-only fields are not persisted, although specifying 'persist' forces the field to be persisted.
    | handCodedDecor        // Indicates that the GraphQL resolver is to be hand coded
    | dictDecor             // Type can be serialized as a Dictionary of Name/Value pairs
    | typeDiscriminator     // Indicates that this field is used as a type discriminator for deserialization
    | multiLineDecor        // Indicates that a string field may be multiline
    | questionDecor         // Indicates the field has a related question and should be discussed.  It has no effect on behavior
    | camelCaseDecor        // Allows the specification of the name in camelCase.  Used when simply lowercasing first character is undesirable
    | labelDecor            // Associates a label with the field, which can be used generated User Interfaces
    | autoFillDecor         // Specifies the field can be auto-filled by a UI in one of the context-dependent methods
    | attributeDecor        // Allows for the addition of arbitrary name/value pair attributes that can be accessed from the DOM
    | factDimensionDecor    // Indicates the field is a structure which holds fact data for a fact dimension
    | dimensionKeyDecor     // Indicates the field acts as the key for a fact dimension
    | journalEntriesDecor   // Indicates the field contains a list of journal entries for a transaction
    );
    
indexDecor: 'index' indexAttrs;
uniqueIndexDecor: 'uniqueIndex' indexAttrs;
indexAttrs: '(' name (',' caseInsensitive)? (',' nullHandling)? (indexAttrDecors)? ')';
caseInsensitive: ('caseSensitive' | 'caseInSensitive');
nullHandling: ('ignoreNulls' | 'includeNulls');
inlineEnumDecor: 'enum' '(' inlineEnumValue (',' inlineEnumValue)* ')';
inlineEnumValue: IDENTIFIER;
composedDecor: 'composed';
requiredDecor: 'req';
readOnlyDecor: 'ro';
hiddenDecor: 'hidden';
calculatedDecor: 'calc';
immutableDecor: 'immutable';
clonePartitionerDecor: 'clonePartitioner';
cloneIdAsIsDecor: 'cloneIdAsIs';
fieldUmlDecor: 'uml' '(' fieldUmlGroupTitle (direction | horizontalVertical | lineLength | hideRelationship | lineLabel)* ')';
fieldUmlGroupTitle: IDENTIFIER;
direction: ( 'left' | 'right' | 'up' | 'down' | 'l' | 'r' | 'u' | 'd' );
horizontalVertical: ( 'horizontal' | 'vertical' | 'h' | 'v' );
lineLength: ( 'v1' | 'v2' | 'v3' | 'v4' | 'v5' | 'v6' | 'v7' | 'v8' | 'v9');
hideRelationship: ( 'x' );
lineLabel: QUOTED_STR;
sectionDecor: 'section' '(' breakType ')';
breakType: ( '..' | '--' | '==' );
asValueTypeDecor: 'asValueType';
sameAsDecor: 'sameAs' '(' dottedId (',' sameAsPersist)? ')';        // No uses found as of 2023-02-06
sameAsPersist: 'persist';                                           // No uses found as of 2023-02-06
constantDecor: 'const';
notPersistedDecor: 'notPersisted';
handCodedDecor: 'handCoded';
dictDecor: 'dictionary';
typeDiscriminator: 'typeDiscriminator';
multiLineDecor: 'multiLine';
questionDecor: 'question';
labelDecor: 'label' '(' label ')';
label: QUOTED_STR;
autoFillDecor: 'autofill' '(' autoFillType ')';
autoFillType: ( 'utcnow' | 'geotag' );
attributeDecor: 'attributes' '(' attributePair (',' attributePair)* ')';
attributePair: attributeKey ':' attributeValue;
attributeKey: QUOTED_STR;
attributeValue: QUOTED_STR;
factDimensionDecor: 'factDimension' '(' (dimPrimary | dimInx) ')';
dimPrimary: 'primary';
dimInx: ( POS_INT );
dimensionKeyDecor: 'dimensionKey';
journalEntriesDecor: 'journalEntries'; 

/*-----------
Events
-----------*/

entityEvents: beforeComment 'events' eventsBlock eventsDecors;
eventsBlock: '{' createdEventDef eventDef* '}';
createdEventDef: beforeComment createdEvent eventNameDecors comment;
createdEvent: IDENTIFIER;                       // Verify that it is value 'Created';
eventDef: beforeComment eventName eventNameDecors comment;
eventName: IDENTIFIER;

eventsDecors: eventsDecor*;
eventsDecor: '@'
    ( umlGroupDecor      // Indicates what UmlGroup diagrams the dictionary should be part of
    );

eventNameDecors: eventNameDecor*;
eventNameDecor: '@'
    ( hashLookupDecor    // Indicates that entries of this event can be looked up by hash
    );


/*-----------
Commands
-----------*/

entityCommands: beforeComment 'commands' commandsBlock;
commandsBlock: '{' command* '}' commandsBlockDecorators;

commandsBlockDecorator: '@'
   (
     authPolicyDecor              // Allows one or more authorization policies to be applied to all commands in this block
   );
commandsBlockDecorators: (commandsBlockDecorator esslComment)*;
                               
command: beforeComment name commandParams commandDecorators commandResultsIn esslComment;
commandParams: '(' ( param (',' param)* )? ')';

param: beforeComment paramDecl paramInitializer (paramDecorators | esslComment);
paramDecl: optionalTypeRef name;
optionalTypeRef: ( typeRefName paramNameModifiers )?;
paramNameModifiers: (listIndicator)? (nullableIndicator)?;
paramInitializer: ('=' ( nullValue | initialEnumVal | boolValue | numValue | stringValue ))?;
commandResultsIn: ( yields | returnsType );
yields: 'yields' eventList;
eventList: eventRef (',' eventRef)*;
eventRef: IDENTIFIER;
returnsType: 'returns' returnTypeRef paramNameModifiers;
returnTypeRef: typeRefName;

paramDecorator: '@'
    ( typeQualifierDecor    // entity and optional associated unique index to use as a key
    | byValueDecor          // The parameter's content contains the value (model) and not a reference (when an entity).
    | idDecor               // The string parameter should be treated as an Id
    );
    
typeQualifierDecor: 'type' '(' typeQualifierName (',' indexQualifier )? (',' filterExpr )? ')';   
    
paramDecorators: (paramDecorator esslComment)*;
typeQualifierName: IDENTIFIER;
indexQualifier: IDENTIFIER;  // The name of a qualifying index in the case of an entity.
filterExpr: FILTEREXPR;     // Allow injection filter expressions
                            // For dictionaries, json field expression (x => x["type"]=="Fiat")
                            // For entities (or any value types), (t => t.Field.Equals("somevalue"))
                            // Combo sort (by default alpha) decorator?
FILTEREXPR: '<FilterExpr>' .*? '</FilterExpr>';
byValueDecor: 'byValue';
idDecor: 'id';

commandDecorator: '@'
    ( modelDecor                // command should auto-validate the model
    | asyncDecor                // Validation and/or BusinessLogic should use async implementations
    | syncDecor                 // Implementation is synchronous (HelperLogic implementations only)
    | transDecor                // command is a transaction particpant
    | continuationDecor         // command is a continuation command
    | effectiveDateDecor        // command supports recording an effective date
    | explicitDecor             // command name should be generated explicitly as given
    | internalDecor             // command should not be exposed externally
    | messageSourceDecor        // command is capable of generating subscription messages
    | createDecor               // command is an alternate form of creating this entity
    | deleteDecor               // command deletes an instance
    | partialErrorDecor         // command can return a data result as well as error information
    | generateDecor             // generate implementation based on options
    | authPolicyDecor           // Allows one or more authorization policies to be applied to this command API
    | publicDecor               // The command should be publicly callable.
    | provideGraphQLSchemaDecor // The GraphQL Schema is provided so that the query may use it to perform GraphQL calls back into itself.
    );

commandDecorators: (commandDecorator esslComment)*;
modelDecor: 'model';
asyncDecor: 'async' '(' asyncSpec ( ',' asyncSpec )? ( ',' asyncSpec )? ')';
asyncSpec: ( asyncParamValidation | asyncModelValidation | asyncBusinessLogic );
asyncParamValidation: 'paramValidation';
asyncModelValidation: 'modelValidation';
asyncBusinessLogic: 'businessLogic';
syncDecor: 'sync';
transDecor: 'trans' '(' transAttr ')';
transAttr: ( 'coordinator' | 'pending' | 'committed' | 'reversed' );
continuationDecor: 'continuation';
effectiveDateDecor: 'eff';
explicitDecor: 'explicit';
internalDecor: 'internal';
messageSourceDecor: 'messageSource';
createDecor: 'create';
deleteDecor: 'del';
partialErrorDecor: 'partialError';
generateDecor: 'gen' '(' generatorOption ')';

generatorOption:  ( genSetField | genAssignments | genAddToSet | genRemoveFromSet | genClearSet | genUpdate );
genSetField:      ('setField' refField) jsonKey?;

genAssignments:   ('assignments' assignmentList);
assignmentList:   assignment (',' assignment)?;
assignment:       refField jsonKey? valueExpression?;
valueExpression:  DELIMITED_EXPRESSION;
DELIMITED_EXPRESSION: '=' WHITESPACE? '{' .*? '}'; 

genAddToSet:      ('addToSet' refField) jsonKey?;
genRemoveFromSet: ('removeFromSet' refField) jsonKey?;
genClearSet:      ('clearSet' refField);
genUpdate:        'update';
jsonKey: QUOTED_STR;
refField: IDENTIFIER;
/*-----------
Queries
-----------*/

entityQueries: beforeComment 'queries' queriesBlock;
queriesBlock: comment '{' query* '}' queriesBlockDecorators;
queriesBlockDecorator: '@'
    (
      authPolicyDecor       // Allows one or more authorization policies to be applied to all queries in this block
    );
queriesBlockDecorators: (queriesBlockDecorator esslComment)*;

query: beforeComment name commandParams queryDecorators returnsType;

queryDecorator: '@'
    ( temporalDecor             // query is temporal (support "AsOf" querying)
    | provideUserContextDecor   // The GraphQL UserContext is provided to the query for it to write to so returned nested types may use it
    | internalDecor             // query should not be exposed externally
    | syncDecor                 // Implementation is synchronous
    | authPolicyDecor           // Allows one or more authorization policies to be applied to this query API
    | publicDecor               // The query should be publicly callable.
    | provideGraphQLSchemaDecor // The GraphQL Schema is provided so that the query may use it to perform GraphQL calls back into itself.
    );

queryDecorators: (queryDecorator esslComment)*;
temporalDecor: 'temporal';
provideUserContextDecor: 'provideUserContext';
provideGraphQLSchemaDecor: 'provideGraphQLSchema';

/*-----------
Subscriptions
-----------*/

entitySubscriptions: beforeComment 'subscriptions' subscriptionsBlock;
subscriptionsBlock: comment '{' subscription* '}' subscriptionsBlockDecorators;
subscriptionsBlockDecorator: '@'
    (
      authPolicyDecor       // Allows one or more authorization policies to be applied to all subscriptions in this block
    );
subscriptionsBlockDecorators: (subscriptionsBlockDecorator esslComment)*;

subscription: beforeComment name commandParams subscriptionDecorators returnsType;

subscriptionDecorator: '@'
    (   // Add new ones as needed (for example generated filtering behavior based on type?)
      authPolicyDecor       // Allows one or more authorization policies to be applied to this subscription
    );

subscriptionDecorators: (subscriptionDecorator esslComment)*;

/*-----------
Grammar Constructs
-----------*/

dottedId: IDENTIFIER ( '.' IDENTIFIER)*;

comment: ( domBlockComment | esslBlockComment | esslLineComment )*;
beforeComment: ( domBeforeLineComment | esslBlockComment | esslLineComment )*;
afterComment: ( domBeforeLineComment | esslBlockComment | esslLineComment )*;
esslComment: ( esslBlockComment | esslLineComment )*;
domBeforeLineComment: BEFORE_COMMENT_LINE;
domBlockComment: COMMENT_BLOCK;
esslBlockComment: ESSL_COMMENT_BLOCK;
esslLineComment: ESSL_COMMENT_LINE+;

BEFORE_COMMENT_LINE: '///' ~[\r\n]* ; 
COMMENT_BLOCK: '/*' .*? '*/';
//COMMENT_LINE: '//' ~[\r\n]* ;
ESSL_COMMENT_BLOCK: '/#' .*? '#/';
ESSL_COMMENT_LINE: '##' ~[\r\n]* ; 
QUOTED_STR: '"' .*? '"';
/*STR_LIT: '"' ~["\r\n]* '"';     // This collides with QUOTED_STR.  Need to look at Lexer rules to figure out how to have more than one which starts with a quote (") */
/*  This is another example I found.  Question is how to keep them from colliding.
StringLiteral :   '"' ( Escape | ~('"' | '\\' | '\n' | '\r') ) + '"';
fragment Escape : '\\' ( '"' | '\\' );
*/

POS_DEC: [0-9]* '.' [0-9]+;
POS_INT: [0-9]+;

IDENTIFIER:  [A-Z][0-9A-Za-z_]*; // Identifiers are a mix of letters and digits and _, but must start with a capital letter
CAMELCASE_IDENTIFIER: [a-z][0-9A-Za-z_]*; // Identifiers are a mix of letters and digits and _, but must start with a capital letter 

WHITESPACE : (' '|'\t')+ -> skip ;
NEWLINE : ('\r'? '\n' | '\r')+ -> skip;
