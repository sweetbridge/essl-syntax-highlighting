cd C:\Users\charl\.vscode\extensions\essl.syntax
rd release/s /q
md release
cd release
copy ..\README.md
copy ..\package.json
copy ..\tsconfig.json
copy ..\package-lock.json
copy ..\CHANGELOG.md
copy ..\.eslintrc.json
robocopy ..\out C:\Users\charl\.vscode\extensions\essl.syntax\release\out\ /e
robocopy ..\src C:\Users\charl\.vscode\extensions\essl.syntax\release\src\ /e
robocopy ..\images C:\Users\charl\.vscode\extensions\essl.syntax\release\images\ /e
robocopy ..\syntaxes C:\Users\charl\.vscode\extensions\essl.syntax\release\syntaxes\ /e
robocopy ..\node_modules C:\Users\charl\.vscode\extensions\essl.syntax\release\node_modules\ /e

cd ..

# zip the folder and rename.
# find C:\Users\charl\.vscode\extensions\essl.syntax\release\ -path '*/.*' -prune -o -type f -print | zip ~/file.zip -@

cmd /k