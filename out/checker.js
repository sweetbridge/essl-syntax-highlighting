"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.checkSolutionPath = void 0;
const utility = require("./utility");
const vscode = require("vscode");
function checkSolutionPath() {
    // Try to find .esslsln file
    if (vscode.workspace.workspaceFolders) {
        var workspacePath = vscode.workspace.workspaceFolders[0].uri.fsPath;
        var solutionPath = utility.getSolutionPath(workspacePath);
        // If the path is "", no .esslsln was found. Warn the user.
        if (solutionPath === "") {
            vscode.window.showWarningMessage("Could not find .esslsln file. Without one, commands will not work.");
            return "warning";
        }
        return solutionPath;
    }
    else {
        vscode.window.showErrorMessage("Loading essl extension failed. Please open a workspace");
        return "";
    }
}
exports.checkSolutionPath = checkSolutionPath;
//# sourceMappingURL=checker.js.map