"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.activate = exports.deactivate = exports.diagnosticCollection = void 0;
const vscode = require("vscode");
const commands = require("./commands");
const semantics = require("./semantics");
const completion = require("./completion/completion");
const hover = require("./hover");
const definition = require("./definition");
const fileSystem = require("fs");
const reference = require("./reference");
const artifacts = require("./artifacts");
const checker = require("./checker");
// this method is called when your extension is deactivated
function deactivate() { }
exports.deactivate = deactivate;
function activate(context) {
    // #region Check configuration of the workspace for errors.
    // If problems are found, quit the extension activation.
    var solutionPath = checker.checkSolutionPath();
    if (!solutionPath) {
        return;
    }
    // #endregion
    // #region Register commands for the extension
    context.subscriptions.push(vscode.commands.registerCommand('essl-extension.compile', (path) => {
        commands.compile(path);
    }));
    context.subscriptions.push(vscode.commands.registerCommand('essl-extension.compile-choose', (path) => __awaiter(this, void 0, void 0, function* () {
        yield commands.compileChoose(path);
    })));
    context.subscriptions.push(vscode.commands.registerCommand('essl-extension.compare', (path) => {
        commands.compare(path);
    }));
    context.subscriptions.push(vscode.commands.registerCommand('essl-extension.generate-plantuml', (path) => __awaiter(this, void 0, void 0, function* () {
        yield commands.generatePlantuml(path);
    })));
    context.subscriptions.push(vscode.commands.registerCommand('essl-extension.generate', (path) => __awaiter(this, void 0, void 0, function* () {
        yield commands.generate(path);
    })));
    context.subscriptions.push(vscode.commands.registerCommand('essl-extension.generate-puml-image-png', (path) => __awaiter(this, void 0, void 0, function* () {
        yield artifacts.generatePlantUMLImages(path, "png");
    })));
    context.subscriptions.push(vscode.commands.registerCommand('essl-extension.generate-puml-image-svg', (path) => __awaiter(this, void 0, void 0, function* () {
        yield artifacts.generatePlantUMLImages(path, "svg");
    })));
    context.subscriptions.push(vscode.commands.registerCommand('essl-extension.generate-puml-image-pdf', (path) => __awaiter(this, void 0, void 0, function* () {
        yield artifacts.generatePlantUMLImages(path, "pdf");
    })));
    context.subscriptions.push(vscode.commands.registerCommand('essl-extension.generate-csharp', (path) => __awaiter(this, void 0, void 0, function* () {
        yield artifacts.generateTypes(path, "CSharp");
    })));
    context.subscriptions.push(vscode.commands.registerCommand('essl-extension.generate-csharpnewtonsoftjson', (path) => __awaiter(this, void 0, void 0, function* () {
        yield artifacts.generateTypes(path, "CSharpNewtonSoftJson");
    })));
    context.subscriptions.push(vscode.commands.registerCommand('essl-extension.generate-kotlin', (path) => __awaiter(this, void 0, void 0, function* () {
        yield artifacts.generateTypes(path, "Kotlin");
    })));
    context.subscriptions.push(vscode.commands.registerCommand('essl-extension.generate-ims-datascape', (path) => __awaiter(this, void 0, void 0, function* () {
        yield artifacts.generateTypes(path, "IMSDataScapeTemplate");
    })));
    context.subscriptions.push(vscode.commands.registerCommand('essl-extension.copy-to-project', (path) => __awaiter(this, void 0, void 0, function* () {
        yield commands.copyToProject(path);
    })));
    context.subscriptions.push(vscode.commands.registerCommand('essl-extension.copy-from-project', (path) => __awaiter(this, void 0, void 0, function* () {
        yield commands.copyFromProject(path);
    })));
    context.subscriptions.push(vscode.commands.registerCommand('essl-extension.back', (path) => {
        vscode.commands.executeCommand("workbench.action.navigateBack");
    }));
    context.subscriptions.push(vscode.commands.registerCommand('essl-extension.forward', (path) => {
        vscode.commands.executeCommand("workbench.action.navigateForward");
    }));
    // #endregion
    // #region Register document semantics
    const selector = { language: 'essl', scheme: 'file' };
    vscode.languages.registerDocumentSemanticTokensProvider(selector, new semantics.EsslSemanticTokensProvider(), semantics.legend);
    // #endregion
    // #region Register diagnostics
    exports.diagnosticCollection = vscode.languages.createDiagnosticCollection('essl');
    context.subscriptions.push(exports.diagnosticCollection);
    // #endregion
    // #region Register code completion
    // Include characters at the end that represent needed code completion
    var completionProvider = vscode.languages.registerCompletionItemProvider(selector, new completion.EsslCompletionItemProvider(), '.', '\"', '@');
    context.subscriptions.push(completionProvider);
    //#endregion
    // #region Register code hovering
    var hoverProvider = vscode.languages.registerHoverProvider(selector, new hover.EsslHoverProvider());
    context.subscriptions.push(hoverProvider);
    // #endregion
    // #region Register definition provider (go to definition)
    var definitionProvider = vscode.languages.registerDefinitionProvider(selector, new definition.EsslDefinitionProvider());
    context.subscriptions.push(definitionProvider);
    // #endregion
    // #region Register references provider (find usages)
    var referenceProvider = vscode.languages.registerReferenceProvider(selector, new reference.EsslReferenceProvider());
    context.subscriptions.push(referenceProvider);
    //#endregion
    // #region Perform diagnostics on all .essl files in the open workspace
    var openWorkspaceFolders = vscode.workspace.workspaceFolders;
    if (openWorkspaceFolders) {
        var workspace = openWorkspaceFolders[0];
        var files = completion.getEsslFiles(workspace.uri.fsPath);
        for (var i = 0; i < files.length; i++) {
            var content = fileSystem.readFileSync(files[i], "utf8");
            var uri = vscode.Uri.file(files[i]);
            semantics.EsslSemanticTokensProvider.performDiagnostics(content, uri);
        }
    }
    // #endregion
    // Get the config for whether to display success dialog
    const config = vscode.workspace.getConfiguration('ESSL');
    var enable = config.get("EnableLoadSuccessfulDialog");
    if (enable) {
        vscode.window.showInformationMessage("Essl extension activation was successful.");
    }
}
exports.activate = activate;
//# sourceMappingURL=extension.js.map