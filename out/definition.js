"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EsslDefinitionProvider = void 0;
const vscode = require("vscode");
const hover = require("./hover");
const completion_1 = require("./completion/completion");
class EsslDefinitionProvider {
    provideDefinition(document, position, token) {
        // Adjust the given position to be the start of the identifier.
        // If undefined, user is not hovering on an identifier, so return.
        var adjustedChar = hover.getPositionOfIdentifierStart(document, position);
        if (adjustedChar === undefined) {
            return;
        }
        // Declare the new position of the hovered location
        var adjustedPosition = new vscode.Position(position.line, adjustedChar);
        // Loop through all predefined declarations to find the corresponding one
        for (var i = 0; i < completion_1.declarations.length; i++) {
            var declaration = completion_1.declarations[i];
            if (declaration.usages.length > 0) {
                for (var j = 0; j < declaration.usages.length; j++) {
                    var usage = declaration.usages[j];
                    if (usage.fileName === document.uri.fsPath && usage.position.isEqual(adjustedPosition)) {
                        var definition = new vscode.Location(declaration.fsPath, declaration.position);
                        return definition;
                    }
                }
            }
        }
        return undefined;
    }
}
exports.EsslDefinitionProvider = EsslDefinitionProvider;
//# sourceMappingURL=definition.js.map