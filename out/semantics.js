"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EsslSemanticTokensProvider = exports.currentFile = exports.diagnostics = exports.builder = exports.legend = void 0;
const vscode = require("vscode");
const extension = require("./extension");
const parser = require("./parser");
const completion = require("./completion/completion");
const antlr4ts_1 = require("antlr4ts");
const essl_lexer = require("./grammar/ESSL_Lexer");
const essl_parser = require("./grammar/ESSL_Parser");
const reg = require("./completion/regions");
// #region Create legend for semantics
// List of colors for default dark+ https://github.com/microsoft/vscode/blob/main/extensions/theme-defaults/themes/dark_plus.json
const tokenTypesLegend = [
    'class', 'variable', 'property', 'enumMember', 'function', 'string'
];
const tokenModifiersLegend = [
    'declaration'
];
exports.legend = new vscode.SemanticTokensLegend(tokenTypesLegend, tokenModifiersLegend);
// #endregion
// #region Declare variables to be used by the parser
exports.builder = new vscode.SemanticTokensBuilder(exports.legend);
exports.diagnostics = [];
const parseListener = new parser.ParseListener;
const errorParserListener = new parser.ErrorParserListener;
// #endregion
class EsslSemanticTokensProvider {
    // This method is called by the vscode client when the user loads a file, edits a file, etc.
    provideDocumentSemanticTokens(document, token) {
        EsslSemanticTokensProvider.performDiagnostics(document.getText(), document.uri);
        return exports.builder.build();
    }
    static performDiagnostics(document, documentPath) {
        exports.currentFile = documentPath;
        // Build autocompletion hierarchy (declarations)
        completion.buildCompletion();
        // Reset the semantics builder
        exports.builder = new vscode.SemanticTokensBuilder(exports.legend);
        // Parse the doc and provide semantics
        // Also update the list of regions for code completion
        EsslSemanticTokensProvider.updateRegions(document);
        // By this point, the listeners have finished collecting info.
        // Send the client diagnostics, and return the finished builder.
        extension.diagnosticCollection.set(documentPath, exports.diagnostics);
    }
    // Parse the doc and provide semantics
    // Also update the list of regions for code completion
    static updateRegions(text) {
        // Declare the ESSL Parser
        let inputStream = new antlr4ts_1.ANTLRInputStream(text);
        let esslLexer = new essl_lexer.ESSL_Lexer(inputStream);
        let tokenStream = new antlr4ts_1.CommonTokenStream(esslLexer);
        let esslParser = new essl_parser.ESSL_Parser(tokenStream);
        // Remove the predefined console listeners
        esslParser.removeParseListeners();
        esslParser.removeErrorListeners();
        esslLexer.removeErrorListeners();
        // Add the listeners to the parser
        // ParseListener is in charge of syntax highlighting, and uses the exported builder variable
        // ErrorListener is in charge of diagnostics, and uses the exported diagnostics variable
        esslLexer.addErrorListener(errorParserListener);
        esslParser.addParseListener(parseListener);
        esslParser.addErrorListener(errorParserListener);
        // Reset the list of diagnostic errors
        exports.diagnostics = [];
        reg.RegionType.regionTypes = [];
        // Parse the ESSL file, starting at top (declared in ESSL_.g4 as the start)
        esslParser.top();
    }
}
exports.EsslSemanticTokensProvider = EsslSemanticTokensProvider;
//# sourceMappingURL=semantics.js.map