"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateTypes = exports.generatePlantUMLImages = void 0;
const vscode = require("vscode");
const utility = require("./utility");
const systemPath = require("path");
const fileSystem = require("fs");
/*
Generate artifacts command
Shows a dropdown of which services to generate for.
*/
function generatePlantUMLImages(uri, format) {
    return __awaiter(this, void 0, void 0, function* () {
        var path = uri.fsPath;
        utility.runSaveAll();
        // Search for the absolute path to the .esslsln file
        var solutionPath = utility.getSolutionPath(path);
        // If the path is "", no .esslsln was found.
        if (solutionPath === "") {
            vscode.window.showErrorMessage("Could not find .esslsln file");
            return;
        }
        // Verify the folder exists
        var content = fileSystem.readFileSync(solutionPath).toString();
        // Strip content of comments
        // Regex expression from https://stackoverflow.com/questions/33483667/
        content = content.replace(/\\"|"(?:\\"|[^"])*"|(\/\/.*|\/\*[\s\S]*?\*\/)/g, (m, g) => g ? "" : m);
        var json = JSON.parse(content);
        var rootPath = json["root"];
        // Show a dropdown for the user to choose services to generate
        var selectedFoldersArray = yield utility.genServicePicker(path);
        // If the selected folders array is empty, none were chosen.
        // This can also be empty if no .esslproj files were found.
        // There should not be a message because if none were chosen,
        // it is self explanatory and if an error occurred, a warning was
        // already shown.
        if (selectedFoldersArray.length === 0) {
            return;
        }
        // Convert the array of selected folders into a string to be concat'd
        // into the terminal command
        var selectedFoldersString = selectedFoldersArray.join(" ");
        var dirnamePath = systemPath.dirname(solutionPath);
        var selectedFile = yield getDirectoryToGenerateInto(dirnamePath);
        // If no directory is selected, cancel
        if (selectedFile === "") {
            return;
        }
        vscode.window.showInformationMessage("Generating into " + selectedFile);
        if (solutionPath.includes(" ")) {
            solutionPath = "\"" + solutionPath + "\"";
        }
        // Open the terminal and run the command
        let terminal = utility.enableTerminal("Generate");
        terminal.sendText("essl.generator genArtifacts " + solutionPath + " -r -q -f " + format + " -s " + selectedFoldersString + utility.getReadableColor() + " -o " + selectedFile);
        // If the artifacts directory exists, open it with explorer/finder
        var artifactsPath = systemPath.resolve(selectedFile);
        if (fileSystem.existsSync(artifactsPath)) {
            let os = process.platform;
            switch (os) {
                case "darwin":
                    require('child_process').exec(`open "${artifactsPath}"`);
                    break;
                default:
                    require('child_process').exec(`start "" "${artifactsPath}"`);
                    break;
            }
        }
    });
}
exports.generatePlantUMLImages = generatePlantUMLImages;
/*
Generate types command
Shows a dropdown of which services to generate for.
*/
function generateTypes(uri, format) {
    return __awaiter(this, void 0, void 0, function* () {
        var path = uri.fsPath;
        utility.runSaveAll();
        // Search for the absolute path to the .esslsln file
        var solutionPath = utility.getSolutionPathQuoted(path);
        // If the path is "", no .esslsln was found.
        if (solutionPath === "") {
            vscode.window.showErrorMessage("Could not find .esslsln file");
            return;
        }
        // Show a dropdown for the user to choose services to generate
        var selectedFoldersArray = yield utility.genServicePicker(path);
        // If the selected folders array is empty, none were chosen.
        // This can also be empty if no .esslproj files were found.
        // There should not be a message because if none were chosen,
        // it is self explanatory and if an error occurred, a warning was
        // already shown.
        if (selectedFoldersArray.length === 0) {
            return;
        }
        // Convert the array of selected folders into a string to be concat'd
        // into the terminal command
        var selectedFoldersString = selectedFoldersArray.join(" ");
        var dirnamePath = systemPath.dirname(solutionPath);
        var selectedFile = yield getDirectoryToGenerateInto(dirnamePath);
        // If no directory is selected, cancel
        if (selectedFile === "") {
            return;
        }
        vscode.window.showInformationMessage("Generating into " + selectedFile);
        // Open the terminal and run the command
        let terminal = utility.enableTerminal("Generate");
        terminal.sendText("essl.generator genTypes " + solutionPath + " -q -g " + format + " -s " + selectedFoldersString + utility.getReadableColor() + " -o " + selectedFile);
        // If the artifacts directory exists, open it with explorer/finder
        var artifactsPath = systemPath.resolve(selectedFile);
        if (fileSystem.existsSync(artifactsPath)) {
            let os = process.platform;
            switch (os) {
                case "darwin":
                    require('child_process').exec(`open "${artifactsPath}"`);
                    break;
                default:
                    require('child_process').exec(`start "" "${artifactsPath}"`);
                    break;
            }
        }
    });
}
exports.generateTypes = generateTypes;
// Open a dialog menu for the user to pick a menu
function getDirectoryToGenerateInto(dirnamePath) {
    var _a;
    return __awaiter(this, void 0, void 0, function* () {
        // Get the config'd path
        const config = vscode.workspace.getConfiguration('ESSL');
        var defPath = config.get("DefaultArtifactsPath");
        // If the config'd path is valid, return it.
        if (defPath && defPath !== "") {
            if (fileSystem.existsSync(defPath)) {
                return defPath;
            }
            vscode.window.showErrorMessage("Default path to artifacts is configured, but the configured path is invalid.");
        }
        const downloadsPath = systemPath.resolve(systemPath.join((_a = process.env.HOMEPATH) !== null && _a !== void 0 ? _a : systemPath.resolve(dirnamePath), "Downloads"));
        // Open a picker to display where to generate the artifacts
        var uri = vscode.Uri.file(downloadsPath);
        var options = {
            canSelectFiles: false,
            canSelectFolders: true,
            canSelectMany: false,
            defaultUri: uri,
            openLabel: "Generate into this directory"
        };
        var selectedFile = "";
        yield vscode.window.showOpenDialog(options).then(fileUri => {
            if (fileUri && fileUri[0]) {
                selectedFile = fileUri[0].fsPath;
            }
        });
        return selectedFile;
    });
}
//# sourceMappingURL=artifacts.js.map