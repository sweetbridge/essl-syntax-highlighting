"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.genPicker = exports.genServicePicker = exports.enableTerminal = exports.getServiceName = exports.getSolutionPathQuoted = exports.getSolutionPath = exports.getReadableColor = exports.runSaveAll = exports.savedSolutionPath = void 0;
const vscode = require("vscode");
const path = require("path");
const fileSystem = require("fs");
/*
Run the saveAll VS Code command. This method is generally called
when the user runs a command
*/
function runSaveAll() {
    vscode.workspace.saveAll();
}
exports.runSaveAll = runSaveAll;
/*
Get the color of the opened theme and terminal in VS Code, then return
the appropriate color
*/
function getReadableColor() {
    var color = vscode.window.activeColorTheme.kind;
    switch (color) {
        case 1: // light
            return " -c light";
        case 2: // dark
            return " -c dark";
    }
    return "";
}
exports.getReadableColor = getReadableColor;
/*
Read the directory of the file and a number of its parent directories to
find the .esslsln file.
*/
function getSolutionPath(filePath) {
    var dirPath = fileSystem.lstatSync(filePath).isDirectory() ? filePath : path.resolve(path.dirname(filePath));
    var rootPath = filePath.split(path.sep)[0] + path.sep;
    var solutionPath = "";
    while (true) {
        fileSystem.readdirSync(dirPath).forEach(file => {
            var ext = path.extname(file);
            if (path.extname(file) === ".esslsln") {
                solutionPath = path.join(dirPath, file);
                return;
            }
        });
        if (solutionPath !== "") {
            exports.savedSolutionPath = solutionPath;
            return solutionPath;
        }
        if (dirPath === rootPath) {
            break;
        }
        dirPath = path.resolve(path.dirname(dirPath));
    }
    // If no .esslsln is found, return ""
    exports.savedSolutionPath = "";
    return "";
}
exports.getSolutionPath = getSolutionPath;
function getSolutionPathQuoted(filePath) {
    var solutionPath = getSolutionPath(filePath);
    if (solutionPath.includes(" ")) {
        solutionPath = "\"" + solutionPath + "\"";
    }
    return solutionPath;
}
exports.getSolutionPathQuoted = getSolutionPathQuoted;
/*
Get the service name of the current project. Reads the json of the .esslproj file.
*/
function getServiceName(filePath, getParent) {
    var dirPath = filePath;
    // Get the absolute path of the folder than contains the file
    if (getParent) {
        dirPath = path.resolve(path.dirname(filePath));
    }
    // Loop through the folder's files until it finds .esslproj
    var serviceName = "";
    var foundSln = false;
    fileSystem.readdirSync(dirPath).forEach(file => {
        if (path.extname(file) === ".esslproj") {
            // Parse the .esslproj file's json for serviceName
            var esslprojPath = path.join(dirPath, file);
            var data = fileSystem.readFileSync(esslprojPath, "utf8");
            var json = JSON.parse(data);
            serviceName = json.serviceName;
        }
        else if (path.extname(file) === ".esslsln") {
            foundSln = true;
        }
    });
    if (serviceName === "" && !foundSln) {
        return getServiceName(dirPath, true);
    }
    // If nothing is found and the current dir contains the .esslsln, return ""
    return serviceName;
}
exports.getServiceName = getServiceName;
/*
Returns a new, valid terminal with the name provided.
*/
function enableTerminal(name) {
    // Get the open terminal. If it exists, close it.
    let terminal = vscode.window.activeTerminal;
    if (terminal !== undefined) {
        terminal.dispose();
    }
    // Initialize a new terminal and return it.
    terminal = vscode.window.createTerminal(name);
    terminal.show();
    return terminal;
}
exports.enableTerminal = enableTerminal;
/*
Opens a dropdown to select which services to use.
Returns the names of these services.
*/
function genServicePicker(filePath) {
    return __awaiter(this, void 0, void 0, function* () {
        // Get every service name to be put in the picker
        var folderNames = getFolderNames(filePath);
        // If the array is empty, none were found.
        if (folderNames.length === 0) {
            vscode.window.showErrorMessage("Could not find any service names");
            return [];
        }
        // If there is only one project, no need to show it to the user to select.  Just return it.
        if (folderNames.length === 1) {
            return folderNames;
        }
        // Get the name of the service to preselect via the .esslproj of the directory that contains the currently opened file.
        var preselectedService = getServiceName(filePath, true);
        // Create the array of the items for the quickPicker.
        // This is needed because of the preselect feature.
        var quickPickItems = [];
        for (var i = 0; i < folderNames.length; i++) {
            quickPickItems.push({ label: folderNames[i], picked: folderNames[i] === preselectedService });
        }
        // Show the quickPicker dropdown and await the user's input.
        var selectedFolders = [];
        yield vscode.window.showQuickPick(quickPickItems, {
            canPickMany: true,
            title: "Choose services"
        }).then((selection) => {
            // If selection is undefined, leave the array to return empty.
            // If not, copy the selection's labels as strings into the array.
            if (selection !== undefined) {
                for (var i = 0; i < selection.length; i++) {
                    var item = selection[i].label;
                    selectedFolders.push(item);
                }
            }
        });
        // Return the selected folder names.
        // This is either empty or has values, never undefined or null.
        return selectedFolders;
    });
}
exports.genServicePicker = genServicePicker;
/*
Opens a single-choice dropdown for the user to choose from one of the provided options
Returns the name of the selected option
*/
function genPicker(...items) {
    return __awaiter(this, void 0, void 0, function* () {
        // Show the quickPicker dropdown and await the user's input.
        var selectedItem = "";
        yield vscode.window.showQuickPick(items, {
            title: "Choose an option"
        }).then((selection) => {
            if (selection !== undefined) {
                selectedItem = selection;
            }
        });
        // Return the selection
        return selectedItem;
    });
}
exports.genPicker = genPicker;
/*
Used for genPicker.
Gets every serviceName by looping through every directory to find the .esslproj files.
Returns an array of strings of these names.
*/
function getFolderNames(filePath) {
    // Get the absolute path of the .esslsln file
    var solutionPath = getSolutionPath(filePath);
    // Get the absolute path of the directory the solution file is in
    var directoryPath = path.resolve(path.dirname(solutionPath));
    var serviceNames = [];
    // Loop through the directory that contains the .esslsln file
    fileSystem.readdirSync(directoryPath).forEach(file => {
        var absPath = path.join(directoryPath, file);
        if (fileSystem.lstatSync(absPath).isDirectory()) {
            // If the file is a directory, loop through it to find the .esslproj file
            var serviceName = getServiceName(absPath, false);
            if (serviceName !== "" && !serviceNames.includes(serviceName)) {
                serviceNames.push(serviceName);
            }
        }
    });
    // Return the array of service names. Will be length 0 if none were found.
    return serviceNames;
}
//# sourceMappingURL=utility.js.map