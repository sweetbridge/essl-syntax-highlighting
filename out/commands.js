"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.parse = exports.copyFromProject = exports.copyToProject = exports.generate = exports.generatePlantuml = exports.compare = exports.compileChoose = exports.compile = void 0;
const vscode = require("vscode");
const utility = require("./utility");
/*
Local compile command.
This displays only on .essl and .esslproj files in the title headbar.
Compiles the service that the open file is from and that service's dependencies.
*/
function compile(uri) {
    var path = uri.fsPath;
    utility.runSaveAll();
    // Search for the absolute path to the .esslsln file
    var solutionPath = utility.getSolutionPathQuoted(path);
    // If the path is "", no .esslsln was found.
    if (solutionPath === "") {
        vscode.window.showErrorMessage("Could not find .esslsln file");
        return;
    }
    // Get the current service name from the .esslproj file
    // Necessary because the directory's name is sometimes different than the serviceName.
    var serviceName = utility.getServiceName(path, true);
    // If the name is "", no .esslproj file was found
    if (serviceName === "") {
        vscode.window.showErrorMessage("Could not find the .esslsproj file");
        return;
    }
    // Open the terminal and run the command
    let terminal = utility.enableTerminal("Compile");
    terminal.sendText("essl.generator compile " + solutionPath + " -s " + serviceName + utility.getReadableColor());
}
exports.compile = compile;
/*
Service compile command.
Shows a dropdown of which services to compile.
Will compile those services and their dependencies.
*/
function compileChoose(uri) {
    return __awaiter(this, void 0, void 0, function* () {
        var path = uri.fsPath;
        utility.runSaveAll();
        // Search for the absolute path to the .esslsln file
        var solutionPath = utility.getSolutionPathQuoted(path);
        // If the path is "", no .esslsln was found.
        if (solutionPath === "") {
            vscode.window.showErrorMessage("Could not find .esslsln file");
            return;
        }
        // Show a dropdown for the user to choose services to compile
        var selectedFoldersArray = yield utility.genServicePicker(path);
        // If the selected folders array is empty, none were chosen.
        // This can also be empty if no .esslproj files were found.
        // There should not be a message because if none were chosen,
        // it is self explanatory and if an error occurred, a warning was
        // already shown.
        if (selectedFoldersArray.length === 0) {
            return;
        }
        // Convert the array of selected folders into a string to be concat'd
        // into the terminal command
        var selectedFoldersString = selectedFoldersArray.join(" ");
        // Open the terminal and run the command
        let terminal = utility.enableTerminal("Compile");
        terminal.sendText("essl.generator compile " + solutionPath + " -s " + selectedFoldersString + utility.getReadableColor());
    });
}
exports.compileChoose = compileChoose;
/*
Compare services command
Compares all .essl files locally to the project files
*/
function compare(uri) {
    var path = uri.fsPath;
    utility.runSaveAll();
    // Search for the absolute path to the .esslsln file
    var solutionPath = utility.getSolutionPathQuoted(path);
    // If the path is "", no .esslsln was found.
    if (solutionPath === "") {
        vscode.window.showErrorMessage("Could not find .esslsln file");
        return;
    }
    // Open the terminal and run the command
    let terminal = utility.enableTerminal("Compare");
    terminal.sendText("essl.generator compare " + solutionPath + utility.getReadableColor());
}
exports.compare = compare;
/*
Generate plant uml files command
Shows a dropdown of which services to generate for.
*/
function generatePlantuml(uri) {
    return __awaiter(this, void 0, void 0, function* () {
        var path = uri.fsPath;
        utility.runSaveAll();
        // Search for the absolute path to the .esslsln file
        var solutionPath = utility.getSolutionPathQuoted(path);
        // If the path is "", no .esslsln was found.
        if (solutionPath === "") {
            vscode.window.showErrorMessage("Could not find .esslsln file");
            return;
        }
        // Show a dropdown for the user to choose services to generate
        var selectedFoldersArray = yield utility.genServicePicker(path);
        // If the selected folders array is empty, none were chosen.
        // This can also be empty if no .esslproj files were found.
        // There should not be a message because if none were chosen,
        // it is self explanatory and if an error occurred, a warning was
        // already shown.
        if (selectedFoldersArray.length === 0) {
            return;
        }
        // Convert the array of selected folders into a string to be concat'd
        // into the terminal command
        var selectedFoldersString = selectedFoldersArray.join(" ");
        // Open the terminal and run the command
        let terminal = utility.enableTerminal("Generate");
        terminal.sendText("essl.generator genPlantuml " + solutionPath + " -s " + selectedFoldersString + utility.getReadableColor());
    });
}
exports.generatePlantuml = generatePlantuml;
/*
Generate services command
Shows a dropdown of which services to generate.
*/
function generate(uri) {
    return __awaiter(this, void 0, void 0, function* () {
        var path = uri.fsPath;
        utility.runSaveAll();
        // Search for the absolute path to the .esslsln file
        var solutionPath = utility.getSolutionPathQuoted(path);
        // If the path is "", no .esslsln was found.
        if (solutionPath === "") {
            vscode.window.showErrorMessage("Could not find .esslsln file");
            return;
        }
        // Show a dropdown for the user to choose services to generate
        var selectedFoldersArray = yield utility.genServicePicker(path);
        // If the selected folders array is empty, none were chosen.
        // This can also be empty if no .esslproj files were found.
        // There should not be a message because if none were chosen,
        // it is self explanatory and if an error occurred, a warning was
        // already shown.
        if (selectedFoldersArray.length === 0) {
            return;
        }
        // Convert the array of selected folders into a string to be concat'd
        // into the terminal command
        var selectedFoldersString = selectedFoldersArray.join(" ");
        // Open the terminal and run the command
        let terminal = utility.enableTerminal("Generate");
        terminal.sendText("essl.generator genService " + solutionPath + " -s " + selectedFoldersString + utility.getReadableColor());
    });
}
exports.generate = generate;
/*
Command to copy the .essl files from local to project directory
Project path is hardcoded in .esslsln file
*/
function copyToProject(uri) {
    return __awaiter(this, void 0, void 0, function* () {
        var path = uri.fsPath;
        utility.runSaveAll();
        // Search for the absolute path to the .esslsln file
        var solutionPath = utility.getSolutionPathQuoted(path);
        // If the path is "", no .esslsln was found.
        if (solutionPath === "") {
            vscode.window.showErrorMessage("Could not find .esslsln file");
            return;
        }
        // Show a dropdown for the user to choose services to copy
        var selectedFoldersArray = yield utility.genServicePicker(path);
        // If the selected folders array is empty, none were chosen.
        // This can also be empty if no .esslproj files were found.
        // There should not be a message because if none were chosen,
        // it is self explanatory and if an error occurred, a warning was
        // already shown.
        if (selectedFoldersArray.length === 0) {
            return;
        }
        // Convert the array of selected folders into a string to be concat'd
        // into the terminal command
        var selectedFoldersString = selectedFoldersArray.join(" ");
        // Open the terminal and run the command
        let terminal = utility.enableTerminal("Copy");
        terminal.sendText("essl.generator copy " + solutionPath + " -d ToProject -s " + selectedFoldersString + utility.getReadableColor());
    });
}
exports.copyToProject = copyToProject;
/*
Command to copy the .essl files from project directory to local
Project path is hardcoded in .esslsln file
*/
function copyFromProject(uri) {
    return __awaiter(this, void 0, void 0, function* () {
        var path = uri.fsPath;
        utility.runSaveAll();
        // Search for the absolute path to the .esslsln file
        var solutionPath = utility.getSolutionPathQuoted(path);
        // If the path is "", no .esslsln was found.
        if (solutionPath === "") {
            vscode.window.showErrorMessage("Could not find .esslsln file");
            return;
        }
        // Show a dropdown for the user to choose services to copy
        var selectedFoldersArray = yield utility.genServicePicker(path);
        // If the selected folders array is empty, none were chosen.
        // This can also be empty if no .esslproj files were found.
        // There should not be a message because if none were chosen,
        // it is self explanatory and if an error occurred, a warning was
        // already shown.
        if (selectedFoldersArray.length === 0) {
            return;
        }
        // Convert the array of selected folders into a string to be concat'd
        // into the terminal command
        var selectedFoldersString = selectedFoldersArray.join(" ");
        // Open the terminal and run the command
        let terminal = utility.enableTerminal("Copy");
        terminal.sendText("essl.generator copy " + solutionPath + " -d FromProject -s " + selectedFoldersString + utility.getReadableColor());
    });
}
exports.copyFromProject = copyFromProject;
function parse(uri) {
    utility.runSaveAll();
}
exports.parse = parse;
//# sourceMappingURL=commands.js.map