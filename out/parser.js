"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ESSLDiagnostic = exports.ErrorParserListener = exports.ParseListener = void 0;
const vscode = require("vscode");
const vscode_1 = require("vscode");
const semantics = require("./semantics");
const reg = require("./completion/regions");
const decl = require("./completion/declaration");
const path = require("path");
const utility = require("./utility");
const attributeRegex = /\b(bool|date|datetime|decimal|float|int|string|uuid|null)\b/;
// ParseListener used for semantics. Detects enter/exit actions by the parser when
// delving into the generated code from ESSL_.g4
class ParseListener {
    // Utility method. Used by most listener functions below
    // Using the context, pushes the new semantic token structure into the builder
    push(context, tokenType) {
        semantics.builder.push(new vscode.Range(new vscode.Position(context._start.line - 1, context._start.charPositionInLine), new vscode.Position(context._stop.line - 1, context._stop.charPositionInLine + context.text.length)), tokenType, ["declaration"]);
    }
    // #region Semantic building
    exitTypeRefName(context) {
        if (!context.text.match(attributeRegex)) {
            this.push(context, "property");
        }
        else {
            return;
        }
        var text = context.text;
        var projPath = semantics.currentFile.fsPath.replace(path.basename(semantics.currentFile.fsPath), "");
        var relProjPath = path.relative(path.dirname(utility.savedSolutionPath), projPath).replace(path.sep, "/");
        ;
        var declaration = decl.Declaration.getDeclaration(text, semantics.currentFile, projPath);
        if (declaration === undefined) {
            return;
        }
        declaration.usages.push(new decl.DeclarationUsage(new vscode.Position(context.start.line - 1, context.start.charPositionInLine), semantics.currentFile.fsPath, relProjPath));
    }
    exitLineLabel(context) {
        this.push(context, "string");
    }
    exitEventRef(context) {
        this.push(context, "property");
    }
    exitEnumValue(context) {
        this.push(context, "enumMember");
    }
    exitEventName(context) {
        this.push(context, "enumMember");
    }
    exitCreatedEvent(context) {
        this.push(context, "enumMember");
    }
    exitName(context) {
        switch (context.parent.constructor.name) {
            case "CommandContext":
            case "QueryContext":
                this.push(context, "function");
                break;
            case "ValueTypeContext":
            case "EnumDeclContext":
            case "InterfaceTypeContext":
            case "DictionaryContext":
                this.push(context, "class");
                break;
        }
    }
    exitDictDefault(context) {
        this.push(context, "property");
    }
    exitEntityName(context) {
        this.push(context, "class");
    }
    exitDottedId(context) {
        this.push(context, "variable");
    }
    exitBaseName(context) {
        this.push(context, "class");
        var text = context.text;
        var projPath = semantics.currentFile.fsPath.replace(path.basename(semantics.currentFile.fsPath), "");
        var relProjPath = path.relative(path.dirname(utility.savedSolutionPath), projPath).replace(path.sep, "/");
        ;
        var declaration = decl.Declaration.getDeclaration(text, semantics.currentFile, projPath);
        if (declaration === undefined) {
            return;
        }
        declaration.usages.push(new decl.DeclarationUsage(new vscode.Position(context.start.line - 1, context.start.charPositionInLine), semantics.currentFile.fsPath, relProjPath));
    }
    // #endregion
    // #region Utility
    exitTypeBlock(context) {
        reg.Region.genRegion(context, "type");
    }
    exitCommandParams(context) {
        reg.Region.genRegion(context, "param");
    }
    // #endregion
    // #region Decorators for code completion
    exitServiceDecor(context) {
        reg.Region.genRegion(context, "serviceDecor");
    }
    exitEntityTypeDecor(context) {
        reg.Region.genRegion(context, "entityTypeDecor");
    }
    exitIndexAttrDecor(context) {
        reg.Region.genRegion(context, "indexAttrDecor");
    }
    exitValueTypeDecor(context) {
        reg.Region.genRegion(context, "valueTypeDecor");
    }
    exitInterfaceTypeDecor(context) {
        reg.Region.genRegion(context, "interfaceTypeDecor");
    }
    exitEnumDecor(context) {
        reg.Region.genRegion(context, "enumDecor");
    }
    exitDictionaryDecor(context) {
        reg.Region.genRegion(context, "dictionaryDecor");
    }
    exitEventsDecor(context) {
        reg.Region.genRegion(context, "eventsDecor");
    }
    exitEventNameDecor(context) {
        reg.Region.genRegion(context, "eventNameDecor");
    }
    exitFieldDecorator(context) {
        reg.Region.genRegion(context, "fieldDecorator");
    }
    exitCommandsBlockDecorator(context) {
        reg.Region.genRegion(context, "commandsBlockDecorator");
    }
    exitParamDecorator(context) {
        reg.Region.genRegion(context, "paramDecorator");
    }
    exitQueriesBlockDecorator(context) {
        reg.Region.genRegion(context, "queriesBlockDecorator");
    }
    exitQueryDecorator(context) {
        reg.Region.genRegion(context, "queryDecorator");
    }
    exitSubscriptionsBlockDecorator(context) {
        reg.Region.genRegion(context, "subscriptionsBlockDecorator");
    }
    exitSubscriptionDecorator(context) {
        reg.Region.genRegion(context, "subscriptionDecorator");
    }
    exitCommandDecorator(context) {
        reg.Region.genRegion(context, "commandDecorator");
    }
}
exports.ParseListener = ParseListener;
// ErrorListener used for diagnostics. Detects syntax errors
// Listener for both lexer and parser
class ErrorParserListener {
    // This method is called by the client when a syntax error is detected
    syntaxError(recognizer, offendingSymbol, line, charPositionInLine, msg, e) {
        // Declare the diagnostic via the params of this method
        var diagnostic = new ESSLDiagnostic(new vscode.Range(new vscode.Position(line - 1, charPositionInLine), new vscode.Position(line - 1, charPositionInLine + 1)), msg);
        // Check if diagnostic already exists
        // Diagnostic will already exist if multiple syntax errors occur on the same line
        for (var i = 0; i < semantics.diagnostics.length; i++) {
            let thatDiagnostic = semantics.diagnostics[i];
            if (diagnostic.equals(thatDiagnostic)) {
                return;
            }
        }
        // Push the new diagnostic to the list
        semantics.diagnostics.push(diagnostic);
    }
}
exports.ErrorParserListener = ErrorParserListener;
// Subclass of vscode.Diagnostic for ESSL files
class ESSLDiagnostic extends vscode_1.Diagnostic {
    // Equals method
    // Used so there are not multiple of the same diagnostic on the same line
    equals(that) {
        return this.range.isEqual(that.range) &&
            this.message === that.message;
    }
}
exports.ESSLDiagnostic = ESSLDiagnostic;
//# sourceMappingURL=parser.js.map