"use strict";
// Generated from syntaxes/grammar/ESSL_.g4 by ANTLR 4.9.0-SNAPSHOT
Object.defineProperty(exports, "__esModule", { value: true });
exports.DefaultConstructorContext = exports.TransCoordinatorContext = exports.PartialCommandContext = exports.EntityTypeDecorContext = exports.EntityTypeDecorsContext = exports.PossiblyEmptyTypeBlockContext = exports.TypeBlockContext = exports.BaseNameContext = exports.ExtendsListContext = exports.NameContext = exports.InterfaceTypeContext = exports.ValueTypeContext = exports.EntityTypeContext = exports.UmlImageSubFolderContext = exports.UmlImageRemoveTitleContext = exports.UmlImageFormatContext = exports.UmlGroupImageNameContext = exports.UmlImageParamsContext = exports.UmlImageDecorContext = exports.UmlStrReplNewValueContext = exports.UmlStrReplOldValueContext = exports.UmlGroupTitleStrReplContext = exports.UmlGroupStrReplParamsContext = exports.UmlGroupStrReplDecorContext = exports.EcosystemSecretsMethodNameContext = exports.EcosystemSecretsMethodContext = exports.EcosystemPoliciesMethodNameContext = exports.EcosystemPoliciesMethodContext = exports.AuthPolicyNamesContext = exports.AllAdminAuthPolicyContext = exports.AllSubscriptionAuthPolicyContext = exports.AllQueryAuthPolicyContext = exports.AllCommandAuthPolicyContext = exports.AllApiAuthPolicyContext = exports.ServiceDecorContext = exports.ServiceDecorsContext = exports.ServiceContext = exports.DependsOnNameSpaceContext = exports.DependsOnContext = exports.NamespaceNameContext = exports.NamespaceContext = exports.VirtualEntityBlockContext = exports.BaseEntityNameContext = exports.ExtendsEntityContext = exports.VirtualEntityContext = exports.EntityBlockContext = exports.EntityNameContext = exports.EntityContext = exports.TopContext = exports.ESSL_Parser = void 0;
exports.AllowTemporalContext = exports.NoCacheModelContext = exports.CacheModelContext = exports.IndexAttrDecorContext = exports.IndexAttrDecorsContext = exports.FactLedgerContext = exports.LedgerContext = exports.SlowlyChangingContext = exports.EventsContext = exports.StyleDecorContext = exports.DimensionNameDecorContext = exports.GenerateIfNameContext = exports.GenerateIfDecorContext = exports.CamelCaseNameContext = exports.CamelCaseDecorContext = exports.InterfaceNameContext = exports.InterfaceDecorContext = exports.Base64RefIdsDecorContext = exports.GraphQLPluralCamelCaseContext = exports.GraphQLPluralCamelCaseDecorContext = exports.GraphQLCamelCaseContext = exports.GraphQLCamelCaseDecorContext = exports.GraphQLPluralNameContext = exports.GraphQLPluralNameDecorContext = exports.GraphQLNameContext = exports.GraphQLNameDecorContext = exports.TableNameContext = exports.TableNameDecorContext = exports.PluralNameContext = exports.PluralNameDecorContext = exports.PublicDecorContext = exports.AuthPolicyDecorContext = exports.AuthPolicyNameContext = exports.BeforeEventPersistContext = exports.HashLookupDecorContext = exports.UmlSubGroupContext = exports.UmlGroupTitleContext = exports.UmlGroupContext = exports.UmlGroupDecorContext = exports.CrossPartitionDecorContext = exports.PartitionTypeUniqueIndexContext = exports.PartitionIndexContext = exports.QueryPartitionAttrsContext = exports.QueryPartitionDecorContext = exports.IndexedFieldContext = exports.IndexKeyAttrContext = exports.IndexKeyAttrsContext = exports.IndexTypeAttrsContext = exports.UniqueIndexTypeDecorContext = exports.IndexTypeDecorContext = void 0;
exports.NumValueContext = exports.InitialEnumValContext = exports.NewInstanceContext = exports.BoolValueContext = exports.NullValueContext = exports.FieldInitializerContext = exports.NullableIndicatorContext = exports.ListIndicatorContext = exports.ValueTypeRefContext = exports.PrimitiveTypeContext = exports.TypeRefNameContext = exports.TypeRefContext = exports.FieldDefContext = exports.DictionaryDecorContext = exports.DictionaryDecorsContext = exports.DictEntriesContext = exports.DictDefaultContext = exports.DictDefaultDeclContext = exports.DictValueTypeContext = exports.DictValueTypeDeclContext = exports.DictPartitionedContext = exports.DictionaryBlockContext = exports.DictionaryContext = exports.FlagsDecorContext = exports.InputTypeOptionContext = exports.GraphQlsuppressOptionContext = exports.ModelsuppressOptionContext = exports.SuppressOptionContext = exports.SuppressDecorContext = exports.EnumDecorContext = exports.EnumDecorsContext = exports.EnumExplicitValueContext = exports.EnumValueContext = exports.EnumItemContext = exports.EnumBlockContext = exports.EnumDeclContext = exports.InterfaceTypeDecorContext = exports.InterfaceTypeDecorsContext = exports.VoidPrimaryFactContext = exports.OutputTypeContext = exports.DiscriminatedByValueContext = exports.DiscriminatedByContext = exports.WebValueMethodContext = exports.WebValueStringContext = exports.JsonConstructorContext = exports.PartialContext = exports.ValueTypeDecorContext = exports.ValueTypeDecorsContext = exports.IndexNameContext = exports.IndexNameDecorContext = void 0;
exports.FactDimensionDecorContext = exports.AttributeValueContext = exports.AttributeKeyContext = exports.AttributePairContext = exports.AttributeDecorContext = exports.AutoFillTypeContext = exports.AutoFillDecorContext = exports.LabelContext = exports.LabelDecorContext = exports.QuestionDecorContext = exports.MultiLineDecorContext = exports.TypeDiscriminatorContext = exports.DictDecorContext = exports.HandCodedDecorContext = exports.NotPersistedDecorContext = exports.ConstantDecorContext = exports.SameAsPersistContext = exports.SameAsDecorContext = exports.AsValueTypeDecorContext = exports.BreakTypeContext = exports.SectionDecorContext = exports.LineLabelContext = exports.HideRelationshipContext = exports.LineLengthContext = exports.HorizontalVerticalContext = exports.DirectionContext = exports.FieldUmlGroupTitleContext = exports.FieldUmlDecorContext = exports.CloneIdAsIsDecorContext = exports.ClonePartitionerDecorContext = exports.ImmutableDecorContext = exports.CalculatedDecorContext = exports.HiddenDecorContext = exports.ReadOnlyDecorContext = exports.RequiredDecorContext = exports.ComposedDecorContext = exports.InlineEnumValueContext = exports.InlineEnumDecorContext = exports.NullHandlingContext = exports.CaseInsensitiveContext = exports.IndexAttrsContext = exports.UniqueIndexDecorContext = exports.IndexDecorContext = exports.FieldDecoratorContext = exports.FieldDecoratorsContext = exports.FlagsEnumValueContext = exports.FlagsEnumSetContext = exports.StringValueContext = exports.DecimalContext = exports.IntegerContext = void 0;
exports.TransAttrContext = exports.TransDecorContext = exports.SyncDecorContext = exports.AsyncBusinessLogicContext = exports.AsyncModelValidationContext = exports.AsyncParamValidationContext = exports.AsyncSpecContext = exports.AsyncDecorContext = exports.ModelDecorContext = exports.CommandDecoratorsContext = exports.CommandDecoratorContext = exports.IdDecorContext = exports.ByValueDecorContext = exports.FilterExprContext = exports.IndexQualifierContext = exports.TypeQualifierNameContext = exports.ParamDecoratorsContext = exports.TypeQualifierDecorContext = exports.ParamDecoratorContext = exports.ReturnTypeRefContext = exports.ReturnsTypeContext = exports.EventRefContext = exports.EventListContext = exports.YieldsContext = exports.CommandResultsInContext = exports.ParamInitializerContext = exports.ParamNameModifiersContext = exports.OptionalTypeRefContext = exports.ParamDeclContext = exports.ParamContext = exports.CommandParamsContext = exports.CommandContext = exports.CommandsBlockDecoratorsContext = exports.CommandsBlockDecoratorContext = exports.CommandsBlockContext = exports.EntityCommandsContext = exports.EventNameDecorContext = exports.EventNameDecorsContext = exports.EventsDecorContext = exports.EventsDecorsContext = exports.EventNameContext = exports.EventDefContext = exports.CreatedEventContext = exports.CreatedEventDefContext = exports.EventsBlockContext = exports.EntityEventsContext = exports.JournalEntriesDecorContext = exports.DimensionKeyDecorContext = exports.DimInxContext = exports.DimPrimaryContext = void 0;
exports.EsslLineCommentContext = exports.EsslBlockCommentContext = exports.DomBlockCommentContext = exports.DomBeforeLineCommentContext = exports.EsslCommentContext = exports.AfterCommentContext = exports.BeforeCommentContext = exports.CommentContext = exports.DottedIdContext = exports.SubscriptionDecoratorsContext = exports.SubscriptionDecoratorContext = exports.SubscriptionContext = exports.SubscriptionsBlockDecoratorsContext = exports.SubscriptionsBlockDecoratorContext = exports.SubscriptionsBlockContext = exports.EntitySubscriptionsContext = exports.ProvideGraphQLSchemaDecorContext = exports.ProvideUserContextDecorContext = exports.TemporalDecorContext = exports.QueryDecoratorsContext = exports.QueryDecoratorContext = exports.QueryContext = exports.QueriesBlockDecoratorsContext = exports.QueriesBlockDecoratorContext = exports.QueriesBlockContext = exports.EntityQueriesContext = exports.RefFieldContext = exports.JsonKeyContext = exports.GenUpdateContext = exports.GenClearSetContext = exports.GenRemoveFromSetContext = exports.GenAddToSetContext = exports.ValueExpressionContext = exports.AssignmentContext = exports.AssignmentListContext = exports.GenAssignmentsContext = exports.GenSetFieldContext = exports.GeneratorOptionContext = exports.GenerateDecorContext = exports.PartialErrorDecorContext = exports.DeleteDecorContext = exports.CreateDecorContext = exports.MessageSourceDecorContext = exports.InternalDecorContext = exports.ExplicitDecorContext = exports.EffectiveDateDecorContext = exports.ContinuationDecorContext = void 0;
const ATN_1 = require("antlr4ts/atn/ATN");
const ATNDeserializer_1 = require("antlr4ts/atn/ATNDeserializer");
const FailedPredicateException_1 = require("antlr4ts/FailedPredicateException");
const NoViableAltException_1 = require("antlr4ts/NoViableAltException");
const Parser_1 = require("antlr4ts/Parser");
const ParserRuleContext_1 = require("antlr4ts/ParserRuleContext");
const ParserATNSimulator_1 = require("antlr4ts/atn/ParserATNSimulator");
const RecognitionException_1 = require("antlr4ts/RecognitionException");
const Token_1 = require("antlr4ts/Token");
const VocabularyImpl_1 = require("antlr4ts/VocabularyImpl");
const Utils = require("antlr4ts/misc/Utils");
class ESSL_Parser extends Parser_1.Parser {
    constructor(input) {
        super(input);
        this._interp = new ParserATNSimulator_1.ParserATNSimulator(ESSL_Parser._ATN, this);
    }
    // @Override
    // @NotNull
    get vocabulary() {
        return ESSL_Parser.VOCABULARY;
    }
    // tslint:enable:no-trailing-whitespace
    // @Override
    get grammarFileName() { return "ESSL_.g4"; }
    // @Override
    get ruleNames() { return ESSL_Parser.ruleNames; }
    // @Override
    get serializedATN() { return ESSL_Parser._serializedATN; }
    createFailedPredicateException(predicate, message) {
        return new FailedPredicateException_1.FailedPredicateException(this, predicate, message);
    }
    // @RuleVersion(0)
    top() {
        let _localctx = new TopContext(this._ctx, this.state);
        this.enterRule(_localctx, 0, ESSL_Parser.RULE_top);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 594;
                this._errHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this._input, 0, this._ctx)) {
                    case 1:
                        {
                            this.state = 592;
                            this.entity();
                        }
                        break;
                    case 2:
                        {
                            this.state = 593;
                            this.virtualEntity();
                        }
                        break;
                }
                this.state = 596;
                this.match(ESSL_Parser.EOF);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    entity() {
        let _localctx = new EntityContext(this._ctx, this.state);
        this.enterRule(_localctx, 2, ESSL_Parser.RULE_entity);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 598;
                this.beforeComment();
                this.state = 599;
                this.match(ESSL_Parser.T__0);
                this.state = 600;
                this.entityName();
                this.state = 601;
                this.entityBlock();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    entityName() {
        let _localctx = new EntityNameContext(this._ctx, this.state);
        this.enterRule(_localctx, 4, ESSL_Parser.RULE_entityName);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 603;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    entityBlock() {
        let _localctx = new EntityBlockContext(this._ctx, this.state);
        this.enterRule(_localctx, 6, ESSL_Parser.RULE_entityBlock);
        let _la;
        try {
            let _alt;
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 605;
                this.esslComment();
                this.state = 606;
                this.match(ESSL_Parser.T__1);
                this.state = 607;
                this.namespace();
                this.state = 611;
                this._errHandler.sync(this);
                _alt = this.interpreter.adaptivePredict(this._input, 1, this._ctx);
                while (_alt !== 2 && _alt !== ATN_1.ATN.INVALID_ALT_NUMBER) {
                    if (_alt === 1) {
                        {
                            {
                                this.state = 608;
                                this.dependsOn();
                            }
                        }
                    }
                    this.state = 613;
                    this._errHandler.sync(this);
                    _alt = this.interpreter.adaptivePredict(this._input, 1, this._ctx);
                }
                this.state = 615;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__7) {
                    {
                        this.state = 614;
                        this.service();
                    }
                }
                this.state = 617;
                this.entityType();
                this.state = 624;
                this._errHandler.sync(this);
                _alt = this.interpreter.adaptivePredict(this._input, 4, this._ctx);
                while (_alt !== 2 && _alt !== ATN_1.ATN.INVALID_ALT_NUMBER) {
                    if (_alt === 1) {
                        {
                            this.state = 622;
                            this._errHandler.sync(this);
                            switch (this.interpreter.adaptivePredict(this._input, 3, this._ctx)) {
                                case 1:
                                    {
                                        this.state = 618;
                                        this.enumDecl();
                                    }
                                    break;
                                case 2:
                                    {
                                        this.state = 619;
                                        this.dictionary();
                                    }
                                    break;
                                case 3:
                                    {
                                        this.state = 620;
                                        this.valueType();
                                    }
                                    break;
                                case 4:
                                    {
                                        this.state = 621;
                                        this.interfaceType();
                                    }
                                    break;
                            }
                        }
                    }
                    this.state = 626;
                    this._errHandler.sync(this);
                    _alt = this.interpreter.adaptivePredict(this._input, 4, this._ctx);
                }
                this.state = 627;
                this.entityEvents();
                this.state = 629;
                this._errHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this._input, 5, this._ctx)) {
                    case 1:
                        {
                            this.state = 628;
                            this.entityCommands();
                        }
                        break;
                }
                this.state = 632;
                this._errHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this._input, 6, this._ctx)) {
                    case 1:
                        {
                            this.state = 631;
                            this.entityQueries();
                        }
                        break;
                }
                this.state = 635;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if ((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << ESSL_Parser.T__0) | (1 << ESSL_Parser.T__3) | (1 << ESSL_Parser.T__23) | (1 << ESSL_Parser.T__24))) !== 0) || ((((_la - 49)) & ~0x1F) === 0 && ((1 << (_la - 49)) & ((1 << (ESSL_Parser.T__48 - 49)) | (1 << (ESSL_Parser.T__62 - 49)) | (1 << (ESSL_Parser.T__69 - 49)) | (1 << (ESSL_Parser.T__73 - 49)) | (1 << (ESSL_Parser.T__74 - 49)) | (1 << (ESSL_Parser.T__75 - 49)) | (1 << (ESSL_Parser.T__76 - 49)) | (1 << (ESSL_Parser.T__77 - 49)) | (1 << (ESSL_Parser.T__78 - 49)) | (1 << (ESSL_Parser.T__79 - 49)))) !== 0) || _la === ESSL_Parser.T__80 || _la === ESSL_Parser.T__145 || _la === ESSL_Parser.T__175 || ((((_la - 180)) & ~0x1F) === 0 && ((1 << (_la - 180)) & ((1 << (ESSL_Parser.T__179 - 180)) | (1 << (ESSL_Parser.BEFORE_COMMENT_LINE - 180)) | (1 << (ESSL_Parser.ESSL_COMMENT_BLOCK - 180)) | (1 << (ESSL_Parser.ESSL_COMMENT_LINE - 180)) | (1 << (ESSL_Parser.IDENTIFIER - 180)))) !== 0)) {
                    {
                        this.state = 634;
                        this.entitySubscriptions();
                    }
                }
                this.state = 637;
                this.match(ESSL_Parser.T__2);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    virtualEntity() {
        let _localctx = new VirtualEntityContext(this._ctx, this.state);
        this.enterRule(_localctx, 8, ESSL_Parser.RULE_virtualEntity);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 639;
                this.beforeComment();
                this.state = 640;
                this.match(ESSL_Parser.T__3);
                this.state = 641;
                this.match(ESSL_Parser.T__0);
                this.state = 642;
                this.entityName();
                this.state = 643;
                this.extendsEntity();
                this.state = 644;
                this.virtualEntityBlock();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    extendsEntity() {
        let _localctx = new ExtendsEntityContext(this._ctx, this.state);
        this.enterRule(_localctx, 10, ESSL_Parser.RULE_extendsEntity);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 648;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__4) {
                    {
                        this.state = 646;
                        this.match(ESSL_Parser.T__4);
                        this.state = 647;
                        this.baseEntityName();
                    }
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    baseEntityName() {
        let _localctx = new BaseEntityNameContext(this._ctx, this.state);
        this.enterRule(_localctx, 12, ESSL_Parser.RULE_baseEntityName);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 650;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    virtualEntityBlock() {
        let _localctx = new VirtualEntityBlockContext(this._ctx, this.state);
        this.enterRule(_localctx, 14, ESSL_Parser.RULE_virtualEntityBlock);
        let _la;
        try {
            let _alt;
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 652;
                this.match(ESSL_Parser.T__1);
                this.state = 653;
                this.namespace();
                this.state = 657;
                this._errHandler.sync(this);
                _alt = this.interpreter.adaptivePredict(this._input, 9, this._ctx);
                while (_alt !== 2 && _alt !== ATN_1.ATN.INVALID_ALT_NUMBER) {
                    if (_alt === 1) {
                        {
                            {
                                this.state = 654;
                                this.dependsOn();
                            }
                        }
                    }
                    this.state = 659;
                    this._errHandler.sync(this);
                    _alt = this.interpreter.adaptivePredict(this._input, 9, this._ctx);
                }
                this.state = 661;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__7) {
                    {
                        this.state = 660;
                        this.service();
                    }
                }
                this.state = 669;
                this._errHandler.sync(this);
                _alt = this.interpreter.adaptivePredict(this._input, 12, this._ctx);
                while (_alt !== 2 && _alt !== ATN_1.ATN.INVALID_ALT_NUMBER) {
                    if (_alt === 1) {
                        {
                            this.state = 667;
                            this._errHandler.sync(this);
                            switch (this.interpreter.adaptivePredict(this._input, 11, this._ctx)) {
                                case 1:
                                    {
                                        this.state = 663;
                                        this.enumDecl();
                                    }
                                    break;
                                case 2:
                                    {
                                        this.state = 664;
                                        this.dictionary();
                                    }
                                    break;
                                case 3:
                                    {
                                        this.state = 665;
                                        this.valueType();
                                    }
                                    break;
                                case 4:
                                    {
                                        this.state = 666;
                                        this.interfaceType();
                                    }
                                    break;
                            }
                        }
                    }
                    this.state = 671;
                    this._errHandler.sync(this);
                    _alt = this.interpreter.adaptivePredict(this._input, 12, this._ctx);
                }
                this.state = 673;
                this._errHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this._input, 13, this._ctx)) {
                    case 1:
                        {
                            this.state = 672;
                            this.entityCommands();
                        }
                        break;
                }
                this.state = 676;
                this._errHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this._input, 14, this._ctx)) {
                    case 1:
                        {
                            this.state = 675;
                            this.entityQueries();
                        }
                        break;
                }
                this.state = 679;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if ((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << ESSL_Parser.T__0) | (1 << ESSL_Parser.T__3) | (1 << ESSL_Parser.T__23) | (1 << ESSL_Parser.T__24))) !== 0) || ((((_la - 49)) & ~0x1F) === 0 && ((1 << (_la - 49)) & ((1 << (ESSL_Parser.T__48 - 49)) | (1 << (ESSL_Parser.T__62 - 49)) | (1 << (ESSL_Parser.T__69 - 49)) | (1 << (ESSL_Parser.T__73 - 49)) | (1 << (ESSL_Parser.T__74 - 49)) | (1 << (ESSL_Parser.T__75 - 49)) | (1 << (ESSL_Parser.T__76 - 49)) | (1 << (ESSL_Parser.T__77 - 49)) | (1 << (ESSL_Parser.T__78 - 49)) | (1 << (ESSL_Parser.T__79 - 49)))) !== 0) || _la === ESSL_Parser.T__80 || _la === ESSL_Parser.T__145 || _la === ESSL_Parser.T__175 || ((((_la - 180)) & ~0x1F) === 0 && ((1 << (_la - 180)) & ((1 << (ESSL_Parser.T__179 - 180)) | (1 << (ESSL_Parser.BEFORE_COMMENT_LINE - 180)) | (1 << (ESSL_Parser.ESSL_COMMENT_BLOCK - 180)) | (1 << (ESSL_Parser.ESSL_COMMENT_LINE - 180)) | (1 << (ESSL_Parser.IDENTIFIER - 180)))) !== 0)) {
                    {
                        this.state = 678;
                        this.entitySubscriptions();
                    }
                }
                this.state = 681;
                this.match(ESSL_Parser.T__2);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    namespace() {
        let _localctx = new NamespaceContext(this._ctx, this.state);
        this.enterRule(_localctx, 16, ESSL_Parser.RULE_namespace);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 683;
                this.esslComment();
                this.state = 684;
                this.match(ESSL_Parser.T__5);
                this.state = 685;
                this.namespaceName();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    namespaceName() {
        let _localctx = new NamespaceNameContext(this._ctx, this.state);
        this.enterRule(_localctx, 18, ESSL_Parser.RULE_namespaceName);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 687;
                this.dottedId();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    dependsOn() {
        let _localctx = new DependsOnContext(this._ctx, this.state);
        this.enterRule(_localctx, 20, ESSL_Parser.RULE_dependsOn);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 689;
                this.esslComment();
                this.state = 690;
                this.match(ESSL_Parser.T__6);
                this.state = 691;
                this.dependsOnNameSpace();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    dependsOnNameSpace() {
        let _localctx = new DependsOnNameSpaceContext(this._ctx, this.state);
        this.enterRule(_localctx, 22, ESSL_Parser.RULE_dependsOnNameSpace);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 693;
                this.dottedId();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    service() {
        let _localctx = new ServiceContext(this._ctx, this.state);
        this.enterRule(_localctx, 24, ESSL_Parser.RULE_service);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 695;
                this.match(ESSL_Parser.T__7);
                this.state = 696;
                this.match(ESSL_Parser.T__1);
                this.state = 697;
                this.match(ESSL_Parser.T__2);
                this.state = 698;
                this.serviceDecors();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    serviceDecors() {
        let _localctx = new ServiceDecorsContext(this._ctx, this.state);
        this.enterRule(_localctx, 26, ESSL_Parser.RULE_serviceDecors);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 705;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__8) {
                    {
                        {
                            this.state = 700;
                            this.serviceDecor();
                            this.state = 701;
                            this.esslComment();
                        }
                    }
                    this.state = 707;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    serviceDecor() {
        let _localctx = new ServiceDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 28, ESSL_Parser.RULE_serviceDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 708;
                this.match(ESSL_Parser.T__8);
                this.state = 718;
                this._errHandler.sync(this);
                switch (this._input.LA(1)) {
                    case ESSL_Parser.T__9:
                        {
                            this.state = 709;
                            this.allApiAuthPolicy();
                        }
                        break;
                    case ESSL_Parser.T__10:
                        {
                            this.state = 710;
                            this.allCommandAuthPolicy();
                        }
                        break;
                    case ESSL_Parser.T__11:
                        {
                            this.state = 711;
                            this.allQueryAuthPolicy();
                        }
                        break;
                    case ESSL_Parser.T__12:
                        {
                            this.state = 712;
                            this.allSubscriptionAuthPolicy();
                        }
                        break;
                    case ESSL_Parser.T__13:
                        {
                            this.state = 713;
                            this.allAdminAuthPolicy();
                        }
                        break;
                    case ESSL_Parser.T__17:
                        {
                            this.state = 714;
                            this.ecosystemPoliciesMethod();
                        }
                        break;
                    case ESSL_Parser.T__18:
                        {
                            this.state = 715;
                            this.ecosystemSecretsMethod();
                        }
                        break;
                    case ESSL_Parser.T__19:
                        {
                            this.state = 716;
                            this.umlGroupStrReplDecor();
                        }
                        break;
                    case ESSL_Parser.T__20:
                        {
                            this.state = 717;
                            this.umlImageDecor();
                        }
                        break;
                    default:
                        throw new NoViableAltException_1.NoViableAltException(this);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    allApiAuthPolicy() {
        let _localctx = new AllApiAuthPolicyContext(this._ctx, this.state);
        this.enterRule(_localctx, 30, ESSL_Parser.RULE_allApiAuthPolicy);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 720;
                this.match(ESSL_Parser.T__9);
                this.state = 721;
                this.authPolicyNames();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    allCommandAuthPolicy() {
        let _localctx = new AllCommandAuthPolicyContext(this._ctx, this.state);
        this.enterRule(_localctx, 32, ESSL_Parser.RULE_allCommandAuthPolicy);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 723;
                this.match(ESSL_Parser.T__10);
                this.state = 724;
                this.authPolicyNames();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    allQueryAuthPolicy() {
        let _localctx = new AllQueryAuthPolicyContext(this._ctx, this.state);
        this.enterRule(_localctx, 34, ESSL_Parser.RULE_allQueryAuthPolicy);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 726;
                this.match(ESSL_Parser.T__11);
                this.state = 727;
                this.authPolicyNames();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    allSubscriptionAuthPolicy() {
        let _localctx = new AllSubscriptionAuthPolicyContext(this._ctx, this.state);
        this.enterRule(_localctx, 36, ESSL_Parser.RULE_allSubscriptionAuthPolicy);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 729;
                this.match(ESSL_Parser.T__12);
                this.state = 730;
                this.authPolicyNames();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    allAdminAuthPolicy() {
        let _localctx = new AllAdminAuthPolicyContext(this._ctx, this.state);
        this.enterRule(_localctx, 38, ESSL_Parser.RULE_allAdminAuthPolicy);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 732;
                this.match(ESSL_Parser.T__13);
                this.state = 733;
                this.authPolicyNames();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    authPolicyNames() {
        let _localctx = new AuthPolicyNamesContext(this._ctx, this.state);
        this.enterRule(_localctx, 40, ESSL_Parser.RULE_authPolicyNames);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 735;
                this.match(ESSL_Parser.T__14);
                this.state = 736;
                this.authPolicyName();
                this.state = 741;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__15) {
                    {
                        {
                            this.state = 737;
                            this.match(ESSL_Parser.T__15);
                            this.state = 738;
                            this.authPolicyName();
                        }
                    }
                    this.state = 743;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
                this.state = 744;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    ecosystemPoliciesMethod() {
        let _localctx = new EcosystemPoliciesMethodContext(this._ctx, this.state);
        this.enterRule(_localctx, 42, ESSL_Parser.RULE_ecosystemPoliciesMethod);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 746;
                this.match(ESSL_Parser.T__17);
                this.state = 747;
                this.match(ESSL_Parser.T__14);
                this.state = 748;
                this.ecosystemPoliciesMethodName();
                this.state = 749;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    ecosystemPoliciesMethodName() {
        let _localctx = new EcosystemPoliciesMethodNameContext(this._ctx, this.state);
        this.enterRule(_localctx, 44, ESSL_Parser.RULE_ecosystemPoliciesMethodName);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 751;
                this.dottedId();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    ecosystemSecretsMethod() {
        let _localctx = new EcosystemSecretsMethodContext(this._ctx, this.state);
        this.enterRule(_localctx, 46, ESSL_Parser.RULE_ecosystemSecretsMethod);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 753;
                this.match(ESSL_Parser.T__18);
                this.state = 754;
                this.match(ESSL_Parser.T__14);
                this.state = 755;
                this.ecosystemSecretsMethodName();
                this.state = 756;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    ecosystemSecretsMethodName() {
        let _localctx = new EcosystemSecretsMethodNameContext(this._ctx, this.state);
        this.enterRule(_localctx, 48, ESSL_Parser.RULE_ecosystemSecretsMethodName);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 758;
                this.dottedId();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    umlGroupStrReplDecor() {
        let _localctx = new UmlGroupStrReplDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 50, ESSL_Parser.RULE_umlGroupStrReplDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 760;
                this.match(ESSL_Parser.T__19);
                this.state = 761;
                this.umlGroupStrReplParams();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    umlGroupStrReplParams() {
        let _localctx = new UmlGroupStrReplParamsContext(this._ctx, this.state);
        this.enterRule(_localctx, 52, ESSL_Parser.RULE_umlGroupStrReplParams);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 763;
                this.match(ESSL_Parser.T__14);
                this.state = 764;
                this.umlGroupTitleStrRepl();
                this.state = 765;
                this.match(ESSL_Parser.T__15);
                this.state = 766;
                this.umlStrReplOldValue();
                this.state = 767;
                this.match(ESSL_Parser.T__15);
                this.state = 768;
                this.umlStrReplNewValue();
                this.state = 769;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    umlGroupTitleStrRepl() {
        let _localctx = new UmlGroupTitleStrReplContext(this._ctx, this.state);
        this.enterRule(_localctx, 54, ESSL_Parser.RULE_umlGroupTitleStrRepl);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 771;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    umlStrReplOldValue() {
        let _localctx = new UmlStrReplOldValueContext(this._ctx, this.state);
        this.enterRule(_localctx, 56, ESSL_Parser.RULE_umlStrReplOldValue);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 773;
                this.match(ESSL_Parser.QUOTED_STR);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    umlStrReplNewValue() {
        let _localctx = new UmlStrReplNewValueContext(this._ctx, this.state);
        this.enterRule(_localctx, 58, ESSL_Parser.RULE_umlStrReplNewValue);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 775;
                this.match(ESSL_Parser.QUOTED_STR);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    umlImageDecor() {
        let _localctx = new UmlImageDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 60, ESSL_Parser.RULE_umlImageDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 777;
                this.match(ESSL_Parser.T__20);
                this.state = 778;
                this.umlImageParams();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    umlImageParams() {
        let _localctx = new UmlImageParamsContext(this._ctx, this.state);
        this.enterRule(_localctx, 62, ESSL_Parser.RULE_umlImageParams);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 780;
                this.match(ESSL_Parser.T__14);
                this.state = 781;
                this.umlGroupImageName();
                this.state = 784;
                this._errHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this._input, 19, this._ctx)) {
                    case 1:
                        {
                            this.state = 782;
                            this.match(ESSL_Parser.T__15);
                            this.state = 783;
                            this.umlImageFormat();
                        }
                        break;
                }
                this.state = 788;
                this._errHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this._input, 20, this._ctx)) {
                    case 1:
                        {
                            this.state = 786;
                            this.match(ESSL_Parser.T__15);
                            this.state = 787;
                            this.umlImageRemoveTitle();
                        }
                        break;
                }
                this.state = 792;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__15) {
                    {
                        this.state = 790;
                        this.match(ESSL_Parser.T__15);
                        this.state = 791;
                        this.umlImageSubFolder();
                    }
                }
                this.state = 794;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    umlGroupImageName() {
        let _localctx = new UmlGroupImageNameContext(this._ctx, this.state);
        this.enterRule(_localctx, 64, ESSL_Parser.RULE_umlGroupImageName);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 796;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    umlImageFormat() {
        let _localctx = new UmlImageFormatContext(this._ctx, this.state);
        this.enterRule(_localctx, 66, ESSL_Parser.RULE_umlImageFormat);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 798;
                _la = this._input.LA(1);
                if (!(_la === ESSL_Parser.T__21 || _la === ESSL_Parser.T__22)) {
                    this._errHandler.recoverInline(this);
                }
                else {
                    if (this._input.LA(1) === Token_1.Token.EOF) {
                        this.matchedEOF = true;
                    }
                    this._errHandler.reportMatch(this);
                    this.consume();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    umlImageRemoveTitle() {
        let _localctx = new UmlImageRemoveTitleContext(this._ctx, this.state);
        this.enterRule(_localctx, 68, ESSL_Parser.RULE_umlImageRemoveTitle);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 800;
                this.boolValue();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    umlImageSubFolder() {
        let _localctx = new UmlImageSubFolderContext(this._ctx, this.state);
        this.enterRule(_localctx, 70, ESSL_Parser.RULE_umlImageSubFolder);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 802;
                this.match(ESSL_Parser.QUOTED_STR);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    entityType() {
        let _localctx = new EntityTypeContext(this._ctx, this.state);
        this.enterRule(_localctx, 72, ESSL_Parser.RULE_entityType);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 804;
                this.beforeComment();
                this.state = 805;
                this.match(ESSL_Parser.T__23);
                this.state = 806;
                this.extendsList();
                this.state = 807;
                this.typeBlock();
                this.state = 808;
                this.entityTypeDecors();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    valueType() {
        let _localctx = new ValueTypeContext(this._ctx, this.state);
        this.enterRule(_localctx, 74, ESSL_Parser.RULE_valueType);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 810;
                this.beforeComment();
                this.state = 811;
                this.match(ESSL_Parser.T__23);
                this.state = 812;
                this.name();
                this.state = 813;
                this.extendsList();
                this.state = 814;
                this.typeBlock();
                this.state = 815;
                this.valueTypeDecors();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    interfaceType() {
        let _localctx = new InterfaceTypeContext(this._ctx, this.state);
        this.enterRule(_localctx, 76, ESSL_Parser.RULE_interfaceType);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 817;
                this.beforeComment();
                this.state = 818;
                this.match(ESSL_Parser.T__24);
                this.state = 819;
                this.name();
                this.state = 820;
                this.extendsList();
                this.state = 821;
                this.possiblyEmptyTypeBlock();
                this.state = 822;
                this.interfaceTypeDecors();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    name() {
        let _localctx = new NameContext(this._ctx, this.state);
        this.enterRule(_localctx, 78, ESSL_Parser.RULE_name);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 824;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    extendsList() {
        let _localctx = new ExtendsListContext(this._ctx, this.state);
        this.enterRule(_localctx, 80, ESSL_Parser.RULE_extendsList);
        let _la;
        try {
            let _alt;
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 835;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__4) {
                    {
                        this.state = 826;
                        this.match(ESSL_Parser.T__4);
                        this.state = 827;
                        this.baseName();
                        this.state = 832;
                        this._errHandler.sync(this);
                        _alt = this.interpreter.adaptivePredict(this._input, 22, this._ctx);
                        while (_alt !== 2 && _alt !== ATN_1.ATN.INVALID_ALT_NUMBER) {
                            if (_alt === 1) {
                                {
                                    {
                                        this.state = 828;
                                        this.match(ESSL_Parser.T__15);
                                        this.state = 829;
                                        this.baseName();
                                    }
                                }
                            }
                            this.state = 834;
                            this._errHandler.sync(this);
                            _alt = this.interpreter.adaptivePredict(this._input, 22, this._ctx);
                        }
                    }
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    baseName() {
        let _localctx = new BaseNameContext(this._ctx, this.state);
        this.enterRule(_localctx, 82, ESSL_Parser.RULE_baseName);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 837;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    typeBlock() {
        let _localctx = new TypeBlockContext(this._ctx, this.state);
        this.enterRule(_localctx, 84, ESSL_Parser.RULE_typeBlock);
        try {
            let _alt;
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 839;
                this.match(ESSL_Parser.T__1);
                this.state = 841;
                this._errHandler.sync(this);
                _alt = 1;
                do {
                    switch (_alt) {
                        case 1:
                            {
                                {
                                    this.state = 840;
                                    this.fieldDef();
                                }
                            }
                            break;
                        default:
                            throw new NoViableAltException_1.NoViableAltException(this);
                    }
                    this.state = 843;
                    this._errHandler.sync(this);
                    _alt = this.interpreter.adaptivePredict(this._input, 24, this._ctx);
                } while (_alt !== 2 && _alt !== ATN_1.ATN.INVALID_ALT_NUMBER);
                this.state = 845;
                this.afterComment();
                this.state = 846;
                this.match(ESSL_Parser.T__2);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    possiblyEmptyTypeBlock() {
        let _localctx = new PossiblyEmptyTypeBlockContext(this._ctx, this.state);
        this.enterRule(_localctx, 86, ESSL_Parser.RULE_possiblyEmptyTypeBlock);
        try {
            let _alt;
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 848;
                this.match(ESSL_Parser.T__1);
                this.state = 852;
                this._errHandler.sync(this);
                _alt = this.interpreter.adaptivePredict(this._input, 25, this._ctx);
                while (_alt !== 2 && _alt !== ATN_1.ATN.INVALID_ALT_NUMBER) {
                    if (_alt === 1) {
                        {
                            {
                                this.state = 849;
                                this.fieldDef();
                            }
                        }
                    }
                    this.state = 854;
                    this._errHandler.sync(this);
                    _alt = this.interpreter.adaptivePredict(this._input, 25, this._ctx);
                }
                this.state = 855;
                this.afterComment();
                this.state = 856;
                this.match(ESSL_Parser.T__2);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    entityTypeDecors() {
        let _localctx = new EntityTypeDecorsContext(this._ctx, this.state);
        this.enterRule(_localctx, 88, ESSL_Parser.RULE_entityTypeDecors);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 863;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__8) {
                    {
                        {
                            this.state = 858;
                            this.entityTypeDecor();
                            this.state = 859;
                            this.esslComment();
                        }
                    }
                    this.state = 865;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    entityTypeDecor() {
        let _localctx = new EntityTypeDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 90, ESSL_Parser.RULE_entityTypeDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 866;
                this.match(ESSL_Parser.T__8);
                this.state = 893;
                this._errHandler.sync(this);
                switch (this._input.LA(1)) {
                    case ESSL_Parser.T__56:
                        {
                            this.state = 867;
                            this.partial();
                        }
                        break;
                    case ESSL_Parser.T__25:
                        {
                            this.state = 868;
                            this.partialCommand();
                        }
                        break;
                    case ESSL_Parser.T__26:
                        {
                            this.state = 869;
                            this.transCoordinator();
                        }
                        break;
                    case ESSL_Parser.T__27:
                        {
                            this.state = 870;
                            this.defaultConstructor();
                        }
                        break;
                    case ESSL_Parser.T__28:
                        {
                            this.state = 871;
                            this.indexTypeDecor();
                        }
                        break;
                    case ESSL_Parser.T__29:
                        {
                            this.state = 872;
                            this.uniqueIndexTypeDecor();
                        }
                        break;
                    case ESSL_Parser.T__30:
                        {
                            this.state = 873;
                            this.queryPartitionDecor();
                        }
                        break;
                    case ESSL_Parser.T__31:
                        {
                            this.state = 874;
                            this.crossPartitionDecor();
                        }
                        break;
                    case ESSL_Parser.T__32:
                        {
                            this.state = 875;
                            this.umlGroupDecor();
                        }
                        break;
                    case ESSL_Parser.T__58:
                        {
                            this.state = 876;
                            this.webValueString();
                        }
                        break;
                    case ESSL_Parser.T__33:
                        {
                            this.state = 877;
                            this.hashLookupDecor();
                        }
                        break;
                    case ESSL_Parser.T__34:
                        {
                            this.state = 878;
                            this.beforeEventPersist();
                        }
                        break;
                    case ESSL_Parser.T__35:
                        {
                            this.state = 879;
                            this.authPolicyDecor();
                        }
                        break;
                    case ESSL_Parser.T__47:
                        {
                            this.state = 880;
                            this.styleDecor();
                        }
                        break;
                    case ESSL_Parser.T__59:
                        {
                            this.state = 881;
                            this.discriminatedBy();
                        }
                        break;
                    case ESSL_Parser.T__37:
                        {
                            this.state = 882;
                            this.pluralNameDecor();
                        }
                        break;
                    case ESSL_Parser.T__38:
                        {
                            this.state = 883;
                            this.tableNameDecor();
                        }
                        break;
                    case ESSL_Parser.T__39:
                        {
                            this.state = 884;
                            this.graphQLNameDecor();
                        }
                        break;
                    case ESSL_Parser.T__40:
                        {
                            this.state = 885;
                            this.graphQLPluralNameDecor();
                        }
                        break;
                    case ESSL_Parser.T__41:
                        {
                            this.state = 886;
                            this.graphQLCamelCaseDecor();
                        }
                        break;
                    case ESSL_Parser.T__42:
                        {
                            this.state = 887;
                            this.graphQLPluralCamelCaseDecor();
                        }
                        break;
                    case ESSL_Parser.T__43:
                        {
                            this.state = 888;
                            this.base64RefIdsDecor();
                        }
                        break;
                    case ESSL_Parser.T__24:
                        {
                            this.state = 889;
                            this.interfaceDecor();
                        }
                        break;
                    case ESSL_Parser.T__44:
                        {
                            this.state = 890;
                            this.camelCaseDecor();
                        }
                        break;
                    case ESSL_Parser.T__45:
                        {
                            this.state = 891;
                            this.generateIfDecor();
                        }
                        break;
                    case ESSL_Parser.T__141:
                        {
                            this.state = 892;
                            this.attributeDecor();
                        }
                        break;
                    default:
                        throw new NoViableAltException_1.NoViableAltException(this);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    partialCommand() {
        let _localctx = new PartialCommandContext(this._ctx, this.state);
        this.enterRule(_localctx, 92, ESSL_Parser.RULE_partialCommand);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 895;
                this.match(ESSL_Parser.T__25);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    transCoordinator() {
        let _localctx = new TransCoordinatorContext(this._ctx, this.state);
        this.enterRule(_localctx, 94, ESSL_Parser.RULE_transCoordinator);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 897;
                this.match(ESSL_Parser.T__26);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    defaultConstructor() {
        let _localctx = new DefaultConstructorContext(this._ctx, this.state);
        this.enterRule(_localctx, 96, ESSL_Parser.RULE_defaultConstructor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 899;
                this.match(ESSL_Parser.T__27);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    indexTypeDecor() {
        let _localctx = new IndexTypeDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 98, ESSL_Parser.RULE_indexTypeDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 901;
                this.match(ESSL_Parser.T__28);
                this.state = 902;
                this.indexTypeAttrs();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    uniqueIndexTypeDecor() {
        let _localctx = new UniqueIndexTypeDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 100, ESSL_Parser.RULE_uniqueIndexTypeDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 904;
                this.match(ESSL_Parser.T__29);
                this.state = 905;
                this.indexTypeAttrs();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    indexTypeAttrs() {
        let _localctx = new IndexTypeAttrsContext(this._ctx, this.state);
        this.enterRule(_localctx, 102, ESSL_Parser.RULE_indexTypeAttrs);
        let _la;
        try {
            let _alt;
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 907;
                this.match(ESSL_Parser.T__14);
                this.state = 908;
                this.name();
                this.state = 917;
                this._errHandler.sync(this);
                _alt = 1;
                do {
                    switch (_alt) {
                        case 1:
                            {
                                {
                                    this.state = 909;
                                    this.match(ESSL_Parser.T__15);
                                    this.state = 910;
                                    this.indexedField();
                                    this.state = 915;
                                    this._errHandler.sync(this);
                                    _la = this._input.LA(1);
                                    if (_la === ESSL_Parser.T__14) {
                                        {
                                            this.state = 911;
                                            this.match(ESSL_Parser.T__14);
                                            this.state = 912;
                                            this.indexKeyAttrs();
                                            this.state = 913;
                                            this.match(ESSL_Parser.T__16);
                                        }
                                    }
                                }
                            }
                            break;
                        default:
                            throw new NoViableAltException_1.NoViableAltException(this);
                    }
                    this.state = 919;
                    this._errHandler.sync(this);
                    _alt = this.interpreter.adaptivePredict(this._input, 29, this._ctx);
                } while (_alt !== 2 && _alt !== ATN_1.ATN.INVALID_ALT_NUMBER);
                this.state = 922;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__15) {
                    {
                        this.state = 921;
                        this.indexAttrDecors();
                    }
                }
                this.state = 924;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    indexKeyAttrs() {
        let _localctx = new IndexKeyAttrsContext(this._ctx, this.state);
        this.enterRule(_localctx, 104, ESSL_Parser.RULE_indexKeyAttrs);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                {
                    this.state = 926;
                    this.indexKeyAttr();
                    this.state = 929;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                    if (_la === ESSL_Parser.T__15) {
                        {
                            this.state = 927;
                            this.match(ESSL_Parser.T__15);
                            this.state = 928;
                            this.indexKeyAttr();
                        }
                    }
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    indexKeyAttr() {
        let _localctx = new IndexKeyAttrContext(this._ctx, this.state);
        this.enterRule(_localctx, 106, ESSL_Parser.RULE_indexKeyAttr);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 935;
                this._errHandler.sync(this);
                switch (this._input.LA(1)) {
                    case ESSL_Parser.T__89:
                    case ESSL_Parser.T__90:
                        {
                            this.state = 931;
                            this.caseInsensitive();
                        }
                        break;
                    case ESSL_Parser.T__91:
                    case ESSL_Parser.T__92:
                        {
                            this.state = 932;
                            this.nullHandling();
                        }
                        break;
                    case ESSL_Parser.T__46:
                        {
                            this.state = 933;
                            this.dimensionNameDecor();
                        }
                        break;
                    case ESSL_Parser.T__143:
                        {
                            this.state = 934;
                            this.dimensionKeyDecor();
                        }
                        break;
                    default:
                        throw new NoViableAltException_1.NoViableAltException(this);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    indexedField() {
        let _localctx = new IndexedFieldContext(this._ctx, this.state);
        this.enterRule(_localctx, 108, ESSL_Parser.RULE_indexedField);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 937;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    queryPartitionDecor() {
        let _localctx = new QueryPartitionDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 110, ESSL_Parser.RULE_queryPartitionDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 939;
                this.match(ESSL_Parser.T__30);
                this.state = 940;
                this.queryPartitionAttrs();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    queryPartitionAttrs() {
        let _localctx = new QueryPartitionAttrsContext(this._ctx, this.state);
        this.enterRule(_localctx, 112, ESSL_Parser.RULE_queryPartitionAttrs);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 942;
                this.match(ESSL_Parser.T__14);
                this.state = 943;
                this.partitionIndex();
                this.state = 944;
                this.match(ESSL_Parser.T__15);
                this.state = 945;
                this.partitionTypeUniqueIndex();
                this.state = 946;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    partitionIndex() {
        let _localctx = new PartitionIndexContext(this._ctx, this.state);
        this.enterRule(_localctx, 114, ESSL_Parser.RULE_partitionIndex);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 948;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    partitionTypeUniqueIndex() {
        let _localctx = new PartitionTypeUniqueIndexContext(this._ctx, this.state);
        this.enterRule(_localctx, 116, ESSL_Parser.RULE_partitionTypeUniqueIndex);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 950;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    crossPartitionDecor() {
        let _localctx = new CrossPartitionDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 118, ESSL_Parser.RULE_crossPartitionDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 952;
                this.match(ESSL_Parser.T__31);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    umlGroupDecor() {
        let _localctx = new UmlGroupDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 120, ESSL_Parser.RULE_umlGroupDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 954;
                this.match(ESSL_Parser.T__32);
                this.state = 955;
                this.umlGroup();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    umlGroup() {
        let _localctx = new UmlGroupContext(this._ctx, this.state);
        this.enterRule(_localctx, 122, ESSL_Parser.RULE_umlGroup);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 957;
                this.match(ESSL_Parser.T__14);
                this.state = 958;
                this.umlGroupTitle();
                this.state = 961;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__15) {
                    {
                        this.state = 959;
                        this.match(ESSL_Parser.T__15);
                        this.state = 960;
                        this.umlSubGroup();
                    }
                }
                this.state = 963;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    umlGroupTitle() {
        let _localctx = new UmlGroupTitleContext(this._ctx, this.state);
        this.enterRule(_localctx, 124, ESSL_Parser.RULE_umlGroupTitle);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 965;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    umlSubGroup() {
        let _localctx = new UmlSubGroupContext(this._ctx, this.state);
        this.enterRule(_localctx, 126, ESSL_Parser.RULE_umlSubGroup);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 967;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    hashLookupDecor() {
        let _localctx = new HashLookupDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 128, ESSL_Parser.RULE_hashLookupDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 969;
                this.match(ESSL_Parser.T__33);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    beforeEventPersist() {
        let _localctx = new BeforeEventPersistContext(this._ctx, this.state);
        this.enterRule(_localctx, 130, ESSL_Parser.RULE_beforeEventPersist);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 971;
                this.match(ESSL_Parser.T__34);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    authPolicyName() {
        let _localctx = new AuthPolicyNameContext(this._ctx, this.state);
        this.enterRule(_localctx, 132, ESSL_Parser.RULE_authPolicyName);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 973;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    authPolicyDecor() {
        let _localctx = new AuthPolicyDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 134, ESSL_Parser.RULE_authPolicyDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 975;
                this.match(ESSL_Parser.T__35);
                this.state = 976;
                this.authPolicyNames();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    publicDecor() {
        let _localctx = new PublicDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 136, ESSL_Parser.RULE_publicDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 978;
                this.match(ESSL_Parser.T__36);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    pluralNameDecor() {
        let _localctx = new PluralNameDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 138, ESSL_Parser.RULE_pluralNameDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 980;
                this.match(ESSL_Parser.T__37);
                this.state = 981;
                this.match(ESSL_Parser.T__14);
                this.state = 982;
                this.pluralName();
                this.state = 983;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    pluralName() {
        let _localctx = new PluralNameContext(this._ctx, this.state);
        this.enterRule(_localctx, 140, ESSL_Parser.RULE_pluralName);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 985;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    tableNameDecor() {
        let _localctx = new TableNameDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 142, ESSL_Parser.RULE_tableNameDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 987;
                this.match(ESSL_Parser.T__38);
                this.state = 988;
                this.match(ESSL_Parser.T__14);
                this.state = 989;
                this.tableName();
                this.state = 990;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    tableName() {
        let _localctx = new TableNameContext(this._ctx, this.state);
        this.enterRule(_localctx, 144, ESSL_Parser.RULE_tableName);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 992;
                this.dottedId();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    graphQLNameDecor() {
        let _localctx = new GraphQLNameDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 146, ESSL_Parser.RULE_graphQLNameDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 994;
                this.match(ESSL_Parser.T__39);
                this.state = 995;
                this.match(ESSL_Parser.T__14);
                this.state = 996;
                this.graphQLName();
                this.state = 997;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    graphQLName() {
        let _localctx = new GraphQLNameContext(this._ctx, this.state);
        this.enterRule(_localctx, 148, ESSL_Parser.RULE_graphQLName);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 999;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    graphQLPluralNameDecor() {
        let _localctx = new GraphQLPluralNameDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 150, ESSL_Parser.RULE_graphQLPluralNameDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1001;
                this.match(ESSL_Parser.T__40);
                this.state = 1002;
                this.match(ESSL_Parser.T__14);
                this.state = 1003;
                this.graphQLPluralName();
                this.state = 1004;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    graphQLPluralName() {
        let _localctx = new GraphQLPluralNameContext(this._ctx, this.state);
        this.enterRule(_localctx, 152, ESSL_Parser.RULE_graphQLPluralName);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1006;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    graphQLCamelCaseDecor() {
        let _localctx = new GraphQLCamelCaseDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 154, ESSL_Parser.RULE_graphQLCamelCaseDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1008;
                this.match(ESSL_Parser.T__41);
                this.state = 1009;
                this.match(ESSL_Parser.T__14);
                this.state = 1010;
                this.graphQLCamelCase();
                this.state = 1011;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    graphQLCamelCase() {
        let _localctx = new GraphQLCamelCaseContext(this._ctx, this.state);
        this.enterRule(_localctx, 156, ESSL_Parser.RULE_graphQLCamelCase);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1013;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    graphQLPluralCamelCaseDecor() {
        let _localctx = new GraphQLPluralCamelCaseDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 158, ESSL_Parser.RULE_graphQLPluralCamelCaseDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1015;
                this.match(ESSL_Parser.T__42);
                this.state = 1016;
                this.match(ESSL_Parser.T__14);
                this.state = 1017;
                this.graphQLPluralCamelCase();
                this.state = 1018;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    graphQLPluralCamelCase() {
        let _localctx = new GraphQLPluralCamelCaseContext(this._ctx, this.state);
        this.enterRule(_localctx, 160, ESSL_Parser.RULE_graphQLPluralCamelCase);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1020;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    base64RefIdsDecor() {
        let _localctx = new Base64RefIdsDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 162, ESSL_Parser.RULE_base64RefIdsDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1022;
                this.match(ESSL_Parser.T__43);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    interfaceDecor() {
        let _localctx = new InterfaceDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 164, ESSL_Parser.RULE_interfaceDecor);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1024;
                this.match(ESSL_Parser.T__24);
                this.state = 1025;
                this.match(ESSL_Parser.T__14);
                this.state = 1026;
                this.interfaceName();
                this.state = 1027;
                this.extendsList();
                this.state = 1030;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__15) {
                    {
                        this.state = 1028;
                        this.match(ESSL_Parser.T__15);
                        this.state = 1029;
                        this.umlGroup();
                    }
                }
                this.state = 1032;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    interfaceName() {
        let _localctx = new InterfaceNameContext(this._ctx, this.state);
        this.enterRule(_localctx, 166, ESSL_Parser.RULE_interfaceName);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1034;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    camelCaseDecor() {
        let _localctx = new CamelCaseDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 168, ESSL_Parser.RULE_camelCaseDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1036;
                this.match(ESSL_Parser.T__44);
                this.state = 1037;
                this.match(ESSL_Parser.T__14);
                this.state = 1038;
                this.camelCaseName();
                this.state = 1039;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    camelCaseName() {
        let _localctx = new CamelCaseNameContext(this._ctx, this.state);
        this.enterRule(_localctx, 170, ESSL_Parser.RULE_camelCaseName);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1041;
                this.match(ESSL_Parser.CAMELCASE_IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    generateIfDecor() {
        let _localctx = new GenerateIfDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 172, ESSL_Parser.RULE_generateIfDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1043;
                this.match(ESSL_Parser.T__45);
                this.state = 1044;
                this.match(ESSL_Parser.T__14);
                this.state = 1045;
                this.generateIfName();
                this.state = 1046;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    generateIfName() {
        let _localctx = new GenerateIfNameContext(this._ctx, this.state);
        this.enterRule(_localctx, 174, ESSL_Parser.RULE_generateIfName);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1048;
                _la = this._input.LA(1);
                if (!(_la === ESSL_Parser.IDENTIFIER || _la === ESSL_Parser.CAMELCASE_IDENTIFIER)) {
                    this._errHandler.recoverInline(this);
                }
                else {
                    if (this._input.LA(1) === Token_1.Token.EOF) {
                        this.matchedEOF = true;
                    }
                    this._errHandler.reportMatch(this);
                    this.consume();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    dimensionNameDecor() {
        let _localctx = new DimensionNameDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 176, ESSL_Parser.RULE_dimensionNameDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1050;
                this.match(ESSL_Parser.T__46);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    styleDecor() {
        let _localctx = new StyleDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 178, ESSL_Parser.RULE_styleDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1052;
                this.match(ESSL_Parser.T__47);
                this.state = 1053;
                this.match(ESSL_Parser.T__14);
                this.state = 1058;
                this._errHandler.sync(this);
                switch (this._input.LA(1)) {
                    case ESSL_Parser.T__48:
                        {
                            this.state = 1054;
                            this.events();
                        }
                        break;
                    case ESSL_Parser.T__49:
                        {
                            this.state = 1055;
                            this.slowlyChanging();
                        }
                        break;
                    case ESSL_Parser.T__50:
                        {
                            this.state = 1056;
                            this.ledger();
                        }
                        break;
                    case ESSL_Parser.T__51:
                        {
                            this.state = 1057;
                            this.factLedger();
                        }
                        break;
                    default:
                        throw new NoViableAltException_1.NoViableAltException(this);
                }
                this.state = 1060;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    events() {
        let _localctx = new EventsContext(this._ctx, this.state);
        this.enterRule(_localctx, 180, ESSL_Parser.RULE_events);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1062;
                this.match(ESSL_Parser.T__48);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    slowlyChanging() {
        let _localctx = new SlowlyChangingContext(this._ctx, this.state);
        this.enterRule(_localctx, 182, ESSL_Parser.RULE_slowlyChanging);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1064;
                this.match(ESSL_Parser.T__49);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    ledger() {
        let _localctx = new LedgerContext(this._ctx, this.state);
        this.enterRule(_localctx, 184, ESSL_Parser.RULE_ledger);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1066;
                this.match(ESSL_Parser.T__50);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    factLedger() {
        let _localctx = new FactLedgerContext(this._ctx, this.state);
        this.enterRule(_localctx, 186, ESSL_Parser.RULE_factLedger);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1068;
                this.match(ESSL_Parser.T__51);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    indexAttrDecors() {
        let _localctx = new IndexAttrDecorsContext(this._ctx, this.state);
        this.enterRule(_localctx, 188, ESSL_Parser.RULE_indexAttrDecors);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1070;
                this.match(ESSL_Parser.T__15);
                this.state = 1074;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__8) {
                    {
                        {
                            this.state = 1071;
                            this.indexAttrDecor();
                        }
                    }
                    this.state = 1076;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    indexAttrDecor() {
        let _localctx = new IndexAttrDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 190, ESSL_Parser.RULE_indexAttrDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1077;
                this.match(ESSL_Parser.T__8);
                this.state = 1082;
                this._errHandler.sync(this);
                switch (this._input.LA(1)) {
                    case ESSL_Parser.T__52:
                        {
                            this.state = 1078;
                            this.cacheModel();
                        }
                        break;
                    case ESSL_Parser.T__53:
                        {
                            this.state = 1079;
                            this.noCacheModel();
                        }
                        break;
                    case ESSL_Parser.T__54:
                        {
                            this.state = 1080;
                            this.allowTemporal();
                        }
                        break;
                    case ESSL_Parser.T__55:
                        {
                            this.state = 1081;
                            this.indexNameDecor();
                        }
                        break;
                    default:
                        throw new NoViableAltException_1.NoViableAltException(this);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    cacheModel() {
        let _localctx = new CacheModelContext(this._ctx, this.state);
        this.enterRule(_localctx, 192, ESSL_Parser.RULE_cacheModel);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1084;
                this.match(ESSL_Parser.T__52);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    noCacheModel() {
        let _localctx = new NoCacheModelContext(this._ctx, this.state);
        this.enterRule(_localctx, 194, ESSL_Parser.RULE_noCacheModel);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1086;
                this.match(ESSL_Parser.T__53);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    allowTemporal() {
        let _localctx = new AllowTemporalContext(this._ctx, this.state);
        this.enterRule(_localctx, 196, ESSL_Parser.RULE_allowTemporal);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1088;
                this.match(ESSL_Parser.T__54);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    indexNameDecor() {
        let _localctx = new IndexNameDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 198, ESSL_Parser.RULE_indexNameDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1090;
                this.match(ESSL_Parser.T__55);
                this.state = 1091;
                this.match(ESSL_Parser.T__14);
                this.state = 1092;
                this.indexName();
                this.state = 1093;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    indexName() {
        let _localctx = new IndexNameContext(this._ctx, this.state);
        this.enterRule(_localctx, 200, ESSL_Parser.RULE_indexName);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1095;
                this.dottedId();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    valueTypeDecors() {
        let _localctx = new ValueTypeDecorsContext(this._ctx, this.state);
        this.enterRule(_localctx, 202, ESSL_Parser.RULE_valueTypeDecors);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1102;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__8) {
                    {
                        {
                            this.state = 1097;
                            this.valueTypeDecor();
                            this.state = 1098;
                            this.esslComment();
                        }
                    }
                    this.state = 1104;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    valueTypeDecor() {
        let _localctx = new ValueTypeDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 204, ESSL_Parser.RULE_valueTypeDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1105;
                this.match(ESSL_Parser.T__8);
                this.state = 1122;
                this._errHandler.sync(this);
                switch (this._input.LA(1)) {
                    case ESSL_Parser.T__56:
                        {
                            this.state = 1106;
                            this.partial();
                        }
                        break;
                    case ESSL_Parser.T__27:
                        {
                            this.state = 1107;
                            this.defaultConstructor();
                        }
                        break;
                    case ESSL_Parser.T__57:
                        {
                            this.state = 1108;
                            this.jsonConstructor();
                        }
                        break;
                    case ESSL_Parser.T__58:
                        {
                            this.state = 1109;
                            this.webValueString();
                        }
                        break;
                    case ESSL_Parser.T__32:
                        {
                            this.state = 1110;
                            this.umlGroupDecor();
                        }
                        break;
                    case ESSL_Parser.T__64:
                        {
                            this.state = 1111;
                            this.suppressDecor();
                        }
                        break;
                    case ESSL_Parser.T__35:
                        {
                            this.state = 1112;
                            this.authPolicyDecor();
                        }
                        break;
                    case ESSL_Parser.T__59:
                        {
                            this.state = 1113;
                            this.discriminatedBy();
                        }
                        break;
                    case ESSL_Parser.T__43:
                        {
                            this.state = 1114;
                            this.base64RefIdsDecor();
                        }
                        break;
                    case ESSL_Parser.T__60:
                        {
                            this.state = 1115;
                            this.outputType();
                        }
                        break;
                    case ESSL_Parser.T__24:
                        {
                            this.state = 1116;
                            this.interfaceDecor();
                        }
                        break;
                    case ESSL_Parser.T__44:
                        {
                            this.state = 1117;
                            this.camelCaseDecor();
                        }
                        break;
                    case ESSL_Parser.T__45:
                        {
                            this.state = 1118;
                            this.generateIfDecor();
                        }
                        break;
                    case ESSL_Parser.T__141:
                        {
                            this.state = 1119;
                            this.attributeDecor();
                        }
                        break;
                    case ESSL_Parser.T__46:
                        {
                            this.state = 1120;
                            this.factDimensionDecor();
                        }
                        break;
                    case ESSL_Parser.T__61:
                        {
                            this.state = 1121;
                            this.voidPrimaryFact();
                        }
                        break;
                    default:
                        throw new NoViableAltException_1.NoViableAltException(this);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    partial() {
        let _localctx = new PartialContext(this._ctx, this.state);
        this.enterRule(_localctx, 206, ESSL_Parser.RULE_partial);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1124;
                this.match(ESSL_Parser.T__56);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    jsonConstructor() {
        let _localctx = new JsonConstructorContext(this._ctx, this.state);
        this.enterRule(_localctx, 208, ESSL_Parser.RULE_jsonConstructor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1126;
                this.match(ESSL_Parser.T__57);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    webValueString() {
        let _localctx = new WebValueStringContext(this._ctx, this.state);
        this.enterRule(_localctx, 210, ESSL_Parser.RULE_webValueString);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1128;
                this.match(ESSL_Parser.T__58);
                this.state = 1129;
                this.match(ESSL_Parser.T__14);
                this.state = 1130;
                this.webValueMethod();
                this.state = 1131;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    webValueMethod() {
        let _localctx = new WebValueMethodContext(this._ctx, this.state);
        this.enterRule(_localctx, 212, ESSL_Parser.RULE_webValueMethod);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1133;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    discriminatedBy() {
        let _localctx = new DiscriminatedByContext(this._ctx, this.state);
        this.enterRule(_localctx, 214, ESSL_Parser.RULE_discriminatedBy);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1135;
                this.match(ESSL_Parser.T__59);
                this.state = 1136;
                this.match(ESSL_Parser.T__14);
                this.state = 1137;
                this.discriminatedByValue();
                this.state = 1138;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    discriminatedByValue() {
        let _localctx = new DiscriminatedByValueContext(this._ctx, this.state);
        this.enterRule(_localctx, 216, ESSL_Parser.RULE_discriminatedByValue);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1140;
                this.dottedId();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    outputType() {
        let _localctx = new OutputTypeContext(this._ctx, this.state);
        this.enterRule(_localctx, 218, ESSL_Parser.RULE_outputType);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1142;
                this.match(ESSL_Parser.T__60);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    voidPrimaryFact() {
        let _localctx = new VoidPrimaryFactContext(this._ctx, this.state);
        this.enterRule(_localctx, 220, ESSL_Parser.RULE_voidPrimaryFact);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1144;
                this.match(ESSL_Parser.T__61);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    interfaceTypeDecors() {
        let _localctx = new InterfaceTypeDecorsContext(this._ctx, this.state);
        this.enterRule(_localctx, 222, ESSL_Parser.RULE_interfaceTypeDecors);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1151;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__8) {
                    {
                        {
                            this.state = 1146;
                            this.interfaceTypeDecor();
                            this.state = 1147;
                            this.esslComment();
                        }
                    }
                    this.state = 1153;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    interfaceTypeDecor() {
        let _localctx = new InterfaceTypeDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 224, ESSL_Parser.RULE_interfaceTypeDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1154;
                this.match(ESSL_Parser.T__8);
                this.state = 1158;
                this._errHandler.sync(this);
                switch (this._input.LA(1)) {
                    case ESSL_Parser.T__56:
                        {
                            this.state = 1155;
                            this.partial();
                        }
                        break;
                    case ESSL_Parser.T__163:
                        {
                            this.state = 1156;
                            this.internalDecor();
                        }
                        break;
                    case ESSL_Parser.T__32:
                        {
                            this.state = 1157;
                            this.umlGroupDecor();
                        }
                        break;
                    default:
                        throw new NoViableAltException_1.NoViableAltException(this);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    enumDecl() {
        let _localctx = new EnumDeclContext(this._ctx, this.state);
        this.enterRule(_localctx, 226, ESSL_Parser.RULE_enumDecl);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1160;
                this.beforeComment();
                this.state = 1161;
                this.match(ESSL_Parser.T__62);
                this.state = 1162;
                this.name();
                this.state = 1163;
                this.enumBlock();
                this.state = 1164;
                this.enumDecors();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    enumBlock() {
        let _localctx = new EnumBlockContext(this._ctx, this.state);
        this.enterRule(_localctx, 228, ESSL_Parser.RULE_enumBlock);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1166;
                this.match(ESSL_Parser.T__1);
                this.state = 1167;
                this.enumItem();
                this.state = 1174;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while ((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << ESSL_Parser.T__0) | (1 << ESSL_Parser.T__3) | (1 << ESSL_Parser.T__15) | (1 << ESSL_Parser.T__23) | (1 << ESSL_Parser.T__24))) !== 0) || ((((_la - 49)) & ~0x1F) === 0 && ((1 << (_la - 49)) & ((1 << (ESSL_Parser.T__48 - 49)) | (1 << (ESSL_Parser.T__62 - 49)) | (1 << (ESSL_Parser.T__69 - 49)) | (1 << (ESSL_Parser.T__73 - 49)) | (1 << (ESSL_Parser.T__74 - 49)) | (1 << (ESSL_Parser.T__75 - 49)) | (1 << (ESSL_Parser.T__76 - 49)) | (1 << (ESSL_Parser.T__77 - 49)) | (1 << (ESSL_Parser.T__78 - 49)) | (1 << (ESSL_Parser.T__79 - 49)))) !== 0) || _la === ESSL_Parser.T__80 || _la === ESSL_Parser.T__145 || _la === ESSL_Parser.T__175 || ((((_la - 180)) & ~0x1F) === 0 && ((1 << (_la - 180)) & ((1 << (ESSL_Parser.T__179 - 180)) | (1 << (ESSL_Parser.BEFORE_COMMENT_LINE - 180)) | (1 << (ESSL_Parser.ESSL_COMMENT_BLOCK - 180)) | (1 << (ESSL_Parser.ESSL_COMMENT_LINE - 180)) | (1 << (ESSL_Parser.IDENTIFIER - 180)))) !== 0)) {
                    {
                        {
                            this.state = 1169;
                            this._errHandler.sync(this);
                            _la = this._input.LA(1);
                            if (_la === ESSL_Parser.T__15) {
                                {
                                    this.state = 1168;
                                    this.match(ESSL_Parser.T__15);
                                }
                            }
                            this.state = 1171;
                            this.enumItem();
                        }
                    }
                    this.state = 1176;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
                this.state = 1177;
                this.match(ESSL_Parser.T__2);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    enumItem() {
        let _localctx = new EnumItemContext(this._ctx, this.state);
        this.enterRule(_localctx, 230, ESSL_Parser.RULE_enumItem);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1179;
                this.beforeComment();
                this.state = 1180;
                this.enumValue();
                this.state = 1183;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__63) {
                    {
                        this.state = 1181;
                        this.match(ESSL_Parser.T__63);
                        this.state = 1182;
                        this.enumExplicitValue();
                    }
                }
                this.state = 1185;
                this.comment();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    enumValue() {
        let _localctx = new EnumValueContext(this._ctx, this.state);
        this.enterRule(_localctx, 232, ESSL_Parser.RULE_enumValue);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1187;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    enumExplicitValue() {
        let _localctx = new EnumExplicitValueContext(this._ctx, this.state);
        this.enterRule(_localctx, 234, ESSL_Parser.RULE_enumExplicitValue);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1189;
                this.integer();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    enumDecors() {
        let _localctx = new EnumDecorsContext(this._ctx, this.state);
        this.enterRule(_localctx, 236, ESSL_Parser.RULE_enumDecors);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1194;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__8) {
                    {
                        {
                            this.state = 1191;
                            this.enumDecor();
                        }
                    }
                    this.state = 1196;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    enumDecor() {
        let _localctx = new EnumDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 238, ESSL_Parser.RULE_enumDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1197;
                this.match(ESSL_Parser.T__8);
                this.state = 1201;
                this._errHandler.sync(this);
                switch (this._input.LA(1)) {
                    case ESSL_Parser.T__32:
                        {
                            this.state = 1198;
                            this.umlGroupDecor();
                        }
                        break;
                    case ESSL_Parser.T__64:
                        {
                            this.state = 1199;
                            this.suppressDecor();
                        }
                        break;
                    case ESSL_Parser.T__68:
                        {
                            this.state = 1200;
                            this.flagsDecor();
                        }
                        break;
                    default:
                        throw new NoViableAltException_1.NoViableAltException(this);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    suppressDecor() {
        let _localctx = new SuppressDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 240, ESSL_Parser.RULE_suppressDecor);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1203;
                this.match(ESSL_Parser.T__64);
                this.state = 1204;
                this.match(ESSL_Parser.T__14);
                this.state = 1205;
                this.suppressOption();
                this.state = 1212;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__15 || ((((_la - 66)) & ~0x1F) === 0 && ((1 << (_la - 66)) & ((1 << (ESSL_Parser.T__65 - 66)) | (1 << (ESSL_Parser.T__66 - 66)) | (1 << (ESSL_Parser.T__67 - 66)))) !== 0)) {
                    {
                        {
                            this.state = 1207;
                            this._errHandler.sync(this);
                            _la = this._input.LA(1);
                            if (_la === ESSL_Parser.T__15) {
                                {
                                    this.state = 1206;
                                    this.match(ESSL_Parser.T__15);
                                }
                            }
                            this.state = 1209;
                            this.suppressOption();
                        }
                    }
                    this.state = 1214;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
                this.state = 1215;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    suppressOption() {
        let _localctx = new SuppressOptionContext(this._ctx, this.state);
        this.enterRule(_localctx, 242, ESSL_Parser.RULE_suppressOption);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1220;
                this._errHandler.sync(this);
                switch (this._input.LA(1)) {
                    case ESSL_Parser.T__65:
                        {
                            this.state = 1217;
                            this.modelsuppressOption();
                        }
                        break;
                    case ESSL_Parser.T__66:
                        {
                            this.state = 1218;
                            this.graphQlsuppressOption();
                        }
                        break;
                    case ESSL_Parser.T__67:
                        {
                            this.state = 1219;
                            this.inputTypeOption();
                        }
                        break;
                    default:
                        throw new NoViableAltException_1.NoViableAltException(this);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    modelsuppressOption() {
        let _localctx = new ModelsuppressOptionContext(this._ctx, this.state);
        this.enterRule(_localctx, 244, ESSL_Parser.RULE_modelsuppressOption);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1222;
                this.match(ESSL_Parser.T__65);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    graphQlsuppressOption() {
        let _localctx = new GraphQlsuppressOptionContext(this._ctx, this.state);
        this.enterRule(_localctx, 246, ESSL_Parser.RULE_graphQlsuppressOption);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1224;
                this.match(ESSL_Parser.T__66);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    inputTypeOption() {
        let _localctx = new InputTypeOptionContext(this._ctx, this.state);
        this.enterRule(_localctx, 248, ESSL_Parser.RULE_inputTypeOption);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1226;
                this.match(ESSL_Parser.T__67);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    flagsDecor() {
        let _localctx = new FlagsDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 250, ESSL_Parser.RULE_flagsDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1228;
                this.match(ESSL_Parser.T__68);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    dictionary() {
        let _localctx = new DictionaryContext(this._ctx, this.state);
        this.enterRule(_localctx, 252, ESSL_Parser.RULE_dictionary);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1230;
                this.beforeComment();
                this.state = 1231;
                this.match(ESSL_Parser.T__69);
                this.state = 1232;
                this.name();
                this.state = 1233;
                this.dictionaryBlock();
                this.state = 1234;
                this.dictionaryDecors();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    dictionaryBlock() {
        let _localctx = new DictionaryBlockContext(this._ctx, this.state);
        this.enterRule(_localctx, 254, ESSL_Parser.RULE_dictionaryBlock);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1236;
                this.match(ESSL_Parser.T__1);
                this.state = 1243;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (((((_la - 71)) & ~0x1F) === 0 && ((1 << (_la - 71)) & ((1 << (ESSL_Parser.T__70 - 71)) | (1 << (ESSL_Parser.T__71 - 71)) | (1 << (ESSL_Parser.T__72 - 71)) | (1 << (ESSL_Parser.T__89 - 71)) | (1 << (ESSL_Parser.T__90 - 71)))) !== 0)) {
                    {
                        this.state = 1241;
                        this._errHandler.sync(this);
                        switch (this._input.LA(1)) {
                            case ESSL_Parser.T__89:
                            case ESSL_Parser.T__90:
                                {
                                    this.state = 1237;
                                    this.caseInsensitive();
                                }
                                break;
                            case ESSL_Parser.T__70:
                                {
                                    this.state = 1238;
                                    this.dictPartitioned();
                                }
                                break;
                            case ESSL_Parser.T__71:
                                {
                                    this.state = 1239;
                                    this.dictValueTypeDecl();
                                }
                                break;
                            case ESSL_Parser.T__72:
                                {
                                    this.state = 1240;
                                    this.dictDefaultDecl();
                                }
                                break;
                            default:
                                throw new NoViableAltException_1.NoViableAltException(this);
                        }
                    }
                    this.state = 1245;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
                this.state = 1247;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.DICTENTRIES) {
                    {
                        this.state = 1246;
                        this.dictEntries();
                    }
                }
                this.state = 1249;
                this.match(ESSL_Parser.T__2);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    dictPartitioned() {
        let _localctx = new DictPartitionedContext(this._ctx, this.state);
        this.enterRule(_localctx, 256, ESSL_Parser.RULE_dictPartitioned);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1251;
                this.match(ESSL_Parser.T__70);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    dictValueTypeDecl() {
        let _localctx = new DictValueTypeDeclContext(this._ctx, this.state);
        this.enterRule(_localctx, 258, ESSL_Parser.RULE_dictValueTypeDecl);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1253;
                this.match(ESSL_Parser.T__71);
                this.state = 1254;
                this.match(ESSL_Parser.T__14);
                this.state = 1255;
                this.dictValueType();
                this.state = 1256;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    dictValueType() {
        let _localctx = new DictValueTypeContext(this._ctx, this.state);
        this.enterRule(_localctx, 260, ESSL_Parser.RULE_dictValueType);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1258;
                this.typeRef();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    dictDefaultDecl() {
        let _localctx = new DictDefaultDeclContext(this._ctx, this.state);
        this.enterRule(_localctx, 262, ESSL_Parser.RULE_dictDefaultDecl);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1260;
                this.match(ESSL_Parser.T__72);
                this.state = 1261;
                this.match(ESSL_Parser.T__14);
                this.state = 1262;
                this.dictDefault();
                this.state = 1263;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    dictDefault() {
        let _localctx = new DictDefaultContext(this._ctx, this.state);
        this.enterRule(_localctx, 264, ESSL_Parser.RULE_dictDefault);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1265;
                _la = this._input.LA(1);
                if (!(_la === ESSL_Parser.IDENTIFIER || _la === ESSL_Parser.CAMELCASE_IDENTIFIER)) {
                    this._errHandler.recoverInline(this);
                }
                else {
                    if (this._input.LA(1) === Token_1.Token.EOF) {
                        this.matchedEOF = true;
                    }
                    this._errHandler.reportMatch(this);
                    this.consume();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    dictEntries() {
        let _localctx = new DictEntriesContext(this._ctx, this.state);
        this.enterRule(_localctx, 266, ESSL_Parser.RULE_dictEntries);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1267;
                this.match(ESSL_Parser.DICTENTRIES);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    dictionaryDecors() {
        let _localctx = new DictionaryDecorsContext(this._ctx, this.state);
        this.enterRule(_localctx, 268, ESSL_Parser.RULE_dictionaryDecors);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1272;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__8) {
                    {
                        {
                            this.state = 1269;
                            this.dictionaryDecor();
                        }
                    }
                    this.state = 1274;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    dictionaryDecor() {
        let _localctx = new DictionaryDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 270, ESSL_Parser.RULE_dictionaryDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1275;
                this.match(ESSL_Parser.T__8);
                {
                    this.state = 1276;
                    this.umlGroupDecor();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    fieldDef() {
        let _localctx = new FieldDefContext(this._ctx, this.state);
        this.enterRule(_localctx, 272, ESSL_Parser.RULE_fieldDef);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1278;
                this.beforeComment();
                this.state = 1279;
                this.typeRef();
                this.state = 1280;
                this.name();
                this.state = 1281;
                this.fieldInitializer();
                this.state = 1282;
                this.fieldDecorators();
                this.state = 1283;
                this.comment();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    typeRef() {
        let _localctx = new TypeRefContext(this._ctx, this.state);
        this.enterRule(_localctx, 274, ESSL_Parser.RULE_typeRef);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1285;
                this.typeRefName();
                this.state = 1287;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__81) {
                    {
                        this.state = 1286;
                        this.listIndicator();
                    }
                }
                this.state = 1290;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__83) {
                    {
                        this.state = 1289;
                        this.nullableIndicator();
                    }
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    typeRefName() {
        let _localctx = new TypeRefNameContext(this._ctx, this.state);
        this.enterRule(_localctx, 276, ESSL_Parser.RULE_typeRefName);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1294;
                this._errHandler.sync(this);
                switch (this._input.LA(1)) {
                    case ESSL_Parser.T__73:
                    case ESSL_Parser.T__74:
                    case ESSL_Parser.T__75:
                    case ESSL_Parser.T__76:
                    case ESSL_Parser.T__77:
                    case ESSL_Parser.T__78:
                    case ESSL_Parser.T__79:
                    case ESSL_Parser.T__80:
                        {
                            this.state = 1292;
                            this.primitiveType();
                        }
                        break;
                    case ESSL_Parser.IDENTIFIER:
                        {
                            this.state = 1293;
                            this.valueTypeRef();
                        }
                        break;
                    default:
                        throw new NoViableAltException_1.NoViableAltException(this);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    primitiveType() {
        let _localctx = new PrimitiveTypeContext(this._ctx, this.state);
        this.enterRule(_localctx, 278, ESSL_Parser.RULE_primitiveType);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1296;
                _la = this._input.LA(1);
                if (!(((((_la - 74)) & ~0x1F) === 0 && ((1 << (_la - 74)) & ((1 << (ESSL_Parser.T__73 - 74)) | (1 << (ESSL_Parser.T__74 - 74)) | (1 << (ESSL_Parser.T__75 - 74)) | (1 << (ESSL_Parser.T__76 - 74)) | (1 << (ESSL_Parser.T__77 - 74)) | (1 << (ESSL_Parser.T__78 - 74)) | (1 << (ESSL_Parser.T__79 - 74)) | (1 << (ESSL_Parser.T__80 - 74)))) !== 0))) {
                    this._errHandler.recoverInline(this);
                }
                else {
                    if (this._input.LA(1) === Token_1.Token.EOF) {
                        this.matchedEOF = true;
                    }
                    this._errHandler.reportMatch(this);
                    this.consume();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    valueTypeRef() {
        let _localctx = new ValueTypeRefContext(this._ctx, this.state);
        this.enterRule(_localctx, 280, ESSL_Parser.RULE_valueTypeRef);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1298;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    listIndicator() {
        let _localctx = new ListIndicatorContext(this._ctx, this.state);
        this.enterRule(_localctx, 282, ESSL_Parser.RULE_listIndicator);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1300;
                this.match(ESSL_Parser.T__81);
                this.state = 1301;
                this.match(ESSL_Parser.T__82);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    nullableIndicator() {
        let _localctx = new NullableIndicatorContext(this._ctx, this.state);
        this.enterRule(_localctx, 284, ESSL_Parser.RULE_nullableIndicator);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1303;
                this.match(ESSL_Parser.T__83);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    fieldInitializer() {
        let _localctx = new FieldInitializerContext(this._ctx, this.state);
        this.enterRule(_localctx, 286, ESSL_Parser.RULE_fieldInitializer);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1315;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__63) {
                    {
                        this.state = 1305;
                        this.match(ESSL_Parser.T__63);
                        this.state = 1313;
                        this._errHandler.sync(this);
                        switch (this._input.LA(1)) {
                            case ESSL_Parser.T__84:
                                {
                                    this.state = 1306;
                                    this.nullValue();
                                }
                                break;
                            case ESSL_Parser.IDENTIFIER:
                                {
                                    this.state = 1307;
                                    this.initialEnumVal();
                                }
                                break;
                            case ESSL_Parser.T__85:
                            case ESSL_Parser.T__86:
                                {
                                    this.state = 1308;
                                    this.boolValue();
                                }
                                break;
                            case ESSL_Parser.QUOTED_STR:
                                {
                                    this.state = 1309;
                                    this.stringValue();
                                }
                                break;
                            case ESSL_Parser.T__88:
                            case ESSL_Parser.POS_DEC:
                            case ESSL_Parser.POS_INT:
                                {
                                    this.state = 1310;
                                    this.numValue();
                                }
                                break;
                            case ESSL_Parser.T__87:
                                {
                                    this.state = 1311;
                                    this.newInstance();
                                }
                                break;
                            case ESSL_Parser.T__81:
                                {
                                    this.state = 1312;
                                    this.flagsEnumSet();
                                }
                                break;
                            default:
                                throw new NoViableAltException_1.NoViableAltException(this);
                        }
                    }
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    nullValue() {
        let _localctx = new NullValueContext(this._ctx, this.state);
        this.enterRule(_localctx, 288, ESSL_Parser.RULE_nullValue);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1317;
                this.match(ESSL_Parser.T__84);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    boolValue() {
        let _localctx = new BoolValueContext(this._ctx, this.state);
        this.enterRule(_localctx, 290, ESSL_Parser.RULE_boolValue);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1319;
                _la = this._input.LA(1);
                if (!(_la === ESSL_Parser.T__85 || _la === ESSL_Parser.T__86)) {
                    this._errHandler.recoverInline(this);
                }
                else {
                    if (this._input.LA(1) === Token_1.Token.EOF) {
                        this.matchedEOF = true;
                    }
                    this._errHandler.reportMatch(this);
                    this.consume();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    newInstance() {
        let _localctx = new NewInstanceContext(this._ctx, this.state);
        this.enterRule(_localctx, 292, ESSL_Parser.RULE_newInstance);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1321;
                this.match(ESSL_Parser.T__87);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    initialEnumVal() {
        let _localctx = new InitialEnumValContext(this._ctx, this.state);
        this.enterRule(_localctx, 294, ESSL_Parser.RULE_initialEnumVal);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1323;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    numValue() {
        let _localctx = new NumValueContext(this._ctx, this.state);
        this.enterRule(_localctx, 296, ESSL_Parser.RULE_numValue);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1327;
                this._errHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this._input, 59, this._ctx)) {
                    case 1:
                        {
                            this.state = 1325;
                            this.integer();
                        }
                        break;
                    case 2:
                        {
                            this.state = 1326;
                            this.decimal();
                        }
                        break;
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    integer() {
        let _localctx = new IntegerContext(this._ctx, this.state);
        this.enterRule(_localctx, 298, ESSL_Parser.RULE_integer);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1330;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__88) {
                    {
                        this.state = 1329;
                        this.match(ESSL_Parser.T__88);
                    }
                }
                this.state = 1332;
                this.match(ESSL_Parser.POS_INT);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    decimal() {
        let _localctx = new DecimalContext(this._ctx, this.state);
        this.enterRule(_localctx, 300, ESSL_Parser.RULE_decimal);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1335;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__88) {
                    {
                        this.state = 1334;
                        this.match(ESSL_Parser.T__88);
                    }
                }
                this.state = 1337;
                this.match(ESSL_Parser.POS_DEC);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    stringValue() {
        let _localctx = new StringValueContext(this._ctx, this.state);
        this.enterRule(_localctx, 302, ESSL_Parser.RULE_stringValue);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1339;
                this.match(ESSL_Parser.QUOTED_STR);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    flagsEnumSet() {
        let _localctx = new FlagsEnumSetContext(this._ctx, this.state);
        this.enterRule(_localctx, 304, ESSL_Parser.RULE_flagsEnumSet);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1341;
                this.match(ESSL_Parser.T__81);
                this.state = 1350;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.IDENTIFIER) {
                    {
                        this.state = 1342;
                        this.flagsEnumValue();
                        this.state = 1347;
                        this._errHandler.sync(this);
                        _la = this._input.LA(1);
                        while (_la === ESSL_Parser.T__15) {
                            {
                                {
                                    this.state = 1343;
                                    this.match(ESSL_Parser.T__15);
                                    this.state = 1344;
                                    this.flagsEnumValue();
                                }
                            }
                            this.state = 1349;
                            this._errHandler.sync(this);
                            _la = this._input.LA(1);
                        }
                    }
                }
                this.state = 1352;
                this.match(ESSL_Parser.T__82);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    flagsEnumValue() {
        let _localctx = new FlagsEnumValueContext(this._ctx, this.state);
        this.enterRule(_localctx, 306, ESSL_Parser.RULE_flagsEnumValue);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1354;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    fieldDecorators() {
        let _localctx = new FieldDecoratorsContext(this._ctx, this.state);
        this.enterRule(_localctx, 308, ESSL_Parser.RULE_fieldDecorators);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1361;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__8) {
                    {
                        {
                            this.state = 1356;
                            this.fieldDecorator();
                            this.state = 1357;
                            this.esslComment();
                        }
                    }
                    this.state = 1363;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    fieldDecorator() {
        let _localctx = new FieldDecoratorContext(this._ctx, this.state);
        this.enterRule(_localctx, 310, ESSL_Parser.RULE_fieldDecorator);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1364;
                this.match(ESSL_Parser.T__8);
                this.state = 1396;
                this._errHandler.sync(this);
                switch (this._input.LA(1)) {
                    case ESSL_Parser.T__28:
                        {
                            this.state = 1365;
                            this.indexDecor();
                        }
                        break;
                    case ESSL_Parser.T__29:
                        {
                            this.state = 1366;
                            this.uniqueIndexDecor();
                        }
                        break;
                    case ESSL_Parser.T__62:
                        {
                            this.state = 1367;
                            this.inlineEnumDecor();
                        }
                        break;
                    case ESSL_Parser.T__93:
                        {
                            this.state = 1368;
                            this.composedDecor();
                        }
                        break;
                    case ESSL_Parser.T__94:
                        {
                            this.state = 1369;
                            this.requiredDecor();
                        }
                        break;
                    case ESSL_Parser.T__95:
                        {
                            this.state = 1370;
                            this.readOnlyDecor();
                        }
                        break;
                    case ESSL_Parser.T__96:
                        {
                            this.state = 1371;
                            this.hiddenDecor();
                        }
                        break;
                    case ESSL_Parser.T__97:
                        {
                            this.state = 1372;
                            this.calculatedDecor();
                        }
                        break;
                    case ESSL_Parser.T__131:
                        {
                            this.state = 1373;
                            this.constantDecor();
                        }
                        break;
                    case ESSL_Parser.T__132:
                        {
                            this.state = 1374;
                            this.notPersistedDecor();
                        }
                        break;
                    case ESSL_Parser.T__98:
                        {
                            this.state = 1375;
                            this.immutableDecor();
                        }
                        break;
                    case ESSL_Parser.T__99:
                        {
                            this.state = 1376;
                            this.clonePartitionerDecor();
                        }
                        break;
                    case ESSL_Parser.T__100:
                        {
                            this.state = 1377;
                            this.cloneIdAsIsDecor();
                        }
                        break;
                    case ESSL_Parser.T__101:
                        {
                            this.state = 1378;
                            this.fieldUmlDecor();
                        }
                        break;
                    case ESSL_Parser.T__124:
                        {
                            this.state = 1379;
                            this.sectionDecor();
                        }
                        break;
                    case ESSL_Parser.T__128:
                        {
                            this.state = 1380;
                            this.asValueTypeDecor();
                        }
                        break;
                    case ESSL_Parser.T__35:
                        {
                            this.state = 1381;
                            this.authPolicyDecor();
                        }
                        break;
                    case ESSL_Parser.T__149:
                        {
                            this.state = 1382;
                            this.idDecor();
                        }
                        break;
                    case ESSL_Parser.T__129:
                        {
                            this.state = 1383;
                            this.sameAsDecor();
                        }
                        break;
                    case ESSL_Parser.T__133:
                        {
                            this.state = 1384;
                            this.handCodedDecor();
                        }
                        break;
                    case ESSL_Parser.T__69:
                        {
                            this.state = 1385;
                            this.dictDecor();
                        }
                        break;
                    case ESSL_Parser.T__134:
                        {
                            this.state = 1386;
                            this.typeDiscriminator();
                        }
                        break;
                    case ESSL_Parser.T__135:
                        {
                            this.state = 1387;
                            this.multiLineDecor();
                        }
                        break;
                    case ESSL_Parser.T__136:
                        {
                            this.state = 1388;
                            this.questionDecor();
                        }
                        break;
                    case ESSL_Parser.T__44:
                        {
                            this.state = 1389;
                            this.camelCaseDecor();
                        }
                        break;
                    case ESSL_Parser.T__137:
                        {
                            this.state = 1390;
                            this.labelDecor();
                        }
                        break;
                    case ESSL_Parser.T__138:
                        {
                            this.state = 1391;
                            this.autoFillDecor();
                        }
                        break;
                    case ESSL_Parser.T__141:
                        {
                            this.state = 1392;
                            this.attributeDecor();
                        }
                        break;
                    case ESSL_Parser.T__46:
                        {
                            this.state = 1393;
                            this.factDimensionDecor();
                        }
                        break;
                    case ESSL_Parser.T__143:
                        {
                            this.state = 1394;
                            this.dimensionKeyDecor();
                        }
                        break;
                    case ESSL_Parser.T__144:
                        {
                            this.state = 1395;
                            this.journalEntriesDecor();
                        }
                        break;
                    default:
                        throw new NoViableAltException_1.NoViableAltException(this);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    indexDecor() {
        let _localctx = new IndexDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 312, ESSL_Parser.RULE_indexDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1398;
                this.match(ESSL_Parser.T__28);
                this.state = 1399;
                this.indexAttrs();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    uniqueIndexDecor() {
        let _localctx = new UniqueIndexDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 314, ESSL_Parser.RULE_uniqueIndexDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1401;
                this.match(ESSL_Parser.T__29);
                this.state = 1402;
                this.indexAttrs();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    indexAttrs() {
        let _localctx = new IndexAttrsContext(this._ctx, this.state);
        this.enterRule(_localctx, 316, ESSL_Parser.RULE_indexAttrs);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1404;
                this.match(ESSL_Parser.T__14);
                this.state = 1405;
                this.name();
                this.state = 1408;
                this._errHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this._input, 66, this._ctx)) {
                    case 1:
                        {
                            this.state = 1406;
                            this.match(ESSL_Parser.T__15);
                            this.state = 1407;
                            this.caseInsensitive();
                        }
                        break;
                }
                this.state = 1412;
                this._errHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this._input, 67, this._ctx)) {
                    case 1:
                        {
                            this.state = 1410;
                            this.match(ESSL_Parser.T__15);
                            this.state = 1411;
                            this.nullHandling();
                        }
                        break;
                }
                this.state = 1415;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__15) {
                    {
                        this.state = 1414;
                        this.indexAttrDecors();
                    }
                }
                this.state = 1417;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    caseInsensitive() {
        let _localctx = new CaseInsensitiveContext(this._ctx, this.state);
        this.enterRule(_localctx, 318, ESSL_Parser.RULE_caseInsensitive);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1419;
                _la = this._input.LA(1);
                if (!(_la === ESSL_Parser.T__89 || _la === ESSL_Parser.T__90)) {
                    this._errHandler.recoverInline(this);
                }
                else {
                    if (this._input.LA(1) === Token_1.Token.EOF) {
                        this.matchedEOF = true;
                    }
                    this._errHandler.reportMatch(this);
                    this.consume();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    nullHandling() {
        let _localctx = new NullHandlingContext(this._ctx, this.state);
        this.enterRule(_localctx, 320, ESSL_Parser.RULE_nullHandling);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1421;
                _la = this._input.LA(1);
                if (!(_la === ESSL_Parser.T__91 || _la === ESSL_Parser.T__92)) {
                    this._errHandler.recoverInline(this);
                }
                else {
                    if (this._input.LA(1) === Token_1.Token.EOF) {
                        this.matchedEOF = true;
                    }
                    this._errHandler.reportMatch(this);
                    this.consume();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    inlineEnumDecor() {
        let _localctx = new InlineEnumDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 322, ESSL_Parser.RULE_inlineEnumDecor);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1423;
                this.match(ESSL_Parser.T__62);
                this.state = 1424;
                this.match(ESSL_Parser.T__14);
                this.state = 1425;
                this.inlineEnumValue();
                this.state = 1430;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__15) {
                    {
                        {
                            this.state = 1426;
                            this.match(ESSL_Parser.T__15);
                            this.state = 1427;
                            this.inlineEnumValue();
                        }
                    }
                    this.state = 1432;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
                this.state = 1433;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    inlineEnumValue() {
        let _localctx = new InlineEnumValueContext(this._ctx, this.state);
        this.enterRule(_localctx, 324, ESSL_Parser.RULE_inlineEnumValue);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1435;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    composedDecor() {
        let _localctx = new ComposedDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 326, ESSL_Parser.RULE_composedDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1437;
                this.match(ESSL_Parser.T__93);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    requiredDecor() {
        let _localctx = new RequiredDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 328, ESSL_Parser.RULE_requiredDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1439;
                this.match(ESSL_Parser.T__94);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    readOnlyDecor() {
        let _localctx = new ReadOnlyDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 330, ESSL_Parser.RULE_readOnlyDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1441;
                this.match(ESSL_Parser.T__95);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    hiddenDecor() {
        let _localctx = new HiddenDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 332, ESSL_Parser.RULE_hiddenDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1443;
                this.match(ESSL_Parser.T__96);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    calculatedDecor() {
        let _localctx = new CalculatedDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 334, ESSL_Parser.RULE_calculatedDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1445;
                this.match(ESSL_Parser.T__97);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    immutableDecor() {
        let _localctx = new ImmutableDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 336, ESSL_Parser.RULE_immutableDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1447;
                this.match(ESSL_Parser.T__98);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    clonePartitionerDecor() {
        let _localctx = new ClonePartitionerDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 338, ESSL_Parser.RULE_clonePartitionerDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1449;
                this.match(ESSL_Parser.T__99);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    cloneIdAsIsDecor() {
        let _localctx = new CloneIdAsIsDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 340, ESSL_Parser.RULE_cloneIdAsIsDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1451;
                this.match(ESSL_Parser.T__100);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    fieldUmlDecor() {
        let _localctx = new FieldUmlDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 342, ESSL_Parser.RULE_fieldUmlDecor);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1453;
                this.match(ESSL_Parser.T__101);
                this.state = 1454;
                this.match(ESSL_Parser.T__14);
                this.state = 1455;
                this.fieldUmlGroupTitle();
                this.state = 1463;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (((((_la - 103)) & ~0x1F) === 0 && ((1 << (_la - 103)) & ((1 << (ESSL_Parser.T__102 - 103)) | (1 << (ESSL_Parser.T__103 - 103)) | (1 << (ESSL_Parser.T__104 - 103)) | (1 << (ESSL_Parser.T__105 - 103)) | (1 << (ESSL_Parser.T__106 - 103)) | (1 << (ESSL_Parser.T__107 - 103)) | (1 << (ESSL_Parser.T__108 - 103)) | (1 << (ESSL_Parser.T__109 - 103)) | (1 << (ESSL_Parser.T__110 - 103)) | (1 << (ESSL_Parser.T__111 - 103)) | (1 << (ESSL_Parser.T__112 - 103)) | (1 << (ESSL_Parser.T__113 - 103)) | (1 << (ESSL_Parser.T__114 - 103)) | (1 << (ESSL_Parser.T__115 - 103)) | (1 << (ESSL_Parser.T__116 - 103)) | (1 << (ESSL_Parser.T__117 - 103)) | (1 << (ESSL_Parser.T__118 - 103)) | (1 << (ESSL_Parser.T__119 - 103)) | (1 << (ESSL_Parser.T__120 - 103)) | (1 << (ESSL_Parser.T__121 - 103)) | (1 << (ESSL_Parser.T__122 - 103)) | (1 << (ESSL_Parser.T__123 - 103)))) !== 0) || _la === ESSL_Parser.QUOTED_STR) {
                    {
                        this.state = 1461;
                        this._errHandler.sync(this);
                        switch (this._input.LA(1)) {
                            case ESSL_Parser.T__102:
                            case ESSL_Parser.T__103:
                            case ESSL_Parser.T__104:
                            case ESSL_Parser.T__105:
                            case ESSL_Parser.T__106:
                            case ESSL_Parser.T__107:
                            case ESSL_Parser.T__108:
                            case ESSL_Parser.T__109:
                                {
                                    this.state = 1456;
                                    this.direction();
                                }
                                break;
                            case ESSL_Parser.T__110:
                            case ESSL_Parser.T__111:
                            case ESSL_Parser.T__112:
                            case ESSL_Parser.T__113:
                                {
                                    this.state = 1457;
                                    this.horizontalVertical();
                                }
                                break;
                            case ESSL_Parser.T__114:
                            case ESSL_Parser.T__115:
                            case ESSL_Parser.T__116:
                            case ESSL_Parser.T__117:
                            case ESSL_Parser.T__118:
                            case ESSL_Parser.T__119:
                            case ESSL_Parser.T__120:
                            case ESSL_Parser.T__121:
                            case ESSL_Parser.T__122:
                                {
                                    this.state = 1458;
                                    this.lineLength();
                                }
                                break;
                            case ESSL_Parser.T__123:
                                {
                                    this.state = 1459;
                                    this.hideRelationship();
                                }
                                break;
                            case ESSL_Parser.QUOTED_STR:
                                {
                                    this.state = 1460;
                                    this.lineLabel();
                                }
                                break;
                            default:
                                throw new NoViableAltException_1.NoViableAltException(this);
                        }
                    }
                    this.state = 1465;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
                this.state = 1466;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    fieldUmlGroupTitle() {
        let _localctx = new FieldUmlGroupTitleContext(this._ctx, this.state);
        this.enterRule(_localctx, 344, ESSL_Parser.RULE_fieldUmlGroupTitle);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1468;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    direction() {
        let _localctx = new DirectionContext(this._ctx, this.state);
        this.enterRule(_localctx, 346, ESSL_Parser.RULE_direction);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1470;
                _la = this._input.LA(1);
                if (!(((((_la - 103)) & ~0x1F) === 0 && ((1 << (_la - 103)) & ((1 << (ESSL_Parser.T__102 - 103)) | (1 << (ESSL_Parser.T__103 - 103)) | (1 << (ESSL_Parser.T__104 - 103)) | (1 << (ESSL_Parser.T__105 - 103)) | (1 << (ESSL_Parser.T__106 - 103)) | (1 << (ESSL_Parser.T__107 - 103)) | (1 << (ESSL_Parser.T__108 - 103)) | (1 << (ESSL_Parser.T__109 - 103)))) !== 0))) {
                    this._errHandler.recoverInline(this);
                }
                else {
                    if (this._input.LA(1) === Token_1.Token.EOF) {
                        this.matchedEOF = true;
                    }
                    this._errHandler.reportMatch(this);
                    this.consume();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    horizontalVertical() {
        let _localctx = new HorizontalVerticalContext(this._ctx, this.state);
        this.enterRule(_localctx, 348, ESSL_Parser.RULE_horizontalVertical);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1472;
                _la = this._input.LA(1);
                if (!(((((_la - 111)) & ~0x1F) === 0 && ((1 << (_la - 111)) & ((1 << (ESSL_Parser.T__110 - 111)) | (1 << (ESSL_Parser.T__111 - 111)) | (1 << (ESSL_Parser.T__112 - 111)) | (1 << (ESSL_Parser.T__113 - 111)))) !== 0))) {
                    this._errHandler.recoverInline(this);
                }
                else {
                    if (this._input.LA(1) === Token_1.Token.EOF) {
                        this.matchedEOF = true;
                    }
                    this._errHandler.reportMatch(this);
                    this.consume();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    lineLength() {
        let _localctx = new LineLengthContext(this._ctx, this.state);
        this.enterRule(_localctx, 350, ESSL_Parser.RULE_lineLength);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1474;
                _la = this._input.LA(1);
                if (!(((((_la - 115)) & ~0x1F) === 0 && ((1 << (_la - 115)) & ((1 << (ESSL_Parser.T__114 - 115)) | (1 << (ESSL_Parser.T__115 - 115)) | (1 << (ESSL_Parser.T__116 - 115)) | (1 << (ESSL_Parser.T__117 - 115)) | (1 << (ESSL_Parser.T__118 - 115)) | (1 << (ESSL_Parser.T__119 - 115)) | (1 << (ESSL_Parser.T__120 - 115)) | (1 << (ESSL_Parser.T__121 - 115)) | (1 << (ESSL_Parser.T__122 - 115)))) !== 0))) {
                    this._errHandler.recoverInline(this);
                }
                else {
                    if (this._input.LA(1) === Token_1.Token.EOF) {
                        this.matchedEOF = true;
                    }
                    this._errHandler.reportMatch(this);
                    this.consume();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    hideRelationship() {
        let _localctx = new HideRelationshipContext(this._ctx, this.state);
        this.enterRule(_localctx, 352, ESSL_Parser.RULE_hideRelationship);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                {
                    this.state = 1476;
                    this.match(ESSL_Parser.T__123);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    lineLabel() {
        let _localctx = new LineLabelContext(this._ctx, this.state);
        this.enterRule(_localctx, 354, ESSL_Parser.RULE_lineLabel);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1478;
                this.match(ESSL_Parser.QUOTED_STR);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    sectionDecor() {
        let _localctx = new SectionDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 356, ESSL_Parser.RULE_sectionDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1480;
                this.match(ESSL_Parser.T__124);
                this.state = 1481;
                this.match(ESSL_Parser.T__14);
                this.state = 1482;
                this.breakType();
                this.state = 1483;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    breakType() {
        let _localctx = new BreakTypeContext(this._ctx, this.state);
        this.enterRule(_localctx, 358, ESSL_Parser.RULE_breakType);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1485;
                _la = this._input.LA(1);
                if (!(((((_la - 126)) & ~0x1F) === 0 && ((1 << (_la - 126)) & ((1 << (ESSL_Parser.T__125 - 126)) | (1 << (ESSL_Parser.T__126 - 126)) | (1 << (ESSL_Parser.T__127 - 126)))) !== 0))) {
                    this._errHandler.recoverInline(this);
                }
                else {
                    if (this._input.LA(1) === Token_1.Token.EOF) {
                        this.matchedEOF = true;
                    }
                    this._errHandler.reportMatch(this);
                    this.consume();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    asValueTypeDecor() {
        let _localctx = new AsValueTypeDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 360, ESSL_Parser.RULE_asValueTypeDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1487;
                this.match(ESSL_Parser.T__128);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    sameAsDecor() {
        let _localctx = new SameAsDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 362, ESSL_Parser.RULE_sameAsDecor);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1489;
                this.match(ESSL_Parser.T__129);
                this.state = 1490;
                this.match(ESSL_Parser.T__14);
                this.state = 1491;
                this.dottedId();
                this.state = 1494;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__15) {
                    {
                        this.state = 1492;
                        this.match(ESSL_Parser.T__15);
                        this.state = 1493;
                        this.sameAsPersist();
                    }
                }
                this.state = 1496;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    sameAsPersist() {
        let _localctx = new SameAsPersistContext(this._ctx, this.state);
        this.enterRule(_localctx, 364, ESSL_Parser.RULE_sameAsPersist);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1498;
                this.match(ESSL_Parser.T__130);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    constantDecor() {
        let _localctx = new ConstantDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 366, ESSL_Parser.RULE_constantDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1500;
                this.match(ESSL_Parser.T__131);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    notPersistedDecor() {
        let _localctx = new NotPersistedDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 368, ESSL_Parser.RULE_notPersistedDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1502;
                this.match(ESSL_Parser.T__132);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    handCodedDecor() {
        let _localctx = new HandCodedDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 370, ESSL_Parser.RULE_handCodedDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1504;
                this.match(ESSL_Parser.T__133);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    dictDecor() {
        let _localctx = new DictDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 372, ESSL_Parser.RULE_dictDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1506;
                this.match(ESSL_Parser.T__69);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    typeDiscriminator() {
        let _localctx = new TypeDiscriminatorContext(this._ctx, this.state);
        this.enterRule(_localctx, 374, ESSL_Parser.RULE_typeDiscriminator);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1508;
                this.match(ESSL_Parser.T__134);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    multiLineDecor() {
        let _localctx = new MultiLineDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 376, ESSL_Parser.RULE_multiLineDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1510;
                this.match(ESSL_Parser.T__135);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    questionDecor() {
        let _localctx = new QuestionDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 378, ESSL_Parser.RULE_questionDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1512;
                this.match(ESSL_Parser.T__136);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    labelDecor() {
        let _localctx = new LabelDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 380, ESSL_Parser.RULE_labelDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1514;
                this.match(ESSL_Parser.T__137);
                this.state = 1515;
                this.match(ESSL_Parser.T__14);
                this.state = 1516;
                this.label();
                this.state = 1517;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    label() {
        let _localctx = new LabelContext(this._ctx, this.state);
        this.enterRule(_localctx, 382, ESSL_Parser.RULE_label);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1519;
                this.match(ESSL_Parser.QUOTED_STR);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    autoFillDecor() {
        let _localctx = new AutoFillDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 384, ESSL_Parser.RULE_autoFillDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1521;
                this.match(ESSL_Parser.T__138);
                this.state = 1522;
                this.match(ESSL_Parser.T__14);
                this.state = 1523;
                this.autoFillType();
                this.state = 1524;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    autoFillType() {
        let _localctx = new AutoFillTypeContext(this._ctx, this.state);
        this.enterRule(_localctx, 386, ESSL_Parser.RULE_autoFillType);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1526;
                _la = this._input.LA(1);
                if (!(_la === ESSL_Parser.T__139 || _la === ESSL_Parser.T__140)) {
                    this._errHandler.recoverInline(this);
                }
                else {
                    if (this._input.LA(1) === Token_1.Token.EOF) {
                        this.matchedEOF = true;
                    }
                    this._errHandler.reportMatch(this);
                    this.consume();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    attributeDecor() {
        let _localctx = new AttributeDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 388, ESSL_Parser.RULE_attributeDecor);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1528;
                this.match(ESSL_Parser.T__141);
                this.state = 1529;
                this.match(ESSL_Parser.T__14);
                this.state = 1530;
                this.attributePair();
                this.state = 1535;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__15) {
                    {
                        {
                            this.state = 1531;
                            this.match(ESSL_Parser.T__15);
                            this.state = 1532;
                            this.attributePair();
                        }
                    }
                    this.state = 1537;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
                this.state = 1538;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    attributePair() {
        let _localctx = new AttributePairContext(this._ctx, this.state);
        this.enterRule(_localctx, 390, ESSL_Parser.RULE_attributePair);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1540;
                this.attributeKey();
                this.state = 1541;
                this.match(ESSL_Parser.T__4);
                this.state = 1542;
                this.attributeValue();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    attributeKey() {
        let _localctx = new AttributeKeyContext(this._ctx, this.state);
        this.enterRule(_localctx, 392, ESSL_Parser.RULE_attributeKey);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1544;
                this.match(ESSL_Parser.QUOTED_STR);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    attributeValue() {
        let _localctx = new AttributeValueContext(this._ctx, this.state);
        this.enterRule(_localctx, 394, ESSL_Parser.RULE_attributeValue);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1546;
                this.match(ESSL_Parser.QUOTED_STR);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    factDimensionDecor() {
        let _localctx = new FactDimensionDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 396, ESSL_Parser.RULE_factDimensionDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1548;
                this.match(ESSL_Parser.T__46);
                this.state = 1549;
                this.match(ESSL_Parser.T__14);
                this.state = 1552;
                this._errHandler.sync(this);
                switch (this._input.LA(1)) {
                    case ESSL_Parser.T__142:
                        {
                            this.state = 1550;
                            this.dimPrimary();
                        }
                        break;
                    case ESSL_Parser.POS_INT:
                        {
                            this.state = 1551;
                            this.dimInx();
                        }
                        break;
                    default:
                        throw new NoViableAltException_1.NoViableAltException(this);
                }
                this.state = 1554;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    dimPrimary() {
        let _localctx = new DimPrimaryContext(this._ctx, this.state);
        this.enterRule(_localctx, 398, ESSL_Parser.RULE_dimPrimary);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1556;
                this.match(ESSL_Parser.T__142);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    dimInx() {
        let _localctx = new DimInxContext(this._ctx, this.state);
        this.enterRule(_localctx, 400, ESSL_Parser.RULE_dimInx);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                {
                    this.state = 1558;
                    this.match(ESSL_Parser.POS_INT);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    dimensionKeyDecor() {
        let _localctx = new DimensionKeyDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 402, ESSL_Parser.RULE_dimensionKeyDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1560;
                this.match(ESSL_Parser.T__143);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    journalEntriesDecor() {
        let _localctx = new JournalEntriesDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 404, ESSL_Parser.RULE_journalEntriesDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1562;
                this.match(ESSL_Parser.T__144);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    entityEvents() {
        let _localctx = new EntityEventsContext(this._ctx, this.state);
        this.enterRule(_localctx, 406, ESSL_Parser.RULE_entityEvents);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1564;
                this.beforeComment();
                this.state = 1565;
                this.match(ESSL_Parser.T__48);
                this.state = 1566;
                this.eventsBlock();
                this.state = 1567;
                this.eventsDecors();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    eventsBlock() {
        let _localctx = new EventsBlockContext(this._ctx, this.state);
        this.enterRule(_localctx, 408, ESSL_Parser.RULE_eventsBlock);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1569;
                this.match(ESSL_Parser.T__1);
                this.state = 1570;
                this.createdEventDef();
                this.state = 1574;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while ((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << ESSL_Parser.T__0) | (1 << ESSL_Parser.T__3) | (1 << ESSL_Parser.T__23) | (1 << ESSL_Parser.T__24))) !== 0) || ((((_la - 49)) & ~0x1F) === 0 && ((1 << (_la - 49)) & ((1 << (ESSL_Parser.T__48 - 49)) | (1 << (ESSL_Parser.T__62 - 49)) | (1 << (ESSL_Parser.T__69 - 49)) | (1 << (ESSL_Parser.T__73 - 49)) | (1 << (ESSL_Parser.T__74 - 49)) | (1 << (ESSL_Parser.T__75 - 49)) | (1 << (ESSL_Parser.T__76 - 49)) | (1 << (ESSL_Parser.T__77 - 49)) | (1 << (ESSL_Parser.T__78 - 49)) | (1 << (ESSL_Parser.T__79 - 49)))) !== 0) || _la === ESSL_Parser.T__80 || _la === ESSL_Parser.T__145 || _la === ESSL_Parser.T__175 || ((((_la - 180)) & ~0x1F) === 0 && ((1 << (_la - 180)) & ((1 << (ESSL_Parser.T__179 - 180)) | (1 << (ESSL_Parser.BEFORE_COMMENT_LINE - 180)) | (1 << (ESSL_Parser.ESSL_COMMENT_BLOCK - 180)) | (1 << (ESSL_Parser.ESSL_COMMENT_LINE - 180)) | (1 << (ESSL_Parser.IDENTIFIER - 180)))) !== 0)) {
                    {
                        {
                            this.state = 1571;
                            this.eventDef();
                        }
                    }
                    this.state = 1576;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
                this.state = 1577;
                this.match(ESSL_Parser.T__2);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    createdEventDef() {
        let _localctx = new CreatedEventDefContext(this._ctx, this.state);
        this.enterRule(_localctx, 410, ESSL_Parser.RULE_createdEventDef);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1579;
                this.beforeComment();
                this.state = 1580;
                this.createdEvent();
                this.state = 1581;
                this.eventNameDecors();
                this.state = 1582;
                this.comment();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    createdEvent() {
        let _localctx = new CreatedEventContext(this._ctx, this.state);
        this.enterRule(_localctx, 412, ESSL_Parser.RULE_createdEvent);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1584;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    eventDef() {
        let _localctx = new EventDefContext(this._ctx, this.state);
        this.enterRule(_localctx, 414, ESSL_Parser.RULE_eventDef);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1586;
                this.beforeComment();
                this.state = 1587;
                this.eventName();
                this.state = 1588;
                this.eventNameDecors();
                this.state = 1589;
                this.comment();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    eventName() {
        let _localctx = new EventNameContext(this._ctx, this.state);
        this.enterRule(_localctx, 416, ESSL_Parser.RULE_eventName);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1591;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    eventsDecors() {
        let _localctx = new EventsDecorsContext(this._ctx, this.state);
        this.enterRule(_localctx, 418, ESSL_Parser.RULE_eventsDecors);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1596;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__8) {
                    {
                        {
                            this.state = 1593;
                            this.eventsDecor();
                        }
                    }
                    this.state = 1598;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    eventsDecor() {
        let _localctx = new EventsDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 420, ESSL_Parser.RULE_eventsDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1599;
                this.match(ESSL_Parser.T__8);
                {
                    this.state = 1600;
                    this.umlGroupDecor();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    eventNameDecors() {
        let _localctx = new EventNameDecorsContext(this._ctx, this.state);
        this.enterRule(_localctx, 422, ESSL_Parser.RULE_eventNameDecors);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1605;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__8) {
                    {
                        {
                            this.state = 1602;
                            this.eventNameDecor();
                        }
                    }
                    this.state = 1607;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    eventNameDecor() {
        let _localctx = new EventNameDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 424, ESSL_Parser.RULE_eventNameDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1608;
                this.match(ESSL_Parser.T__8);
                {
                    this.state = 1609;
                    this.hashLookupDecor();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    entityCommands() {
        let _localctx = new EntityCommandsContext(this._ctx, this.state);
        this.enterRule(_localctx, 426, ESSL_Parser.RULE_entityCommands);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1611;
                this.beforeComment();
                this.state = 1612;
                this.match(ESSL_Parser.T__145);
                this.state = 1613;
                this.commandsBlock();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    commandsBlock() {
        let _localctx = new CommandsBlockContext(this._ctx, this.state);
        this.enterRule(_localctx, 428, ESSL_Parser.RULE_commandsBlock);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1615;
                this.match(ESSL_Parser.T__1);
                this.state = 1619;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while ((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << ESSL_Parser.T__0) | (1 << ESSL_Parser.T__3) | (1 << ESSL_Parser.T__23) | (1 << ESSL_Parser.T__24))) !== 0) || ((((_la - 49)) & ~0x1F) === 0 && ((1 << (_la - 49)) & ((1 << (ESSL_Parser.T__48 - 49)) | (1 << (ESSL_Parser.T__62 - 49)) | (1 << (ESSL_Parser.T__69 - 49)) | (1 << (ESSL_Parser.T__73 - 49)) | (1 << (ESSL_Parser.T__74 - 49)) | (1 << (ESSL_Parser.T__75 - 49)) | (1 << (ESSL_Parser.T__76 - 49)) | (1 << (ESSL_Parser.T__77 - 49)) | (1 << (ESSL_Parser.T__78 - 49)) | (1 << (ESSL_Parser.T__79 - 49)))) !== 0) || _la === ESSL_Parser.T__80 || _la === ESSL_Parser.T__145 || _la === ESSL_Parser.T__175 || ((((_la - 180)) & ~0x1F) === 0 && ((1 << (_la - 180)) & ((1 << (ESSL_Parser.T__179 - 180)) | (1 << (ESSL_Parser.BEFORE_COMMENT_LINE - 180)) | (1 << (ESSL_Parser.ESSL_COMMENT_BLOCK - 180)) | (1 << (ESSL_Parser.ESSL_COMMENT_LINE - 180)) | (1 << (ESSL_Parser.IDENTIFIER - 180)))) !== 0)) {
                    {
                        {
                            this.state = 1616;
                            this.command();
                        }
                    }
                    this.state = 1621;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
                this.state = 1622;
                this.match(ESSL_Parser.T__2);
                this.state = 1623;
                this.commandsBlockDecorators();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    commandsBlockDecorator() {
        let _localctx = new CommandsBlockDecoratorContext(this._ctx, this.state);
        this.enterRule(_localctx, 430, ESSL_Parser.RULE_commandsBlockDecorator);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1625;
                this.match(ESSL_Parser.T__8);
                {
                    this.state = 1626;
                    this.authPolicyDecor();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    commandsBlockDecorators() {
        let _localctx = new CommandsBlockDecoratorsContext(this._ctx, this.state);
        this.enterRule(_localctx, 432, ESSL_Parser.RULE_commandsBlockDecorators);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1633;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__8) {
                    {
                        {
                            this.state = 1628;
                            this.commandsBlockDecorator();
                            this.state = 1629;
                            this.esslComment();
                        }
                    }
                    this.state = 1635;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    command() {
        let _localctx = new CommandContext(this._ctx, this.state);
        this.enterRule(_localctx, 434, ESSL_Parser.RULE_command);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1636;
                this.beforeComment();
                this.state = 1637;
                this.name();
                this.state = 1638;
                this.commandParams();
                this.state = 1639;
                this.commandDecorators();
                this.state = 1640;
                this.commandResultsIn();
                this.state = 1641;
                this.esslComment();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    commandParams() {
        let _localctx = new CommandParamsContext(this._ctx, this.state);
        this.enterRule(_localctx, 436, ESSL_Parser.RULE_commandParams);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1643;
                this.match(ESSL_Parser.T__14);
                this.state = 1652;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if ((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << ESSL_Parser.T__0) | (1 << ESSL_Parser.T__3) | (1 << ESSL_Parser.T__23) | (1 << ESSL_Parser.T__24))) !== 0) || ((((_la - 49)) & ~0x1F) === 0 && ((1 << (_la - 49)) & ((1 << (ESSL_Parser.T__48 - 49)) | (1 << (ESSL_Parser.T__62 - 49)) | (1 << (ESSL_Parser.T__69 - 49)) | (1 << (ESSL_Parser.T__73 - 49)) | (1 << (ESSL_Parser.T__74 - 49)) | (1 << (ESSL_Parser.T__75 - 49)) | (1 << (ESSL_Parser.T__76 - 49)) | (1 << (ESSL_Parser.T__77 - 49)) | (1 << (ESSL_Parser.T__78 - 49)) | (1 << (ESSL_Parser.T__79 - 49)))) !== 0) || _la === ESSL_Parser.T__80 || _la === ESSL_Parser.T__145 || _la === ESSL_Parser.T__175 || ((((_la - 180)) & ~0x1F) === 0 && ((1 << (_la - 180)) & ((1 << (ESSL_Parser.T__179 - 180)) | (1 << (ESSL_Parser.BEFORE_COMMENT_LINE - 180)) | (1 << (ESSL_Parser.ESSL_COMMENT_BLOCK - 180)) | (1 << (ESSL_Parser.ESSL_COMMENT_LINE - 180)) | (1 << (ESSL_Parser.IDENTIFIER - 180)))) !== 0)) {
                    {
                        this.state = 1644;
                        this.param();
                        this.state = 1649;
                        this._errHandler.sync(this);
                        _la = this._input.LA(1);
                        while (_la === ESSL_Parser.T__15) {
                            {
                                {
                                    this.state = 1645;
                                    this.match(ESSL_Parser.T__15);
                                    this.state = 1646;
                                    this.param();
                                }
                            }
                            this.state = 1651;
                            this._errHandler.sync(this);
                            _la = this._input.LA(1);
                        }
                    }
                }
                this.state = 1654;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    param() {
        let _localctx = new ParamContext(this._ctx, this.state);
        this.enterRule(_localctx, 438, ESSL_Parser.RULE_param);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1656;
                this.beforeComment();
                this.state = 1657;
                this.paramDecl();
                this.state = 1658;
                this.paramInitializer();
                this.state = 1661;
                this._errHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this._input, 82, this._ctx)) {
                    case 1:
                        {
                            this.state = 1659;
                            this.paramDecorators();
                        }
                        break;
                    case 2:
                        {
                            this.state = 1660;
                            this.esslComment();
                        }
                        break;
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    paramDecl() {
        let _localctx = new ParamDeclContext(this._ctx, this.state);
        this.enterRule(_localctx, 440, ESSL_Parser.RULE_paramDecl);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1663;
                this.optionalTypeRef();
                this.state = 1664;
                this.name();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    optionalTypeRef() {
        let _localctx = new OptionalTypeRefContext(this._ctx, this.state);
        this.enterRule(_localctx, 442, ESSL_Parser.RULE_optionalTypeRef);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1669;
                this._errHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this._input, 83, this._ctx)) {
                    case 1:
                        {
                            this.state = 1666;
                            this.typeRefName();
                            this.state = 1667;
                            this.paramNameModifiers();
                        }
                        break;
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    paramNameModifiers() {
        let _localctx = new ParamNameModifiersContext(this._ctx, this.state);
        this.enterRule(_localctx, 444, ESSL_Parser.RULE_paramNameModifiers);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1672;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__81) {
                    {
                        this.state = 1671;
                        this.listIndicator();
                    }
                }
                this.state = 1675;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__83) {
                    {
                        this.state = 1674;
                        this.nullableIndicator();
                    }
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    paramInitializer() {
        let _localctx = new ParamInitializerContext(this._ctx, this.state);
        this.enterRule(_localctx, 446, ESSL_Parser.RULE_paramInitializer);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1685;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__63) {
                    {
                        this.state = 1677;
                        this.match(ESSL_Parser.T__63);
                        this.state = 1683;
                        this._errHandler.sync(this);
                        switch (this._input.LA(1)) {
                            case ESSL_Parser.T__84:
                                {
                                    this.state = 1678;
                                    this.nullValue();
                                }
                                break;
                            case ESSL_Parser.IDENTIFIER:
                                {
                                    this.state = 1679;
                                    this.initialEnumVal();
                                }
                                break;
                            case ESSL_Parser.T__85:
                            case ESSL_Parser.T__86:
                                {
                                    this.state = 1680;
                                    this.boolValue();
                                }
                                break;
                            case ESSL_Parser.T__88:
                            case ESSL_Parser.POS_DEC:
                            case ESSL_Parser.POS_INT:
                                {
                                    this.state = 1681;
                                    this.numValue();
                                }
                                break;
                            case ESSL_Parser.QUOTED_STR:
                                {
                                    this.state = 1682;
                                    this.stringValue();
                                }
                                break;
                            default:
                                throw new NoViableAltException_1.NoViableAltException(this);
                        }
                    }
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    commandResultsIn() {
        let _localctx = new CommandResultsInContext(this._ctx, this.state);
        this.enterRule(_localctx, 448, ESSL_Parser.RULE_commandResultsIn);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1689;
                this._errHandler.sync(this);
                switch (this._input.LA(1)) {
                    case ESSL_Parser.T__146:
                        {
                            this.state = 1687;
                            this.yields();
                        }
                        break;
                    case ESSL_Parser.T__147:
                        {
                            this.state = 1688;
                            this.returnsType();
                        }
                        break;
                    default:
                        throw new NoViableAltException_1.NoViableAltException(this);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    yields() {
        let _localctx = new YieldsContext(this._ctx, this.state);
        this.enterRule(_localctx, 450, ESSL_Parser.RULE_yields);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1691;
                this.match(ESSL_Parser.T__146);
                this.state = 1692;
                this.eventList();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    eventList() {
        let _localctx = new EventListContext(this._ctx, this.state);
        this.enterRule(_localctx, 452, ESSL_Parser.RULE_eventList);
        try {
            let _alt;
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1694;
                this.eventRef();
                this.state = 1699;
                this._errHandler.sync(this);
                _alt = this.interpreter.adaptivePredict(this._input, 89, this._ctx);
                while (_alt !== 2 && _alt !== ATN_1.ATN.INVALID_ALT_NUMBER) {
                    if (_alt === 1) {
                        {
                            {
                                this.state = 1695;
                                this.match(ESSL_Parser.T__15);
                                this.state = 1696;
                                this.eventRef();
                            }
                        }
                    }
                    this.state = 1701;
                    this._errHandler.sync(this);
                    _alt = this.interpreter.adaptivePredict(this._input, 89, this._ctx);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    eventRef() {
        let _localctx = new EventRefContext(this._ctx, this.state);
        this.enterRule(_localctx, 454, ESSL_Parser.RULE_eventRef);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1702;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    returnsType() {
        let _localctx = new ReturnsTypeContext(this._ctx, this.state);
        this.enterRule(_localctx, 456, ESSL_Parser.RULE_returnsType);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1704;
                this.match(ESSL_Parser.T__147);
                this.state = 1705;
                this.returnTypeRef();
                this.state = 1706;
                this.paramNameModifiers();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    returnTypeRef() {
        let _localctx = new ReturnTypeRefContext(this._ctx, this.state);
        this.enterRule(_localctx, 458, ESSL_Parser.RULE_returnTypeRef);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1708;
                this.typeRefName();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    paramDecorator() {
        let _localctx = new ParamDecoratorContext(this._ctx, this.state);
        this.enterRule(_localctx, 460, ESSL_Parser.RULE_paramDecorator);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1710;
                this.match(ESSL_Parser.T__8);
                this.state = 1714;
                this._errHandler.sync(this);
                switch (this._input.LA(1)) {
                    case ESSL_Parser.T__23:
                        {
                            this.state = 1711;
                            this.typeQualifierDecor();
                        }
                        break;
                    case ESSL_Parser.T__148:
                        {
                            this.state = 1712;
                            this.byValueDecor();
                        }
                        break;
                    case ESSL_Parser.T__149:
                        {
                            this.state = 1713;
                            this.idDecor();
                        }
                        break;
                    default:
                        throw new NoViableAltException_1.NoViableAltException(this);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    typeQualifierDecor() {
        let _localctx = new TypeQualifierDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 462, ESSL_Parser.RULE_typeQualifierDecor);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1716;
                this.match(ESSL_Parser.T__23);
                this.state = 1717;
                this.match(ESSL_Parser.T__14);
                this.state = 1718;
                this.typeQualifierName();
                this.state = 1721;
                this._errHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this._input, 91, this._ctx)) {
                    case 1:
                        {
                            this.state = 1719;
                            this.match(ESSL_Parser.T__15);
                            this.state = 1720;
                            this.indexQualifier();
                        }
                        break;
                }
                this.state = 1725;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__15) {
                    {
                        this.state = 1723;
                        this.match(ESSL_Parser.T__15);
                        this.state = 1724;
                        this.filterExpr();
                    }
                }
                this.state = 1727;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    paramDecorators() {
        let _localctx = new ParamDecoratorsContext(this._ctx, this.state);
        this.enterRule(_localctx, 464, ESSL_Parser.RULE_paramDecorators);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1734;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__8) {
                    {
                        {
                            this.state = 1729;
                            this.paramDecorator();
                            this.state = 1730;
                            this.esslComment();
                        }
                    }
                    this.state = 1736;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    typeQualifierName() {
        let _localctx = new TypeQualifierNameContext(this._ctx, this.state);
        this.enterRule(_localctx, 466, ESSL_Parser.RULE_typeQualifierName);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1737;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    indexQualifier() {
        let _localctx = new IndexQualifierContext(this._ctx, this.state);
        this.enterRule(_localctx, 468, ESSL_Parser.RULE_indexQualifier);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1739;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    filterExpr() {
        let _localctx = new FilterExprContext(this._ctx, this.state);
        this.enterRule(_localctx, 470, ESSL_Parser.RULE_filterExpr);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1741;
                this.match(ESSL_Parser.FILTEREXPR);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    byValueDecor() {
        let _localctx = new ByValueDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 472, ESSL_Parser.RULE_byValueDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1743;
                this.match(ESSL_Parser.T__148);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    idDecor() {
        let _localctx = new IdDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 474, ESSL_Parser.RULE_idDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1745;
                this.match(ESSL_Parser.T__149);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    commandDecorator() {
        let _localctx = new CommandDecoratorContext(this._ctx, this.state);
        this.enterRule(_localctx, 476, ESSL_Parser.RULE_commandDecorator);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1747;
                this.match(ESSL_Parser.T__8);
                this.state = 1764;
                this._errHandler.sync(this);
                switch (this._input.LA(1)) {
                    case ESSL_Parser.T__65:
                        {
                            this.state = 1748;
                            this.modelDecor();
                        }
                        break;
                    case ESSL_Parser.T__150:
                        {
                            this.state = 1749;
                            this.asyncDecor();
                        }
                        break;
                    case ESSL_Parser.T__154:
                        {
                            this.state = 1750;
                            this.syncDecor();
                        }
                        break;
                    case ESSL_Parser.T__155:
                        {
                            this.state = 1751;
                            this.transDecor();
                        }
                        break;
                    case ESSL_Parser.T__160:
                        {
                            this.state = 1752;
                            this.continuationDecor();
                        }
                        break;
                    case ESSL_Parser.T__161:
                        {
                            this.state = 1753;
                            this.effectiveDateDecor();
                        }
                        break;
                    case ESSL_Parser.T__162:
                        {
                            this.state = 1754;
                            this.explicitDecor();
                        }
                        break;
                    case ESSL_Parser.T__163:
                        {
                            this.state = 1755;
                            this.internalDecor();
                        }
                        break;
                    case ESSL_Parser.T__164:
                        {
                            this.state = 1756;
                            this.messageSourceDecor();
                        }
                        break;
                    case ESSL_Parser.T__165:
                        {
                            this.state = 1757;
                            this.createDecor();
                        }
                        break;
                    case ESSL_Parser.T__166:
                        {
                            this.state = 1758;
                            this.deleteDecor();
                        }
                        break;
                    case ESSL_Parser.T__167:
                        {
                            this.state = 1759;
                            this.partialErrorDecor();
                        }
                        break;
                    case ESSL_Parser.T__168:
                        {
                            this.state = 1760;
                            this.generateDecor();
                        }
                        break;
                    case ESSL_Parser.T__35:
                        {
                            this.state = 1761;
                            this.authPolicyDecor();
                        }
                        break;
                    case ESSL_Parser.T__36:
                        {
                            this.state = 1762;
                            this.publicDecor();
                        }
                        break;
                    case ESSL_Parser.T__178:
                        {
                            this.state = 1763;
                            this.provideGraphQLSchemaDecor();
                        }
                        break;
                    default:
                        throw new NoViableAltException_1.NoViableAltException(this);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    commandDecorators() {
        let _localctx = new CommandDecoratorsContext(this._ctx, this.state);
        this.enterRule(_localctx, 478, ESSL_Parser.RULE_commandDecorators);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1771;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__8) {
                    {
                        {
                            this.state = 1766;
                            this.commandDecorator();
                            this.state = 1767;
                            this.esslComment();
                        }
                    }
                    this.state = 1773;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    modelDecor() {
        let _localctx = new ModelDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 480, ESSL_Parser.RULE_modelDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1774;
                this.match(ESSL_Parser.T__65);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    asyncDecor() {
        let _localctx = new AsyncDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 482, ESSL_Parser.RULE_asyncDecor);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1776;
                this.match(ESSL_Parser.T__150);
                this.state = 1777;
                this.match(ESSL_Parser.T__14);
                this.state = 1778;
                this.asyncSpec();
                this.state = 1781;
                this._errHandler.sync(this);
                switch (this.interpreter.adaptivePredict(this._input, 96, this._ctx)) {
                    case 1:
                        {
                            this.state = 1779;
                            this.match(ESSL_Parser.T__15);
                            this.state = 1780;
                            this.asyncSpec();
                        }
                        break;
                }
                this.state = 1785;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__15) {
                    {
                        this.state = 1783;
                        this.match(ESSL_Parser.T__15);
                        this.state = 1784;
                        this.asyncSpec();
                    }
                }
                this.state = 1787;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    asyncSpec() {
        let _localctx = new AsyncSpecContext(this._ctx, this.state);
        this.enterRule(_localctx, 484, ESSL_Parser.RULE_asyncSpec);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1792;
                this._errHandler.sync(this);
                switch (this._input.LA(1)) {
                    case ESSL_Parser.T__151:
                        {
                            this.state = 1789;
                            this.asyncParamValidation();
                        }
                        break;
                    case ESSL_Parser.T__152:
                        {
                            this.state = 1790;
                            this.asyncModelValidation();
                        }
                        break;
                    case ESSL_Parser.T__153:
                        {
                            this.state = 1791;
                            this.asyncBusinessLogic();
                        }
                        break;
                    default:
                        throw new NoViableAltException_1.NoViableAltException(this);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    asyncParamValidation() {
        let _localctx = new AsyncParamValidationContext(this._ctx, this.state);
        this.enterRule(_localctx, 486, ESSL_Parser.RULE_asyncParamValidation);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1794;
                this.match(ESSL_Parser.T__151);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    asyncModelValidation() {
        let _localctx = new AsyncModelValidationContext(this._ctx, this.state);
        this.enterRule(_localctx, 488, ESSL_Parser.RULE_asyncModelValidation);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1796;
                this.match(ESSL_Parser.T__152);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    asyncBusinessLogic() {
        let _localctx = new AsyncBusinessLogicContext(this._ctx, this.state);
        this.enterRule(_localctx, 490, ESSL_Parser.RULE_asyncBusinessLogic);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1798;
                this.match(ESSL_Parser.T__153);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    syncDecor() {
        let _localctx = new SyncDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 492, ESSL_Parser.RULE_syncDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1800;
                this.match(ESSL_Parser.T__154);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    transDecor() {
        let _localctx = new TransDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 494, ESSL_Parser.RULE_transDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1802;
                this.match(ESSL_Parser.T__155);
                this.state = 1803;
                this.match(ESSL_Parser.T__14);
                this.state = 1804;
                this.transAttr();
                this.state = 1805;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    transAttr() {
        let _localctx = new TransAttrContext(this._ctx, this.state);
        this.enterRule(_localctx, 496, ESSL_Parser.RULE_transAttr);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1807;
                _la = this._input.LA(1);
                if (!(((((_la - 157)) & ~0x1F) === 0 && ((1 << (_la - 157)) & ((1 << (ESSL_Parser.T__156 - 157)) | (1 << (ESSL_Parser.T__157 - 157)) | (1 << (ESSL_Parser.T__158 - 157)) | (1 << (ESSL_Parser.T__159 - 157)))) !== 0))) {
                    this._errHandler.recoverInline(this);
                }
                else {
                    if (this._input.LA(1) === Token_1.Token.EOF) {
                        this.matchedEOF = true;
                    }
                    this._errHandler.reportMatch(this);
                    this.consume();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    continuationDecor() {
        let _localctx = new ContinuationDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 498, ESSL_Parser.RULE_continuationDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1809;
                this.match(ESSL_Parser.T__160);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    effectiveDateDecor() {
        let _localctx = new EffectiveDateDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 500, ESSL_Parser.RULE_effectiveDateDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1811;
                this.match(ESSL_Parser.T__161);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    explicitDecor() {
        let _localctx = new ExplicitDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 502, ESSL_Parser.RULE_explicitDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1813;
                this.match(ESSL_Parser.T__162);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    internalDecor() {
        let _localctx = new InternalDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 504, ESSL_Parser.RULE_internalDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1815;
                this.match(ESSL_Parser.T__163);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    messageSourceDecor() {
        let _localctx = new MessageSourceDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 506, ESSL_Parser.RULE_messageSourceDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1817;
                this.match(ESSL_Parser.T__164);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    createDecor() {
        let _localctx = new CreateDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 508, ESSL_Parser.RULE_createDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1819;
                this.match(ESSL_Parser.T__165);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    deleteDecor() {
        let _localctx = new DeleteDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 510, ESSL_Parser.RULE_deleteDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1821;
                this.match(ESSL_Parser.T__166);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    partialErrorDecor() {
        let _localctx = new PartialErrorDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 512, ESSL_Parser.RULE_partialErrorDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1823;
                this.match(ESSL_Parser.T__167);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    generateDecor() {
        let _localctx = new GenerateDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 514, ESSL_Parser.RULE_generateDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1825;
                this.match(ESSL_Parser.T__168);
                this.state = 1826;
                this.match(ESSL_Parser.T__14);
                this.state = 1827;
                this.generatorOption();
                this.state = 1828;
                this.match(ESSL_Parser.T__16);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    generatorOption() {
        let _localctx = new GeneratorOptionContext(this._ctx, this.state);
        this.enterRule(_localctx, 516, ESSL_Parser.RULE_generatorOption);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1836;
                this._errHandler.sync(this);
                switch (this._input.LA(1)) {
                    case ESSL_Parser.T__169:
                        {
                            this.state = 1830;
                            this.genSetField();
                        }
                        break;
                    case ESSL_Parser.T__170:
                        {
                            this.state = 1831;
                            this.genAssignments();
                        }
                        break;
                    case ESSL_Parser.T__171:
                        {
                            this.state = 1832;
                            this.genAddToSet();
                        }
                        break;
                    case ESSL_Parser.T__172:
                        {
                            this.state = 1833;
                            this.genRemoveFromSet();
                        }
                        break;
                    case ESSL_Parser.T__173:
                        {
                            this.state = 1834;
                            this.genClearSet();
                        }
                        break;
                    case ESSL_Parser.T__174:
                        {
                            this.state = 1835;
                            this.genUpdate();
                        }
                        break;
                    default:
                        throw new NoViableAltException_1.NoViableAltException(this);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    genSetField() {
        let _localctx = new GenSetFieldContext(this._ctx, this.state);
        this.enterRule(_localctx, 518, ESSL_Parser.RULE_genSetField);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                {
                    this.state = 1838;
                    this.match(ESSL_Parser.T__169);
                    this.state = 1839;
                    this.refField();
                }
                this.state = 1842;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.QUOTED_STR) {
                    {
                        this.state = 1841;
                        this.jsonKey();
                    }
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    genAssignments() {
        let _localctx = new GenAssignmentsContext(this._ctx, this.state);
        this.enterRule(_localctx, 520, ESSL_Parser.RULE_genAssignments);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                {
                    this.state = 1844;
                    this.match(ESSL_Parser.T__170);
                    this.state = 1845;
                    this.assignmentList();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    assignmentList() {
        let _localctx = new AssignmentListContext(this._ctx, this.state);
        this.enterRule(_localctx, 522, ESSL_Parser.RULE_assignmentList);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1847;
                this.assignment();
                this.state = 1850;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.T__15) {
                    {
                        this.state = 1848;
                        this.match(ESSL_Parser.T__15);
                        this.state = 1849;
                        this.assignment();
                    }
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    assignment() {
        let _localctx = new AssignmentContext(this._ctx, this.state);
        this.enterRule(_localctx, 524, ESSL_Parser.RULE_assignment);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1852;
                this.refField();
                this.state = 1854;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.QUOTED_STR) {
                    {
                        this.state = 1853;
                        this.jsonKey();
                    }
                }
                this.state = 1857;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.DELIMITED_EXPRESSION) {
                    {
                        this.state = 1856;
                        this.valueExpression();
                    }
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    valueExpression() {
        let _localctx = new ValueExpressionContext(this._ctx, this.state);
        this.enterRule(_localctx, 526, ESSL_Parser.RULE_valueExpression);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1859;
                this.match(ESSL_Parser.DELIMITED_EXPRESSION);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    genAddToSet() {
        let _localctx = new GenAddToSetContext(this._ctx, this.state);
        this.enterRule(_localctx, 528, ESSL_Parser.RULE_genAddToSet);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                {
                    this.state = 1861;
                    this.match(ESSL_Parser.T__171);
                    this.state = 1862;
                    this.refField();
                }
                this.state = 1865;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.QUOTED_STR) {
                    {
                        this.state = 1864;
                        this.jsonKey();
                    }
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    genRemoveFromSet() {
        let _localctx = new GenRemoveFromSetContext(this._ctx, this.state);
        this.enterRule(_localctx, 530, ESSL_Parser.RULE_genRemoveFromSet);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                {
                    this.state = 1867;
                    this.match(ESSL_Parser.T__172);
                    this.state = 1868;
                    this.refField();
                }
                this.state = 1871;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                if (_la === ESSL_Parser.QUOTED_STR) {
                    {
                        this.state = 1870;
                        this.jsonKey();
                    }
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    genClearSet() {
        let _localctx = new GenClearSetContext(this._ctx, this.state);
        this.enterRule(_localctx, 532, ESSL_Parser.RULE_genClearSet);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                {
                    this.state = 1873;
                    this.match(ESSL_Parser.T__173);
                    this.state = 1874;
                    this.refField();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    genUpdate() {
        let _localctx = new GenUpdateContext(this._ctx, this.state);
        this.enterRule(_localctx, 534, ESSL_Parser.RULE_genUpdate);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1876;
                this.match(ESSL_Parser.T__174);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    jsonKey() {
        let _localctx = new JsonKeyContext(this._ctx, this.state);
        this.enterRule(_localctx, 536, ESSL_Parser.RULE_jsonKey);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1878;
                this.match(ESSL_Parser.QUOTED_STR);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    refField() {
        let _localctx = new RefFieldContext(this._ctx, this.state);
        this.enterRule(_localctx, 538, ESSL_Parser.RULE_refField);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1880;
                this.match(ESSL_Parser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    entityQueries() {
        let _localctx = new EntityQueriesContext(this._ctx, this.state);
        this.enterRule(_localctx, 540, ESSL_Parser.RULE_entityQueries);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1882;
                this.beforeComment();
                this.state = 1883;
                this.match(ESSL_Parser.T__175);
                this.state = 1884;
                this.queriesBlock();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    queriesBlock() {
        let _localctx = new QueriesBlockContext(this._ctx, this.state);
        this.enterRule(_localctx, 542, ESSL_Parser.RULE_queriesBlock);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1886;
                this.comment();
                this.state = 1887;
                this.match(ESSL_Parser.T__1);
                this.state = 1891;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while ((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << ESSL_Parser.T__0) | (1 << ESSL_Parser.T__3) | (1 << ESSL_Parser.T__23) | (1 << ESSL_Parser.T__24))) !== 0) || ((((_la - 49)) & ~0x1F) === 0 && ((1 << (_la - 49)) & ((1 << (ESSL_Parser.T__48 - 49)) | (1 << (ESSL_Parser.T__62 - 49)) | (1 << (ESSL_Parser.T__69 - 49)) | (1 << (ESSL_Parser.T__73 - 49)) | (1 << (ESSL_Parser.T__74 - 49)) | (1 << (ESSL_Parser.T__75 - 49)) | (1 << (ESSL_Parser.T__76 - 49)) | (1 << (ESSL_Parser.T__77 - 49)) | (1 << (ESSL_Parser.T__78 - 49)) | (1 << (ESSL_Parser.T__79 - 49)))) !== 0) || _la === ESSL_Parser.T__80 || _la === ESSL_Parser.T__145 || _la === ESSL_Parser.T__175 || ((((_la - 180)) & ~0x1F) === 0 && ((1 << (_la - 180)) & ((1 << (ESSL_Parser.T__179 - 180)) | (1 << (ESSL_Parser.BEFORE_COMMENT_LINE - 180)) | (1 << (ESSL_Parser.ESSL_COMMENT_BLOCK - 180)) | (1 << (ESSL_Parser.ESSL_COMMENT_LINE - 180)) | (1 << (ESSL_Parser.IDENTIFIER - 180)))) !== 0)) {
                    {
                        {
                            this.state = 1888;
                            this.query();
                        }
                    }
                    this.state = 1893;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
                this.state = 1894;
                this.match(ESSL_Parser.T__2);
                this.state = 1895;
                this.queriesBlockDecorators();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    queriesBlockDecorator() {
        let _localctx = new QueriesBlockDecoratorContext(this._ctx, this.state);
        this.enterRule(_localctx, 544, ESSL_Parser.RULE_queriesBlockDecorator);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1897;
                this.match(ESSL_Parser.T__8);
                {
                    this.state = 1898;
                    this.authPolicyDecor();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    queriesBlockDecorators() {
        let _localctx = new QueriesBlockDecoratorsContext(this._ctx, this.state);
        this.enterRule(_localctx, 546, ESSL_Parser.RULE_queriesBlockDecorators);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1905;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__8) {
                    {
                        {
                            this.state = 1900;
                            this.queriesBlockDecorator();
                            this.state = 1901;
                            this.esslComment();
                        }
                    }
                    this.state = 1907;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    query() {
        let _localctx = new QueryContext(this._ctx, this.state);
        this.enterRule(_localctx, 548, ESSL_Parser.RULE_query);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1908;
                this.beforeComment();
                this.state = 1909;
                this.name();
                this.state = 1910;
                this.commandParams();
                this.state = 1911;
                this.queryDecorators();
                this.state = 1912;
                this.returnsType();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    queryDecorator() {
        let _localctx = new QueryDecoratorContext(this._ctx, this.state);
        this.enterRule(_localctx, 550, ESSL_Parser.RULE_queryDecorator);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1914;
                this.match(ESSL_Parser.T__8);
                this.state = 1922;
                this._errHandler.sync(this);
                switch (this._input.LA(1)) {
                    case ESSL_Parser.T__176:
                        {
                            this.state = 1915;
                            this.temporalDecor();
                        }
                        break;
                    case ESSL_Parser.T__177:
                        {
                            this.state = 1916;
                            this.provideUserContextDecor();
                        }
                        break;
                    case ESSL_Parser.T__163:
                        {
                            this.state = 1917;
                            this.internalDecor();
                        }
                        break;
                    case ESSL_Parser.T__154:
                        {
                            this.state = 1918;
                            this.syncDecor();
                        }
                        break;
                    case ESSL_Parser.T__35:
                        {
                            this.state = 1919;
                            this.authPolicyDecor();
                        }
                        break;
                    case ESSL_Parser.T__36:
                        {
                            this.state = 1920;
                            this.publicDecor();
                        }
                        break;
                    case ESSL_Parser.T__178:
                        {
                            this.state = 1921;
                            this.provideGraphQLSchemaDecor();
                        }
                        break;
                    default:
                        throw new NoViableAltException_1.NoViableAltException(this);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    queryDecorators() {
        let _localctx = new QueryDecoratorsContext(this._ctx, this.state);
        this.enterRule(_localctx, 552, ESSL_Parser.RULE_queryDecorators);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1929;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__8) {
                    {
                        {
                            this.state = 1924;
                            this.queryDecorator();
                            this.state = 1925;
                            this.esslComment();
                        }
                    }
                    this.state = 1931;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    temporalDecor() {
        let _localctx = new TemporalDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 554, ESSL_Parser.RULE_temporalDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1932;
                this.match(ESSL_Parser.T__176);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    provideUserContextDecor() {
        let _localctx = new ProvideUserContextDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 556, ESSL_Parser.RULE_provideUserContextDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1934;
                this.match(ESSL_Parser.T__177);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    provideGraphQLSchemaDecor() {
        let _localctx = new ProvideGraphQLSchemaDecorContext(this._ctx, this.state);
        this.enterRule(_localctx, 558, ESSL_Parser.RULE_provideGraphQLSchemaDecor);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1936;
                this.match(ESSL_Parser.T__178);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    entitySubscriptions() {
        let _localctx = new EntitySubscriptionsContext(this._ctx, this.state);
        this.enterRule(_localctx, 560, ESSL_Parser.RULE_entitySubscriptions);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1938;
                this.beforeComment();
                this.state = 1939;
                this.match(ESSL_Parser.T__179);
                this.state = 1940;
                this.subscriptionsBlock();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    subscriptionsBlock() {
        let _localctx = new SubscriptionsBlockContext(this._ctx, this.state);
        this.enterRule(_localctx, 562, ESSL_Parser.RULE_subscriptionsBlock);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1942;
                this.comment();
                this.state = 1943;
                this.match(ESSL_Parser.T__1);
                this.state = 1947;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while ((((_la) & ~0x1F) === 0 && ((1 << _la) & ((1 << ESSL_Parser.T__0) | (1 << ESSL_Parser.T__3) | (1 << ESSL_Parser.T__23) | (1 << ESSL_Parser.T__24))) !== 0) || ((((_la - 49)) & ~0x1F) === 0 && ((1 << (_la - 49)) & ((1 << (ESSL_Parser.T__48 - 49)) | (1 << (ESSL_Parser.T__62 - 49)) | (1 << (ESSL_Parser.T__69 - 49)) | (1 << (ESSL_Parser.T__73 - 49)) | (1 << (ESSL_Parser.T__74 - 49)) | (1 << (ESSL_Parser.T__75 - 49)) | (1 << (ESSL_Parser.T__76 - 49)) | (1 << (ESSL_Parser.T__77 - 49)) | (1 << (ESSL_Parser.T__78 - 49)) | (1 << (ESSL_Parser.T__79 - 49)))) !== 0) || _la === ESSL_Parser.T__80 || _la === ESSL_Parser.T__145 || _la === ESSL_Parser.T__175 || ((((_la - 180)) & ~0x1F) === 0 && ((1 << (_la - 180)) & ((1 << (ESSL_Parser.T__179 - 180)) | (1 << (ESSL_Parser.BEFORE_COMMENT_LINE - 180)) | (1 << (ESSL_Parser.ESSL_COMMENT_BLOCK - 180)) | (1 << (ESSL_Parser.ESSL_COMMENT_LINE - 180)) | (1 << (ESSL_Parser.IDENTIFIER - 180)))) !== 0)) {
                    {
                        {
                            this.state = 1944;
                            this.subscription();
                        }
                    }
                    this.state = 1949;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
                this.state = 1950;
                this.match(ESSL_Parser.T__2);
                this.state = 1951;
                this.subscriptionsBlockDecorators();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    subscriptionsBlockDecorator() {
        let _localctx = new SubscriptionsBlockDecoratorContext(this._ctx, this.state);
        this.enterRule(_localctx, 564, ESSL_Parser.RULE_subscriptionsBlockDecorator);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1953;
                this.match(ESSL_Parser.T__8);
                {
                    this.state = 1954;
                    this.authPolicyDecor();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    subscriptionsBlockDecorators() {
        let _localctx = new SubscriptionsBlockDecoratorsContext(this._ctx, this.state);
        this.enterRule(_localctx, 566, ESSL_Parser.RULE_subscriptionsBlockDecorators);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1961;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__8) {
                    {
                        {
                            this.state = 1956;
                            this.subscriptionsBlockDecorator();
                            this.state = 1957;
                            this.esslComment();
                        }
                    }
                    this.state = 1963;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    subscription() {
        let _localctx = new SubscriptionContext(this._ctx, this.state);
        this.enterRule(_localctx, 568, ESSL_Parser.RULE_subscription);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1964;
                this.beforeComment();
                this.state = 1965;
                this.name();
                this.state = 1966;
                this.commandParams();
                this.state = 1967;
                this.subscriptionDecorators();
                this.state = 1968;
                this.returnsType();
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    subscriptionDecorator() {
        let _localctx = new SubscriptionDecoratorContext(this._ctx, this.state);
        this.enterRule(_localctx, 570, ESSL_Parser.RULE_subscriptionDecorator);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1970;
                this.match(ESSL_Parser.T__8);
                {
                    this.state = 1971;
                    this.authPolicyDecor();
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    subscriptionDecorators() {
        let _localctx = new SubscriptionDecoratorsContext(this._ctx, this.state);
        this.enterRule(_localctx, 572, ESSL_Parser.RULE_subscriptionDecorators);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1978;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__8) {
                    {
                        {
                            this.state = 1973;
                            this.subscriptionDecorator();
                            this.state = 1974;
                            this.esslComment();
                        }
                    }
                    this.state = 1980;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    dottedId() {
        let _localctx = new DottedIdContext(this._ctx, this.state);
        this.enterRule(_localctx, 574, ESSL_Parser.RULE_dottedId);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1981;
                this.match(ESSL_Parser.IDENTIFIER);
                this.state = 1986;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (_la === ESSL_Parser.T__180) {
                    {
                        {
                            this.state = 1982;
                            this.match(ESSL_Parser.T__180);
                            this.state = 1983;
                            this.match(ESSL_Parser.IDENTIFIER);
                        }
                    }
                    this.state = 1988;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    comment() {
        let _localctx = new CommentContext(this._ctx, this.state);
        this.enterRule(_localctx, 576, ESSL_Parser.RULE_comment);
        try {
            let _alt;
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 1994;
                this._errHandler.sync(this);
                _alt = this.interpreter.adaptivePredict(this._input, 115, this._ctx);
                while (_alt !== 2 && _alt !== ATN_1.ATN.INVALID_ALT_NUMBER) {
                    if (_alt === 1) {
                        {
                            this.state = 1992;
                            this._errHandler.sync(this);
                            switch (this._input.LA(1)) {
                                case ESSL_Parser.COMMENT_BLOCK:
                                    {
                                        this.state = 1989;
                                        this.domBlockComment();
                                    }
                                    break;
                                case ESSL_Parser.ESSL_COMMENT_BLOCK:
                                    {
                                        this.state = 1990;
                                        this.esslBlockComment();
                                    }
                                    break;
                                case ESSL_Parser.ESSL_COMMENT_LINE:
                                    {
                                        this.state = 1991;
                                        this.esslLineComment();
                                    }
                                    break;
                                default:
                                    throw new NoViableAltException_1.NoViableAltException(this);
                            }
                        }
                    }
                    this.state = 1996;
                    this._errHandler.sync(this);
                    _alt = this.interpreter.adaptivePredict(this._input, 115, this._ctx);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    beforeComment() {
        let _localctx = new BeforeCommentContext(this._ctx, this.state);
        this.enterRule(_localctx, 578, ESSL_Parser.RULE_beforeComment);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 2002;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (((((_la - 185)) & ~0x1F) === 0 && ((1 << (_la - 185)) & ((1 << (ESSL_Parser.BEFORE_COMMENT_LINE - 185)) | (1 << (ESSL_Parser.ESSL_COMMENT_BLOCK - 185)) | (1 << (ESSL_Parser.ESSL_COMMENT_LINE - 185)))) !== 0)) {
                    {
                        this.state = 2000;
                        this._errHandler.sync(this);
                        switch (this._input.LA(1)) {
                            case ESSL_Parser.BEFORE_COMMENT_LINE:
                                {
                                    this.state = 1997;
                                    this.domBeforeLineComment();
                                }
                                break;
                            case ESSL_Parser.ESSL_COMMENT_BLOCK:
                                {
                                    this.state = 1998;
                                    this.esslBlockComment();
                                }
                                break;
                            case ESSL_Parser.ESSL_COMMENT_LINE:
                                {
                                    this.state = 1999;
                                    this.esslLineComment();
                                }
                                break;
                            default:
                                throw new NoViableAltException_1.NoViableAltException(this);
                        }
                    }
                    this.state = 2004;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    afterComment() {
        let _localctx = new AfterCommentContext(this._ctx, this.state);
        this.enterRule(_localctx, 580, ESSL_Parser.RULE_afterComment);
        let _la;
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 2010;
                this._errHandler.sync(this);
                _la = this._input.LA(1);
                while (((((_la - 185)) & ~0x1F) === 0 && ((1 << (_la - 185)) & ((1 << (ESSL_Parser.BEFORE_COMMENT_LINE - 185)) | (1 << (ESSL_Parser.ESSL_COMMENT_BLOCK - 185)) | (1 << (ESSL_Parser.ESSL_COMMENT_LINE - 185)))) !== 0)) {
                    {
                        this.state = 2008;
                        this._errHandler.sync(this);
                        switch (this._input.LA(1)) {
                            case ESSL_Parser.BEFORE_COMMENT_LINE:
                                {
                                    this.state = 2005;
                                    this.domBeforeLineComment();
                                }
                                break;
                            case ESSL_Parser.ESSL_COMMENT_BLOCK:
                                {
                                    this.state = 2006;
                                    this.esslBlockComment();
                                }
                                break;
                            case ESSL_Parser.ESSL_COMMENT_LINE:
                                {
                                    this.state = 2007;
                                    this.esslLineComment();
                                }
                                break;
                            default:
                                throw new NoViableAltException_1.NoViableAltException(this);
                        }
                    }
                    this.state = 2012;
                    this._errHandler.sync(this);
                    _la = this._input.LA(1);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    esslComment() {
        let _localctx = new EsslCommentContext(this._ctx, this.state);
        this.enterRule(_localctx, 582, ESSL_Parser.RULE_esslComment);
        try {
            let _alt;
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 2017;
                this._errHandler.sync(this);
                _alt = this.interpreter.adaptivePredict(this._input, 121, this._ctx);
                while (_alt !== 2 && _alt !== ATN_1.ATN.INVALID_ALT_NUMBER) {
                    if (_alt === 1) {
                        {
                            this.state = 2015;
                            this._errHandler.sync(this);
                            switch (this._input.LA(1)) {
                                case ESSL_Parser.ESSL_COMMENT_BLOCK:
                                    {
                                        this.state = 2013;
                                        this.esslBlockComment();
                                    }
                                    break;
                                case ESSL_Parser.ESSL_COMMENT_LINE:
                                    {
                                        this.state = 2014;
                                        this.esslLineComment();
                                    }
                                    break;
                                default:
                                    throw new NoViableAltException_1.NoViableAltException(this);
                            }
                        }
                    }
                    this.state = 2019;
                    this._errHandler.sync(this);
                    _alt = this.interpreter.adaptivePredict(this._input, 121, this._ctx);
                }
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    domBeforeLineComment() {
        let _localctx = new DomBeforeLineCommentContext(this._ctx, this.state);
        this.enterRule(_localctx, 584, ESSL_Parser.RULE_domBeforeLineComment);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 2020;
                this.match(ESSL_Parser.BEFORE_COMMENT_LINE);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    domBlockComment() {
        let _localctx = new DomBlockCommentContext(this._ctx, this.state);
        this.enterRule(_localctx, 586, ESSL_Parser.RULE_domBlockComment);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 2022;
                this.match(ESSL_Parser.COMMENT_BLOCK);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    esslBlockComment() {
        let _localctx = new EsslBlockCommentContext(this._ctx, this.state);
        this.enterRule(_localctx, 588, ESSL_Parser.RULE_esslBlockComment);
        try {
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 2024;
                this.match(ESSL_Parser.ESSL_COMMENT_BLOCK);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    // @RuleVersion(0)
    esslLineComment() {
        let _localctx = new EsslLineCommentContext(this._ctx, this.state);
        this.enterRule(_localctx, 590, ESSL_Parser.RULE_esslLineComment);
        try {
            let _alt;
            this.enterOuterAlt(_localctx, 1);
            {
                this.state = 2027;
                this._errHandler.sync(this);
                _alt = 1;
                do {
                    switch (_alt) {
                        case 1:
                            {
                                {
                                    this.state = 2026;
                                    this.match(ESSL_Parser.ESSL_COMMENT_LINE);
                                }
                            }
                            break;
                        default:
                            throw new NoViableAltException_1.NoViableAltException(this);
                    }
                    this.state = 2029;
                    this._errHandler.sync(this);
                    _alt = this.interpreter.adaptivePredict(this._input, 122, this._ctx);
                } while (_alt !== 2 && _alt !== ATN_1.ATN.INVALID_ALT_NUMBER);
            }
        }
        catch (re) {
            if (re instanceof RecognitionException_1.RecognitionException) {
                _localctx.exception = re;
                this._errHandler.reportError(this, re);
                this._errHandler.recover(this, re);
            }
            else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return _localctx;
    }
    static get _ATN() {
        if (!ESSL_Parser.__ATN) {
            ESSL_Parser.__ATN = new ATNDeserializer_1.ATNDeserializer().deserialize(Utils.toCharArray(ESSL_Parser._serializedATN));
        }
        return ESSL_Parser.__ATN;
    }
}
exports.ESSL_Parser = ESSL_Parser;
ESSL_Parser.T__0 = 1;
ESSL_Parser.T__1 = 2;
ESSL_Parser.T__2 = 3;
ESSL_Parser.T__3 = 4;
ESSL_Parser.T__4 = 5;
ESSL_Parser.T__5 = 6;
ESSL_Parser.T__6 = 7;
ESSL_Parser.T__7 = 8;
ESSL_Parser.T__8 = 9;
ESSL_Parser.T__9 = 10;
ESSL_Parser.T__10 = 11;
ESSL_Parser.T__11 = 12;
ESSL_Parser.T__12 = 13;
ESSL_Parser.T__13 = 14;
ESSL_Parser.T__14 = 15;
ESSL_Parser.T__15 = 16;
ESSL_Parser.T__16 = 17;
ESSL_Parser.T__17 = 18;
ESSL_Parser.T__18 = 19;
ESSL_Parser.T__19 = 20;
ESSL_Parser.T__20 = 21;
ESSL_Parser.T__21 = 22;
ESSL_Parser.T__22 = 23;
ESSL_Parser.T__23 = 24;
ESSL_Parser.T__24 = 25;
ESSL_Parser.T__25 = 26;
ESSL_Parser.T__26 = 27;
ESSL_Parser.T__27 = 28;
ESSL_Parser.T__28 = 29;
ESSL_Parser.T__29 = 30;
ESSL_Parser.T__30 = 31;
ESSL_Parser.T__31 = 32;
ESSL_Parser.T__32 = 33;
ESSL_Parser.T__33 = 34;
ESSL_Parser.T__34 = 35;
ESSL_Parser.T__35 = 36;
ESSL_Parser.T__36 = 37;
ESSL_Parser.T__37 = 38;
ESSL_Parser.T__38 = 39;
ESSL_Parser.T__39 = 40;
ESSL_Parser.T__40 = 41;
ESSL_Parser.T__41 = 42;
ESSL_Parser.T__42 = 43;
ESSL_Parser.T__43 = 44;
ESSL_Parser.T__44 = 45;
ESSL_Parser.T__45 = 46;
ESSL_Parser.T__46 = 47;
ESSL_Parser.T__47 = 48;
ESSL_Parser.T__48 = 49;
ESSL_Parser.T__49 = 50;
ESSL_Parser.T__50 = 51;
ESSL_Parser.T__51 = 52;
ESSL_Parser.T__52 = 53;
ESSL_Parser.T__53 = 54;
ESSL_Parser.T__54 = 55;
ESSL_Parser.T__55 = 56;
ESSL_Parser.T__56 = 57;
ESSL_Parser.T__57 = 58;
ESSL_Parser.T__58 = 59;
ESSL_Parser.T__59 = 60;
ESSL_Parser.T__60 = 61;
ESSL_Parser.T__61 = 62;
ESSL_Parser.T__62 = 63;
ESSL_Parser.T__63 = 64;
ESSL_Parser.T__64 = 65;
ESSL_Parser.T__65 = 66;
ESSL_Parser.T__66 = 67;
ESSL_Parser.T__67 = 68;
ESSL_Parser.T__68 = 69;
ESSL_Parser.T__69 = 70;
ESSL_Parser.T__70 = 71;
ESSL_Parser.T__71 = 72;
ESSL_Parser.T__72 = 73;
ESSL_Parser.T__73 = 74;
ESSL_Parser.T__74 = 75;
ESSL_Parser.T__75 = 76;
ESSL_Parser.T__76 = 77;
ESSL_Parser.T__77 = 78;
ESSL_Parser.T__78 = 79;
ESSL_Parser.T__79 = 80;
ESSL_Parser.T__80 = 81;
ESSL_Parser.T__81 = 82;
ESSL_Parser.T__82 = 83;
ESSL_Parser.T__83 = 84;
ESSL_Parser.T__84 = 85;
ESSL_Parser.T__85 = 86;
ESSL_Parser.T__86 = 87;
ESSL_Parser.T__87 = 88;
ESSL_Parser.T__88 = 89;
ESSL_Parser.T__89 = 90;
ESSL_Parser.T__90 = 91;
ESSL_Parser.T__91 = 92;
ESSL_Parser.T__92 = 93;
ESSL_Parser.T__93 = 94;
ESSL_Parser.T__94 = 95;
ESSL_Parser.T__95 = 96;
ESSL_Parser.T__96 = 97;
ESSL_Parser.T__97 = 98;
ESSL_Parser.T__98 = 99;
ESSL_Parser.T__99 = 100;
ESSL_Parser.T__100 = 101;
ESSL_Parser.T__101 = 102;
ESSL_Parser.T__102 = 103;
ESSL_Parser.T__103 = 104;
ESSL_Parser.T__104 = 105;
ESSL_Parser.T__105 = 106;
ESSL_Parser.T__106 = 107;
ESSL_Parser.T__107 = 108;
ESSL_Parser.T__108 = 109;
ESSL_Parser.T__109 = 110;
ESSL_Parser.T__110 = 111;
ESSL_Parser.T__111 = 112;
ESSL_Parser.T__112 = 113;
ESSL_Parser.T__113 = 114;
ESSL_Parser.T__114 = 115;
ESSL_Parser.T__115 = 116;
ESSL_Parser.T__116 = 117;
ESSL_Parser.T__117 = 118;
ESSL_Parser.T__118 = 119;
ESSL_Parser.T__119 = 120;
ESSL_Parser.T__120 = 121;
ESSL_Parser.T__121 = 122;
ESSL_Parser.T__122 = 123;
ESSL_Parser.T__123 = 124;
ESSL_Parser.T__124 = 125;
ESSL_Parser.T__125 = 126;
ESSL_Parser.T__126 = 127;
ESSL_Parser.T__127 = 128;
ESSL_Parser.T__128 = 129;
ESSL_Parser.T__129 = 130;
ESSL_Parser.T__130 = 131;
ESSL_Parser.T__131 = 132;
ESSL_Parser.T__132 = 133;
ESSL_Parser.T__133 = 134;
ESSL_Parser.T__134 = 135;
ESSL_Parser.T__135 = 136;
ESSL_Parser.T__136 = 137;
ESSL_Parser.T__137 = 138;
ESSL_Parser.T__138 = 139;
ESSL_Parser.T__139 = 140;
ESSL_Parser.T__140 = 141;
ESSL_Parser.T__141 = 142;
ESSL_Parser.T__142 = 143;
ESSL_Parser.T__143 = 144;
ESSL_Parser.T__144 = 145;
ESSL_Parser.T__145 = 146;
ESSL_Parser.T__146 = 147;
ESSL_Parser.T__147 = 148;
ESSL_Parser.T__148 = 149;
ESSL_Parser.T__149 = 150;
ESSL_Parser.T__150 = 151;
ESSL_Parser.T__151 = 152;
ESSL_Parser.T__152 = 153;
ESSL_Parser.T__153 = 154;
ESSL_Parser.T__154 = 155;
ESSL_Parser.T__155 = 156;
ESSL_Parser.T__156 = 157;
ESSL_Parser.T__157 = 158;
ESSL_Parser.T__158 = 159;
ESSL_Parser.T__159 = 160;
ESSL_Parser.T__160 = 161;
ESSL_Parser.T__161 = 162;
ESSL_Parser.T__162 = 163;
ESSL_Parser.T__163 = 164;
ESSL_Parser.T__164 = 165;
ESSL_Parser.T__165 = 166;
ESSL_Parser.T__166 = 167;
ESSL_Parser.T__167 = 168;
ESSL_Parser.T__168 = 169;
ESSL_Parser.T__169 = 170;
ESSL_Parser.T__170 = 171;
ESSL_Parser.T__171 = 172;
ESSL_Parser.T__172 = 173;
ESSL_Parser.T__173 = 174;
ESSL_Parser.T__174 = 175;
ESSL_Parser.T__175 = 176;
ESSL_Parser.T__176 = 177;
ESSL_Parser.T__177 = 178;
ESSL_Parser.T__178 = 179;
ESSL_Parser.T__179 = 180;
ESSL_Parser.T__180 = 181;
ESSL_Parser.DICTENTRIES = 182;
ESSL_Parser.FILTEREXPR = 183;
ESSL_Parser.DELIMITED_EXPRESSION = 184;
ESSL_Parser.BEFORE_COMMENT_LINE = 185;
ESSL_Parser.COMMENT_BLOCK = 186;
ESSL_Parser.ESSL_COMMENT_BLOCK = 187;
ESSL_Parser.ESSL_COMMENT_LINE = 188;
ESSL_Parser.QUOTED_STR = 189;
ESSL_Parser.POS_DEC = 190;
ESSL_Parser.POS_INT = 191;
ESSL_Parser.IDENTIFIER = 192;
ESSL_Parser.CAMELCASE_IDENTIFIER = 193;
ESSL_Parser.WHITESPACE = 194;
ESSL_Parser.NEWLINE = 195;
ESSL_Parser.RULE_top = 0;
ESSL_Parser.RULE_entity = 1;
ESSL_Parser.RULE_entityName = 2;
ESSL_Parser.RULE_entityBlock = 3;
ESSL_Parser.RULE_virtualEntity = 4;
ESSL_Parser.RULE_extendsEntity = 5;
ESSL_Parser.RULE_baseEntityName = 6;
ESSL_Parser.RULE_virtualEntityBlock = 7;
ESSL_Parser.RULE_namespace = 8;
ESSL_Parser.RULE_namespaceName = 9;
ESSL_Parser.RULE_dependsOn = 10;
ESSL_Parser.RULE_dependsOnNameSpace = 11;
ESSL_Parser.RULE_service = 12;
ESSL_Parser.RULE_serviceDecors = 13;
ESSL_Parser.RULE_serviceDecor = 14;
ESSL_Parser.RULE_allApiAuthPolicy = 15;
ESSL_Parser.RULE_allCommandAuthPolicy = 16;
ESSL_Parser.RULE_allQueryAuthPolicy = 17;
ESSL_Parser.RULE_allSubscriptionAuthPolicy = 18;
ESSL_Parser.RULE_allAdminAuthPolicy = 19;
ESSL_Parser.RULE_authPolicyNames = 20;
ESSL_Parser.RULE_ecosystemPoliciesMethod = 21;
ESSL_Parser.RULE_ecosystemPoliciesMethodName = 22;
ESSL_Parser.RULE_ecosystemSecretsMethod = 23;
ESSL_Parser.RULE_ecosystemSecretsMethodName = 24;
ESSL_Parser.RULE_umlGroupStrReplDecor = 25;
ESSL_Parser.RULE_umlGroupStrReplParams = 26;
ESSL_Parser.RULE_umlGroupTitleStrRepl = 27;
ESSL_Parser.RULE_umlStrReplOldValue = 28;
ESSL_Parser.RULE_umlStrReplNewValue = 29;
ESSL_Parser.RULE_umlImageDecor = 30;
ESSL_Parser.RULE_umlImageParams = 31;
ESSL_Parser.RULE_umlGroupImageName = 32;
ESSL_Parser.RULE_umlImageFormat = 33;
ESSL_Parser.RULE_umlImageRemoveTitle = 34;
ESSL_Parser.RULE_umlImageSubFolder = 35;
ESSL_Parser.RULE_entityType = 36;
ESSL_Parser.RULE_valueType = 37;
ESSL_Parser.RULE_interfaceType = 38;
ESSL_Parser.RULE_name = 39;
ESSL_Parser.RULE_extendsList = 40;
ESSL_Parser.RULE_baseName = 41;
ESSL_Parser.RULE_typeBlock = 42;
ESSL_Parser.RULE_possiblyEmptyTypeBlock = 43;
ESSL_Parser.RULE_entityTypeDecors = 44;
ESSL_Parser.RULE_entityTypeDecor = 45;
ESSL_Parser.RULE_partialCommand = 46;
ESSL_Parser.RULE_transCoordinator = 47;
ESSL_Parser.RULE_defaultConstructor = 48;
ESSL_Parser.RULE_indexTypeDecor = 49;
ESSL_Parser.RULE_uniqueIndexTypeDecor = 50;
ESSL_Parser.RULE_indexTypeAttrs = 51;
ESSL_Parser.RULE_indexKeyAttrs = 52;
ESSL_Parser.RULE_indexKeyAttr = 53;
ESSL_Parser.RULE_indexedField = 54;
ESSL_Parser.RULE_queryPartitionDecor = 55;
ESSL_Parser.RULE_queryPartitionAttrs = 56;
ESSL_Parser.RULE_partitionIndex = 57;
ESSL_Parser.RULE_partitionTypeUniqueIndex = 58;
ESSL_Parser.RULE_crossPartitionDecor = 59;
ESSL_Parser.RULE_umlGroupDecor = 60;
ESSL_Parser.RULE_umlGroup = 61;
ESSL_Parser.RULE_umlGroupTitle = 62;
ESSL_Parser.RULE_umlSubGroup = 63;
ESSL_Parser.RULE_hashLookupDecor = 64;
ESSL_Parser.RULE_beforeEventPersist = 65;
ESSL_Parser.RULE_authPolicyName = 66;
ESSL_Parser.RULE_authPolicyDecor = 67;
ESSL_Parser.RULE_publicDecor = 68;
ESSL_Parser.RULE_pluralNameDecor = 69;
ESSL_Parser.RULE_pluralName = 70;
ESSL_Parser.RULE_tableNameDecor = 71;
ESSL_Parser.RULE_tableName = 72;
ESSL_Parser.RULE_graphQLNameDecor = 73;
ESSL_Parser.RULE_graphQLName = 74;
ESSL_Parser.RULE_graphQLPluralNameDecor = 75;
ESSL_Parser.RULE_graphQLPluralName = 76;
ESSL_Parser.RULE_graphQLCamelCaseDecor = 77;
ESSL_Parser.RULE_graphQLCamelCase = 78;
ESSL_Parser.RULE_graphQLPluralCamelCaseDecor = 79;
ESSL_Parser.RULE_graphQLPluralCamelCase = 80;
ESSL_Parser.RULE_base64RefIdsDecor = 81;
ESSL_Parser.RULE_interfaceDecor = 82;
ESSL_Parser.RULE_interfaceName = 83;
ESSL_Parser.RULE_camelCaseDecor = 84;
ESSL_Parser.RULE_camelCaseName = 85;
ESSL_Parser.RULE_generateIfDecor = 86;
ESSL_Parser.RULE_generateIfName = 87;
ESSL_Parser.RULE_dimensionNameDecor = 88;
ESSL_Parser.RULE_styleDecor = 89;
ESSL_Parser.RULE_events = 90;
ESSL_Parser.RULE_slowlyChanging = 91;
ESSL_Parser.RULE_ledger = 92;
ESSL_Parser.RULE_factLedger = 93;
ESSL_Parser.RULE_indexAttrDecors = 94;
ESSL_Parser.RULE_indexAttrDecor = 95;
ESSL_Parser.RULE_cacheModel = 96;
ESSL_Parser.RULE_noCacheModel = 97;
ESSL_Parser.RULE_allowTemporal = 98;
ESSL_Parser.RULE_indexNameDecor = 99;
ESSL_Parser.RULE_indexName = 100;
ESSL_Parser.RULE_valueTypeDecors = 101;
ESSL_Parser.RULE_valueTypeDecor = 102;
ESSL_Parser.RULE_partial = 103;
ESSL_Parser.RULE_jsonConstructor = 104;
ESSL_Parser.RULE_webValueString = 105;
ESSL_Parser.RULE_webValueMethod = 106;
ESSL_Parser.RULE_discriminatedBy = 107;
ESSL_Parser.RULE_discriminatedByValue = 108;
ESSL_Parser.RULE_outputType = 109;
ESSL_Parser.RULE_voidPrimaryFact = 110;
ESSL_Parser.RULE_interfaceTypeDecors = 111;
ESSL_Parser.RULE_interfaceTypeDecor = 112;
ESSL_Parser.RULE_enumDecl = 113;
ESSL_Parser.RULE_enumBlock = 114;
ESSL_Parser.RULE_enumItem = 115;
ESSL_Parser.RULE_enumValue = 116;
ESSL_Parser.RULE_enumExplicitValue = 117;
ESSL_Parser.RULE_enumDecors = 118;
ESSL_Parser.RULE_enumDecor = 119;
ESSL_Parser.RULE_suppressDecor = 120;
ESSL_Parser.RULE_suppressOption = 121;
ESSL_Parser.RULE_modelsuppressOption = 122;
ESSL_Parser.RULE_graphQlsuppressOption = 123;
ESSL_Parser.RULE_inputTypeOption = 124;
ESSL_Parser.RULE_flagsDecor = 125;
ESSL_Parser.RULE_dictionary = 126;
ESSL_Parser.RULE_dictionaryBlock = 127;
ESSL_Parser.RULE_dictPartitioned = 128;
ESSL_Parser.RULE_dictValueTypeDecl = 129;
ESSL_Parser.RULE_dictValueType = 130;
ESSL_Parser.RULE_dictDefaultDecl = 131;
ESSL_Parser.RULE_dictDefault = 132;
ESSL_Parser.RULE_dictEntries = 133;
ESSL_Parser.RULE_dictionaryDecors = 134;
ESSL_Parser.RULE_dictionaryDecor = 135;
ESSL_Parser.RULE_fieldDef = 136;
ESSL_Parser.RULE_typeRef = 137;
ESSL_Parser.RULE_typeRefName = 138;
ESSL_Parser.RULE_primitiveType = 139;
ESSL_Parser.RULE_valueTypeRef = 140;
ESSL_Parser.RULE_listIndicator = 141;
ESSL_Parser.RULE_nullableIndicator = 142;
ESSL_Parser.RULE_fieldInitializer = 143;
ESSL_Parser.RULE_nullValue = 144;
ESSL_Parser.RULE_boolValue = 145;
ESSL_Parser.RULE_newInstance = 146;
ESSL_Parser.RULE_initialEnumVal = 147;
ESSL_Parser.RULE_numValue = 148;
ESSL_Parser.RULE_integer = 149;
ESSL_Parser.RULE_decimal = 150;
ESSL_Parser.RULE_stringValue = 151;
ESSL_Parser.RULE_flagsEnumSet = 152;
ESSL_Parser.RULE_flagsEnumValue = 153;
ESSL_Parser.RULE_fieldDecorators = 154;
ESSL_Parser.RULE_fieldDecorator = 155;
ESSL_Parser.RULE_indexDecor = 156;
ESSL_Parser.RULE_uniqueIndexDecor = 157;
ESSL_Parser.RULE_indexAttrs = 158;
ESSL_Parser.RULE_caseInsensitive = 159;
ESSL_Parser.RULE_nullHandling = 160;
ESSL_Parser.RULE_inlineEnumDecor = 161;
ESSL_Parser.RULE_inlineEnumValue = 162;
ESSL_Parser.RULE_composedDecor = 163;
ESSL_Parser.RULE_requiredDecor = 164;
ESSL_Parser.RULE_readOnlyDecor = 165;
ESSL_Parser.RULE_hiddenDecor = 166;
ESSL_Parser.RULE_calculatedDecor = 167;
ESSL_Parser.RULE_immutableDecor = 168;
ESSL_Parser.RULE_clonePartitionerDecor = 169;
ESSL_Parser.RULE_cloneIdAsIsDecor = 170;
ESSL_Parser.RULE_fieldUmlDecor = 171;
ESSL_Parser.RULE_fieldUmlGroupTitle = 172;
ESSL_Parser.RULE_direction = 173;
ESSL_Parser.RULE_horizontalVertical = 174;
ESSL_Parser.RULE_lineLength = 175;
ESSL_Parser.RULE_hideRelationship = 176;
ESSL_Parser.RULE_lineLabel = 177;
ESSL_Parser.RULE_sectionDecor = 178;
ESSL_Parser.RULE_breakType = 179;
ESSL_Parser.RULE_asValueTypeDecor = 180;
ESSL_Parser.RULE_sameAsDecor = 181;
ESSL_Parser.RULE_sameAsPersist = 182;
ESSL_Parser.RULE_constantDecor = 183;
ESSL_Parser.RULE_notPersistedDecor = 184;
ESSL_Parser.RULE_handCodedDecor = 185;
ESSL_Parser.RULE_dictDecor = 186;
ESSL_Parser.RULE_typeDiscriminator = 187;
ESSL_Parser.RULE_multiLineDecor = 188;
ESSL_Parser.RULE_questionDecor = 189;
ESSL_Parser.RULE_labelDecor = 190;
ESSL_Parser.RULE_label = 191;
ESSL_Parser.RULE_autoFillDecor = 192;
ESSL_Parser.RULE_autoFillType = 193;
ESSL_Parser.RULE_attributeDecor = 194;
ESSL_Parser.RULE_attributePair = 195;
ESSL_Parser.RULE_attributeKey = 196;
ESSL_Parser.RULE_attributeValue = 197;
ESSL_Parser.RULE_factDimensionDecor = 198;
ESSL_Parser.RULE_dimPrimary = 199;
ESSL_Parser.RULE_dimInx = 200;
ESSL_Parser.RULE_dimensionKeyDecor = 201;
ESSL_Parser.RULE_journalEntriesDecor = 202;
ESSL_Parser.RULE_entityEvents = 203;
ESSL_Parser.RULE_eventsBlock = 204;
ESSL_Parser.RULE_createdEventDef = 205;
ESSL_Parser.RULE_createdEvent = 206;
ESSL_Parser.RULE_eventDef = 207;
ESSL_Parser.RULE_eventName = 208;
ESSL_Parser.RULE_eventsDecors = 209;
ESSL_Parser.RULE_eventsDecor = 210;
ESSL_Parser.RULE_eventNameDecors = 211;
ESSL_Parser.RULE_eventNameDecor = 212;
ESSL_Parser.RULE_entityCommands = 213;
ESSL_Parser.RULE_commandsBlock = 214;
ESSL_Parser.RULE_commandsBlockDecorator = 215;
ESSL_Parser.RULE_commandsBlockDecorators = 216;
ESSL_Parser.RULE_command = 217;
ESSL_Parser.RULE_commandParams = 218;
ESSL_Parser.RULE_param = 219;
ESSL_Parser.RULE_paramDecl = 220;
ESSL_Parser.RULE_optionalTypeRef = 221;
ESSL_Parser.RULE_paramNameModifiers = 222;
ESSL_Parser.RULE_paramInitializer = 223;
ESSL_Parser.RULE_commandResultsIn = 224;
ESSL_Parser.RULE_yields = 225;
ESSL_Parser.RULE_eventList = 226;
ESSL_Parser.RULE_eventRef = 227;
ESSL_Parser.RULE_returnsType = 228;
ESSL_Parser.RULE_returnTypeRef = 229;
ESSL_Parser.RULE_paramDecorator = 230;
ESSL_Parser.RULE_typeQualifierDecor = 231;
ESSL_Parser.RULE_paramDecorators = 232;
ESSL_Parser.RULE_typeQualifierName = 233;
ESSL_Parser.RULE_indexQualifier = 234;
ESSL_Parser.RULE_filterExpr = 235;
ESSL_Parser.RULE_byValueDecor = 236;
ESSL_Parser.RULE_idDecor = 237;
ESSL_Parser.RULE_commandDecorator = 238;
ESSL_Parser.RULE_commandDecorators = 239;
ESSL_Parser.RULE_modelDecor = 240;
ESSL_Parser.RULE_asyncDecor = 241;
ESSL_Parser.RULE_asyncSpec = 242;
ESSL_Parser.RULE_asyncParamValidation = 243;
ESSL_Parser.RULE_asyncModelValidation = 244;
ESSL_Parser.RULE_asyncBusinessLogic = 245;
ESSL_Parser.RULE_syncDecor = 246;
ESSL_Parser.RULE_transDecor = 247;
ESSL_Parser.RULE_transAttr = 248;
ESSL_Parser.RULE_continuationDecor = 249;
ESSL_Parser.RULE_effectiveDateDecor = 250;
ESSL_Parser.RULE_explicitDecor = 251;
ESSL_Parser.RULE_internalDecor = 252;
ESSL_Parser.RULE_messageSourceDecor = 253;
ESSL_Parser.RULE_createDecor = 254;
ESSL_Parser.RULE_deleteDecor = 255;
ESSL_Parser.RULE_partialErrorDecor = 256;
ESSL_Parser.RULE_generateDecor = 257;
ESSL_Parser.RULE_generatorOption = 258;
ESSL_Parser.RULE_genSetField = 259;
ESSL_Parser.RULE_genAssignments = 260;
ESSL_Parser.RULE_assignmentList = 261;
ESSL_Parser.RULE_assignment = 262;
ESSL_Parser.RULE_valueExpression = 263;
ESSL_Parser.RULE_genAddToSet = 264;
ESSL_Parser.RULE_genRemoveFromSet = 265;
ESSL_Parser.RULE_genClearSet = 266;
ESSL_Parser.RULE_genUpdate = 267;
ESSL_Parser.RULE_jsonKey = 268;
ESSL_Parser.RULE_refField = 269;
ESSL_Parser.RULE_entityQueries = 270;
ESSL_Parser.RULE_queriesBlock = 271;
ESSL_Parser.RULE_queriesBlockDecorator = 272;
ESSL_Parser.RULE_queriesBlockDecorators = 273;
ESSL_Parser.RULE_query = 274;
ESSL_Parser.RULE_queryDecorator = 275;
ESSL_Parser.RULE_queryDecorators = 276;
ESSL_Parser.RULE_temporalDecor = 277;
ESSL_Parser.RULE_provideUserContextDecor = 278;
ESSL_Parser.RULE_provideGraphQLSchemaDecor = 279;
ESSL_Parser.RULE_entitySubscriptions = 280;
ESSL_Parser.RULE_subscriptionsBlock = 281;
ESSL_Parser.RULE_subscriptionsBlockDecorator = 282;
ESSL_Parser.RULE_subscriptionsBlockDecorators = 283;
ESSL_Parser.RULE_subscription = 284;
ESSL_Parser.RULE_subscriptionDecorator = 285;
ESSL_Parser.RULE_subscriptionDecorators = 286;
ESSL_Parser.RULE_dottedId = 287;
ESSL_Parser.RULE_comment = 288;
ESSL_Parser.RULE_beforeComment = 289;
ESSL_Parser.RULE_afterComment = 290;
ESSL_Parser.RULE_esslComment = 291;
ESSL_Parser.RULE_domBeforeLineComment = 292;
ESSL_Parser.RULE_domBlockComment = 293;
ESSL_Parser.RULE_esslBlockComment = 294;
ESSL_Parser.RULE_esslLineComment = 295;
// tslint:disable:no-trailing-whitespace
ESSL_Parser.ruleNames = [
    "top", "entity", "entityName", "entityBlock", "virtualEntity", "extendsEntity",
    "baseEntityName", "virtualEntityBlock", "namespace", "namespaceName",
    "dependsOn", "dependsOnNameSpace", "service", "serviceDecors", "serviceDecor",
    "allApiAuthPolicy", "allCommandAuthPolicy", "allQueryAuthPolicy", "allSubscriptionAuthPolicy",
    "allAdminAuthPolicy", "authPolicyNames", "ecosystemPoliciesMethod", "ecosystemPoliciesMethodName",
    "ecosystemSecretsMethod", "ecosystemSecretsMethodName", "umlGroupStrReplDecor",
    "umlGroupStrReplParams", "umlGroupTitleStrRepl", "umlStrReplOldValue",
    "umlStrReplNewValue", "umlImageDecor", "umlImageParams", "umlGroupImageName",
    "umlImageFormat", "umlImageRemoveTitle", "umlImageSubFolder", "entityType",
    "valueType", "interfaceType", "name", "extendsList", "baseName", "typeBlock",
    "possiblyEmptyTypeBlock", "entityTypeDecors", "entityTypeDecor", "partialCommand",
    "transCoordinator", "defaultConstructor", "indexTypeDecor", "uniqueIndexTypeDecor",
    "indexTypeAttrs", "indexKeyAttrs", "indexKeyAttr", "indexedField", "queryPartitionDecor",
    "queryPartitionAttrs", "partitionIndex", "partitionTypeUniqueIndex", "crossPartitionDecor",
    "umlGroupDecor", "umlGroup", "umlGroupTitle", "umlSubGroup", "hashLookupDecor",
    "beforeEventPersist", "authPolicyName", "authPolicyDecor", "publicDecor",
    "pluralNameDecor", "pluralName", "tableNameDecor", "tableName", "graphQLNameDecor",
    "graphQLName", "graphQLPluralNameDecor", "graphQLPluralName", "graphQLCamelCaseDecor",
    "graphQLCamelCase", "graphQLPluralCamelCaseDecor", "graphQLPluralCamelCase",
    "base64RefIdsDecor", "interfaceDecor", "interfaceName", "camelCaseDecor",
    "camelCaseName", "generateIfDecor", "generateIfName", "dimensionNameDecor",
    "styleDecor", "events", "slowlyChanging", "ledger", "factLedger", "indexAttrDecors",
    "indexAttrDecor", "cacheModel", "noCacheModel", "allowTemporal", "indexNameDecor",
    "indexName", "valueTypeDecors", "valueTypeDecor", "partial", "jsonConstructor",
    "webValueString", "webValueMethod", "discriminatedBy", "discriminatedByValue",
    "outputType", "voidPrimaryFact", "interfaceTypeDecors", "interfaceTypeDecor",
    "enumDecl", "enumBlock", "enumItem", "enumValue", "enumExplicitValue",
    "enumDecors", "enumDecor", "suppressDecor", "suppressOption", "modelsuppressOption",
    "graphQlsuppressOption", "inputTypeOption", "flagsDecor", "dictionary",
    "dictionaryBlock", "dictPartitioned", "dictValueTypeDecl", "dictValueType",
    "dictDefaultDecl", "dictDefault", "dictEntries", "dictionaryDecors", "dictionaryDecor",
    "fieldDef", "typeRef", "typeRefName", "primitiveType", "valueTypeRef",
    "listIndicator", "nullableIndicator", "fieldInitializer", "nullValue",
    "boolValue", "newInstance", "initialEnumVal", "numValue", "integer", "decimal",
    "stringValue", "flagsEnumSet", "flagsEnumValue", "fieldDecorators", "fieldDecorator",
    "indexDecor", "uniqueIndexDecor", "indexAttrs", "caseInsensitive", "nullHandling",
    "inlineEnumDecor", "inlineEnumValue", "composedDecor", "requiredDecor",
    "readOnlyDecor", "hiddenDecor", "calculatedDecor", "immutableDecor", "clonePartitionerDecor",
    "cloneIdAsIsDecor", "fieldUmlDecor", "fieldUmlGroupTitle", "direction",
    "horizontalVertical", "lineLength", "hideRelationship", "lineLabel", "sectionDecor",
    "breakType", "asValueTypeDecor", "sameAsDecor", "sameAsPersist", "constantDecor",
    "notPersistedDecor", "handCodedDecor", "dictDecor", "typeDiscriminator",
    "multiLineDecor", "questionDecor", "labelDecor", "label", "autoFillDecor",
    "autoFillType", "attributeDecor", "attributePair", "attributeKey", "attributeValue",
    "factDimensionDecor", "dimPrimary", "dimInx", "dimensionKeyDecor", "journalEntriesDecor",
    "entityEvents", "eventsBlock", "createdEventDef", "createdEvent", "eventDef",
    "eventName", "eventsDecors", "eventsDecor", "eventNameDecors", "eventNameDecor",
    "entityCommands", "commandsBlock", "commandsBlockDecorator", "commandsBlockDecorators",
    "command", "commandParams", "param", "paramDecl", "optionalTypeRef", "paramNameModifiers",
    "paramInitializer", "commandResultsIn", "yields", "eventList", "eventRef",
    "returnsType", "returnTypeRef", "paramDecorator", "typeQualifierDecor",
    "paramDecorators", "typeQualifierName", "indexQualifier", "filterExpr",
    "byValueDecor", "idDecor", "commandDecorator", "commandDecorators", "modelDecor",
    "asyncDecor", "asyncSpec", "asyncParamValidation", "asyncModelValidation",
    "asyncBusinessLogic", "syncDecor", "transDecor", "transAttr", "continuationDecor",
    "effectiveDateDecor", "explicitDecor", "internalDecor", "messageSourceDecor",
    "createDecor", "deleteDecor", "partialErrorDecor", "generateDecor", "generatorOption",
    "genSetField", "genAssignments", "assignmentList", "assignment", "valueExpression",
    "genAddToSet", "genRemoveFromSet", "genClearSet", "genUpdate", "jsonKey",
    "refField", "entityQueries", "queriesBlock", "queriesBlockDecorator",
    "queriesBlockDecorators", "query", "queryDecorator", "queryDecorators",
    "temporalDecor", "provideUserContextDecor", "provideGraphQLSchemaDecor",
    "entitySubscriptions", "subscriptionsBlock", "subscriptionsBlockDecorator",
    "subscriptionsBlockDecorators", "subscription", "subscriptionDecorator",
    "subscriptionDecorators", "dottedId", "comment", "beforeComment", "afterComment",
    "esslComment", "domBeforeLineComment", "domBlockComment", "esslBlockComment",
    "esslLineComment",
];
ESSL_Parser._LITERAL_NAMES = [
    undefined, "'entity'", "'{'", "'}'", "'virtual'", "':'", "'namespace'",
    "'dependsOn'", "'service'", "'@'", "'allApiAuthPolicy'", "'allCommandAuthPolicy'",
    "'allQueryAuthPolicy'", "'allSubscriptionAuthPolicy'", "'allAdminAuthPolicy'",
    "'('", "','", "')'", "'ecosystemPoliciesMethod'", "'ecosystemSecretsMethod'",
    "'umlGroupStrRepl'", "'umlImage'", "'svg'", "'png'", "'type'", "'interface'",
    "'partialCommand'", "'transCoordinator'", "'defCtor'", "'index'", "'uniqueIndex'",
    "'queryPartition'", "'crossPartition'", "'umlGroup'", "'hashLookup'",
    "'beforeEventPersist'", "'authPolicy'", "'public'", "'pluralName'", "'tableName'",
    "'graphQLName'", "'graphQLPluralName'", "'graphQLCamelCase'", "'graphQLPluralCamelCase'",
    "'base64RefIds'", "'camelCase'", "'generateIf'", "'factDimension'", "'style'",
    "'events'", "'slowlyChanging'", "'ledger'", "'factLedger'", "'cacheModel'",
    "'noCacheModel'", "'allowTemporal'", "'indexName'", "'partial'", "'jCtor'",
    "'webString'", "'discriminatedBy'", "'outputType'", "'voidPrimaryFactDimension'",
    "'enum'", "'='", "'suppress'", "'model'", "'graphQl'", "'inputType'",
    "'flags'", "'dictionary'", "'partitioned'", "'valueType'", "'default'",
    "'bool'", "'date'", "'datetime'", "'decimal'", "'float'", "'int'", "'string'",
    "'uuid'", "'['", "']'", "'?'", "'null'", "'true'", "'false'", "'new'",
    "'-'", "'caseSensitive'", "'caseInSensitive'", "'ignoreNulls'", "'includeNulls'",
    "'composed'", "'req'", "'ro'", "'hidden'", "'calc'", "'immutable'", "'clonePartitioner'",
    "'cloneIdAsIs'", "'uml'", "'left'", "'right'", "'up'", "'down'", "'l'",
    "'r'", "'u'", "'d'", "'horizontal'", "'vertical'", "'h'", "'v'", "'v1'",
    "'v2'", "'v3'", "'v4'", "'v5'", "'v6'", "'v7'", "'v8'", "'v9'", "'x'",
    "'section'", "'..'", "'--'", "'=='", "'asValueType'", "'sameAs'", "'persist'",
    "'const'", "'notPersisted'", "'handCoded'", "'typeDiscriminator'", "'multiLine'",
    "'question'", "'label'", "'autofill'", "'utcnow'", "'geotag'", "'attributes'",
    "'primary'", "'dimensionKey'", "'journalEntries'", "'commands'", "'yields'",
    "'returns'", "'byValue'", "'id'", "'async'", "'paramValidation'", "'modelValidation'",
    "'businessLogic'", "'sync'", "'trans'", "'coordinator'", "'pending'",
    "'committed'", "'reversed'", "'continuation'", "'eff'", "'explicit'",
    "'internal'", "'messageSource'", "'create'", "'del'", "'partialError'",
    "'gen'", "'setField'", "'assignments'", "'addToSet'", "'removeFromSet'",
    "'clearSet'", "'update'", "'queries'", "'temporal'", "'provideUserContext'",
    "'provideGraphQLSchema'", "'subscriptions'", "'.'",
];
ESSL_Parser._SYMBOLIC_NAMES = [
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    undefined, undefined, undefined, undefined, undefined, undefined, undefined,
    "DICTENTRIES", "FILTEREXPR", "DELIMITED_EXPRESSION", "BEFORE_COMMENT_LINE",
    "COMMENT_BLOCK", "ESSL_COMMENT_BLOCK", "ESSL_COMMENT_LINE", "QUOTED_STR",
    "POS_DEC", "POS_INT", "IDENTIFIER", "CAMELCASE_IDENTIFIER", "WHITESPACE",
    "NEWLINE",
];
ESSL_Parser.VOCABULARY = new VocabularyImpl_1.VocabularyImpl(ESSL_Parser._LITERAL_NAMES, ESSL_Parser._SYMBOLIC_NAMES, []);
ESSL_Parser._serializedATNSegments = 4;
ESSL_Parser._serializedATNSegment0 = "\x03\uC91D\uCABA\u058D\uAFBA\u4F53\u0607\uEA8B\uC241\x03\xC5\u07F2\x04" +
    "\x02\t\x02\x04\x03\t\x03\x04\x04\t\x04\x04\x05\t\x05\x04\x06\t\x06\x04" +
    "\x07\t\x07\x04\b\t\b\x04\t\t\t\x04\n\t\n\x04\v\t\v\x04\f\t\f\x04\r\t\r" +
    "\x04\x0E\t\x0E\x04\x0F\t\x0F\x04\x10\t\x10\x04\x11\t\x11\x04\x12\t\x12" +
    "\x04\x13\t\x13\x04\x14\t\x14\x04\x15\t\x15\x04\x16\t\x16\x04\x17\t\x17" +
    "\x04\x18\t\x18\x04\x19\t\x19\x04\x1A\t\x1A\x04\x1B\t\x1B\x04\x1C\t\x1C" +
    "\x04\x1D\t\x1D\x04\x1E\t\x1E\x04\x1F\t\x1F\x04 \t \x04!\t!\x04\"\t\"\x04" +
    "#\t#\x04$\t$\x04%\t%\x04&\t&\x04\'\t\'\x04(\t(\x04)\t)\x04*\t*\x04+\t" +
    "+\x04,\t,\x04-\t-\x04.\t.\x04/\t/\x040\t0\x041\t1\x042\t2\x043\t3\x04" +
    "4\t4\x045\t5\x046\t6\x047\t7\x048\t8\x049\t9\x04:\t:\x04;\t;\x04<\t<\x04" +
    "=\t=\x04>\t>\x04?\t?\x04@\t@\x04A\tA\x04B\tB\x04C\tC\x04D\tD\x04E\tE\x04" +
    "F\tF\x04G\tG\x04H\tH\x04I\tI\x04J\tJ\x04K\tK\x04L\tL\x04M\tM\x04N\tN\x04" +
    "O\tO\x04P\tP\x04Q\tQ\x04R\tR\x04S\tS\x04T\tT\x04U\tU\x04V\tV\x04W\tW\x04" +
    "X\tX\x04Y\tY\x04Z\tZ\x04[\t[\x04\\\t\\\x04]\t]\x04^\t^\x04_\t_\x04`\t" +
    "`\x04a\ta\x04b\tb\x04c\tc\x04d\td\x04e\te\x04f\tf\x04g\tg\x04h\th\x04" +
    "i\ti\x04j\tj\x04k\tk\x04l\tl\x04m\tm\x04n\tn\x04o\to\x04p\tp\x04q\tq\x04" +
    "r\tr\x04s\ts\x04t\tt\x04u\tu\x04v\tv\x04w\tw\x04x\tx\x04y\ty\x04z\tz\x04" +
    "{\t{\x04|\t|\x04}\t}\x04~\t~\x04\x7F\t\x7F\x04\x80\t\x80\x04\x81\t\x81" +
    "\x04\x82\t\x82\x04\x83\t\x83\x04\x84\t\x84\x04\x85\t\x85\x04\x86\t\x86" +
    "\x04\x87\t\x87\x04\x88\t\x88\x04\x89\t\x89\x04\x8A\t\x8A\x04\x8B\t\x8B" +
    "\x04\x8C\t\x8C\x04\x8D\t\x8D\x04\x8E\t\x8E\x04\x8F\t\x8F\x04\x90\t\x90" +
    "\x04\x91\t\x91\x04\x92\t\x92\x04\x93\t\x93\x04\x94\t\x94\x04\x95\t\x95" +
    "\x04\x96\t\x96\x04\x97\t\x97\x04\x98\t\x98\x04\x99\t\x99\x04\x9A\t\x9A" +
    "\x04\x9B\t\x9B\x04\x9C\t\x9C\x04\x9D\t\x9D\x04\x9E\t\x9E\x04\x9F\t\x9F" +
    "\x04\xA0\t\xA0\x04\xA1\t\xA1\x04\xA2\t\xA2\x04\xA3\t\xA3\x04\xA4\t\xA4" +
    "\x04\xA5\t\xA5\x04\xA6\t\xA6\x04\xA7\t\xA7\x04\xA8\t\xA8\x04\xA9\t\xA9" +
    "\x04\xAA\t\xAA\x04\xAB\t\xAB\x04\xAC\t\xAC\x04\xAD\t\xAD\x04\xAE\t\xAE" +
    "\x04\xAF\t\xAF\x04\xB0\t\xB0\x04\xB1\t\xB1\x04\xB2\t\xB2\x04\xB3\t\xB3" +
    "\x04\xB4\t\xB4\x04\xB5\t\xB5\x04\xB6\t\xB6\x04\xB7\t\xB7\x04\xB8\t\xB8" +
    "\x04\xB9\t\xB9\x04\xBA\t\xBA\x04\xBB\t\xBB\x04\xBC\t\xBC\x04\xBD\t\xBD" +
    "\x04\xBE\t\xBE\x04\xBF\t\xBF\x04\xC0\t\xC0\x04\xC1\t\xC1\x04\xC2\t\xC2" +
    "\x04\xC3\t\xC3\x04\xC4\t\xC4\x04\xC5\t\xC5\x04\xC6\t\xC6\x04\xC7\t\xC7" +
    "\x04\xC8\t\xC8\x04\xC9\t\xC9\x04\xCA\t\xCA\x04\xCB\t\xCB\x04\xCC\t\xCC" +
    "\x04\xCD\t\xCD\x04\xCE\t\xCE\x04\xCF\t\xCF\x04\xD0\t\xD0\x04\xD1\t\xD1" +
    "\x04\xD2\t\xD2\x04\xD3\t\xD3\x04\xD4\t\xD4\x04\xD5\t\xD5\x04\xD6\t\xD6" +
    "\x04\xD7\t\xD7\x04\xD8\t\xD8\x04\xD9\t\xD9\x04\xDA\t\xDA\x04\xDB\t\xDB" +
    "\x04\xDC\t\xDC\x04\xDD\t\xDD\x04\xDE\t\xDE\x04\xDF\t\xDF\x04\xE0\t\xE0" +
    "\x04\xE1\t\xE1\x04\xE2\t\xE2\x04\xE3\t\xE3\x04\xE4\t\xE4\x04\xE5\t\xE5" +
    "\x04\xE6\t\xE6\x04\xE7\t\xE7\x04\xE8\t\xE8\x04\xE9\t\xE9\x04\xEA\t\xEA" +
    "\x04\xEB\t\xEB\x04\xEC\t\xEC\x04\xED\t\xED\x04\xEE\t\xEE\x04\xEF\t\xEF" +
    "\x04\xF0\t\xF0\x04\xF1\t\xF1\x04\xF2\t\xF2\x04\xF3\t\xF3\x04\xF4\t\xF4" +
    "\x04\xF5\t\xF5\x04\xF6\t\xF6\x04\xF7\t\xF7\x04\xF8\t\xF8\x04\xF9\t\xF9" +
    "\x04\xFA\t\xFA\x04\xFB\t\xFB\x04\xFC\t\xFC\x04\xFD\t\xFD\x04\xFE\t\xFE" +
    "\x04\xFF\t\xFF\x04\u0100\t\u0100\x04\u0101\t\u0101\x04\u0102\t\u0102\x04" +
    "\u0103\t\u0103\x04\u0104\t\u0104\x04\u0105\t\u0105\x04\u0106\t\u0106\x04" +
    "\u0107\t\u0107\x04\u0108\t\u0108\x04\u0109\t\u0109\x04\u010A\t\u010A\x04" +
    "\u010B\t\u010B\x04\u010C\t\u010C\x04\u010D\t\u010D\x04\u010E\t\u010E\x04" +
    "\u010F\t\u010F\x04\u0110\t\u0110\x04\u0111\t\u0111\x04\u0112\t\u0112\x04" +
    "\u0113\t\u0113\x04\u0114\t\u0114\x04\u0115\t\u0115\x04\u0116\t\u0116\x04" +
    "\u0117\t\u0117\x04\u0118\t\u0118\x04\u0119\t\u0119\x04\u011A\t\u011A\x04" +
    "\u011B\t\u011B\x04\u011C\t\u011C\x04\u011D\t\u011D\x04\u011E\t\u011E\x04" +
    "\u011F\t\u011F\x04\u0120\t\u0120\x04\u0121\t\u0121\x04\u0122\t\u0122\x04" +
    "\u0123\t\u0123\x04\u0124\t\u0124\x04\u0125\t\u0125\x04\u0126\t\u0126\x04" +
    "\u0127\t\u0127\x04\u0128\t\u0128\x04\u0129\t\u0129\x03\x02\x03\x02\x05" +
    "\x02\u0255\n\x02\x03\x02\x03\x02\x03\x03\x03\x03\x03\x03\x03\x03\x03\x03" +
    "\x03\x04\x03\x04\x03\x05\x03\x05\x03\x05\x03\x05\x07\x05\u0264\n\x05\f" +
    "\x05\x0E\x05\u0267\v\x05\x03\x05\x05\x05\u026A\n\x05\x03\x05\x03\x05\x03" +
    "\x05\x03\x05\x03\x05\x07\x05\u0271\n\x05\f\x05\x0E\x05\u0274\v\x05\x03" +
    "\x05\x03\x05\x05\x05\u0278\n\x05\x03\x05\x05\x05\u027B\n\x05\x03\x05\x05" +
    "\x05\u027E\n\x05\x03\x05\x03\x05\x03\x06\x03\x06\x03\x06\x03\x06\x03\x06" +
    "\x03\x06\x03\x06\x03\x07\x03\x07\x05\x07\u028B\n\x07\x03\b\x03\b\x03\t" +
    "\x03\t\x03\t\x07\t\u0292\n\t\f\t\x0E\t\u0295\v\t\x03\t\x05\t\u0298\n\t" +
    "\x03\t\x03\t\x03\t\x03\t\x07\t\u029E\n\t\f\t\x0E\t\u02A1\v\t\x03\t\x05" +
    "\t\u02A4\n\t\x03\t\x05\t\u02A7\n\t\x03\t\x05\t\u02AA\n\t\x03\t\x03\t\x03" +
    "\n\x03\n\x03\n\x03\n\x03\v\x03\v\x03\f\x03\f\x03\f\x03\f\x03\r\x03\r\x03" +
    "\x0E\x03\x0E\x03\x0E\x03\x0E\x03\x0E\x03\x0F\x03\x0F\x03\x0F\x07\x0F\u02C2" +
    "\n\x0F\f\x0F\x0E\x0F\u02C5\v\x0F\x03\x10\x03\x10\x03\x10\x03\x10\x03\x10" +
    "\x03\x10\x03\x10\x03\x10\x03\x10\x03\x10\x05\x10\u02D1\n\x10\x03\x11\x03" +
    "\x11\x03\x11\x03\x12\x03\x12\x03\x12\x03\x13\x03\x13\x03\x13\x03\x14\x03" +
    "\x14\x03\x14\x03\x15\x03\x15\x03\x15\x03\x16\x03\x16\x03\x16\x03\x16\x07" +
    "\x16\u02E6\n\x16\f\x16\x0E\x16\u02E9\v\x16\x03\x16\x03\x16\x03\x17\x03" +
    "\x17\x03\x17\x03\x17\x03\x17\x03\x18\x03\x18\x03\x19\x03\x19\x03\x19\x03" +
    "\x19\x03\x19\x03\x1A\x03\x1A\x03\x1B\x03\x1B\x03\x1B\x03\x1C\x03\x1C\x03" +
    "\x1C\x03\x1C\x03\x1C\x03\x1C\x03\x1C\x03\x1C\x03\x1D\x03\x1D\x03\x1E\x03" +
    "\x1E\x03\x1F\x03\x1F\x03 \x03 \x03 \x03!\x03!\x03!\x03!\x05!\u0313\n!" +
    "\x03!\x03!\x05!\u0317\n!\x03!\x03!\x05!\u031B\n!\x03!\x03!\x03\"\x03\"" +
    "\x03#\x03#\x03$\x03$\x03%\x03%\x03&\x03&\x03&\x03&\x03&\x03&\x03\'\x03" +
    "\'\x03\'\x03\'\x03\'\x03\'\x03\'\x03(\x03(\x03(\x03(\x03(\x03(\x03(\x03" +
    ")\x03)\x03*\x03*\x03*\x03*\x07*\u0341\n*\f*\x0E*\u0344\v*\x05*\u0346\n" +
    "*\x03+\x03+\x03,\x03,\x06,\u034C\n,\r,\x0E,\u034D\x03,\x03,\x03,\x03-" +
    "\x03-\x07-\u0355\n-\f-\x0E-\u0358\v-\x03-\x03-\x03-\x03.\x03.\x03.\x07" +
    ".\u0360\n.\f.\x0E.\u0363\v.\x03/\x03/\x03/\x03/\x03/\x03/\x03/\x03/\x03" +
    "/\x03/\x03/\x03/\x03/\x03/\x03/\x03/\x03/\x03/\x03/\x03/\x03/\x03/\x03" +
    "/\x03/\x03/\x03/\x03/\x05/\u0380\n/\x030\x030\x031\x031\x032\x032\x03" +
    "3\x033\x033\x034\x034\x034\x035\x035\x035\x035\x035\x035\x035\x035\x05" +
    "5\u0396\n5\x065\u0398\n5\r5\x0E5\u0399\x035\x055\u039D\n5\x035\x035\x03" +
    "6\x036\x036\x056\u03A4\n6\x037\x037\x037\x037\x057\u03AA\n7\x038\x038" +
    "\x039\x039\x039\x03:\x03:\x03:\x03:\x03:\x03:\x03;\x03;\x03<\x03<\x03" +
    "=\x03=\x03>\x03>\x03>\x03?\x03?\x03?\x03?\x05?\u03C4\n?\x03?\x03?\x03" +
    "@\x03@\x03A\x03A\x03B\x03B\x03C\x03C\x03D\x03D\x03E\x03E\x03E\x03F\x03" +
    "F\x03G\x03G\x03G\x03G\x03G\x03H\x03H\x03I\x03I\x03I\x03I\x03I\x03J\x03" +
    "J\x03K\x03K\x03K\x03K\x03K\x03L\x03L\x03M\x03M\x03M\x03M\x03M\x03N\x03" +
    "N\x03O\x03O\x03O\x03O\x03O\x03P\x03P\x03Q\x03Q\x03Q\x03Q\x03Q\x03R\x03" +
    "R\x03S\x03S\x03T\x03T\x03T\x03T\x03T\x03T\x05T\u0409\nT\x03T\x03T\x03" +
    "U\x03U\x03V\x03V\x03V\x03V\x03V\x03W\x03W\x03X\x03X\x03X\x03X\x03X\x03" +
    "Y\x03Y\x03Z\x03Z\x03[\x03[\x03[\x03[\x03[\x03[\x05[\u0425\n[\x03[\x03" +
    "[\x03\\\x03\\\x03]\x03]\x03^\x03^\x03_\x03_\x03`\x03`\x07`\u0433\n`\f" +
    "`\x0E`\u0436\v`\x03a\x03a\x03a\x03a\x03a\x05a\u043D\na\x03b\x03b\x03c" +
    "\x03c\x03d\x03d\x03e\x03e\x03e\x03e\x03e\x03f\x03f\x03g\x03g\x03g\x07" +
    "g\u044F\ng\fg\x0Eg\u0452\vg\x03h\x03h\x03h\x03h\x03h\x03h\x03h\x03h\x03" +
    "h\x03h\x03h\x03h\x03h\x03h\x03h\x03h\x03h\x05h\u0465\nh\x03i\x03i\x03" +
    "j\x03j\x03k\x03k\x03k\x03k\x03k\x03l\x03l\x03m\x03m\x03m\x03m\x03m\x03" +
    "n\x03n\x03o\x03o\x03p\x03p\x03q\x03q\x03q\x07q\u0480\nq\fq\x0Eq\u0483" +
    "\vq\x03r\x03r\x03r\x03r\x05r\u0489\nr\x03s\x03s\x03s\x03s\x03s\x03s\x03" +
    "t\x03t\x03t\x05t\u0494\nt\x03t\x07t\u0497\nt\ft\x0Et\u049A\vt\x03t\x03" +
    "t\x03u\x03u\x03u\x03u\x05u\u04A2\nu\x03u\x03u\x03v\x03v\x03w\x03w\x03" +
    "x\x07x\u04AB\nx\fx\x0Ex\u04AE\vx\x03y\x03y\x03y\x03y\x05y\u04B4\ny\x03" +
    "z\x03z\x03z\x03z\x05z\u04BA\nz\x03z\x07z\u04BD\nz\fz\x0Ez\u04C0\vz\x03" +
    "z\x03z\x03{\x03{\x03{\x05{\u04C7\n{\x03|\x03|\x03}\x03}\x03~\x03~\x03" +
    "\x7F\x03\x7F\x03\x80\x03\x80\x03\x80\x03\x80\x03\x80\x03\x80\x03\x81\x03" +
    "\x81\x03\x81\x03\x81\x03\x81\x07\x81\u04DC\n\x81\f\x81\x0E\x81\u04DF\v" +
    "\x81\x03\x81\x05\x81\u04E2\n\x81\x03\x81\x03\x81\x03\x82\x03\x82\x03\x83" +
    "\x03\x83\x03\x83\x03\x83\x03\x83\x03\x84\x03\x84\x03\x85\x03\x85\x03\x85" +
    "\x03\x85\x03\x85\x03\x86\x03\x86\x03\x87\x03\x87\x03\x88\x07\x88\u04F9" +
    "\n\x88\f\x88\x0E\x88\u04FC\v\x88\x03\x89\x03\x89\x03\x89\x03\x8A\x03\x8A" +
    "\x03\x8A\x03\x8A\x03\x8A\x03\x8A\x03\x8A\x03\x8B\x03\x8B\x05\x8B\u050A" +
    "\n\x8B\x03\x8B\x05\x8B\u050D\n\x8B\x03\x8C\x03\x8C\x05\x8C\u0511\n\x8C" +
    "\x03\x8D\x03\x8D\x03\x8E\x03\x8E\x03\x8F\x03\x8F\x03\x8F\x03\x90\x03\x90" +
    "\x03\x91\x03\x91\x03\x91\x03\x91\x03\x91\x03\x91\x03\x91\x03\x91\x05\x91" +
    "\u0524\n\x91\x05\x91\u0526\n\x91\x03\x92\x03\x92\x03\x93\x03\x93\x03\x94" +
    "\x03\x94\x03\x95\x03\x95\x03\x96\x03\x96\x05\x96\u0532\n\x96\x03\x97\x05" +
    "\x97\u0535\n\x97\x03\x97\x03\x97\x03\x98\x05\x98\u053A\n\x98\x03\x98\x03" +
    "\x98\x03\x99\x03\x99\x03\x9A\x03\x9A\x03\x9A\x03\x9A\x07\x9A\u0544\n\x9A" +
    "\f\x9A\x0E\x9A\u0547\v\x9A\x05\x9A\u0549\n\x9A\x03\x9A\x03\x9A\x03\x9B" +
    "\x03\x9B\x03\x9C\x03\x9C\x03\x9C\x07\x9C\u0552\n\x9C\f\x9C\x0E\x9C\u0555" +
    "\v\x9C\x03\x9D\x03\x9D\x03\x9D\x03\x9D\x03\x9D\x03\x9D\x03\x9D\x03\x9D" +
    "\x03\x9D\x03\x9D\x03\x9D\x03\x9D\x03\x9D\x03\x9D\x03\x9D\x03\x9D\x03\x9D" +
    "\x03\x9D\x03\x9D\x03\x9D\x03\x9D\x03\x9D\x03\x9D\x03\x9D\x03\x9D\x03\x9D" +
    "\x03\x9D\x03\x9D\x03\x9D\x03\x9D\x03\x9D\x03\x9D\x05\x9D\u0577\n\x9D\x03" +
    "\x9E\x03\x9E\x03\x9E\x03\x9F\x03\x9F\x03\x9F\x03\xA0\x03\xA0\x03\xA0\x03" +
    "\xA0\x05\xA0\u0583\n\xA0\x03\xA0\x03\xA0\x05\xA0\u0587\n\xA0\x03\xA0\x05" +
    "\xA0\u058A\n\xA0\x03\xA0\x03\xA0\x03\xA1\x03\xA1\x03\xA2\x03\xA2\x03\xA3" +
    "\x03\xA3\x03\xA3\x03\xA3\x03\xA3\x07\xA3\u0597\n\xA3\f\xA3\x0E\xA3\u059A" +
    "\v\xA3\x03\xA3\x03\xA3\x03\xA4\x03\xA4\x03\xA5\x03\xA5\x03\xA6\x03\xA6" +
    "\x03\xA7\x03\xA7\x03\xA8\x03\xA8\x03\xA9\x03\xA9\x03\xAA\x03\xAA\x03\xAB" +
    "\x03\xAB\x03\xAC\x03\xAC\x03\xAD\x03\xAD\x03\xAD\x03\xAD\x03\xAD\x03\xAD" +
    "\x03\xAD\x03\xAD\x07\xAD\u05B8\n\xAD\f\xAD\x0E\xAD\u05BB\v\xAD\x03\xAD" +
    "\x03\xAD\x03\xAE\x03\xAE\x03\xAF\x03\xAF\x03\xB0\x03\xB0\x03\xB1\x03\xB1" +
    "\x03\xB2\x03\xB2\x03\xB3\x03\xB3\x03\xB4\x03\xB4\x03\xB4\x03\xB4\x03\xB4" +
    "\x03\xB5\x03\xB5\x03\xB6\x03\xB6\x03\xB7\x03\xB7\x03\xB7\x03\xB7\x03\xB7" +
    "\x05\xB7\u05D9\n\xB7\x03\xB7\x03\xB7\x03\xB8\x03\xB8\x03\xB9\x03\xB9\x03" +
    "\xBA\x03\xBA\x03\xBB\x03\xBB\x03\xBC\x03\xBC\x03\xBD\x03\xBD\x03\xBE\x03" +
    "\xBE\x03\xBF\x03\xBF\x03\xC0\x03\xC0\x03\xC0\x03\xC0\x03\xC0\x03\xC1\x03" +
    "\xC1\x03\xC2\x03\xC2\x03\xC2\x03\xC2\x03\xC2\x03\xC3\x03\xC3\x03\xC4\x03" +
    "\xC4\x03\xC4\x03\xC4\x03\xC4\x07\xC4\u0600\n\xC4\f\xC4\x0E\xC4\u0603\v" +
    "\xC4\x03\xC4\x03\xC4\x03\xC5\x03\xC5\x03\xC5\x03\xC5\x03\xC6\x03\xC6\x03" +
    "\xC7\x03\xC7\x03\xC8\x03\xC8\x03\xC8\x03\xC8\x05\xC8\u0613\n\xC8\x03\xC8" +
    "\x03\xC8\x03\xC9\x03\xC9\x03\xCA\x03\xCA\x03\xCB\x03\xCB\x03\xCC\x03\xCC" +
    "\x03\xCD\x03\xCD\x03\xCD\x03\xCD\x03\xCD\x03\xCE\x03\xCE\x03\xCE\x07\xCE" +
    "\u0627\n\xCE\f\xCE\x0E\xCE\u062A\v\xCE\x03\xCE\x03\xCE\x03\xCF\x03\xCF" +
    "\x03\xCF\x03\xCF\x03\xCF\x03\xD0\x03\xD0\x03\xD1\x03\xD1\x03\xD1\x03\xD1" +
    "\x03\xD1\x03\xD2\x03\xD2\x03\xD3\x07\xD3\u063D\n\xD3\f\xD3\x0E\xD3\u0640" +
    "\v\xD3\x03\xD4\x03\xD4\x03\xD4\x03\xD5\x07\xD5\u0646\n\xD5\f\xD5\x0E\xD5" +
    "\u0649\v\xD5\x03\xD6\x03\xD6\x03\xD6\x03\xD7\x03\xD7\x03\xD7\x03\xD7\x03" +
    "\xD8\x03\xD8\x07\xD8\u0654\n\xD8\f\xD8\x0E\xD8\u0657\v\xD8\x03\xD8\x03" +
    "\xD8\x03\xD8\x03\xD9\x03\xD9\x03\xD9\x03\xDA\x03\xDA\x03\xDA\x07\xDA\u0662" +
    "\n\xDA\f\xDA\x0E\xDA\u0665\v\xDA\x03\xDB\x03\xDB\x03\xDB\x03\xDB\x03\xDB" +
    "\x03\xDB\x03\xDB\x03\xDC\x03\xDC\x03\xDC\x03\xDC\x07\xDC\u0672\n\xDC\f" +
    "\xDC\x0E\xDC\u0675\v\xDC\x05\xDC\u0677\n\xDC\x03\xDC\x03\xDC\x03\xDD\x03" +
    "\xDD\x03\xDD\x03\xDD\x03\xDD\x05\xDD\u0680\n\xDD\x03\xDE\x03\xDE\x03\xDE" +
    "\x03\xDF\x03\xDF\x03\xDF\x05\xDF\u0688\n\xDF\x03\xE0\x05\xE0\u068B\n\xE0" +
    "\x03\xE0\x05\xE0\u068E\n\xE0\x03\xE1\x03\xE1\x03\xE1\x03\xE1\x03\xE1\x03" +
    "\xE1\x05\xE1\u0696\n\xE1\x05\xE1\u0698\n\xE1\x03\xE2\x03\xE2\x05\xE2\u069C" +
    "\n\xE2\x03\xE3\x03\xE3\x03\xE3\x03\xE4\x03\xE4\x03\xE4\x07\xE4\u06A4\n" +
    "\xE4\f\xE4\x0E\xE4\u06A7\v\xE4\x03\xE5\x03\xE5\x03\xE6\x03\xE6\x03\xE6" +
    "\x03\xE6\x03\xE7\x03\xE7\x03\xE8\x03\xE8\x03\xE8\x03\xE8\x05\xE8\u06B5" +
    "\n\xE8\x03\xE9\x03\xE9\x03\xE9\x03\xE9\x03\xE9\x05\xE9\u06BC\n\xE9\x03" +
    "\xE9\x03\xE9\x05\xE9\u06C0\n\xE9\x03\xE9\x03\xE9\x03\xEA\x03\xEA\x03\xEA" +
    "\x07\xEA\u06C7\n\xEA\f\xEA\x0E\xEA\u06CA\v\xEA\x03\xEB\x03\xEB\x03\xEC" +
    "\x03\xEC\x03\xED\x03\xED\x03\xEE\x03\xEE\x03\xEF\x03\xEF\x03\xF0\x03\xF0" +
    "\x03\xF0\x03\xF0\x03\xF0\x03\xF0\x03\xF0\x03\xF0\x03\xF0\x03\xF0\x03\xF0" +
    "\x03\xF0\x03\xF0\x03\xF0\x03\xF0\x03\xF0\x03\xF0\x05\xF0\u06E7\n\xF0\x03" +
    "\xF1\x03\xF1\x03\xF1\x07\xF1\u06EC\n\xF1\f\xF1\x0E\xF1\u06EF\v\xF1\x03" +
    "\xF2\x03\xF2\x03\xF3\x03\xF3\x03\xF3\x03\xF3\x03\xF3\x05\xF3\u06F8\n\xF3" +
    "\x03\xF3\x03\xF3\x05\xF3\u06FC\n\xF3\x03\xF3\x03\xF3\x03\xF4\x03\xF4\x03" +
    "\xF4\x05\xF4\u0703\n\xF4\x03\xF5\x03\xF5\x03\xF6\x03\xF6\x03\xF7\x03\xF7" +
    "\x03\xF8\x03\xF8\x03\xF9\x03\xF9\x03\xF9\x03\xF9\x03\xF9\x03\xFA\x03\xFA" +
    "\x03\xFB\x03\xFB\x03\xFC\x03\xFC\x03\xFD\x03\xFD\x03\xFE\x03\xFE\x03\xFF" +
    "\x03\xFF\x03\u0100\x03\u0100\x03\u0101\x03\u0101\x03\u0102\x03\u0102\x03" +
    "\u0103\x03\u0103\x03\u0103\x03\u0103\x03\u0103\x03\u0104\x03\u0104\x03" +
    "\u0104\x03\u0104\x03\u0104\x03\u0104\x05\u0104\u072F\n\u0104\x03\u0105" +
    "\x03\u0105\x03\u0105\x03\u0105\x05\u0105\u0735\n\u0105\x03\u0106\x03\u0106" +
    "\x03\u0106\x03\u0107\x03\u0107\x03\u0107\x05\u0107\u073D\n\u0107\x03\u0108" +
    "\x03\u0108\x05\u0108\u0741\n\u0108\x03\u0108\x05\u0108\u0744\n\u0108\x03" +
    "\u0109\x03\u0109\x03\u010A\x03\u010A\x03\u010A\x03\u010A\x05\u010A\u074C" +
    "\n\u010A\x03\u010B\x03\u010B\x03\u010B\x03\u010B\x05\u010B\u0752\n\u010B" +
    "\x03\u010C\x03\u010C\x03\u010C\x03\u010D\x03\u010D\x03\u010E\x03\u010E" +
    "\x03\u010F\x03\u010F\x03\u0110\x03\u0110\x03\u0110\x03\u0110\x03\u0111" +
    "\x03\u0111\x03\u0111\x07\u0111\u0764\n\u0111\f\u0111\x0E\u0111\u0767\v" +
    "\u0111\x03\u0111\x03\u0111\x03\u0111\x03\u0112\x03\u0112\x03\u0112\x03" +
    "\u0113\x03\u0113\x03\u0113\x07\u0113\u0772\n\u0113\f\u0113\x0E\u0113\u0775" +
    "\v\u0113\x03\u0114\x03\u0114\x03\u0114\x03\u0114\x03\u0114\x03\u0114\x03" +
    "\u0115\x03\u0115\x03\u0115\x03\u0115\x03\u0115\x03\u0115\x03\u0115\x03" +
    "\u0115\x05\u0115\u0785\n\u0115\x03\u0116\x03\u0116\x03\u0116\x07\u0116" +
    "\u078A\n\u0116\f\u0116\x0E\u0116\u078D\v\u0116\x03\u0117\x03\u0117\x03" +
    "\u0118\x03\u0118\x03\u0119\x03\u0119\x03\u011A\x03\u011A\x03\u011A\x03" +
    "\u011A\x03\u011B\x03\u011B\x03\u011B\x07\u011B\u079C\n\u011B\f\u011B\x0E" +
    "\u011B\u079F\v\u011B\x03\u011B\x03\u011B\x03\u011B\x03\u011C\x03\u011C" +
    "\x03\u011C\x03\u011D\x03\u011D\x03\u011D\x07\u011D\u07AA\n\u011D\f\u011D" +
    "\x0E\u011D\u07AD\v\u011D\x03\u011E\x03\u011E\x03\u011E\x03\u011E\x03\u011E" +
    "\x03\u011E\x03\u011F\x03\u011F\x03\u011F\x03\u0120\x03\u0120\x03\u0120" +
    "\x07\u0120\u07BB\n\u0120\f\u0120\x0E\u0120\u07BE\v\u0120\x03\u0121\x03" +
    "\u0121\x03\u0121\x07\u0121\u07C3\n\u0121\f\u0121\x0E\u0121\u07C6\v\u0121" +
    "\x03\u0122\x03\u0122\x03\u0122\x07\u0122\u07CB\n\u0122\f\u0122\x0E\u0122" +
    "\u07CE\v\u0122\x03\u0123\x03\u0123\x03\u0123\x07\u0123\u07D3\n\u0123\f" +
    "\u0123\x0E\u0123\u07D6\v\u0123\x03\u0124\x03\u0124\x03\u0124\x07\u0124" +
    "\u07DB\n\u0124\f\u0124\x0E\u0124\u07DE\v\u0124\x03\u0125\x03\u0125\x07" +
    "\u0125\u07E2\n\u0125\f\u0125\x0E\u0125\u07E5\v\u0125\x03\u0126\x03\u0126" +
    "\x03\u0127\x03\u0127\x03\u0128\x03\u0128\x03\u0129\x06\u0129\u07EE\n\u0129" +
    "\r\u0129\x0E\u0129\u07EF\x03\u0129\x02\x02\x02\u012A\x02\x02\x04\x02\x06" +
    "\x02\b\x02\n\x02\f\x02\x0E\x02\x10\x02\x12\x02\x14\x02\x16\x02\x18\x02" +
    "\x1A\x02\x1C\x02\x1E\x02 \x02\"\x02$\x02&\x02(\x02*\x02,\x02.\x020\x02" +
    "2\x024\x026\x028\x02:\x02<\x02>\x02@\x02B\x02D\x02F\x02H\x02J\x02L\x02" +
    "N\x02P\x02R\x02T\x02V\x02X\x02Z\x02\\\x02^\x02`\x02b\x02d\x02f\x02h\x02" +
    "j\x02l\x02n\x02p\x02r\x02t\x02v\x02x\x02z\x02|\x02~\x02\x80\x02\x82\x02" +
    "\x84\x02\x86\x02\x88\x02\x8A\x02\x8C\x02\x8E\x02\x90\x02\x92\x02\x94\x02" +
    "\x96\x02\x98\x02\x9A\x02\x9C\x02\x9E\x02\xA0\x02\xA2\x02\xA4\x02\xA6\x02" +
    "\xA8\x02\xAA\x02\xAC\x02\xAE\x02\xB0\x02\xB2\x02\xB4\x02\xB6\x02\xB8\x02" +
    "\xBA\x02\xBC\x02\xBE\x02\xC0\x02\xC2\x02\xC4\x02\xC6\x02\xC8\x02\xCA\x02" +
    "\xCC\x02\xCE\x02\xD0\x02\xD2\x02\xD4\x02\xD6\x02\xD8\x02\xDA\x02\xDC\x02" +
    "\xDE\x02\xE0\x02\xE2\x02\xE4\x02\xE6\x02\xE8\x02\xEA\x02\xEC\x02\xEE\x02" +
    "\xF0\x02\xF2\x02\xF4\x02\xF6\x02\xF8\x02\xFA\x02\xFC\x02\xFE\x02\u0100" +
    "\x02\u0102\x02\u0104\x02\u0106\x02\u0108\x02\u010A\x02\u010C\x02\u010E" +
    "\x02\u0110\x02\u0112\x02\u0114\x02\u0116\x02\u0118\x02\u011A\x02\u011C" +
    "\x02\u011E\x02\u0120\x02\u0122\x02\u0124\x02\u0126\x02\u0128\x02\u012A" +
    "\x02\u012C\x02\u012E\x02\u0130\x02\u0132\x02\u0134\x02\u0136\x02\u0138" +
    "\x02\u013A\x02\u013C\x02\u013E\x02\u0140\x02\u0142\x02\u0144\x02\u0146" +
    "\x02\u0148\x02\u014A\x02\u014C\x02\u014E\x02\u0150\x02\u0152\x02\u0154" +
    "\x02\u0156\x02\u0158\x02\u015A\x02\u015C\x02\u015E\x02\u0160\x02\u0162" +
    "\x02\u0164\x02\u0166\x02\u0168\x02\u016A\x02\u016C\x02\u016E\x02\u0170" +
    "\x02\u0172\x02\u0174\x02\u0176\x02\u0178\x02\u017A\x02\u017C\x02\u017E" +
    "\x02\u0180\x02\u0182\x02\u0184\x02\u0186\x02\u0188\x02\u018A\x02\u018C" +
    "\x02\u018E\x02\u0190\x02\u0192\x02\u0194\x02\u0196\x02\u0198\x02\u019A" +
    "\x02\u019C\x02\u019E\x02\u01A0\x02\u01A2\x02\u01A4\x02\u01A6\x02\u01A8" +
    "\x02\u01AA\x02\u01AC\x02\u01AE\x02\u01B0\x02\u01B2\x02\u01B4\x02\u01B6" +
    "\x02\u01B8\x02\u01BA\x02\u01BC\x02\u01BE\x02\u01C0\x02\u01C2\x02\u01C4" +
    "\x02\u01C6\x02\u01C8\x02\u01CA\x02\u01CC\x02\u01CE\x02\u01D0\x02\u01D2" +
    "\x02\u01D4\x02\u01D6\x02\u01D8\x02\u01DA\x02\u01DC\x02\u01DE\x02\u01E0" +
    "\x02\u01E2\x02\u01E4\x02\u01E6\x02\u01E8\x02\u01EA\x02\u01EC\x02\u01EE" +
    "\x02\u01F0\x02\u01F2\x02\u01F4\x02\u01F6\x02\u01F8\x02\u01FA\x02\u01FC" +
    "\x02\u01FE\x02\u0200\x02\u0202\x02\u0204\x02\u0206\x02\u0208\x02\u020A" +
    "\x02\u020C\x02\u020E\x02\u0210\x02\u0212\x02\u0214\x02\u0216\x02\u0218" +
    "\x02\u021A\x02\u021C\x02\u021E\x02\u0220\x02\u0222\x02\u0224\x02\u0226" +
    "\x02\u0228\x02\u022A\x02\u022C\x02\u022E\x02\u0230\x02\u0232\x02\u0234" +
    "\x02\u0236\x02\u0238\x02\u023A\x02\u023C\x02\u023E\x02\u0240\x02\u0242" +
    "\x02\u0244\x02\u0246\x02\u0248\x02\u024A\x02\u024C\x02\u024E\x02\u0250" +
    "\x02\x02\x0E\x03\x02\x18\x19\x03\x02\xC2\xC3\x03\x02LS\x03\x02XY\x03\x02" +
    "\\]\x03\x02^_\x03\x02ip\x03\x02qt\x03\x02u}\x03\x02\x80\x82\x03\x02\x8E" +
    "\x8F\x03\x02\x9F\xA2\x02\u07C4\x02\u0254\x03\x02\x02\x02\x04\u0258\x03" +
    "\x02\x02\x02\x06\u025D\x03\x02\x02\x02\b\u025F\x03\x02\x02\x02\n\u0281" +
    "\x03\x02\x02\x02\f\u028A\x03\x02\x02\x02\x0E\u028C\x03\x02\x02\x02\x10" +
    "\u028E\x03\x02\x02\x02\x12\u02AD\x03\x02\x02\x02\x14\u02B1\x03\x02\x02" +
    "\x02\x16\u02B3\x03\x02\x02\x02\x18\u02B7\x03\x02\x02\x02\x1A\u02B9\x03" +
    "\x02\x02\x02\x1C\u02C3\x03\x02\x02\x02\x1E\u02C6\x03\x02\x02\x02 \u02D2" +
    "\x03\x02\x02\x02\"\u02D5\x03\x02\x02\x02$\u02D8\x03\x02\x02\x02&\u02DB" +
    "\x03\x02\x02\x02(\u02DE";
ESSL_Parser._serializedATNSegment1 = "\x03\x02\x02\x02*\u02E1\x03\x02\x02\x02,\u02EC\x03\x02\x02\x02.\u02F1" +
    "\x03\x02\x02\x020\u02F3\x03\x02\x02\x022\u02F8\x03\x02\x02\x024\u02FA" +
    "\x03\x02\x02\x026\u02FD\x03\x02\x02\x028\u0305\x03\x02\x02\x02:\u0307" +
    "\x03\x02\x02\x02<\u0309\x03\x02\x02\x02>\u030B\x03\x02\x02\x02@\u030E" +
    "\x03\x02\x02\x02B\u031E\x03\x02\x02\x02D\u0320\x03\x02\x02\x02F\u0322" +
    "\x03\x02\x02\x02H\u0324\x03\x02\x02\x02J\u0326\x03\x02\x02\x02L\u032C" +
    "\x03\x02\x02\x02N\u0333\x03\x02\x02\x02P\u033A\x03\x02\x02\x02R\u0345" +
    "\x03\x02\x02\x02T\u0347\x03\x02\x02\x02V\u0349\x03\x02\x02\x02X\u0352" +
    "\x03\x02\x02\x02Z\u0361\x03\x02\x02\x02\\\u0364\x03\x02\x02\x02^\u0381" +
    "\x03\x02\x02\x02`\u0383\x03\x02\x02\x02b\u0385\x03\x02\x02\x02d\u0387" +
    "\x03\x02\x02\x02f\u038A\x03\x02\x02\x02h\u038D\x03\x02\x02\x02j\u03A0" +
    "\x03\x02\x02\x02l\u03A9\x03\x02\x02\x02n\u03AB\x03\x02\x02\x02p\u03AD" +
    "\x03\x02\x02\x02r\u03B0\x03\x02\x02\x02t\u03B6\x03\x02\x02\x02v\u03B8" +
    "\x03\x02\x02\x02x\u03BA\x03\x02\x02\x02z\u03BC\x03\x02\x02\x02|\u03BF" +
    "\x03\x02\x02\x02~\u03C7\x03\x02\x02\x02\x80\u03C9\x03\x02\x02\x02\x82" +
    "\u03CB\x03\x02\x02\x02\x84\u03CD\x03\x02\x02\x02\x86\u03CF\x03\x02\x02" +
    "\x02\x88\u03D1\x03\x02\x02\x02\x8A\u03D4\x03\x02\x02\x02\x8C\u03D6\x03" +
    "\x02\x02\x02\x8E\u03DB\x03\x02\x02\x02\x90\u03DD\x03\x02\x02\x02\x92\u03E2" +
    "\x03\x02\x02\x02\x94\u03E4\x03\x02\x02\x02\x96\u03E9\x03\x02\x02\x02\x98" +
    "\u03EB\x03\x02\x02\x02\x9A\u03F0\x03\x02\x02\x02\x9C\u03F2\x03\x02\x02" +
    "\x02\x9E\u03F7\x03\x02\x02\x02\xA0\u03F9\x03\x02\x02\x02\xA2\u03FE\x03" +
    "\x02\x02\x02\xA4\u0400\x03\x02\x02\x02\xA6\u0402\x03\x02\x02\x02\xA8\u040C" +
    "\x03\x02\x02\x02\xAA\u040E\x03\x02\x02\x02\xAC\u0413\x03\x02\x02\x02\xAE" +
    "\u0415\x03\x02\x02\x02\xB0\u041A\x03\x02\x02\x02\xB2\u041C\x03\x02\x02" +
    "\x02\xB4\u041E\x03\x02\x02\x02\xB6\u0428\x03\x02\x02\x02\xB8\u042A\x03" +
    "\x02\x02\x02\xBA\u042C\x03\x02\x02\x02\xBC\u042E\x03\x02\x02\x02\xBE\u0430" +
    "\x03\x02\x02\x02\xC0\u0437\x03\x02\x02\x02\xC2\u043E\x03\x02\x02\x02\xC4" +
    "\u0440\x03\x02\x02\x02\xC6\u0442\x03\x02\x02\x02\xC8\u0444\x03\x02\x02" +
    "\x02\xCA\u0449\x03\x02\x02\x02\xCC\u0450\x03\x02\x02\x02\xCE\u0453\x03" +
    "\x02\x02\x02\xD0\u0466\x03\x02\x02\x02\xD2\u0468\x03\x02\x02\x02\xD4\u046A" +
    "\x03\x02\x02\x02\xD6\u046F\x03\x02\x02\x02\xD8\u0471\x03\x02\x02\x02\xDA" +
    "\u0476\x03\x02\x02\x02\xDC\u0478\x03\x02\x02\x02\xDE\u047A\x03\x02\x02" +
    "\x02\xE0\u0481\x03\x02\x02\x02\xE2\u0484\x03\x02\x02\x02\xE4\u048A\x03" +
    "\x02\x02\x02\xE6\u0490\x03\x02\x02\x02\xE8\u049D\x03\x02\x02\x02\xEA\u04A5" +
    "\x03\x02\x02\x02\xEC\u04A7\x03\x02\x02\x02\xEE\u04AC\x03\x02\x02\x02\xF0" +
    "\u04AF\x03\x02\x02\x02\xF2\u04B5\x03\x02\x02\x02\xF4\u04C6\x03\x02\x02" +
    "\x02\xF6\u04C8\x03\x02\x02\x02\xF8\u04CA\x03\x02\x02\x02\xFA\u04CC\x03" +
    "\x02\x02\x02\xFC\u04CE\x03\x02\x02\x02\xFE\u04D0\x03\x02\x02\x02\u0100" +
    "\u04D6\x03\x02\x02\x02\u0102\u04E5\x03\x02\x02\x02\u0104\u04E7\x03\x02" +
    "\x02\x02\u0106\u04EC\x03\x02\x02\x02\u0108\u04EE\x03\x02\x02\x02\u010A" +
    "\u04F3\x03\x02\x02\x02\u010C\u04F5\x03\x02\x02\x02\u010E\u04FA\x03\x02" +
    "\x02\x02\u0110\u04FD\x03\x02\x02\x02\u0112\u0500\x03\x02\x02\x02\u0114" +
    "\u0507\x03\x02\x02\x02\u0116\u0510\x03\x02\x02\x02\u0118\u0512\x03\x02" +
    "\x02\x02\u011A\u0514\x03\x02\x02\x02\u011C\u0516\x03\x02\x02\x02\u011E" +
    "\u0519\x03\x02\x02\x02\u0120\u0525\x03\x02\x02\x02\u0122\u0527\x03\x02" +
    "\x02\x02\u0124\u0529\x03\x02\x02\x02\u0126\u052B\x03\x02\x02\x02\u0128" +
    "\u052D\x03\x02\x02\x02\u012A\u0531\x03\x02\x02\x02\u012C\u0534\x03\x02" +
    "\x02\x02\u012E\u0539\x03\x02\x02\x02\u0130\u053D\x03\x02\x02\x02\u0132" +
    "\u053F\x03\x02\x02\x02\u0134\u054C\x03\x02\x02\x02\u0136\u0553\x03\x02" +
    "\x02\x02\u0138\u0556\x03\x02\x02\x02\u013A\u0578\x03\x02\x02\x02\u013C" +
    "\u057B\x03\x02\x02\x02\u013E\u057E\x03\x02\x02\x02\u0140\u058D\x03\x02" +
    "\x02\x02\u0142\u058F\x03\x02\x02\x02\u0144\u0591\x03\x02\x02\x02\u0146" +
    "\u059D\x03\x02\x02\x02\u0148\u059F\x03\x02\x02\x02\u014A\u05A1\x03\x02" +
    "\x02\x02\u014C\u05A3\x03\x02\x02\x02\u014E\u05A5\x03\x02\x02\x02\u0150" +
    "\u05A7\x03\x02\x02\x02\u0152\u05A9\x03\x02\x02\x02\u0154\u05AB\x03\x02" +
    "\x02\x02\u0156\u05AD\x03\x02\x02\x02\u0158\u05AF\x03\x02\x02\x02\u015A" +
    "\u05BE\x03\x02\x02\x02\u015C\u05C0\x03\x02\x02\x02\u015E\u05C2\x03\x02" +
    "\x02\x02\u0160\u05C4\x03\x02\x02\x02\u0162\u05C6\x03\x02\x02\x02\u0164" +
    "\u05C8\x03\x02\x02\x02\u0166\u05CA\x03\x02\x02\x02\u0168\u05CF\x03\x02" +
    "\x02\x02\u016A\u05D1\x03\x02\x02\x02\u016C\u05D3\x03\x02\x02\x02\u016E" +
    "\u05DC\x03\x02\x02\x02\u0170\u05DE\x03\x02\x02\x02\u0172\u05E0\x03\x02" +
    "\x02\x02\u0174\u05E2\x03\x02\x02\x02\u0176\u05E4\x03\x02\x02\x02\u0178" +
    "\u05E6\x03\x02\x02\x02\u017A\u05E8\x03\x02\x02\x02\u017C\u05EA\x03\x02" +
    "\x02\x02\u017E\u05EC\x03\x02\x02\x02\u0180\u05F1\x03\x02\x02\x02\u0182" +
    "\u05F3\x03\x02\x02\x02\u0184\u05F8\x03\x02\x02\x02\u0186\u05FA\x03\x02" +
    "\x02\x02\u0188\u0606\x03\x02\x02\x02\u018A\u060A\x03\x02\x02\x02\u018C" +
    "\u060C\x03\x02\x02\x02\u018E\u060E\x03\x02\x02\x02\u0190\u0616\x03\x02" +
    "\x02\x02\u0192\u0618\x03\x02\x02\x02\u0194\u061A\x03\x02\x02\x02\u0196" +
    "\u061C\x03\x02\x02\x02\u0198\u061E\x03\x02\x02\x02\u019A\u0623\x03\x02" +
    "\x02\x02\u019C\u062D\x03\x02\x02\x02\u019E\u0632\x03\x02\x02\x02\u01A0" +
    "\u0634\x03\x02\x02\x02\u01A2\u0639\x03\x02\x02\x02\u01A4\u063E\x03\x02" +
    "\x02\x02\u01A6\u0641\x03\x02\x02\x02\u01A8\u0647\x03\x02\x02\x02\u01AA" +
    "\u064A\x03\x02\x02\x02\u01AC\u064D\x03\x02\x02\x02\u01AE\u0651\x03\x02" +
    "\x02\x02\u01B0\u065B\x03\x02\x02\x02\u01B2\u0663\x03\x02\x02\x02\u01B4" +
    "\u0666\x03\x02\x02\x02\u01B6\u066D\x03\x02\x02\x02\u01B8\u067A\x03\x02" +
    "\x02\x02\u01BA\u0681\x03\x02\x02\x02\u01BC\u0687\x03\x02\x02\x02\u01BE" +
    "\u068A\x03\x02\x02\x02\u01C0\u0697\x03\x02\x02\x02\u01C2\u069B\x03\x02" +
    "\x02\x02\u01C4\u069D\x03\x02\x02\x02\u01C6\u06A0\x03\x02\x02\x02\u01C8" +
    "\u06A8\x03\x02\x02\x02\u01CA\u06AA\x03\x02\x02\x02\u01CC\u06AE\x03\x02" +
    "\x02\x02\u01CE\u06B0\x03\x02\x02\x02\u01D0\u06B6\x03\x02\x02\x02\u01D2" +
    "\u06C8\x03\x02\x02\x02\u01D4\u06CB\x03\x02\x02\x02\u01D6\u06CD\x03\x02" +
    "\x02\x02\u01D8\u06CF\x03\x02\x02\x02\u01DA\u06D1\x03\x02\x02\x02\u01DC" +
    "\u06D3\x03\x02\x02\x02\u01DE\u06D5\x03\x02\x02\x02\u01E0\u06ED\x03\x02" +
    "\x02\x02\u01E2\u06F0\x03\x02\x02\x02\u01E4\u06F2\x03\x02\x02\x02\u01E6" +
    "\u0702\x03\x02\x02\x02\u01E8\u0704\x03\x02\x02\x02\u01EA\u0706\x03\x02" +
    "\x02\x02\u01EC\u0708\x03\x02\x02\x02\u01EE\u070A\x03\x02\x02\x02\u01F0" +
    "\u070C\x03\x02\x02\x02\u01F2\u0711\x03\x02\x02\x02\u01F4\u0713\x03\x02" +
    "\x02\x02\u01F6\u0715\x03\x02\x02\x02\u01F8\u0717\x03\x02\x02\x02\u01FA" +
    "\u0719\x03\x02\x02\x02\u01FC\u071B\x03\x02\x02\x02\u01FE\u071D\x03\x02" +
    "\x02\x02\u0200\u071F\x03\x02\x02\x02\u0202\u0721\x03\x02\x02\x02\u0204" +
    "\u0723\x03\x02\x02\x02\u0206\u072E\x03\x02\x02\x02\u0208\u0730\x03\x02" +
    "\x02\x02\u020A\u0736\x03\x02\x02\x02\u020C\u0739\x03\x02\x02\x02\u020E" +
    "\u073E\x03\x02\x02\x02\u0210\u0745\x03\x02\x02\x02\u0212\u0747\x03\x02" +
    "\x02\x02\u0214\u074D\x03\x02\x02\x02\u0216\u0753\x03\x02\x02\x02\u0218" +
    "\u0756\x03\x02\x02\x02\u021A\u0758\x03\x02\x02\x02\u021C\u075A\x03\x02" +
    "\x02\x02\u021E\u075C\x03\x02\x02\x02\u0220\u0760\x03\x02\x02\x02\u0222" +
    "\u076B\x03\x02\x02\x02\u0224\u0773\x03\x02\x02\x02\u0226\u0776\x03\x02" +
    "\x02\x02\u0228\u077C\x03\x02\x02\x02\u022A\u078B\x03\x02\x02\x02\u022C" +
    "\u078E\x03\x02\x02\x02\u022E\u0790\x03\x02\x02\x02\u0230\u0792\x03\x02" +
    "\x02\x02\u0232\u0794\x03\x02\x02\x02\u0234\u0798\x03\x02\x02\x02\u0236" +
    "\u07A3\x03\x02\x02\x02\u0238\u07AB\x03\x02\x02\x02\u023A\u07AE\x03\x02" +
    "\x02\x02\u023C\u07B4\x03\x02\x02\x02\u023E\u07BC\x03\x02\x02\x02\u0240" +
    "\u07BF\x03\x02\x02\x02\u0242\u07CC\x03\x02\x02\x02\u0244\u07D4\x03\x02" +
    "\x02\x02\u0246\u07DC\x03\x02\x02\x02\u0248\u07E3\x03\x02\x02\x02\u024A" +
    "\u07E6\x03\x02\x02\x02\u024C\u07E8\x03\x02\x02\x02\u024E\u07EA\x03\x02" +
    "\x02\x02\u0250\u07ED\x03\x02\x02\x02\u0252\u0255\x05\x04\x03\x02\u0253" +
    "\u0255\x05\n\x06\x02\u0254\u0252\x03\x02\x02\x02\u0254\u0253\x03\x02\x02" +
    "\x02\u0255\u0256\x03\x02\x02\x02\u0256\u0257\x07\x02\x02\x03\u0257\x03" +
    "\x03\x02\x02\x02\u0258\u0259\x05\u0244\u0123\x02\u0259\u025A\x07\x03\x02" +
    "\x02\u025A\u025B\x05\x06\x04\x02\u025B\u025C\x05\b\x05\x02\u025C\x05\x03" +
    "\x02\x02\x02\u025D\u025E\x07\xC2\x02\x02\u025E\x07\x03\x02\x02\x02\u025F" +
    "\u0260\x05\u0248\u0125\x02\u0260\u0261\x07\x04\x02\x02\u0261\u0265\x05" +
    "\x12\n\x02\u0262\u0264\x05\x16\f\x02\u0263\u0262\x03\x02\x02\x02\u0264" +
    "\u0267\x03\x02\x02\x02\u0265\u0263\x03\x02\x02\x02\u0265\u0266\x03\x02" +
    "\x02\x02\u0266\u0269\x03\x02\x02\x02\u0267\u0265\x03\x02\x02\x02\u0268" +
    "\u026A\x05\x1A\x0E\x02\u0269\u0268\x03\x02\x02\x02\u0269\u026A\x03\x02" +
    "\x02\x02\u026A\u026B\x03\x02\x02\x02\u026B\u0272\x05J&\x02\u026C\u0271" +
    "\x05\xE4s\x02\u026D\u0271\x05\xFE\x80\x02\u026E\u0271\x05L\'\x02\u026F" +
    "\u0271\x05N(\x02\u0270\u026C\x03\x02\x02\x02\u0270\u026D\x03\x02\x02\x02" +
    "\u0270\u026E\x03\x02\x02\x02\u0270\u026F\x03\x02\x02\x02\u0271\u0274\x03" +
    "\x02\x02\x02\u0272\u0270\x03\x02\x02\x02\u0272\u0273\x03\x02\x02\x02\u0273" +
    "\u0275\x03\x02\x02\x02\u0274\u0272\x03\x02\x02\x02\u0275\u0277\x05\u0198" +
    "\xCD\x02\u0276\u0278\x05\u01AC\xD7\x02\u0277\u0276\x03\x02\x02\x02\u0277" +
    "\u0278\x03\x02\x02\x02\u0278\u027A\x03\x02\x02\x02\u0279\u027B\x05\u021E" +
    "\u0110\x02\u027A\u0279\x03\x02\x02\x02\u027A\u027B\x03\x02\x02\x02\u027B" +
    "\u027D\x03\x02\x02\x02\u027C\u027E\x05\u0232\u011A\x02\u027D\u027C\x03" +
    "\x02\x02\x02\u027D\u027E\x03\x02\x02\x02\u027E\u027F\x03\x02\x02\x02\u027F" +
    "\u0280\x07\x05\x02\x02\u0280\t\x03\x02\x02\x02\u0281\u0282\x05\u0244\u0123" +
    "\x02\u0282\u0283\x07\x06\x02\x02\u0283\u0284\x07\x03\x02\x02\u0284\u0285" +
    "\x05\x06\x04\x02\u0285\u0286\x05\f\x07\x02\u0286\u0287\x05\x10\t\x02\u0287" +
    "\v\x03\x02\x02\x02\u0288\u0289\x07\x07\x02\x02\u0289\u028B\x05\x0E\b\x02" +
    "\u028A\u0288\x03\x02\x02\x02\u028A\u028B\x03\x02\x02\x02\u028B\r\x03\x02" +
    "\x02\x02\u028C\u028D\x07\xC2\x02\x02\u028D\x0F\x03\x02\x02\x02\u028E\u028F" +
    "\x07\x04\x02\x02\u028F\u0293\x05\x12\n\x02\u0290\u0292\x05\x16\f\x02\u0291" +
    "\u0290\x03\x02\x02\x02\u0292\u0295\x03\x02\x02\x02\u0293\u0291\x03\x02" +
    "\x02\x02\u0293\u0294\x03\x02\x02\x02\u0294\u0297\x03\x02\x02\x02\u0295" +
    "\u0293\x03\x02\x02\x02\u0296\u0298\x05\x1A\x0E\x02\u0297\u0296\x03\x02" +
    "\x02\x02\u0297\u0298\x03\x02\x02\x02\u0298\u029F\x03\x02\x02\x02\u0299" +
    "\u029E\x05\xE4s\x02\u029A\u029E\x05\xFE\x80\x02\u029B\u029E\x05L\'\x02" +
    "\u029C\u029E\x05N(\x02\u029D\u0299\x03\x02\x02\x02\u029D\u029A\x03\x02" +
    "\x02\x02\u029D\u029B\x03\x02\x02\x02\u029D\u029C\x03\x02\x02\x02\u029E" +
    "\u02A1\x03\x02\x02\x02\u029F\u029D\x03\x02\x02\x02\u029F\u02A0\x03\x02" +
    "\x02\x02\u02A0\u02A3\x03\x02\x02\x02\u02A1\u029F\x03\x02\x02\x02\u02A2" +
    "\u02A4\x05\u01AC\xD7\x02\u02A3\u02A2\x03\x02\x02\x02\u02A3\u02A4\x03\x02" +
    "\x02\x02\u02A4\u02A6\x03\x02\x02\x02\u02A5\u02A7\x05\u021E\u0110\x02\u02A6" +
    "\u02A5\x03\x02\x02\x02\u02A6\u02A7\x03\x02\x02\x02\u02A7\u02A9\x03\x02" +
    "\x02\x02\u02A8\u02AA\x05\u0232\u011A\x02\u02A9\u02A8\x03\x02\x02\x02\u02A9" +
    "\u02AA\x03\x02\x02\x02\u02AA\u02AB\x03\x02\x02\x02\u02AB\u02AC\x07\x05" +
    "\x02\x02\u02AC\x11\x03\x02\x02\x02\u02AD\u02AE\x05\u0248\u0125\x02\u02AE" +
    "\u02AF\x07\b\x02\x02\u02AF\u02B0\x05\x14\v\x02\u02B0\x13\x03\x02\x02\x02" +
    "\u02B1\u02B2\x05\u0240\u0121\x02\u02B2\x15\x03\x02\x02\x02\u02B3\u02B4" +
    "\x05\u0248\u0125\x02\u02B4\u02B5\x07\t\x02\x02\u02B5\u02B6\x05\x18\r\x02" +
    "\u02B6\x17\x03\x02\x02\x02\u02B7\u02B8\x05\u0240\u0121\x02\u02B8\x19\x03" +
    "\x02\x02\x02\u02B9\u02BA\x07\n\x02\x02\u02BA\u02BB\x07\x04\x02\x02\u02BB" +
    "\u02BC\x07\x05\x02\x02\u02BC\u02BD\x05\x1C\x0F\x02\u02BD\x1B\x03\x02\x02" +
    "\x02\u02BE\u02BF\x05\x1E\x10\x02\u02BF\u02C0\x05\u0248\u0125\x02\u02C0" +
    "\u02C2\x03\x02\x02\x02\u02C1\u02BE\x03\x02\x02\x02\u02C2\u02C5\x03\x02" +
    "\x02\x02\u02C3\u02C1\x03\x02\x02\x02\u02C3\u02C4\x03\x02\x02\x02\u02C4" +
    "\x1D\x03\x02\x02\x02\u02C5\u02C3\x03\x02\x02\x02\u02C6\u02D0\x07\v\x02" +
    "\x02\u02C7\u02D1\x05 \x11\x02\u02C8\u02D1\x05\"\x12\x02\u02C9\u02D1\x05" +
    "$\x13\x02\u02CA\u02D1\x05&\x14\x02\u02CB\u02D1\x05(\x15\x02\u02CC\u02D1" +
    "\x05,\x17\x02\u02CD\u02D1\x050\x19\x02\u02CE\u02D1\x054\x1B\x02\u02CF" +
    "\u02D1\x05> \x02\u02D0\u02C7\x03\x02\x02\x02\u02D0\u02C8\x03\x02\x02\x02" +
    "\u02D0\u02C9\x03\x02\x02\x02\u02D0\u02CA\x03\x02\x02\x02\u02D0\u02CB\x03" +
    "\x02\x02\x02\u02D0\u02CC\x03\x02\x02\x02\u02D0\u02CD\x03\x02\x02\x02\u02D0" +
    "\u02CE\x03\x02\x02\x02\u02D0\u02CF\x03\x02\x02\x02\u02D1\x1F\x03\x02\x02" +
    "\x02\u02D2\u02D3\x07\f\x02\x02\u02D3\u02D4\x05*\x16\x02\u02D4!\x03\x02" +
    "\x02\x02\u02D5\u02D6\x07\r\x02\x02\u02D6\u02D7\x05*\x16\x02\u02D7#\x03" +
    "\x02\x02\x02\u02D8\u02D9\x07\x0E\x02\x02\u02D9\u02DA\x05*\x16\x02\u02DA" +
    "%\x03\x02\x02\x02\u02DB\u02DC\x07\x0F\x02\x02\u02DC\u02DD\x05*\x16\x02" +
    "\u02DD\'\x03\x02\x02\x02\u02DE\u02DF\x07\x10\x02\x02\u02DF\u02E0\x05*" +
    "\x16\x02\u02E0)\x03\x02\x02\x02\u02E1\u02E2\x07\x11\x02\x02\u02E2\u02E7" +
    "\x05\x86D\x02\u02E3\u02E4\x07\x12\x02\x02\u02E4\u02E6\x05\x86D\x02\u02E5" +
    "\u02E3\x03\x02\x02\x02\u02E6\u02E9\x03\x02\x02\x02\u02E7\u02E5\x03\x02" +
    "\x02\x02\u02E7\u02E8\x03\x02\x02\x02\u02E8\u02EA\x03\x02\x02\x02\u02E9" +
    "\u02E7\x03\x02\x02\x02\u02EA\u02EB\x07\x13\x02\x02\u02EB+\x03\x02\x02" +
    "\x02\u02EC\u02ED\x07\x14\x02\x02\u02ED\u02EE\x07\x11\x02\x02\u02EE\u02EF" +
    "\x05.\x18\x02\u02EF\u02F0\x07\x13\x02\x02\u02F0-\x03\x02\x02\x02\u02F1" +
    "\u02F2\x05\u0240\u0121\x02\u02F2/\x03\x02\x02\x02\u02F3\u02F4\x07\x15" +
    "\x02\x02\u02F4\u02F5\x07\x11\x02\x02\u02F5\u02F6\x052\x1A\x02\u02F6\u02F7" +
    "\x07\x13\x02\x02\u02F71\x03\x02\x02\x02\u02F8\u02F9\x05\u0240\u0121\x02" +
    "\u02F93\x03\x02\x02\x02\u02FA\u02FB\x07\x16\x02\x02\u02FB\u02FC\x056\x1C" +
    "\x02\u02FC5\x03\x02\x02\x02\u02FD\u02FE\x07\x11\x02\x02\u02FE\u02FF\x05" +
    "8\x1D\x02\u02FF\u0300\x07\x12\x02\x02\u0300\u0301\x05:\x1E\x02\u0301\u0302" +
    "\x07\x12\x02\x02\u0302\u0303\x05<\x1F\x02\u0303\u0304\x07\x13\x02\x02" +
    "\u03047\x03\x02\x02\x02\u0305\u0306\x07\xC2\x02\x02\u03069\x03\x02\x02" +
    "\x02\u0307\u0308\x07\xBF\x02\x02\u0308;\x03\x02\x02\x02\u0309\u030A\x07" +
    "\xBF\x02\x02\u030A=\x03\x02\x02\x02\u030B\u030C\x07\x17\x02\x02\u030C" +
    "\u030D\x05@!\x02\u030D?\x03\x02\x02\x02\u030E\u030F\x07\x11\x02\x02\u030F" +
    "\u0312\x05B\"\x02\u0310\u0311\x07\x12\x02\x02\u0311\u0313\x05D#\x02\u0312" +
    "\u0310\x03\x02\x02\x02\u0312\u0313\x03\x02\x02\x02\u0313\u0316\x03\x02" +
    "\x02\x02\u0314\u0315\x07\x12\x02\x02\u0315\u0317\x05F$\x02\u0316\u0314" +
    "\x03\x02\x02\x02\u0316\u0317\x03\x02\x02\x02\u0317\u031A\x03\x02\x02\x02" +
    "\u0318\u0319\x07\x12\x02\x02\u0319\u031B\x05H%\x02\u031A\u0318\x03\x02" +
    "\x02\x02\u031A\u031B\x03\x02\x02\x02\u031B\u031C\x03\x02\x02\x02\u031C" +
    "\u031D\x07\x13\x02\x02\u031DA\x03\x02\x02\x02\u031E\u031F\x07\xC2\x02" +
    "\x02\u031FC\x03\x02\x02\x02\u0320\u0321\t\x02\x02\x02\u0321E\x03\x02\x02" +
    "\x02\u0322\u0323\x05\u0124\x93\x02\u0323G\x03\x02\x02\x02\u0324\u0325" +
    "\x07\xBF\x02\x02\u0325I\x03\x02\x02\x02\u0326\u0327\x05\u0244\u0123\x02" +
    "\u0327\u0328\x07\x1A\x02\x02\u0328\u0329\x05R*\x02\u0329\u032A\x05V,\x02" +
    "\u032A\u032B\x05Z.\x02\u032BK\x03\x02\x02\x02\u032C\u032D\x05\u0244\u0123" +
    "\x02\u032D\u032E\x07\x1A\x02\x02\u032E\u032F\x05P)\x02\u032F\u0330\x05" +
    "R*\x02\u0330\u0331\x05V,\x02\u0331\u0332\x05\xCCg\x02\u0332M\x03\x02\x02" +
    "\x02\u0333\u0334\x05\u0244\u0123\x02\u0334\u0335\x07\x1B\x02\x02\u0335" +
    "\u0336\x05P)\x02\u0336\u0337\x05R*\x02\u0337\u0338\x05X-\x02\u0338\u0339" +
    "\x05\xE0q\x02\u0339O\x03\x02\x02\x02\u033A\u033B\x07\xC2\x02\x02\u033B" +
    "Q\x03\x02\x02\x02\u033C\u033D\x07\x07\x02\x02\u033D\u0342\x05T+\x02\u033E" +
    "\u033F\x07\x12\x02\x02\u033F\u0341\x05T+\x02\u0340\u033E\x03\x02\x02\x02" +
    "\u0341\u0344\x03\x02\x02\x02\u0342\u0340\x03\x02\x02\x02\u0342\u0343\x03" +
    "\x02\x02\x02\u0343\u0346\x03\x02\x02\x02\u0344\u0342\x03\x02\x02\x02\u0345" +
    "\u033C\x03\x02\x02\x02\u0345\u0346\x03\x02\x02\x02\u0346S\x03\x02\x02" +
    "\x02\u0347\u0348\x07\xC2\x02\x02\u0348U\x03\x02\x02\x02\u0349\u034B\x07" +
    "\x04\x02\x02\u034A\u034C\x05\u0112\x8A\x02\u034B\u034A\x03\x02\x02\x02" +
    "\u034C\u034D\x03\x02\x02\x02\u034D\u034B\x03\x02\x02\x02\u034D\u034E\x03" +
    "\x02\x02\x02\u034E\u034F\x03\x02\x02\x02\u034F\u0350\x05\u0246\u0124\x02" +
    "\u0350\u0351\x07\x05\x02\x02\u0351W\x03\x02\x02\x02\u0352\u0356\x07\x04" +
    "\x02\x02\u0353\u0355\x05\u0112\x8A\x02\u0354\u0353\x03\x02\x02\x02\u0355" +
    "\u0358\x03\x02\x02\x02\u0356\u0354\x03\x02\x02\x02\u0356\u0357\x03\x02" +
    "\x02\x02\u0357\u0359\x03\x02\x02\x02\u0358\u0356\x03\x02\x02\x02\u0359" +
    "\u035A\x05\u0246\u0124\x02\u035A\u035B\x07\x05\x02\x02\u035BY\x03\x02" +
    "\x02\x02\u035C\u035D\x05\\/\x02\u035D\u035E\x05\u0248\u0125\x02\u035E" +
    "\u0360\x03\x02\x02\x02\u035F\u035C\x03\x02\x02\x02\u0360\u0363\x03\x02" +
    "\x02\x02\u0361\u035F\x03\x02\x02\x02\u0361\u0362\x03\x02\x02\x02\u0362" +
    "[\x03\x02\x02\x02\u0363\u0361\x03\x02\x02\x02\u0364\u037F\x07\v\x02\x02" +
    "\u0365\u0380\x05\xD0i\x02\u0366\u0380\x05^0\x02\u0367\u0380\x05`1\x02" +
    "\u0368\u0380\x05b2\x02\u0369\u0380\x05d3\x02\u036A\u0380\x05f4\x02\u036B" +
    "\u0380\x05p9\x02\u036C\u0380\x05x=\x02\u036D\u0380\x05z>\x02\u036E\u0380" +
    "\x05\xD4k\x02\u036F\u0380\x05\x82B\x02\u0370\u0380\x05\x84C\x02\u0371" +
    "\u0380\x05\x88E\x02\u0372\u0380\x05\xB4[\x02\u0373\u0380\x05\xD8m\x02" +
    "\u0374\u0380\x05\x8CG\x02\u0375\u0380\x05\x90I\x02\u0376\u0380\x05\x94" +
    "K\x02\u0377\u0380\x05\x98M\x02\u0378\u0380\x05\x9CO\x02\u0379\u0380\x05" +
    "\xA0Q\x02\u037A\u0380\x05\xA4S\x02\u037B\u0380\x05\xA6T\x02\u037C\u0380" +
    "\x05\xAAV\x02\u037D\u0380\x05\xAEX\x02\u037E\u0380\x05\u0186\xC4\x02\u037F" +
    "\u0365\x03\x02\x02\x02\u037F\u0366\x03\x02\x02\x02\u037F\u0367\x03\x02" +
    "\x02\x02\u037F\u0368\x03\x02\x02\x02\u037F\u0369\x03\x02\x02\x02\u037F" +
    "\u036A\x03\x02\x02\x02\u037F\u036B\x03\x02\x02\x02\u037F\u036C\x03\x02" +
    "\x02\x02\u037F\u036D\x03\x02\x02\x02\u037F\u036E\x03\x02\x02\x02\u037F" +
    "\u036F\x03\x02\x02\x02\u037F\u0370\x03\x02\x02\x02\u037F\u0371\x03\x02" +
    "\x02\x02\u037F\u0372\x03\x02\x02\x02\u037F\u0373\x03\x02\x02\x02\u037F" +
    "\u0374\x03\x02\x02\x02\u037F\u0375\x03\x02\x02\x02\u037F\u0376\x03\x02" +
    "\x02\x02\u037F\u0377\x03\x02\x02\x02\u037F\u0378\x03\x02\x02\x02\u037F" +
    "\u0379\x03\x02\x02\x02\u037F\u037A\x03\x02\x02\x02\u037F\u037B\x03\x02" +
    "\x02\x02\u037F\u037C\x03\x02\x02\x02\u037F\u037D\x03\x02\x02\x02\u037F" +
    "\u037E\x03\x02\x02\x02\u0380]\x03\x02\x02\x02\u0381\u0382\x07\x1C\x02" +
    "\x02\u0382_\x03\x02\x02\x02\u0383\u0384\x07\x1D\x02\x02\u0384a\x03\x02" +
    "\x02\x02\u0385\u0386\x07\x1E\x02\x02\u0386c\x03\x02\x02\x02\u0387\u0388" +
    "\x07\x1F\x02\x02\u0388\u0389\x05h5\x02\u0389e\x03\x02\x02\x02\u038A\u038B" +
    "\x07 \x02\x02\u038B\u038C\x05h5\x02\u038Cg\x03\x02\x02\x02\u038D\u038E" +
    "\x07\x11\x02\x02\u038E\u0397\x05P)\x02\u038F\u0390\x07\x12\x02\x02\u0390" +
    "\u0395\x05n8\x02\u0391\u0392\x07\x11\x02\x02\u0392\u0393\x05j6\x02\u0393" +
    "\u0394\x07\x13\x02\x02\u0394\u0396\x03\x02\x02\x02\u0395\u0391\x03\x02" +
    "\x02\x02\u0395\u0396\x03\x02\x02\x02\u0396\u0398\x03\x02\x02\x02\u0397" +
    "\u038F\x03\x02\x02\x02\u0398\u0399\x03\x02\x02\x02\u0399\u0397\x03\x02" +
    "\x02\x02\u0399\u039A\x03\x02\x02\x02\u039A\u039C\x03\x02\x02\x02\u039B" +
    "\u039D\x05\xBE`\x02\u039C\u039B\x03\x02\x02\x02\u039C\u039D\x03\x02\x02" +
    "\x02\u039D\u039E\x03\x02\x02\x02\u039E\u039F\x07\x13\x02\x02\u039Fi\x03" +
    "\x02\x02\x02\u03A0\u03A3\x05l7\x02\u03A1\u03A2\x07\x12\x02\x02\u03A2\u03A4" +
    "\x05l7\x02\u03A3\u03A1\x03\x02\x02\x02\u03A3\u03A4\x03\x02\x02\x02\u03A4" +
    "k\x03\x02\x02\x02\u03A5\u03AA\x05\u0140\xA1\x02\u03A6\u03AA\x05\u0142" +
    "\xA2\x02\u03A7\u03AA\x05\xB2Z\x02\u03A8\u03AA\x05\u0194\xCB\x02\u03A9" +
    "\u03A5\x03\x02\x02\x02\u03A9\u03A6\x03\x02\x02\x02\u03A9\u03A7\x03\x02" +
    "\x02\x02\u03A9\u03A8\x03\x02\x02\x02\u03AAm\x03\x02\x02\x02\u03AB\u03AC" +
    "\x07\xC2\x02\x02\u03ACo\x03\x02\x02\x02\u03AD\u03AE\x07!\x02\x02\u03AE" +
    "\u03AF\x05r:\x02\u03AFq\x03\x02\x02\x02\u03B0\u03B1\x07\x11\x02\x02\u03B1" +
    "\u03B2\x05t;\x02\u03B2\u03B3\x07\x12\x02\x02\u03B3\u03B4\x05v<\x02\u03B4" +
    "\u03B5\x07\x13\x02\x02\u03B5s\x03\x02\x02\x02\u03B6\u03B7\x07\xC2\x02" +
    "\x02\u03B7u\x03\x02\x02\x02\u03B8\u03B9\x07\xC2\x02\x02\u03B9w\x03\x02" +
    "\x02\x02\u03BA\u03BB\x07\"\x02\x02\u03BBy\x03\x02\x02\x02\u03BC\u03BD" +
    "\x07#\x02\x02\u03BD\u03BE\x05|?\x02\u03BE{\x03\x02\x02\x02\u03BF\u03C0" +
    "\x07\x11\x02\x02\u03C0\u03C3\x05~@\x02\u03C1\u03C2\x07\x12\x02\x02\u03C2" +
    "\u03C4\x05\x80A\x02\u03C3\u03C1\x03\x02\x02\x02\u03C3\u03C4\x03\x02\x02" +
    "\x02\u03C4\u03C5\x03\x02\x02\x02\u03C5\u03C6\x07\x13\x02\x02\u03C6}\x03" +
    "\x02\x02\x02\u03C7\u03C8\x07\xC2\x02\x02\u03C8\x7F\x03\x02\x02\x02\u03C9" +
    "\u03CA\x07\xC2\x02\x02\u03CA\x81\x03\x02\x02\x02\u03CB\u03CC\x07$\x02" +
    "\x02\u03CC\x83\x03\x02\x02\x02\u03CD\u03CE\x07%\x02\x02\u03CE\x85\x03" +
    "\x02\x02\x02\u03CF\u03D0\x07\xC2\x02\x02\u03D0\x87\x03\x02\x02\x02\u03D1" +
    "\u03D2\x07&\x02\x02\u03D2\u03D3\x05*\x16\x02\u03D3\x89\x03\x02\x02\x02" +
    "\u03D4\u03D5\x07\'\x02\x02\u03D5\x8B\x03\x02\x02\x02\u03D6\u03D7\x07(" +
    "\x02\x02\u03D7\u03D8\x07\x11\x02\x02\u03D8\u03D9\x05\x8EH\x02\u03D9\u03DA" +
    "\x07\x13\x02\x02\u03DA\x8D\x03\x02\x02\x02\u03DB\u03DC\x07\xC2\x02\x02" +
    "\u03DC\x8F\x03\x02\x02\x02\u03DD\u03DE\x07)\x02\x02\u03DE\u03DF\x07\x11" +
    "\x02\x02\u03DF\u03E0\x05\x92J\x02\u03E0\u03E1\x07\x13\x02\x02\u03E1\x91" +
    "\x03\x02\x02\x02\u03E2\u03E3\x05\u0240\u0121\x02\u03E3\x93\x03\x02\x02" +
    "\x02\u03E4\u03E5\x07*\x02\x02\u03E5\u03E6\x07\x11\x02\x02\u03E6\u03E7" +
    "\x05\x96L\x02\u03E7\u03E8\x07\x13\x02\x02\u03E8\x95\x03\x02\x02\x02\u03E9" +
    "\u03EA\x07\xC2\x02\x02\u03EA\x97\x03\x02\x02\x02\u03EB\u03EC\x07+\x02" +
    "\x02\u03EC\u03ED\x07\x11\x02\x02\u03ED\u03EE\x05\x9AN\x02\u03EE\u03EF" +
    "\x07\x13\x02\x02\u03EF\x99\x03\x02\x02\x02\u03F0\u03F1\x07\xC2\x02\x02" +
    "\u03F1\x9B\x03\x02\x02\x02\u03F2\u03F3\x07,\x02\x02\u03F3\u03F4\x07\x11" +
    "\x02\x02\u03F4\u03F5\x05\x9EP\x02\u03F5\u03F6\x07\x13\x02\x02\u03F6\x9D" +
    "\x03\x02\x02\x02\u03F7\u03F8\x07\xC2\x02\x02\u03F8\x9F\x03\x02\x02\x02" +
    "\u03F9\u03FA\x07-\x02\x02\u03FA\u03FB\x07\x11\x02\x02\u03FB\u03FC\x05" +
    "\xA2R\x02\u03FC\u03FD\x07\x13\x02\x02\u03FD\xA1\x03\x02\x02\x02\u03FE" +
    "\u03FF\x07\xC2\x02\x02\u03FF\xA3\x03\x02\x02\x02\u0400\u0401\x07.\x02" +
    "\x02\u0401\xA5\x03\x02\x02\x02\u0402\u0403\x07\x1B\x02\x02\u0403\u0404" +
    "\x07\x11\x02\x02\u0404\u0405\x05\xA8U\x02\u0405\u0408\x05R*\x02\u0406" +
    "\u0407\x07\x12\x02\x02\u0407\u0409\x05|?\x02\u0408\u0406\x03\x02\x02\x02" +
    "\u0408\u0409\x03\x02\x02\x02\u0409\u040A\x03\x02\x02\x02\u040A\u040B\x07" +
    "\x13\x02\x02\u040B\xA7\x03\x02\x02\x02\u040C\u040D\x07\xC2\x02\x02\u040D" +
    "\xA9\x03\x02\x02\x02\u040E\u040F\x07/\x02\x02\u040F\u0410\x07\x11\x02" +
    "\x02\u0410\u0411\x05\xACW\x02\u0411\u0412\x07\x13\x02\x02\u0412\xAB\x03" +
    "\x02\x02\x02\u0413\u0414\x07\xC3\x02\x02\u0414\xAD\x03\x02\x02\x02\u0415" +
    "\u0416\x070\x02\x02\u0416\u0417\x07\x11\x02\x02\u0417\u0418\x05\xB0Y\x02" +
    "\u0418\u0419\x07\x13\x02\x02\u0419\xAF\x03\x02\x02\x02\u041A\u041B\t\x03" +
    "\x02\x02\u041B\xB1\x03\x02\x02\x02\u041C\u041D\x071\x02\x02\u041D\xB3" +
    "\x03\x02\x02\x02\u041E\u041F\x072\x02\x02\u041F\u0424\x07\x11\x02\x02" +
    "\u0420\u0425\x05\xB6\\\x02\u0421\u0425\x05\xB8]\x02\u0422\u0425\x05\xBA" +
    "^\x02\u0423\u0425\x05\xBC_\x02\u0424\u0420\x03\x02\x02\x02\u0424\u0421" +
    "\x03\x02\x02\x02\u0424\u0422\x03\x02\x02\x02\u0424\u0423\x03\x02\x02\x02" +
    "\u0425\u0426\x03\x02\x02\x02\u0426\u0427\x07\x13\x02\x02\u0427\xB5\x03" +
    "\x02\x02\x02\u0428\u0429\x073\x02\x02\u0429\xB7\x03\x02\x02\x02\u042A" +
    "\u042B\x074\x02\x02\u042B\xB9\x03\x02\x02\x02\u042C\u042D\x075\x02\x02" +
    "\u042D\xBB\x03\x02\x02\x02\u042E\u042F\x076\x02\x02\u042F\xBD\x03\x02" +
    "\x02\x02\u0430\u0434\x07\x12\x02\x02\u0431\u0433\x05\xC0a\x02\u0432\u0431" +
    "\x03\x02\x02\x02\u0433\u0436\x03\x02";
ESSL_Parser._serializedATNSegment2 = "\x02\x02\u0434\u0432\x03\x02\x02\x02\u0434\u0435\x03\x02\x02\x02\u0435" +
    "\xBF\x03\x02\x02\x02\u0436\u0434\x03\x02\x02\x02\u0437\u043C\x07\v\x02" +
    "\x02\u0438\u043D\x05\xC2b\x02\u0439\u043D\x05\xC4c\x02\u043A\u043D\x05" +
    "\xC6d\x02\u043B\u043D\x05\xC8e\x02\u043C\u0438\x03\x02\x02\x02\u043C\u0439" +
    "\x03\x02\x02\x02\u043C\u043A\x03\x02\x02\x02\u043C\u043B\x03\x02\x02\x02" +
    "\u043D\xC1\x03\x02\x02\x02\u043E\u043F\x077\x02\x02\u043F\xC3\x03\x02" +
    "\x02\x02\u0440\u0441\x078\x02\x02\u0441\xC5\x03\x02\x02\x02\u0442\u0443" +
    "\x079\x02\x02\u0443\xC7\x03\x02\x02\x02\u0444\u0445\x07:\x02\x02\u0445" +
    "\u0446\x07\x11\x02\x02\u0446\u0447\x05\xCAf\x02\u0447\u0448\x07\x13\x02" +
    "\x02\u0448\xC9\x03\x02\x02\x02\u0449\u044A\x05\u0240\u0121\x02\u044A\xCB" +
    "\x03\x02\x02\x02\u044B\u044C\x05\xCEh\x02\u044C\u044D\x05\u0248\u0125" +
    "\x02\u044D\u044F\x03\x02\x02\x02\u044E\u044B\x03\x02\x02\x02\u044F\u0452" +
    "\x03\x02\x02\x02\u0450\u044E\x03\x02\x02\x02\u0450\u0451\x03\x02\x02\x02" +
    "\u0451\xCD\x03\x02\x02\x02\u0452\u0450\x03\x02\x02\x02\u0453\u0464\x07" +
    "\v\x02\x02\u0454\u0465\x05\xD0i\x02\u0455\u0465\x05b2\x02\u0456\u0465" +
    "\x05\xD2j\x02\u0457\u0465\x05\xD4k\x02\u0458\u0465\x05z>\x02\u0459\u0465" +
    "\x05\xF2z\x02\u045A\u0465\x05\x88E\x02\u045B\u0465\x05\xD8m\x02\u045C" +
    "\u0465\x05\xA4S\x02\u045D\u0465\x05\xDCo\x02\u045E\u0465\x05\xA6T\x02" +
    "\u045F\u0465\x05\xAAV\x02\u0460\u0465\x05\xAEX\x02\u0461\u0465\x05\u0186" +
    "\xC4\x02\u0462\u0465\x05\u018E\xC8\x02\u0463\u0465\x05\xDEp\x02\u0464" +
    "\u0454\x03\x02\x02\x02\u0464\u0455\x03\x02\x02\x02\u0464\u0456\x03\x02" +
    "\x02\x02\u0464\u0457\x03\x02\x02\x02\u0464\u0458\x03\x02\x02\x02\u0464" +
    "\u0459\x03\x02\x02\x02\u0464\u045A\x03\x02\x02\x02\u0464\u045B\x03\x02" +
    "\x02\x02\u0464\u045C\x03\x02\x02\x02\u0464\u045D\x03\x02\x02\x02\u0464" +
    "\u045E\x03\x02\x02\x02\u0464\u045F\x03\x02\x02\x02\u0464\u0460\x03\x02" +
    "\x02\x02\u0464\u0461\x03\x02\x02\x02\u0464\u0462\x03\x02\x02\x02\u0464" +
    "\u0463\x03\x02\x02\x02\u0465\xCF\x03\x02\x02\x02\u0466\u0467\x07;\x02" +
    "\x02\u0467\xD1\x03\x02\x02\x02\u0468\u0469\x07<\x02\x02\u0469\xD3\x03" +
    "\x02\x02\x02\u046A\u046B\x07=\x02\x02\u046B\u046C\x07\x11\x02\x02\u046C" +
    "\u046D\x05\xD6l\x02\u046D\u046E\x07\x13\x02\x02\u046E\xD5\x03\x02\x02" +
    "\x02\u046F\u0470\x07\xC2\x02\x02\u0470\xD7\x03\x02\x02\x02\u0471\u0472" +
    "\x07>\x02\x02\u0472\u0473\x07\x11\x02\x02\u0473\u0474\x05\xDAn\x02\u0474" +
    "\u0475\x07\x13\x02\x02\u0475\xD9\x03\x02\x02\x02\u0476\u0477\x05\u0240" +
    "\u0121\x02\u0477\xDB\x03\x02\x02\x02\u0478\u0479\x07?\x02\x02\u0479\xDD" +
    "\x03\x02\x02\x02\u047A\u047B\x07@\x02\x02\u047B\xDF\x03\x02\x02\x02\u047C" +
    "\u047D\x05\xE2r\x02\u047D\u047E\x05\u0248\u0125\x02\u047E\u0480\x03\x02" +
    "\x02\x02\u047F\u047C\x03\x02\x02\x02\u0480\u0483\x03\x02\x02\x02\u0481" +
    "\u047F\x03\x02\x02\x02\u0481\u0482\x03\x02\x02\x02\u0482\xE1\x03\x02\x02" +
    "\x02\u0483\u0481\x03\x02\x02\x02\u0484\u0488\x07\v\x02\x02\u0485\u0489" +
    "\x05\xD0i\x02\u0486\u0489\x05\u01FA\xFE\x02\u0487\u0489\x05z>\x02\u0488" +
    "\u0485\x03\x02\x02\x02\u0488\u0486\x03\x02\x02\x02\u0488\u0487\x03\x02" +
    "\x02\x02\u0489\xE3\x03\x02\x02\x02\u048A\u048B\x05\u0244\u0123\x02\u048B" +
    "\u048C\x07A\x02\x02\u048C\u048D\x05P)\x02\u048D\u048E\x05\xE6t\x02\u048E" +
    "\u048F\x05\xEEx\x02\u048F\xE5\x03\x02\x02\x02\u0490\u0491\x07\x04\x02" +
    "\x02\u0491\u0498\x05\xE8u\x02\u0492\u0494\x07\x12\x02\x02\u0493\u0492" +
    "\x03\x02\x02\x02\u0493\u0494\x03\x02\x02\x02\u0494\u0495\x03\x02\x02\x02" +
    "\u0495\u0497\x05\xE8u\x02\u0496\u0493\x03\x02\x02\x02\u0497\u049A\x03" +
    "\x02\x02\x02\u0498\u0496\x03\x02\x02\x02\u0498\u0499\x03\x02\x02\x02\u0499" +
    "\u049B\x03\x02\x02\x02\u049A\u0498\x03\x02\x02\x02\u049B\u049C\x07\x05" +
    "\x02\x02\u049C\xE7\x03\x02\x02\x02\u049D\u049E\x05\u0244\u0123\x02\u049E" +
    "\u04A1\x05\xEAv\x02\u049F\u04A0\x07B\x02\x02\u04A0\u04A2\x05\xECw\x02" +
    "\u04A1\u049F\x03\x02\x02\x02\u04A1\u04A2\x03\x02\x02\x02\u04A2\u04A3\x03" +
    "\x02\x02\x02\u04A3\u04A4\x05\u0242\u0122\x02\u04A4\xE9\x03\x02\x02\x02" +
    "\u04A5\u04A6\x07\xC2\x02\x02\u04A6\xEB\x03\x02\x02\x02\u04A7\u04A8\x05" +
    "\u012C\x97\x02\u04A8\xED\x03\x02\x02\x02\u04A9\u04AB\x05\xF0y\x02\u04AA" +
    "\u04A9\x03\x02\x02\x02\u04AB\u04AE\x03\x02\x02\x02\u04AC\u04AA\x03\x02" +
    "\x02\x02\u04AC\u04AD\x03\x02\x02\x02\u04AD\xEF\x03\x02\x02\x02\u04AE\u04AC" +
    "\x03\x02\x02\x02\u04AF\u04B3\x07\v\x02\x02\u04B0\u04B4\x05z>\x02\u04B1" +
    "\u04B4\x05\xF2z\x02\u04B2\u04B4\x05\xFC\x7F\x02\u04B3\u04B0\x03\x02\x02" +
    "\x02\u04B3\u04B1\x03\x02\x02\x02\u04B3\u04B2\x03\x02\x02\x02\u04B4\xF1" +
    "\x03\x02\x02\x02\u04B5\u04B6\x07C\x02\x02\u04B6\u04B7\x07\x11\x02\x02" +
    "\u04B7\u04BE\x05\xF4{\x02\u04B8\u04BA\x07\x12\x02\x02\u04B9\u04B8\x03" +
    "\x02\x02\x02\u04B9\u04BA\x03\x02\x02\x02\u04BA\u04BB\x03\x02\x02\x02\u04BB" +
    "\u04BD\x05\xF4{\x02\u04BC\u04B9\x03\x02\x02\x02\u04BD\u04C0\x03\x02\x02" +
    "\x02\u04BE\u04BC\x03\x02\x02\x02\u04BE\u04BF\x03\x02\x02\x02\u04BF\u04C1" +
    "\x03\x02\x02\x02\u04C0\u04BE\x03\x02\x02\x02\u04C1\u04C2\x07\x13\x02\x02" +
    "\u04C2\xF3\x03\x02\x02\x02\u04C3\u04C7\x05\xF6|\x02\u04C4\u04C7\x05\xF8" +
    "}\x02\u04C5\u04C7\x05\xFA~\x02\u04C6\u04C3\x03\x02\x02\x02\u04C6\u04C4" +
    "\x03\x02\x02\x02\u04C6\u04C5\x03\x02\x02\x02\u04C7\xF5\x03\x02\x02\x02" +
    "\u04C8\u04C9\x07D\x02\x02\u04C9\xF7\x03\x02\x02\x02\u04CA\u04CB\x07E\x02" +
    "\x02\u04CB\xF9\x03\x02\x02\x02\u04CC\u04CD\x07F\x02\x02\u04CD\xFB\x03" +
    "\x02\x02\x02\u04CE\u04CF\x07G\x02\x02\u04CF\xFD\x03\x02\x02\x02\u04D0" +
    "\u04D1\x05\u0244\u0123\x02\u04D1\u04D2\x07H\x02\x02\u04D2\u04D3\x05P)" +
    "\x02\u04D3\u04D4\x05\u0100\x81\x02\u04D4\u04D5\x05\u010E\x88\x02\u04D5" +
    "\xFF\x03\x02\x02\x02\u04D6\u04DD\x07\x04\x02\x02\u04D7\u04DC\x05\u0140" +
    "\xA1\x02\u04D8\u04DC\x05\u0102\x82\x02\u04D9\u04DC\x05\u0104\x83\x02\u04DA" +
    "\u04DC\x05\u0108\x85\x02\u04DB\u04D7\x03\x02\x02\x02\u04DB\u04D8\x03\x02" +
    "\x02\x02\u04DB\u04D9\x03\x02\x02\x02\u04DB\u04DA\x03\x02\x02\x02\u04DC" +
    "\u04DF\x03\x02\x02\x02\u04DD\u04DB\x03\x02\x02\x02\u04DD\u04DE\x03\x02" +
    "\x02\x02\u04DE\u04E1\x03\x02\x02\x02\u04DF\u04DD\x03\x02\x02\x02\u04E0" +
    "\u04E2\x05\u010C\x87\x02\u04E1\u04E0\x03\x02\x02\x02\u04E1\u04E2\x03\x02" +
    "\x02\x02\u04E2\u04E3\x03\x02\x02\x02\u04E3\u04E4\x07\x05\x02\x02\u04E4" +
    "\u0101\x03\x02\x02\x02\u04E5\u04E6\x07I\x02\x02\u04E6\u0103\x03\x02\x02" +
    "\x02\u04E7\u04E8\x07J\x02\x02\u04E8\u04E9\x07\x11\x02\x02\u04E9\u04EA" +
    "\x05\u0106\x84\x02\u04EA\u04EB\x07\x13\x02\x02\u04EB\u0105\x03\x02\x02" +
    "\x02\u04EC\u04ED\x05\u0114\x8B\x02\u04ED\u0107\x03\x02\x02\x02\u04EE\u04EF" +
    "\x07K\x02\x02\u04EF\u04F0\x07\x11\x02\x02\u04F0\u04F1\x05\u010A\x86\x02" +
    "\u04F1\u04F2\x07\x13\x02\x02\u04F2\u0109\x03\x02\x02\x02\u04F3\u04F4\t" +
    "\x03\x02\x02\u04F4\u010B\x03\x02\x02\x02\u04F5\u04F6\x07\xB8\x02\x02\u04F6" +
    "\u010D\x03\x02\x02\x02\u04F7\u04F9\x05\u0110\x89\x02\u04F8\u04F7\x03\x02" +
    "\x02\x02\u04F9\u04FC\x03\x02\x02\x02\u04FA\u04F8\x03\x02\x02\x02\u04FA" +
    "\u04FB\x03\x02\x02\x02\u04FB\u010F\x03\x02\x02\x02\u04FC\u04FA\x03\x02" +
    "\x02\x02\u04FD\u04FE\x07\v\x02\x02\u04FE\u04FF\x05z>\x02\u04FF\u0111\x03" +
    "\x02\x02\x02\u0500\u0501\x05\u0244\u0123\x02\u0501\u0502\x05\u0114\x8B" +
    "\x02\u0502\u0503\x05P)\x02\u0503\u0504\x05\u0120\x91\x02\u0504\u0505\x05" +
    "\u0136\x9C\x02\u0505\u0506\x05\u0242\u0122\x02\u0506\u0113\x03\x02\x02" +
    "\x02\u0507\u0509\x05\u0116\x8C\x02\u0508\u050A\x05\u011C\x8F\x02\u0509" +
    "\u0508\x03\x02\x02\x02\u0509\u050A\x03\x02\x02\x02\u050A\u050C\x03\x02" +
    "\x02\x02\u050B\u050D\x05\u011E\x90\x02\u050C\u050B\x03\x02\x02\x02\u050C" +
    "\u050D\x03\x02\x02\x02\u050D\u0115\x03\x02\x02\x02\u050E\u0511\x05\u0118" +
    "\x8D\x02\u050F\u0511\x05\u011A\x8E\x02\u0510\u050E\x03\x02\x02\x02\u0510" +
    "\u050F\x03\x02\x02\x02\u0511\u0117\x03\x02\x02\x02\u0512\u0513\t\x04\x02" +
    "\x02\u0513\u0119\x03\x02\x02\x02\u0514\u0515\x07\xC2\x02\x02\u0515\u011B" +
    "\x03\x02\x02\x02\u0516\u0517\x07T\x02\x02\u0517\u0518\x07U\x02\x02\u0518" +
    "\u011D\x03\x02\x02\x02\u0519\u051A\x07V\x02\x02\u051A\u011F\x03\x02\x02" +
    "\x02\u051B\u0523\x07B\x02\x02\u051C\u0524\x05\u0122\x92\x02\u051D\u0524" +
    "\x05\u0128\x95\x02\u051E\u0524\x05\u0124\x93\x02\u051F\u0524\x05\u0130" +
    "\x99\x02\u0520\u0524\x05\u012A\x96\x02\u0521\u0524\x05\u0126\x94\x02\u0522" +
    "\u0524\x05\u0132\x9A\x02\u0523\u051C\x03\x02\x02\x02\u0523\u051D\x03\x02" +
    "\x02\x02\u0523\u051E\x03\x02\x02\x02\u0523\u051F\x03\x02\x02\x02\u0523" +
    "\u0520\x03\x02\x02\x02\u0523\u0521\x03\x02\x02\x02\u0523\u0522\x03\x02" +
    "\x02\x02\u0524\u0526\x03\x02\x02\x02\u0525\u051B\x03\x02\x02\x02\u0525" +
    "\u0526\x03\x02\x02\x02\u0526\u0121\x03\x02\x02\x02\u0527\u0528\x07W\x02" +
    "\x02\u0528\u0123\x03\x02\x02\x02\u0529\u052A\t\x05\x02\x02\u052A\u0125" +
    "\x03\x02\x02\x02\u052B\u052C\x07Z\x02\x02\u052C\u0127\x03\x02\x02\x02" +
    "\u052D\u052E\x07\xC2\x02\x02\u052E\u0129\x03\x02\x02\x02\u052F\u0532\x05" +
    "\u012C\x97\x02\u0530\u0532\x05\u012E\x98\x02\u0531\u052F\x03\x02\x02\x02" +
    "\u0531\u0530\x03\x02\x02\x02\u0532\u012B\x03\x02\x02\x02\u0533\u0535\x07" +
    "[\x02\x02\u0534\u0533\x03\x02\x02\x02\u0534\u0535\x03\x02\x02\x02\u0535" +
    "\u0536\x03\x02\x02\x02\u0536\u0537\x07\xC1\x02\x02\u0537\u012D\x03\x02" +
    "\x02\x02\u0538\u053A\x07[\x02\x02\u0539\u0538\x03\x02\x02\x02\u0539\u053A" +
    "\x03\x02\x02\x02\u053A\u053B\x03\x02\x02\x02\u053B\u053C\x07\xC0\x02\x02" +
    "\u053C\u012F\x03\x02\x02\x02\u053D\u053E\x07\xBF\x02\x02\u053E\u0131\x03" +
    "\x02\x02\x02\u053F\u0548\x07T\x02\x02\u0540\u0545\x05\u0134\x9B\x02\u0541" +
    "\u0542\x07\x12\x02\x02\u0542\u0544\x05\u0134\x9B\x02\u0543\u0541\x03\x02" +
    "\x02\x02\u0544\u0547\x03\x02\x02\x02\u0545\u0543\x03\x02\x02\x02\u0545" +
    "\u0546\x03\x02\x02\x02\u0546\u0549\x03\x02\x02\x02\u0547\u0545\x03\x02" +
    "\x02\x02\u0548\u0540\x03\x02\x02\x02\u0548\u0549\x03\x02\x02\x02\u0549" +
    "\u054A\x03\x02\x02\x02\u054A\u054B\x07U\x02\x02\u054B\u0133\x03\x02\x02" +
    "\x02\u054C\u054D\x07\xC2\x02\x02\u054D\u0135\x03\x02\x02\x02\u054E\u054F" +
    "\x05\u0138\x9D\x02\u054F\u0550\x05\u0248\u0125\x02\u0550\u0552\x03\x02" +
    "\x02\x02\u0551\u054E\x03\x02\x02\x02\u0552\u0555\x03\x02\x02\x02\u0553" +
    "\u0551\x03\x02\x02\x02\u0553\u0554\x03\x02\x02\x02\u0554\u0137\x03\x02" +
    "\x02\x02\u0555\u0553\x03\x02\x02\x02\u0556\u0576\x07\v\x02\x02\u0557\u0577" +
    "\x05\u013A\x9E\x02\u0558\u0577\x05\u013C\x9F\x02\u0559\u0577\x05\u0144" +
    "\xA3\x02\u055A\u0577\x05\u0148\xA5\x02\u055B\u0577\x05\u014A\xA6\x02\u055C" +
    "\u0577\x05\u014C\xA7\x02\u055D\u0577\x05\u014E\xA8\x02\u055E\u0577\x05" +
    "\u0150\xA9\x02\u055F\u0577\x05\u0170\xB9\x02\u0560\u0577\x05\u0172\xBA" +
    "\x02\u0561\u0577\x05\u0152\xAA\x02\u0562\u0577\x05\u0154\xAB\x02\u0563" +
    "\u0577\x05\u0156\xAC\x02\u0564\u0577\x05\u0158\xAD\x02\u0565\u0577\x05" +
    "\u0166\xB4\x02\u0566\u0577\x05\u016A\xB6\x02\u0567\u0577\x05\x88E\x02" +
    "\u0568\u0577\x05\u01DC\xEF\x02\u0569\u0577\x05\u016C\xB7\x02\u056A\u0577" +
    "\x05\u0174\xBB\x02\u056B\u0577\x05\u0176\xBC\x02\u056C\u0577\x05\u0178" +
    "\xBD\x02\u056D\u0577\x05\u017A\xBE\x02\u056E\u0577\x05\u017C\xBF\x02\u056F" +
    "\u0577\x05\xAAV\x02\u0570\u0577\x05\u017E\xC0\x02\u0571\u0577\x05\u0182" +
    "\xC2\x02\u0572\u0577\x05\u0186\xC4\x02\u0573\u0577\x05\u018E\xC8\x02\u0574" +
    "\u0577\x05\u0194\xCB\x02\u0575\u0577\x05\u0196\xCC\x02\u0576\u0557\x03" +
    "\x02\x02\x02\u0576\u0558\x03\x02\x02\x02\u0576\u0559\x03\x02\x02\x02\u0576" +
    "\u055A\x03\x02\x02\x02\u0576\u055B\x03\x02\x02\x02\u0576\u055C\x03\x02" +
    "\x02\x02\u0576\u055D\x03\x02\x02\x02\u0576\u055E\x03\x02\x02\x02\u0576" +
    "\u055F\x03\x02\x02\x02\u0576\u0560\x03\x02\x02\x02\u0576\u0561\x03\x02" +
    "\x02\x02\u0576\u0562\x03\x02\x02\x02\u0576\u0563\x03\x02\x02\x02\u0576" +
    "\u0564\x03\x02\x02\x02\u0576\u0565\x03\x02\x02\x02\u0576\u0566\x03\x02" +
    "\x02\x02\u0576\u0567\x03\x02\x02\x02\u0576\u0568\x03\x02\x02\x02\u0576" +
    "\u0569\x03\x02\x02\x02\u0576\u056A\x03\x02\x02\x02\u0576\u056B\x03\x02" +
    "\x02\x02\u0576\u056C\x03\x02\x02\x02\u0576\u056D\x03\x02\x02\x02\u0576" +
    "\u056E\x03\x02\x02\x02\u0576\u056F\x03\x02\x02\x02\u0576\u0570\x03\x02" +
    "\x02\x02\u0576\u0571\x03\x02\x02\x02\u0576\u0572\x03\x02\x02\x02\u0576" +
    "\u0573\x03\x02\x02\x02\u0576\u0574\x03\x02\x02\x02\u0576\u0575\x03\x02" +
    "\x02\x02\u0577\u0139\x03\x02\x02\x02\u0578\u0579\x07\x1F\x02\x02\u0579" +
    "\u057A\x05\u013E\xA0\x02\u057A\u013B\x03\x02\x02\x02\u057B\u057C\x07 " +
    "\x02\x02\u057C\u057D\x05\u013E\xA0\x02\u057D\u013D\x03\x02\x02\x02\u057E" +
    "\u057F\x07\x11\x02\x02\u057F\u0582\x05P)\x02\u0580\u0581\x07\x12\x02\x02" +
    "\u0581\u0583\x05\u0140\xA1\x02\u0582\u0580\x03\x02\x02\x02\u0582\u0583" +
    "\x03\x02\x02\x02\u0583\u0586\x03\x02\x02\x02\u0584\u0585\x07\x12\x02\x02" +
    "\u0585\u0587\x05\u0142\xA2\x02\u0586\u0584\x03\x02\x02\x02\u0586\u0587" +
    "\x03\x02\x02\x02\u0587\u0589\x03\x02\x02\x02\u0588\u058A\x05\xBE`\x02" +
    "\u0589\u0588\x03\x02\x02\x02\u0589\u058A\x03\x02\x02\x02\u058A\u058B\x03" +
    "\x02\x02\x02\u058B\u058C\x07\x13\x02\x02\u058C\u013F\x03\x02\x02\x02\u058D" +
    "\u058E\t\x06\x02\x02\u058E\u0141\x03\x02\x02\x02\u058F\u0590\t\x07\x02" +
    "\x02\u0590\u0143\x03\x02\x02\x02\u0591\u0592\x07A\x02\x02\u0592\u0593" +
    "\x07\x11\x02\x02\u0593\u0598\x05\u0146\xA4\x02\u0594\u0595\x07\x12\x02" +
    "\x02\u0595\u0597\x05\u0146\xA4\x02\u0596\u0594\x03\x02\x02\x02\u0597\u059A" +
    "\x03\x02\x02\x02\u0598\u0596\x03\x02\x02\x02\u0598\u0599\x03\x02\x02\x02" +
    "\u0599\u059B\x03\x02\x02\x02\u059A\u0598\x03\x02\x02\x02\u059B\u059C\x07" +
    "\x13\x02\x02\u059C\u0145\x03\x02\x02\x02\u059D\u059E\x07\xC2\x02\x02\u059E" +
    "\u0147\x03\x02\x02\x02\u059F\u05A0\x07`\x02\x02\u05A0\u0149\x03\x02\x02" +
    "\x02\u05A1\u05A2\x07a\x02\x02\u05A2\u014B\x03\x02\x02\x02\u05A3\u05A4" +
    "\x07b\x02\x02\u05A4\u014D\x03\x02\x02\x02\u05A5\u05A6\x07c\x02\x02\u05A6" +
    "\u014F\x03\x02\x02\x02\u05A7\u05A8\x07d\x02\x02\u05A8\u0151\x03\x02\x02" +
    "\x02\u05A9\u05AA\x07e\x02\x02\u05AA\u0153\x03\x02\x02\x02\u05AB\u05AC" +
    "\x07f\x02\x02\u05AC\u0155\x03\x02\x02\x02\u05AD\u05AE\x07g\x02\x02\u05AE" +
    "\u0157\x03\x02\x02\x02\u05AF\u05B0\x07h\x02\x02\u05B0\u05B1\x07\x11\x02" +
    "\x02\u05B1\u05B9\x05\u015A\xAE\x02\u05B2\u05B8\x05\u015C\xAF\x02\u05B3" +
    "\u05B8\x05\u015E\xB0\x02\u05B4\u05B8\x05\u0160\xB1\x02\u05B5\u05B8\x05" +
    "\u0162\xB2\x02\u05B6\u05B8\x05\u0164\xB3\x02\u05B7\u05B2\x03\x02\x02\x02" +
    "\u05B7\u05B3\x03\x02\x02\x02\u05B7\u05B4\x03\x02\x02\x02\u05B7\u05B5\x03" +
    "\x02\x02\x02\u05B7\u05B6\x03\x02\x02\x02\u05B8\u05BB\x03\x02\x02\x02\u05B9" +
    "\u05B7\x03\x02\x02\x02\u05B9\u05BA\x03\x02\x02\x02\u05BA\u05BC\x03\x02" +
    "\x02\x02\u05BB\u05B9\x03\x02\x02\x02\u05BC\u05BD\x07\x13\x02\x02\u05BD" +
    "\u0159\x03\x02\x02\x02\u05BE\u05BF\x07\xC2\x02\x02\u05BF\u015B\x03\x02" +
    "\x02\x02\u05C0\u05C1\t\b\x02\x02\u05C1\u015D\x03\x02\x02\x02\u05C2\u05C3" +
    "\t\t\x02\x02\u05C3\u015F\x03\x02\x02\x02\u05C4\u05C5\t\n\x02\x02\u05C5" +
    "\u0161\x03\x02\x02\x02\u05C6\u05C7\x07~\x02\x02\u05C7\u0163\x03\x02\x02" +
    "\x02\u05C8\u05C9\x07\xBF\x02\x02\u05C9\u0165\x03\x02\x02\x02\u05CA\u05CB" +
    "\x07\x7F\x02\x02\u05CB\u05CC\x07\x11\x02\x02\u05CC\u05CD\x05\u0168\xB5" +
    "\x02\u05CD\u05CE\x07\x13\x02\x02\u05CE\u0167\x03\x02\x02\x02\u05CF\u05D0" +
    "\t\v\x02\x02\u05D0\u0169\x03\x02\x02\x02\u05D1\u05D2\x07\x83\x02\x02\u05D2" +
    "\u016B\x03\x02\x02\x02\u05D3\u05D4\x07\x84\x02\x02\u05D4\u05D5\x07\x11" +
    "\x02\x02\u05D5\u05D8\x05\u0240\u0121\x02\u05D6\u05D7\x07\x12\x02\x02\u05D7" +
    "\u05D9\x05\u016E\xB8\x02\u05D8\u05D6\x03\x02\x02\x02\u05D8\u05D9\x03\x02" +
    "\x02\x02\u05D9\u05DA\x03\x02\x02\x02\u05DA\u05DB\x07\x13\x02\x02\u05DB" +
    "\u016D\x03\x02\x02\x02\u05DC\u05DD\x07\x85\x02\x02\u05DD\u016F\x03\x02" +
    "\x02\x02\u05DE\u05DF\x07\x86\x02\x02\u05DF\u0171\x03\x02\x02\x02\u05E0" +
    "\u05E1\x07\x87\x02\x02\u05E1\u0173\x03\x02\x02\x02\u05E2\u05E3\x07\x88" +
    "\x02\x02\u05E3\u0175\x03\x02\x02\x02\u05E4\u05E5\x07H\x02\x02\u05E5\u0177" +
    "\x03\x02\x02\x02\u05E6\u05E7\x07\x89\x02\x02\u05E7\u0179\x03\x02\x02\x02" +
    "\u05E8\u05E9\x07\x8A\x02\x02\u05E9\u017B\x03\x02\x02\x02\u05EA\u05EB\x07" +
    "\x8B\x02\x02\u05EB\u017D\x03\x02\x02\x02\u05EC\u05ED\x07\x8C\x02\x02\u05ED" +
    "\u05EE\x07\x11\x02\x02\u05EE\u05EF\x05\u0180\xC1\x02\u05EF\u05F0\x07\x13" +
    "\x02\x02\u05F0\u017F\x03\x02\x02\x02\u05F1\u05F2\x07\xBF\x02\x02\u05F2" +
    "\u0181\x03\x02\x02\x02\u05F3\u05F4\x07\x8D\x02\x02\u05F4\u05F5\x07\x11" +
    "\x02\x02\u05F5\u05F6\x05\u0184\xC3\x02\u05F6\u05F7\x07\x13\x02\x02\u05F7" +
    "\u0183\x03\x02\x02\x02\u05F8\u05F9\t\f\x02\x02\u05F9\u0185\x03\x02\x02" +
    "\x02\u05FA\u05FB\x07\x90\x02\x02\u05FB\u05FC\x07\x11\x02\x02\u05FC\u0601" +
    "\x05\u0188\xC5\x02\u05FD\u05FE\x07\x12\x02\x02\u05FE\u0600\x05\u0188\xC5" +
    "\x02\u05FF\u05FD\x03\x02\x02\x02\u0600\u0603\x03\x02\x02\x02\u0601\u05FF" +
    "\x03\x02\x02\x02\u0601\u0602\x03\x02\x02\x02\u0602\u0604\x03\x02\x02\x02" +
    "\u0603\u0601\x03\x02\x02\x02\u0604\u0605\x07\x13\x02\x02\u0605\u0187\x03" +
    "\x02\x02\x02\u0606\u0607\x05\u018A\xC6\x02\u0607\u0608\x07\x07\x02\x02" +
    "\u0608\u0609\x05\u018C\xC7\x02\u0609\u0189\x03\x02\x02\x02\u060A\u060B" +
    "\x07\xBF\x02\x02\u060B\u018B\x03\x02\x02\x02\u060C\u060D\x07\xBF\x02\x02" +
    "\u060D\u018D\x03\x02\x02\x02\u060E\u060F\x071\x02\x02\u060F\u0612\x07" +
    "\x11\x02\x02\u0610\u0613\x05\u0190\xC9\x02\u0611\u0613\x05\u0192\xCA\x02" +
    "\u0612\u0610\x03\x02\x02\x02\u0612\u0611\x03\x02\x02\x02\u0613\u0614\x03" +
    "\x02\x02\x02\u0614\u0615\x07\x13\x02\x02\u0615\u018F\x03\x02\x02\x02\u0616" +
    "\u0617\x07\x91\x02\x02\u0617\u0191\x03\x02\x02\x02\u0618\u0619\x07\xC1" +
    "\x02\x02\u0619\u0193\x03\x02\x02\x02\u061A\u061B\x07\x92\x02\x02\u061B" +
    "\u0195\x03\x02\x02\x02\u061C\u061D\x07\x93\x02\x02\u061D\u0197\x03\x02" +
    "\x02\x02\u061E\u061F\x05\u0244\u0123\x02\u061F\u0620\x073\x02\x02\u0620" +
    "\u0621\x05\u019A\xCE\x02\u0621\u0622\x05\u01A4\xD3\x02\u0622\u0199\x03" +
    "\x02\x02\x02\u0623\u0624\x07\x04\x02\x02\u0624\u0628\x05\u019C\xCF\x02" +
    "\u0625\u0627\x05\u01A0\xD1\x02\u0626\u0625\x03\x02\x02\x02\u0627\u062A" +
    "\x03\x02\x02\x02\u0628\u0626\x03\x02\x02\x02\u0628\u0629\x03\x02\x02\x02" +
    "\u0629\u062B\x03\x02\x02\x02\u062A\u0628\x03\x02\x02\x02\u062B\u062C\x07" +
    "\x05\x02\x02\u062C\u019B\x03\x02\x02\x02\u062D\u062E\x05\u0244\u0123\x02" +
    "\u062E\u062F\x05\u019E\xD0\x02\u062F\u0630\x05\u01A8\xD5\x02\u0630\u0631" +
    "\x05\u0242\u0122\x02\u0631\u019D\x03\x02\x02\x02\u0632\u0633\x07\xC2\x02" +
    "\x02\u0633\u019F\x03\x02\x02\x02\u0634\u0635\x05\u0244\u0123\x02\u0635" +
    "\u0636\x05\u01A2\xD2\x02\u0636\u0637\x05\u01A8\xD5\x02\u0637\u0638\x05" +
    "\u0242\u0122\x02\u0638\u01A1\x03\x02\x02\x02\u0639\u063A\x07\xC2\x02\x02" +
    "\u063A\u01A3\x03\x02\x02\x02\u063B\u063D\x05\u01A6\xD4\x02\u063C\u063B" +
    "\x03\x02\x02\x02\u063D\u0640\x03\x02\x02\x02\u063E\u063C\x03\x02\x02\x02" +
    "\u063E\u063F\x03\x02\x02\x02\u063F\u01A5\x03\x02\x02\x02\u0640\u063E\x03" +
    "\x02\x02\x02\u0641\u0642\x07\v\x02\x02\u0642\u0643\x05z>\x02\u0643\u01A7" +
    "\x03\x02\x02\x02\u0644\u0646\x05\u01AA\xD6\x02\u0645\u0644\x03\x02\x02" +
    "\x02\u0646\u0649\x03\x02\x02\x02\u0647\u0645\x03\x02\x02\x02\u0647\u0648" +
    "\x03\x02\x02\x02\u0648\u01A9\x03\x02\x02\x02\u0649\u0647\x03\x02\x02\x02" +
    "\u064A\u064B\x07\v\x02\x02\u064B\u064C\x05\x82B\x02\u064C\u01AB\x03\x02" +
    "\x02\x02\u064D\u064E\x05\u0244\u0123\x02\u064E\u064F\x07\x94\x02\x02\u064F" +
    "\u0650\x05\u01AE\xD8\x02\u0650\u01AD\x03\x02\x02\x02\u0651\u0655\x07\x04" +
    "\x02\x02\u0652\u0654\x05\u01B4\xDB\x02\u0653\u0652\x03\x02\x02\x02\u0654" +
    "\u0657\x03\x02\x02\x02\u0655\u0653\x03\x02\x02\x02\u0655\u0656\x03\x02" +
    "\x02\x02\u0656\u0658\x03\x02\x02\x02\u0657\u0655\x03\x02\x02\x02\u0658" +
    "\u0659\x07\x05\x02\x02\u0659\u065A\x05\u01B2\xDA\x02\u065A\u01AF\x03\x02" +
    "\x02\x02\u065B\u065C\x07\v\x02\x02\u065C\u065D\x05\x88E\x02\u065D\u01B1" +
    "\x03\x02\x02\x02\u065E\u065F\x05\u01B0\xD9\x02\u065F\u0660\x05\u0248\u0125" +
    "\x02\u0660\u0662\x03\x02\x02\x02\u0661\u065E\x03\x02\x02\x02\u0662\u0665" +
    "\x03\x02\x02\x02\u0663\u0661\x03\x02\x02\x02\u0663\u0664\x03\x02\x02\x02" +
    "\u0664\u01B3\x03\x02\x02\x02\u0665\u0663\x03\x02\x02\x02\u0666\u0667\x05" +
    "\u0244\u0123\x02\u0667\u0668\x05P)\x02\u0668\u0669\x05\u01B6\xDC\x02\u0669" +
    "\u066A\x05\u01E0\xF1\x02\u066A\u066B\x05\u01C2\xE2\x02\u066B\u066C\x05" +
    "\u0248\u0125\x02\u066C\u01B5\x03\x02\x02\x02\u066D\u0676\x07\x11\x02\x02" +
    "\u066E\u0673\x05\u01B8\xDD\x02\u066F\u0670\x07\x12\x02\x02\u0670\u0672" +
    "\x05\u01B8\xDD\x02\u0671\u066F\x03\x02\x02\x02\u0672\u0675\x03\x02\x02" +
    "\x02\u0673\u0671\x03\x02\x02\x02\u0673\u0674\x03\x02\x02\x02\u0674\u0677" +
    "\x03\x02\x02\x02\u0675\u0673\x03\x02\x02\x02\u0676\u066E\x03\x02\x02\x02" +
    "\u0676\u0677\x03\x02\x02\x02\u0677\u0678\x03\x02\x02\x02\u0678\u0679\x07" +
    "\x13\x02\x02\u0679\u01B7\x03\x02\x02\x02\u067A\u067B\x05\u0244\u0123\x02" +
    "\u067B\u067C\x05\u01BA\xDE\x02\u067C\u067F\x05\u01C0\xE1\x02\u067D\u0680" +
    "\x05\u01D2\xEA\x02\u067E\u0680\x05\u0248\u0125\x02\u067F\u067D\x03\x02" +
    "\x02\x02\u067F\u067E\x03\x02\x02\x02\u0680\u01B9\x03\x02\x02\x02\u0681" +
    "\u0682\x05\u01BC\xDF\x02\u0682\u0683\x05P)\x02\u0683\u01BB\x03\x02\x02" +
    "\x02\u0684\u0685\x05\u0116\x8C\x02\u0685\u0686\x05\u01BE\xE0\x02\u0686" +
    "\u0688\x03\x02\x02\x02\u0687\u0684\x03\x02\x02\x02\u0687\u0688\x03\x02" +
    "\x02\x02\u0688\u01BD\x03\x02\x02\x02\u0689\u068B\x05\u011C\x8F\x02\u068A" +
    "\u0689\x03\x02\x02\x02\u068A\u068B\x03\x02\x02\x02\u068B\u068D\x03\x02" +
    "\x02\x02\u068C\u068E\x05\u011E\x90\x02\u068D\u068C\x03\x02\x02\x02\u068D" +
    "\u068E\x03\x02\x02\x02\u068E\u01BF\x03\x02\x02\x02\u068F\u0695\x07B\x02" +
    "\x02\u0690\u0696\x05\u0122\x92\x02\u0691\u0696\x05\u0128\x95\x02\u0692" +
    "\u0696\x05\u0124\x93\x02\u0693\u0696\x05\u012A\x96\x02\u0694\u0696\x05" +
    "\u0130\x99\x02\u0695\u0690\x03\x02\x02\x02\u0695\u0691\x03\x02\x02\x02" +
    "\u0695\u0692\x03\x02\x02\x02\u0695\u0693\x03\x02\x02\x02\u0695\u0694\x03" +
    "\x02\x02\x02\u0696\u0698\x03\x02\x02\x02\u0697\u068F\x03\x02\x02\x02\u0697" +
    "\u0698\x03\x02\x02\x02\u0698\u01C1\x03\x02\x02\x02\u0699\u069C\x05\u01C4" +
    "\xE3\x02\u069A\u069C\x05\u01CA\xE6\x02\u069B\u0699\x03\x02\x02\x02\u069B" +
    "\u069A\x03\x02\x02\x02\u069C\u01C3\x03\x02\x02\x02\u069D\u069E\x07\x95" +
    "\x02\x02\u069E\u069F\x05\u01C6\xE4\x02\u069F\u01C5\x03\x02\x02\x02\u06A0" +
    "\u06A5\x05\u01C8\xE5\x02\u06A1\u06A2\x07\x12\x02\x02\u06A2\u06A4\x05\u01C8" +
    "\xE5\x02\u06A3\u06A1\x03\x02\x02\x02\u06A4\u06A7\x03\x02\x02\x02\u06A5" +
    "\u06A3\x03\x02\x02\x02\u06A5\u06A6\x03\x02\x02\x02\u06A6\u01C7\x03\x02" +
    "\x02\x02\u06A7\u06A5\x03\x02\x02\x02\u06A8\u06A9\x07\xC2\x02\x02\u06A9" +
    "\u01C9\x03\x02\x02\x02\u06AA\u06AB\x07\x96\x02\x02\u06AB\u06AC\x05\u01CC" +
    "\xE7\x02\u06AC\u06AD\x05\u01BE\xE0\x02\u06AD\u01CB\x03\x02\x02\x02\u06AE" +
    "\u06AF\x05\u0116\x8C\x02\u06AF\u01CD\x03\x02\x02\x02\u06B0\u06B4\x07\v" +
    "\x02\x02\u06B1\u06B5\x05\u01D0\xE9\x02\u06B2\u06B5\x05\u01DA\xEE\x02\u06B3" +
    "\u06B5\x05\u01DC\xEF\x02\u06B4\u06B1\x03\x02\x02\x02\u06B4\u06B2\x03\x02" +
    "\x02\x02\u06B4\u06B3\x03\x02\x02\x02\u06B5\u01CF\x03\x02\x02\x02\u06B6" +
    "\u06B7\x07\x1A\x02\x02\u06B7\u06B8\x07\x11\x02\x02\u06B8\u06BB\x05\u01D4" +
    "\xEB\x02\u06B9\u06BA\x07\x12\x02\x02\u06BA\u06BC\x05\u01D6\xEC\x02\u06BB" +
    "\u06B9\x03\x02\x02\x02\u06BB\u06BC\x03\x02\x02\x02\u06BC\u06BF\x03\x02" +
    "\x02\x02\u06BD\u06BE\x07\x12\x02\x02\u06BE\u06C0\x05\u01D8\xED\x02\u06BF" +
    "\u06BD\x03\x02\x02\x02\u06BF\u06C0\x03\x02\x02\x02\u06C0\u06C1\x03\x02" +
    "\x02\x02\u06C1\u06C2\x07\x13\x02\x02\u06C2\u01D1\x03\x02\x02\x02\u06C3" +
    "\u06C4\x05\u01CE\xE8\x02\u06C4\u06C5\x05\u0248\u0125\x02\u06C5\u06C7\x03" +
    "\x02\x02\x02\u06C6\u06C3\x03\x02\x02\x02\u06C7\u06CA\x03\x02\x02\x02\u06C8" +
    "\u06C6\x03\x02\x02\x02\u06C8\u06C9\x03\x02\x02\x02\u06C9\u01D3\x03\x02" +
    "\x02\x02\u06CA\u06C8\x03\x02\x02\x02\u06CB\u06CC\x07\xC2\x02\x02\u06CC" +
    "\u01D5\x03\x02\x02\x02\u06CD\u06CE\x07\xC2\x02\x02\u06CE\u01D7\x03\x02" +
    "\x02\x02\u06CF\u06D0\x07\xB9\x02\x02\u06D0\u01D9\x03\x02\x02\x02\u06D1" +
    "\u06D2\x07\x97\x02\x02\u06D2\u01DB\x03\x02\x02\x02\u06D3\u06D4\x07\x98" +
    "\x02\x02\u06D4\u01DD\x03\x02\x02\x02\u06D5\u06E6\x07\v\x02\x02\u06D6\u06E7" +
    "\x05\u01E2\xF2\x02\u06D7\u06E7\x05\u01E4\xF3\x02\u06D8\u06E7\x05\u01EE" +
    "\xF8\x02\u06D9\u06E7\x05\u01F0\xF9\x02\u06DA\u06E7\x05\u01F4\xFB\x02\u06DB" +
    "\u06E7\x05\u01F6\xFC\x02\u06DC\u06E7\x05\u01F8\xFD\x02\u06DD\u06E7\x05" +
    "\u01FA\xFE\x02\u06DE\u06E7\x05\u01FC\xFF\x02\u06DF\u06E7\x05\u01FE\u0100" +
    "\x02\u06E0\u06E7\x05\u0200\u0101\x02\u06E1\u06E7\x05\u0202\u0102\x02\u06E2" +
    "\u06E7\x05\u0204\u0103\x02\u06E3\u06E7\x05\x88E\x02\u06E4\u06E7\x05\x8A" +
    "F\x02\u06E5\u06E7\x05\u0230\u0119\x02\u06E6\u06D6\x03\x02\x02\x02\u06E6" +
    "\u06D7\x03\x02\x02\x02\u06E6\u06D8\x03\x02\x02\x02\u06E6\u06D9\x03\x02" +
    "\x02\x02\u06E6\u06DA\x03\x02\x02\x02\u06E6\u06DB\x03\x02\x02\x02\u06E6" +
    "\u06DC\x03\x02\x02\x02\u06E6\u06DD\x03\x02\x02\x02\u06E6\u06DE\x03\x02" +
    "\x02\x02\u06E6\u06DF\x03\x02\x02\x02\u06E6\u06E0\x03\x02\x02\x02\u06E6" +
    "\u06E1\x03\x02\x02\x02\u06E6\u06E2\x03\x02\x02\x02\u06E6\u06E3\x03\x02" +
    "\x02\x02\u06E6\u06E4\x03\x02\x02\x02\u06E6\u06E5\x03\x02\x02\x02\u06E7" +
    "\u01DF\x03\x02\x02\x02\u06E8\u06E9\x05\u01DE\xF0\x02\u06E9\u06EA\x05\u0248" +
    "\u0125\x02\u06EA\u06EC\x03\x02\x02\x02\u06EB\u06E8\x03\x02\x02\x02\u06EC" +
    "\u06EF\x03\x02\x02\x02\u06ED\u06EB\x03\x02\x02\x02";
ESSL_Parser._serializedATNSegment3 = "\u06ED\u06EE\x03\x02\x02\x02\u06EE\u01E1\x03\x02\x02\x02\u06EF\u06ED\x03" +
    "\x02\x02\x02\u06F0\u06F1\x07D\x02\x02\u06F1\u01E3\x03\x02\x02\x02\u06F2" +
    "\u06F3\x07\x99\x02\x02\u06F3\u06F4\x07\x11\x02\x02\u06F4\u06F7\x05\u01E6" +
    "\xF4\x02\u06F5\u06F6\x07\x12\x02\x02\u06F6\u06F8\x05\u01E6\xF4\x02\u06F7" +
    "\u06F5\x03\x02\x02\x02\u06F7\u06F8\x03\x02\x02\x02\u06F8\u06FB\x03\x02" +
    "\x02\x02\u06F9\u06FA\x07\x12\x02\x02\u06FA\u06FC\x05\u01E6\xF4\x02\u06FB" +
    "\u06F9\x03\x02\x02\x02\u06FB\u06FC\x03\x02\x02\x02\u06FC\u06FD\x03\x02" +
    "\x02\x02\u06FD\u06FE\x07\x13\x02\x02\u06FE\u01E5\x03\x02\x02\x02\u06FF" +
    "\u0703\x05\u01E8\xF5\x02\u0700\u0703\x05\u01EA\xF6\x02\u0701\u0703\x05" +
    "\u01EC\xF7\x02\u0702\u06FF\x03\x02\x02\x02\u0702\u0700\x03\x02\x02\x02" +
    "\u0702\u0701\x03\x02\x02\x02\u0703\u01E7\x03\x02\x02\x02\u0704\u0705\x07" +
    "\x9A\x02\x02\u0705\u01E9\x03\x02\x02\x02\u0706\u0707\x07\x9B\x02\x02\u0707" +
    "\u01EB\x03\x02\x02\x02\u0708\u0709\x07\x9C\x02\x02\u0709\u01ED\x03\x02" +
    "\x02\x02\u070A\u070B\x07\x9D\x02\x02\u070B\u01EF\x03\x02\x02\x02\u070C" +
    "\u070D\x07\x9E\x02\x02\u070D\u070E\x07\x11\x02\x02\u070E\u070F\x05\u01F2" +
    "\xFA\x02\u070F\u0710\x07\x13\x02\x02\u0710\u01F1\x03\x02\x02\x02\u0711" +
    "\u0712\t\r\x02\x02\u0712\u01F3\x03\x02\x02\x02\u0713\u0714\x07\xA3\x02" +
    "\x02\u0714\u01F5\x03\x02\x02\x02\u0715\u0716\x07\xA4\x02\x02\u0716\u01F7" +
    "\x03\x02\x02\x02\u0717\u0718\x07\xA5\x02\x02\u0718\u01F9\x03\x02\x02\x02" +
    "\u0719\u071A\x07\xA6\x02\x02\u071A\u01FB\x03\x02\x02\x02\u071B\u071C\x07" +
    "\xA7\x02\x02\u071C\u01FD\x03\x02\x02\x02\u071D\u071E\x07\xA8\x02\x02\u071E" +
    "\u01FF\x03\x02\x02\x02\u071F\u0720\x07\xA9\x02\x02\u0720\u0201\x03\x02" +
    "\x02\x02\u0721\u0722\x07\xAA\x02\x02\u0722\u0203\x03\x02\x02\x02\u0723" +
    "\u0724\x07\xAB\x02\x02\u0724\u0725\x07\x11\x02\x02\u0725\u0726\x05\u0206" +
    "\u0104\x02\u0726\u0727\x07\x13\x02\x02\u0727\u0205\x03\x02\x02\x02\u0728" +
    "\u072F\x05\u0208\u0105\x02\u0729\u072F\x05\u020A\u0106\x02\u072A\u072F" +
    "\x05\u0212\u010A\x02\u072B\u072F\x05\u0214\u010B\x02\u072C\u072F\x05\u0216" +
    "\u010C\x02\u072D\u072F\x05\u0218\u010D\x02\u072E\u0728\x03\x02\x02\x02" +
    "\u072E\u0729\x03\x02\x02\x02\u072E\u072A\x03\x02\x02\x02\u072E\u072B\x03" +
    "\x02\x02\x02\u072E\u072C\x03\x02\x02\x02\u072E\u072D\x03\x02\x02\x02\u072F" +
    "\u0207\x03\x02\x02\x02\u0730\u0731\x07\xAC\x02\x02\u0731\u0732\x05\u021C" +
    "\u010F\x02\u0732\u0734\x03\x02\x02\x02\u0733\u0735\x05\u021A\u010E\x02" +
    "\u0734\u0733\x03\x02\x02\x02\u0734\u0735\x03\x02\x02\x02\u0735\u0209\x03" +
    "\x02\x02\x02\u0736\u0737\x07\xAD\x02\x02\u0737\u0738\x05\u020C\u0107\x02" +
    "\u0738\u020B\x03\x02\x02\x02\u0739\u073C\x05\u020E\u0108\x02\u073A\u073B" +
    "\x07\x12\x02\x02\u073B\u073D\x05\u020E\u0108\x02\u073C\u073A\x03\x02\x02" +
    "\x02\u073C\u073D\x03\x02\x02\x02\u073D\u020D\x03\x02\x02\x02\u073E\u0740" +
    "\x05\u021C\u010F\x02\u073F\u0741\x05\u021A\u010E\x02\u0740\u073F\x03\x02" +
    "\x02\x02\u0740\u0741\x03\x02\x02\x02\u0741\u0743\x03\x02\x02\x02\u0742" +
    "\u0744\x05\u0210\u0109\x02\u0743\u0742\x03\x02\x02\x02\u0743\u0744\x03" +
    "\x02\x02\x02\u0744\u020F\x03\x02\x02\x02\u0745\u0746\x07\xBA\x02\x02\u0746" +
    "\u0211\x03\x02\x02\x02\u0747\u0748\x07\xAE\x02\x02\u0748\u0749\x05\u021C" +
    "\u010F\x02\u0749\u074B\x03\x02\x02\x02\u074A\u074C\x05\u021A\u010E\x02" +
    "\u074B\u074A\x03\x02\x02\x02\u074B\u074C\x03\x02\x02\x02\u074C\u0213\x03" +
    "\x02\x02\x02\u074D\u074E\x07\xAF\x02\x02\u074E\u074F\x05\u021C\u010F\x02" +
    "\u074F\u0751\x03\x02\x02\x02\u0750\u0752\x05\u021A\u010E\x02\u0751\u0750" +
    "\x03\x02\x02\x02\u0751\u0752\x03\x02\x02\x02\u0752\u0215\x03\x02\x02\x02" +
    "\u0753\u0754\x07\xB0\x02\x02\u0754\u0755\x05\u021C\u010F\x02\u0755\u0217" +
    "\x03\x02\x02\x02\u0756\u0757\x07\xB1\x02\x02\u0757\u0219\x03\x02\x02\x02" +
    "\u0758\u0759\x07\xBF\x02\x02\u0759\u021B\x03\x02\x02\x02\u075A\u075B\x07" +
    "\xC2\x02\x02\u075B\u021D\x03\x02\x02\x02\u075C\u075D\x05\u0244\u0123\x02" +
    "\u075D\u075E\x07\xB2\x02\x02\u075E\u075F\x05\u0220\u0111\x02\u075F\u021F" +
    "\x03\x02\x02\x02\u0760\u0761\x05\u0242\u0122\x02\u0761\u0765\x07\x04\x02" +
    "\x02\u0762\u0764\x05\u0226\u0114\x02\u0763\u0762\x03\x02\x02\x02\u0764" +
    "\u0767\x03\x02\x02\x02\u0765\u0763\x03\x02\x02\x02\u0765\u0766\x03\x02" +
    "\x02\x02\u0766\u0768\x03\x02\x02\x02\u0767\u0765\x03\x02\x02\x02\u0768" +
    "\u0769\x07\x05\x02\x02\u0769\u076A\x05\u0224\u0113\x02\u076A\u0221\x03" +
    "\x02\x02\x02\u076B\u076C\x07\v\x02\x02\u076C\u076D\x05\x88E\x02\u076D" +
    "\u0223\x03\x02\x02\x02\u076E\u076F\x05\u0222\u0112\x02\u076F\u0770\x05" +
    "\u0248\u0125\x02\u0770\u0772\x03\x02\x02\x02\u0771\u076E\x03\x02\x02\x02" +
    "\u0772\u0775\x03\x02\x02\x02\u0773\u0771\x03\x02\x02\x02\u0773\u0774\x03" +
    "\x02\x02\x02\u0774\u0225\x03\x02\x02\x02\u0775\u0773\x03\x02\x02\x02\u0776" +
    "\u0777\x05\u0244\u0123\x02\u0777\u0778\x05P)\x02\u0778\u0779\x05\u01B6" +
    "\xDC\x02\u0779\u077A\x05\u022A\u0116\x02\u077A\u077B\x05\u01CA\xE6\x02" +
    "\u077B\u0227\x03\x02\x02\x02\u077C\u0784\x07\v\x02\x02\u077D\u0785\x05" +
    "\u022C\u0117\x02\u077E\u0785\x05\u022E\u0118\x02\u077F\u0785\x05\u01FA" +
    "\xFE\x02\u0780\u0785\x05\u01EE\xF8\x02\u0781\u0785\x05\x88E\x02\u0782" +
    "\u0785\x05\x8AF\x02\u0783\u0785\x05\u0230\u0119\x02\u0784\u077D\x03\x02" +
    "\x02\x02\u0784\u077E\x03\x02\x02\x02\u0784\u077F\x03\x02\x02\x02\u0784" +
    "\u0780\x03\x02\x02\x02\u0784\u0781\x03\x02\x02\x02\u0784\u0782\x03\x02" +
    "\x02\x02\u0784\u0783\x03\x02\x02\x02\u0785\u0229\x03\x02\x02\x02\u0786" +
    "\u0787\x05\u0228\u0115\x02\u0787\u0788\x05\u0248\u0125\x02\u0788\u078A" +
    "\x03\x02\x02\x02\u0789\u0786\x03\x02\x02\x02\u078A\u078D\x03\x02\x02\x02" +
    "\u078B\u0789\x03\x02\x02\x02\u078B\u078C\x03\x02\x02\x02\u078C\u022B\x03" +
    "\x02\x02\x02\u078D\u078B\x03\x02\x02\x02\u078E\u078F\x07\xB3\x02\x02\u078F" +
    "\u022D\x03\x02\x02\x02\u0790\u0791\x07\xB4\x02\x02\u0791\u022F\x03\x02" +
    "\x02\x02\u0792\u0793\x07\xB5\x02\x02\u0793\u0231\x03\x02\x02\x02\u0794" +
    "\u0795\x05\u0244\u0123\x02\u0795\u0796\x07\xB6\x02\x02\u0796\u0797\x05" +
    "\u0234\u011B\x02\u0797\u0233\x03\x02\x02\x02\u0798\u0799\x05\u0242\u0122" +
    "\x02\u0799\u079D\x07\x04\x02\x02\u079A\u079C\x05\u023A\u011E\x02\u079B" +
    "\u079A\x03\x02\x02\x02\u079C\u079F\x03\x02\x02\x02\u079D\u079B\x03\x02" +
    "\x02\x02\u079D\u079E\x03\x02\x02\x02\u079E\u07A0\x03\x02\x02\x02\u079F" +
    "\u079D\x03\x02\x02\x02\u07A0\u07A1\x07\x05\x02\x02\u07A1\u07A2\x05\u0238" +
    "\u011D\x02\u07A2\u0235\x03\x02\x02\x02\u07A3\u07A4\x07\v\x02\x02\u07A4" +
    "\u07A5\x05\x88E\x02\u07A5\u0237\x03\x02\x02\x02\u07A6\u07A7\x05\u0236" +
    "\u011C\x02\u07A7\u07A8\x05\u0248\u0125\x02\u07A8\u07AA\x03\x02\x02\x02" +
    "\u07A9\u07A6\x03\x02\x02\x02\u07AA\u07AD\x03\x02\x02\x02\u07AB\u07A9\x03" +
    "\x02\x02\x02\u07AB\u07AC\x03\x02\x02\x02\u07AC\u0239\x03\x02\x02\x02\u07AD" +
    "\u07AB\x03\x02\x02\x02\u07AE\u07AF\x05\u0244\u0123\x02\u07AF\u07B0\x05" +
    "P)\x02\u07B0\u07B1\x05\u01B6\xDC\x02\u07B1\u07B2\x05\u023E\u0120\x02\u07B2" +
    "\u07B3\x05\u01CA\xE6\x02\u07B3\u023B\x03\x02\x02\x02\u07B4\u07B5\x07\v" +
    "\x02\x02\u07B5\u07B6\x05\x88E\x02\u07B6\u023D\x03\x02\x02\x02\u07B7\u07B8" +
    "\x05\u023C\u011F\x02\u07B8\u07B9\x05\u0248\u0125\x02\u07B9\u07BB\x03\x02" +
    "\x02\x02\u07BA\u07B7\x03\x02\x02\x02\u07BB\u07BE\x03\x02\x02\x02\u07BC" +
    "\u07BA\x03\x02\x02\x02\u07BC\u07BD\x03\x02\x02\x02\u07BD\u023F\x03\x02" +
    "\x02\x02\u07BE\u07BC\x03\x02\x02\x02\u07BF\u07C4\x07\xC2\x02\x02\u07C0" +
    "\u07C1\x07\xB7\x02\x02\u07C1\u07C3\x07\xC2\x02\x02\u07C2\u07C0\x03\x02" +
    "\x02\x02\u07C3\u07C6\x03\x02\x02\x02\u07C4\u07C2\x03\x02\x02\x02\u07C4" +
    "\u07C5\x03\x02\x02\x02\u07C5\u0241\x03\x02\x02\x02\u07C6\u07C4\x03\x02" +
    "\x02\x02\u07C7\u07CB\x05\u024C\u0127\x02\u07C8\u07CB\x05\u024E\u0128\x02" +
    "\u07C9\u07CB\x05\u0250\u0129\x02\u07CA\u07C7\x03\x02\x02\x02\u07CA\u07C8" +
    "\x03\x02\x02\x02\u07CA\u07C9\x03\x02\x02\x02\u07CB\u07CE\x03\x02\x02\x02" +
    "\u07CC\u07CA\x03\x02\x02\x02\u07CC\u07CD\x03\x02\x02\x02\u07CD\u0243\x03" +
    "\x02\x02\x02\u07CE\u07CC\x03\x02\x02\x02\u07CF\u07D3\x05\u024A\u0126\x02" +
    "\u07D0\u07D3\x05\u024E\u0128\x02\u07D1\u07D3\x05\u0250\u0129\x02\u07D2" +
    "\u07CF\x03\x02\x02\x02\u07D2\u07D0\x03\x02\x02\x02\u07D2\u07D1\x03\x02" +
    "\x02\x02\u07D3\u07D6\x03\x02\x02\x02\u07D4\u07D2\x03\x02\x02\x02\u07D4" +
    "\u07D5\x03\x02\x02\x02\u07D5\u0245\x03\x02\x02\x02\u07D6\u07D4\x03\x02" +
    "\x02\x02\u07D7\u07DB\x05\u024A\u0126\x02\u07D8\u07DB\x05\u024E\u0128\x02" +
    "\u07D9\u07DB\x05\u0250\u0129\x02\u07DA\u07D7\x03\x02\x02\x02\u07DA\u07D8" +
    "\x03\x02\x02\x02\u07DA\u07D9\x03\x02\x02\x02\u07DB\u07DE\x03\x02\x02\x02" +
    "\u07DC\u07DA\x03\x02\x02\x02\u07DC\u07DD\x03\x02\x02\x02\u07DD\u0247\x03" +
    "\x02\x02\x02\u07DE\u07DC\x03\x02\x02\x02\u07DF\u07E2\x05\u024E\u0128\x02" +
    "\u07E0\u07E2\x05\u0250\u0129\x02\u07E1\u07DF\x03\x02\x02\x02\u07E1\u07E0" +
    "\x03\x02\x02\x02\u07E2\u07E5\x03\x02\x02\x02\u07E3\u07E1\x03\x02\x02\x02" +
    "\u07E3\u07E4\x03\x02\x02\x02\u07E4\u0249\x03\x02\x02\x02\u07E5\u07E3\x03" +
    "\x02\x02\x02\u07E6\u07E7\x07\xBB\x02\x02\u07E7\u024B\x03\x02\x02\x02\u07E8" +
    "\u07E9\x07\xBC\x02\x02\u07E9\u024D\x03\x02\x02\x02\u07EA\u07EB\x07\xBD" +
    "\x02\x02\u07EB\u024F\x03\x02\x02\x02\u07EC\u07EE\x07\xBE\x02\x02\u07ED" +
    "\u07EC\x03\x02\x02\x02\u07EE\u07EF\x03\x02\x02\x02\u07EF\u07ED\x03\x02" +
    "\x02\x02\u07EF\u07F0\x03\x02\x02\x02\u07F0\u0251\x03\x02\x02\x02}\u0254" +
    "\u0265\u0269\u0270\u0272\u0277\u027A\u027D\u028A\u0293\u0297\u029D\u029F" +
    "\u02A3\u02A6\u02A9\u02C3\u02D0\u02E7\u0312\u0316\u031A\u0342\u0345\u034D" +
    "\u0356\u0361\u037F\u0395\u0399\u039C\u03A3\u03A9\u03C3\u0408\u0424\u0434" +
    "\u043C\u0450\u0464\u0481\u0488\u0493\u0498\u04A1\u04AC\u04B3\u04B9\u04BE" +
    "\u04C6\u04DB\u04DD\u04E1\u04FA\u0509\u050C\u0510\u0523\u0525\u0531\u0534" +
    "\u0539\u0545\u0548\u0553\u0576\u0582\u0586\u0589\u0598\u05B7\u05B9\u05D8" +
    "\u0601\u0612\u0628\u063E\u0647\u0655\u0663\u0673\u0676\u067F\u0687\u068A" +
    "\u068D\u0695\u0697\u069B\u06A5\u06B4\u06BB\u06BF\u06C8\u06E6\u06ED\u06F7" +
    "\u06FB\u0702\u072E\u0734\u073C\u0740\u0743\u074B\u0751\u0765\u0773\u0784" +
    "\u078B\u079D\u07AB\u07BC\u07C4\u07CA\u07CC\u07D2\u07D4\u07DA\u07DC\u07E1" +
    "\u07E3\u07EF";
ESSL_Parser._serializedATN = Utils.join([
    ESSL_Parser._serializedATNSegment0,
    ESSL_Parser._serializedATNSegment1,
    ESSL_Parser._serializedATNSegment2,
    ESSL_Parser._serializedATNSegment3,
], "");
class TopContext extends ParserRuleContext_1.ParserRuleContext {
    EOF() { return this.getToken(ESSL_Parser.EOF, 0); }
    entity() {
        return this.tryGetRuleContext(0, EntityContext);
    }
    virtualEntity() {
        return this.tryGetRuleContext(0, VirtualEntityContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_top; }
    // @Override
    enterRule(listener) {
        if (listener.enterTop) {
            listener.enterTop(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitTop) {
            listener.exitTop(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitTop) {
            return visitor.visitTop(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.TopContext = TopContext;
class EntityContext extends ParserRuleContext_1.ParserRuleContext {
    beforeComment() {
        return this.getRuleContext(0, BeforeCommentContext);
    }
    entityName() {
        return this.getRuleContext(0, EntityNameContext);
    }
    entityBlock() {
        return this.getRuleContext(0, EntityBlockContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_entity; }
    // @Override
    enterRule(listener) {
        if (listener.enterEntity) {
            listener.enterEntity(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEntity) {
            listener.exitEntity(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEntity) {
            return visitor.visitEntity(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EntityContext = EntityContext;
class EntityNameContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_entityName; }
    // @Override
    enterRule(listener) {
        if (listener.enterEntityName) {
            listener.enterEntityName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEntityName) {
            listener.exitEntityName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEntityName) {
            return visitor.visitEntityName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EntityNameContext = EntityNameContext;
class EntityBlockContext extends ParserRuleContext_1.ParserRuleContext {
    esslComment() {
        return this.getRuleContext(0, EsslCommentContext);
    }
    namespace() {
        return this.getRuleContext(0, NamespaceContext);
    }
    entityType() {
        return this.getRuleContext(0, EntityTypeContext);
    }
    entityEvents() {
        return this.getRuleContext(0, EntityEventsContext);
    }
    dependsOn(i) {
        if (i === undefined) {
            return this.getRuleContexts(DependsOnContext);
        }
        else {
            return this.getRuleContext(i, DependsOnContext);
        }
    }
    service() {
        return this.tryGetRuleContext(0, ServiceContext);
    }
    enumDecl(i) {
        if (i === undefined) {
            return this.getRuleContexts(EnumDeclContext);
        }
        else {
            return this.getRuleContext(i, EnumDeclContext);
        }
    }
    dictionary(i) {
        if (i === undefined) {
            return this.getRuleContexts(DictionaryContext);
        }
        else {
            return this.getRuleContext(i, DictionaryContext);
        }
    }
    valueType(i) {
        if (i === undefined) {
            return this.getRuleContexts(ValueTypeContext);
        }
        else {
            return this.getRuleContext(i, ValueTypeContext);
        }
    }
    interfaceType(i) {
        if (i === undefined) {
            return this.getRuleContexts(InterfaceTypeContext);
        }
        else {
            return this.getRuleContext(i, InterfaceTypeContext);
        }
    }
    entityCommands() {
        return this.tryGetRuleContext(0, EntityCommandsContext);
    }
    entityQueries() {
        return this.tryGetRuleContext(0, EntityQueriesContext);
    }
    entitySubscriptions() {
        return this.tryGetRuleContext(0, EntitySubscriptionsContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_entityBlock; }
    // @Override
    enterRule(listener) {
        if (listener.enterEntityBlock) {
            listener.enterEntityBlock(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEntityBlock) {
            listener.exitEntityBlock(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEntityBlock) {
            return visitor.visitEntityBlock(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EntityBlockContext = EntityBlockContext;
class VirtualEntityContext extends ParserRuleContext_1.ParserRuleContext {
    beforeComment() {
        return this.getRuleContext(0, BeforeCommentContext);
    }
    entityName() {
        return this.getRuleContext(0, EntityNameContext);
    }
    extendsEntity() {
        return this.getRuleContext(0, ExtendsEntityContext);
    }
    virtualEntityBlock() {
        return this.getRuleContext(0, VirtualEntityBlockContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_virtualEntity; }
    // @Override
    enterRule(listener) {
        if (listener.enterVirtualEntity) {
            listener.enterVirtualEntity(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitVirtualEntity) {
            listener.exitVirtualEntity(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitVirtualEntity) {
            return visitor.visitVirtualEntity(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.VirtualEntityContext = VirtualEntityContext;
class ExtendsEntityContext extends ParserRuleContext_1.ParserRuleContext {
    baseEntityName() {
        return this.tryGetRuleContext(0, BaseEntityNameContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_extendsEntity; }
    // @Override
    enterRule(listener) {
        if (listener.enterExtendsEntity) {
            listener.enterExtendsEntity(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitExtendsEntity) {
            listener.exitExtendsEntity(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitExtendsEntity) {
            return visitor.visitExtendsEntity(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ExtendsEntityContext = ExtendsEntityContext;
class BaseEntityNameContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_baseEntityName; }
    // @Override
    enterRule(listener) {
        if (listener.enterBaseEntityName) {
            listener.enterBaseEntityName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitBaseEntityName) {
            listener.exitBaseEntityName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitBaseEntityName) {
            return visitor.visitBaseEntityName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.BaseEntityNameContext = BaseEntityNameContext;
class VirtualEntityBlockContext extends ParserRuleContext_1.ParserRuleContext {
    namespace() {
        return this.getRuleContext(0, NamespaceContext);
    }
    dependsOn(i) {
        if (i === undefined) {
            return this.getRuleContexts(DependsOnContext);
        }
        else {
            return this.getRuleContext(i, DependsOnContext);
        }
    }
    service() {
        return this.tryGetRuleContext(0, ServiceContext);
    }
    enumDecl(i) {
        if (i === undefined) {
            return this.getRuleContexts(EnumDeclContext);
        }
        else {
            return this.getRuleContext(i, EnumDeclContext);
        }
    }
    dictionary(i) {
        if (i === undefined) {
            return this.getRuleContexts(DictionaryContext);
        }
        else {
            return this.getRuleContext(i, DictionaryContext);
        }
    }
    valueType(i) {
        if (i === undefined) {
            return this.getRuleContexts(ValueTypeContext);
        }
        else {
            return this.getRuleContext(i, ValueTypeContext);
        }
    }
    interfaceType(i) {
        if (i === undefined) {
            return this.getRuleContexts(InterfaceTypeContext);
        }
        else {
            return this.getRuleContext(i, InterfaceTypeContext);
        }
    }
    entityCommands() {
        return this.tryGetRuleContext(0, EntityCommandsContext);
    }
    entityQueries() {
        return this.tryGetRuleContext(0, EntityQueriesContext);
    }
    entitySubscriptions() {
        return this.tryGetRuleContext(0, EntitySubscriptionsContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_virtualEntityBlock; }
    // @Override
    enterRule(listener) {
        if (listener.enterVirtualEntityBlock) {
            listener.enterVirtualEntityBlock(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitVirtualEntityBlock) {
            listener.exitVirtualEntityBlock(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitVirtualEntityBlock) {
            return visitor.visitVirtualEntityBlock(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.VirtualEntityBlockContext = VirtualEntityBlockContext;
class NamespaceContext extends ParserRuleContext_1.ParserRuleContext {
    esslComment() {
        return this.getRuleContext(0, EsslCommentContext);
    }
    namespaceName() {
        return this.getRuleContext(0, NamespaceNameContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_namespace; }
    // @Override
    enterRule(listener) {
        if (listener.enterNamespace) {
            listener.enterNamespace(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitNamespace) {
            listener.exitNamespace(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitNamespace) {
            return visitor.visitNamespace(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.NamespaceContext = NamespaceContext;
class NamespaceNameContext extends ParserRuleContext_1.ParserRuleContext {
    dottedId() {
        return this.getRuleContext(0, DottedIdContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_namespaceName; }
    // @Override
    enterRule(listener) {
        if (listener.enterNamespaceName) {
            listener.enterNamespaceName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitNamespaceName) {
            listener.exitNamespaceName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitNamespaceName) {
            return visitor.visitNamespaceName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.NamespaceNameContext = NamespaceNameContext;
class DependsOnContext extends ParserRuleContext_1.ParserRuleContext {
    esslComment() {
        return this.getRuleContext(0, EsslCommentContext);
    }
    dependsOnNameSpace() {
        return this.getRuleContext(0, DependsOnNameSpaceContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_dependsOn; }
    // @Override
    enterRule(listener) {
        if (listener.enterDependsOn) {
            listener.enterDependsOn(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDependsOn) {
            listener.exitDependsOn(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDependsOn) {
            return visitor.visitDependsOn(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DependsOnContext = DependsOnContext;
class DependsOnNameSpaceContext extends ParserRuleContext_1.ParserRuleContext {
    dottedId() {
        return this.getRuleContext(0, DottedIdContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_dependsOnNameSpace; }
    // @Override
    enterRule(listener) {
        if (listener.enterDependsOnNameSpace) {
            listener.enterDependsOnNameSpace(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDependsOnNameSpace) {
            listener.exitDependsOnNameSpace(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDependsOnNameSpace) {
            return visitor.visitDependsOnNameSpace(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DependsOnNameSpaceContext = DependsOnNameSpaceContext;
class ServiceContext extends ParserRuleContext_1.ParserRuleContext {
    serviceDecors() {
        return this.getRuleContext(0, ServiceDecorsContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_service; }
    // @Override
    enterRule(listener) {
        if (listener.enterService) {
            listener.enterService(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitService) {
            listener.exitService(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitService) {
            return visitor.visitService(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ServiceContext = ServiceContext;
class ServiceDecorsContext extends ParserRuleContext_1.ParserRuleContext {
    serviceDecor(i) {
        if (i === undefined) {
            return this.getRuleContexts(ServiceDecorContext);
        }
        else {
            return this.getRuleContext(i, ServiceDecorContext);
        }
    }
    esslComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslCommentContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_serviceDecors; }
    // @Override
    enterRule(listener) {
        if (listener.enterServiceDecors) {
            listener.enterServiceDecors(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitServiceDecors) {
            listener.exitServiceDecors(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitServiceDecors) {
            return visitor.visitServiceDecors(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ServiceDecorsContext = ServiceDecorsContext;
class ServiceDecorContext extends ParserRuleContext_1.ParserRuleContext {
    allApiAuthPolicy() {
        return this.tryGetRuleContext(0, AllApiAuthPolicyContext);
    }
    allCommandAuthPolicy() {
        return this.tryGetRuleContext(0, AllCommandAuthPolicyContext);
    }
    allQueryAuthPolicy() {
        return this.tryGetRuleContext(0, AllQueryAuthPolicyContext);
    }
    allSubscriptionAuthPolicy() {
        return this.tryGetRuleContext(0, AllSubscriptionAuthPolicyContext);
    }
    allAdminAuthPolicy() {
        return this.tryGetRuleContext(0, AllAdminAuthPolicyContext);
    }
    ecosystemPoliciesMethod() {
        return this.tryGetRuleContext(0, EcosystemPoliciesMethodContext);
    }
    ecosystemSecretsMethod() {
        return this.tryGetRuleContext(0, EcosystemSecretsMethodContext);
    }
    umlGroupStrReplDecor() {
        return this.tryGetRuleContext(0, UmlGroupStrReplDecorContext);
    }
    umlImageDecor() {
        return this.tryGetRuleContext(0, UmlImageDecorContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_serviceDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterServiceDecor) {
            listener.enterServiceDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitServiceDecor) {
            listener.exitServiceDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitServiceDecor) {
            return visitor.visitServiceDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ServiceDecorContext = ServiceDecorContext;
class AllApiAuthPolicyContext extends ParserRuleContext_1.ParserRuleContext {
    authPolicyNames() {
        return this.getRuleContext(0, AuthPolicyNamesContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_allApiAuthPolicy; }
    // @Override
    enterRule(listener) {
        if (listener.enterAllApiAuthPolicy) {
            listener.enterAllApiAuthPolicy(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAllApiAuthPolicy) {
            listener.exitAllApiAuthPolicy(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAllApiAuthPolicy) {
            return visitor.visitAllApiAuthPolicy(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AllApiAuthPolicyContext = AllApiAuthPolicyContext;
class AllCommandAuthPolicyContext extends ParserRuleContext_1.ParserRuleContext {
    authPolicyNames() {
        return this.getRuleContext(0, AuthPolicyNamesContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_allCommandAuthPolicy; }
    // @Override
    enterRule(listener) {
        if (listener.enterAllCommandAuthPolicy) {
            listener.enterAllCommandAuthPolicy(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAllCommandAuthPolicy) {
            listener.exitAllCommandAuthPolicy(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAllCommandAuthPolicy) {
            return visitor.visitAllCommandAuthPolicy(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AllCommandAuthPolicyContext = AllCommandAuthPolicyContext;
class AllQueryAuthPolicyContext extends ParserRuleContext_1.ParserRuleContext {
    authPolicyNames() {
        return this.getRuleContext(0, AuthPolicyNamesContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_allQueryAuthPolicy; }
    // @Override
    enterRule(listener) {
        if (listener.enterAllQueryAuthPolicy) {
            listener.enterAllQueryAuthPolicy(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAllQueryAuthPolicy) {
            listener.exitAllQueryAuthPolicy(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAllQueryAuthPolicy) {
            return visitor.visitAllQueryAuthPolicy(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AllQueryAuthPolicyContext = AllQueryAuthPolicyContext;
class AllSubscriptionAuthPolicyContext extends ParserRuleContext_1.ParserRuleContext {
    authPolicyNames() {
        return this.getRuleContext(0, AuthPolicyNamesContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_allSubscriptionAuthPolicy; }
    // @Override
    enterRule(listener) {
        if (listener.enterAllSubscriptionAuthPolicy) {
            listener.enterAllSubscriptionAuthPolicy(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAllSubscriptionAuthPolicy) {
            listener.exitAllSubscriptionAuthPolicy(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAllSubscriptionAuthPolicy) {
            return visitor.visitAllSubscriptionAuthPolicy(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AllSubscriptionAuthPolicyContext = AllSubscriptionAuthPolicyContext;
class AllAdminAuthPolicyContext extends ParserRuleContext_1.ParserRuleContext {
    authPolicyNames() {
        return this.getRuleContext(0, AuthPolicyNamesContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_allAdminAuthPolicy; }
    // @Override
    enterRule(listener) {
        if (listener.enterAllAdminAuthPolicy) {
            listener.enterAllAdminAuthPolicy(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAllAdminAuthPolicy) {
            listener.exitAllAdminAuthPolicy(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAllAdminAuthPolicy) {
            return visitor.visitAllAdminAuthPolicy(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AllAdminAuthPolicyContext = AllAdminAuthPolicyContext;
class AuthPolicyNamesContext extends ParserRuleContext_1.ParserRuleContext {
    authPolicyName(i) {
        if (i === undefined) {
            return this.getRuleContexts(AuthPolicyNameContext);
        }
        else {
            return this.getRuleContext(i, AuthPolicyNameContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_authPolicyNames; }
    // @Override
    enterRule(listener) {
        if (listener.enterAuthPolicyNames) {
            listener.enterAuthPolicyNames(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAuthPolicyNames) {
            listener.exitAuthPolicyNames(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAuthPolicyNames) {
            return visitor.visitAuthPolicyNames(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AuthPolicyNamesContext = AuthPolicyNamesContext;
class EcosystemPoliciesMethodContext extends ParserRuleContext_1.ParserRuleContext {
    ecosystemPoliciesMethodName() {
        return this.getRuleContext(0, EcosystemPoliciesMethodNameContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_ecosystemPoliciesMethod; }
    // @Override
    enterRule(listener) {
        if (listener.enterEcosystemPoliciesMethod) {
            listener.enterEcosystemPoliciesMethod(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEcosystemPoliciesMethod) {
            listener.exitEcosystemPoliciesMethod(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEcosystemPoliciesMethod) {
            return visitor.visitEcosystemPoliciesMethod(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EcosystemPoliciesMethodContext = EcosystemPoliciesMethodContext;
class EcosystemPoliciesMethodNameContext extends ParserRuleContext_1.ParserRuleContext {
    dottedId() {
        return this.getRuleContext(0, DottedIdContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_ecosystemPoliciesMethodName; }
    // @Override
    enterRule(listener) {
        if (listener.enterEcosystemPoliciesMethodName) {
            listener.enterEcosystemPoliciesMethodName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEcosystemPoliciesMethodName) {
            listener.exitEcosystemPoliciesMethodName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEcosystemPoliciesMethodName) {
            return visitor.visitEcosystemPoliciesMethodName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EcosystemPoliciesMethodNameContext = EcosystemPoliciesMethodNameContext;
class EcosystemSecretsMethodContext extends ParserRuleContext_1.ParserRuleContext {
    ecosystemSecretsMethodName() {
        return this.getRuleContext(0, EcosystemSecretsMethodNameContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_ecosystemSecretsMethod; }
    // @Override
    enterRule(listener) {
        if (listener.enterEcosystemSecretsMethod) {
            listener.enterEcosystemSecretsMethod(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEcosystemSecretsMethod) {
            listener.exitEcosystemSecretsMethod(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEcosystemSecretsMethod) {
            return visitor.visitEcosystemSecretsMethod(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EcosystemSecretsMethodContext = EcosystemSecretsMethodContext;
class EcosystemSecretsMethodNameContext extends ParserRuleContext_1.ParserRuleContext {
    dottedId() {
        return this.getRuleContext(0, DottedIdContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_ecosystemSecretsMethodName; }
    // @Override
    enterRule(listener) {
        if (listener.enterEcosystemSecretsMethodName) {
            listener.enterEcosystemSecretsMethodName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEcosystemSecretsMethodName) {
            listener.exitEcosystemSecretsMethodName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEcosystemSecretsMethodName) {
            return visitor.visitEcosystemSecretsMethodName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EcosystemSecretsMethodNameContext = EcosystemSecretsMethodNameContext;
class UmlGroupStrReplDecorContext extends ParserRuleContext_1.ParserRuleContext {
    umlGroupStrReplParams() {
        return this.getRuleContext(0, UmlGroupStrReplParamsContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_umlGroupStrReplDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterUmlGroupStrReplDecor) {
            listener.enterUmlGroupStrReplDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitUmlGroupStrReplDecor) {
            listener.exitUmlGroupStrReplDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitUmlGroupStrReplDecor) {
            return visitor.visitUmlGroupStrReplDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.UmlGroupStrReplDecorContext = UmlGroupStrReplDecorContext;
class UmlGroupStrReplParamsContext extends ParserRuleContext_1.ParserRuleContext {
    umlGroupTitleStrRepl() {
        return this.getRuleContext(0, UmlGroupTitleStrReplContext);
    }
    umlStrReplOldValue() {
        return this.getRuleContext(0, UmlStrReplOldValueContext);
    }
    umlStrReplNewValue() {
        return this.getRuleContext(0, UmlStrReplNewValueContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_umlGroupStrReplParams; }
    // @Override
    enterRule(listener) {
        if (listener.enterUmlGroupStrReplParams) {
            listener.enterUmlGroupStrReplParams(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitUmlGroupStrReplParams) {
            listener.exitUmlGroupStrReplParams(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitUmlGroupStrReplParams) {
            return visitor.visitUmlGroupStrReplParams(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.UmlGroupStrReplParamsContext = UmlGroupStrReplParamsContext;
class UmlGroupTitleStrReplContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_umlGroupTitleStrRepl; }
    // @Override
    enterRule(listener) {
        if (listener.enterUmlGroupTitleStrRepl) {
            listener.enterUmlGroupTitleStrRepl(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitUmlGroupTitleStrRepl) {
            listener.exitUmlGroupTitleStrRepl(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitUmlGroupTitleStrRepl) {
            return visitor.visitUmlGroupTitleStrRepl(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.UmlGroupTitleStrReplContext = UmlGroupTitleStrReplContext;
class UmlStrReplOldValueContext extends ParserRuleContext_1.ParserRuleContext {
    QUOTED_STR() { return this.getToken(ESSL_Parser.QUOTED_STR, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_umlStrReplOldValue; }
    // @Override
    enterRule(listener) {
        if (listener.enterUmlStrReplOldValue) {
            listener.enterUmlStrReplOldValue(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitUmlStrReplOldValue) {
            listener.exitUmlStrReplOldValue(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitUmlStrReplOldValue) {
            return visitor.visitUmlStrReplOldValue(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.UmlStrReplOldValueContext = UmlStrReplOldValueContext;
class UmlStrReplNewValueContext extends ParserRuleContext_1.ParserRuleContext {
    QUOTED_STR() { return this.getToken(ESSL_Parser.QUOTED_STR, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_umlStrReplNewValue; }
    // @Override
    enterRule(listener) {
        if (listener.enterUmlStrReplNewValue) {
            listener.enterUmlStrReplNewValue(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitUmlStrReplNewValue) {
            listener.exitUmlStrReplNewValue(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitUmlStrReplNewValue) {
            return visitor.visitUmlStrReplNewValue(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.UmlStrReplNewValueContext = UmlStrReplNewValueContext;
class UmlImageDecorContext extends ParserRuleContext_1.ParserRuleContext {
    umlImageParams() {
        return this.getRuleContext(0, UmlImageParamsContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_umlImageDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterUmlImageDecor) {
            listener.enterUmlImageDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitUmlImageDecor) {
            listener.exitUmlImageDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitUmlImageDecor) {
            return visitor.visitUmlImageDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.UmlImageDecorContext = UmlImageDecorContext;
class UmlImageParamsContext extends ParserRuleContext_1.ParserRuleContext {
    umlGroupImageName() {
        return this.getRuleContext(0, UmlGroupImageNameContext);
    }
    umlImageFormat() {
        return this.tryGetRuleContext(0, UmlImageFormatContext);
    }
    umlImageRemoveTitle() {
        return this.tryGetRuleContext(0, UmlImageRemoveTitleContext);
    }
    umlImageSubFolder() {
        return this.tryGetRuleContext(0, UmlImageSubFolderContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_umlImageParams; }
    // @Override
    enterRule(listener) {
        if (listener.enterUmlImageParams) {
            listener.enterUmlImageParams(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitUmlImageParams) {
            listener.exitUmlImageParams(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitUmlImageParams) {
            return visitor.visitUmlImageParams(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.UmlImageParamsContext = UmlImageParamsContext;
class UmlGroupImageNameContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_umlGroupImageName; }
    // @Override
    enterRule(listener) {
        if (listener.enterUmlGroupImageName) {
            listener.enterUmlGroupImageName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitUmlGroupImageName) {
            listener.exitUmlGroupImageName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitUmlGroupImageName) {
            return visitor.visitUmlGroupImageName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.UmlGroupImageNameContext = UmlGroupImageNameContext;
class UmlImageFormatContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_umlImageFormat; }
    // @Override
    enterRule(listener) {
        if (listener.enterUmlImageFormat) {
            listener.enterUmlImageFormat(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitUmlImageFormat) {
            listener.exitUmlImageFormat(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitUmlImageFormat) {
            return visitor.visitUmlImageFormat(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.UmlImageFormatContext = UmlImageFormatContext;
class UmlImageRemoveTitleContext extends ParserRuleContext_1.ParserRuleContext {
    boolValue() {
        return this.getRuleContext(0, BoolValueContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_umlImageRemoveTitle; }
    // @Override
    enterRule(listener) {
        if (listener.enterUmlImageRemoveTitle) {
            listener.enterUmlImageRemoveTitle(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitUmlImageRemoveTitle) {
            listener.exitUmlImageRemoveTitle(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitUmlImageRemoveTitle) {
            return visitor.visitUmlImageRemoveTitle(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.UmlImageRemoveTitleContext = UmlImageRemoveTitleContext;
class UmlImageSubFolderContext extends ParserRuleContext_1.ParserRuleContext {
    QUOTED_STR() { return this.getToken(ESSL_Parser.QUOTED_STR, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_umlImageSubFolder; }
    // @Override
    enterRule(listener) {
        if (listener.enterUmlImageSubFolder) {
            listener.enterUmlImageSubFolder(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitUmlImageSubFolder) {
            listener.exitUmlImageSubFolder(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitUmlImageSubFolder) {
            return visitor.visitUmlImageSubFolder(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.UmlImageSubFolderContext = UmlImageSubFolderContext;
class EntityTypeContext extends ParserRuleContext_1.ParserRuleContext {
    beforeComment() {
        return this.getRuleContext(0, BeforeCommentContext);
    }
    extendsList() {
        return this.getRuleContext(0, ExtendsListContext);
    }
    typeBlock() {
        return this.getRuleContext(0, TypeBlockContext);
    }
    entityTypeDecors() {
        return this.getRuleContext(0, EntityTypeDecorsContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_entityType; }
    // @Override
    enterRule(listener) {
        if (listener.enterEntityType) {
            listener.enterEntityType(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEntityType) {
            listener.exitEntityType(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEntityType) {
            return visitor.visitEntityType(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EntityTypeContext = EntityTypeContext;
class ValueTypeContext extends ParserRuleContext_1.ParserRuleContext {
    beforeComment() {
        return this.getRuleContext(0, BeforeCommentContext);
    }
    name() {
        return this.getRuleContext(0, NameContext);
    }
    extendsList() {
        return this.getRuleContext(0, ExtendsListContext);
    }
    typeBlock() {
        return this.getRuleContext(0, TypeBlockContext);
    }
    valueTypeDecors() {
        return this.getRuleContext(0, ValueTypeDecorsContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_valueType; }
    // @Override
    enterRule(listener) {
        if (listener.enterValueType) {
            listener.enterValueType(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitValueType) {
            listener.exitValueType(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitValueType) {
            return visitor.visitValueType(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ValueTypeContext = ValueTypeContext;
class InterfaceTypeContext extends ParserRuleContext_1.ParserRuleContext {
    beforeComment() {
        return this.getRuleContext(0, BeforeCommentContext);
    }
    name() {
        return this.getRuleContext(0, NameContext);
    }
    extendsList() {
        return this.getRuleContext(0, ExtendsListContext);
    }
    possiblyEmptyTypeBlock() {
        return this.getRuleContext(0, PossiblyEmptyTypeBlockContext);
    }
    interfaceTypeDecors() {
        return this.getRuleContext(0, InterfaceTypeDecorsContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_interfaceType; }
    // @Override
    enterRule(listener) {
        if (listener.enterInterfaceType) {
            listener.enterInterfaceType(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitInterfaceType) {
            listener.exitInterfaceType(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitInterfaceType) {
            return visitor.visitInterfaceType(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.InterfaceTypeContext = InterfaceTypeContext;
class NameContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_name; }
    // @Override
    enterRule(listener) {
        if (listener.enterName) {
            listener.enterName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitName) {
            listener.exitName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitName) {
            return visitor.visitName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.NameContext = NameContext;
class ExtendsListContext extends ParserRuleContext_1.ParserRuleContext {
    baseName(i) {
        if (i === undefined) {
            return this.getRuleContexts(BaseNameContext);
        }
        else {
            return this.getRuleContext(i, BaseNameContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_extendsList; }
    // @Override
    enterRule(listener) {
        if (listener.enterExtendsList) {
            listener.enterExtendsList(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitExtendsList) {
            listener.exitExtendsList(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitExtendsList) {
            return visitor.visitExtendsList(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ExtendsListContext = ExtendsListContext;
class BaseNameContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_baseName; }
    // @Override
    enterRule(listener) {
        if (listener.enterBaseName) {
            listener.enterBaseName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitBaseName) {
            listener.exitBaseName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitBaseName) {
            return visitor.visitBaseName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.BaseNameContext = BaseNameContext;
class TypeBlockContext extends ParserRuleContext_1.ParserRuleContext {
    afterComment() {
        return this.getRuleContext(0, AfterCommentContext);
    }
    fieldDef(i) {
        if (i === undefined) {
            return this.getRuleContexts(FieldDefContext);
        }
        else {
            return this.getRuleContext(i, FieldDefContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_typeBlock; }
    // @Override
    enterRule(listener) {
        if (listener.enterTypeBlock) {
            listener.enterTypeBlock(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitTypeBlock) {
            listener.exitTypeBlock(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitTypeBlock) {
            return visitor.visitTypeBlock(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.TypeBlockContext = TypeBlockContext;
class PossiblyEmptyTypeBlockContext extends ParserRuleContext_1.ParserRuleContext {
    afterComment() {
        return this.getRuleContext(0, AfterCommentContext);
    }
    fieldDef(i) {
        if (i === undefined) {
            return this.getRuleContexts(FieldDefContext);
        }
        else {
            return this.getRuleContext(i, FieldDefContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_possiblyEmptyTypeBlock; }
    // @Override
    enterRule(listener) {
        if (listener.enterPossiblyEmptyTypeBlock) {
            listener.enterPossiblyEmptyTypeBlock(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitPossiblyEmptyTypeBlock) {
            listener.exitPossiblyEmptyTypeBlock(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitPossiblyEmptyTypeBlock) {
            return visitor.visitPossiblyEmptyTypeBlock(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.PossiblyEmptyTypeBlockContext = PossiblyEmptyTypeBlockContext;
class EntityTypeDecorsContext extends ParserRuleContext_1.ParserRuleContext {
    entityTypeDecor(i) {
        if (i === undefined) {
            return this.getRuleContexts(EntityTypeDecorContext);
        }
        else {
            return this.getRuleContext(i, EntityTypeDecorContext);
        }
    }
    esslComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslCommentContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_entityTypeDecors; }
    // @Override
    enterRule(listener) {
        if (listener.enterEntityTypeDecors) {
            listener.enterEntityTypeDecors(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEntityTypeDecors) {
            listener.exitEntityTypeDecors(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEntityTypeDecors) {
            return visitor.visitEntityTypeDecors(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EntityTypeDecorsContext = EntityTypeDecorsContext;
class EntityTypeDecorContext extends ParserRuleContext_1.ParserRuleContext {
    partial() {
        return this.tryGetRuleContext(0, PartialContext);
    }
    partialCommand() {
        return this.tryGetRuleContext(0, PartialCommandContext);
    }
    transCoordinator() {
        return this.tryGetRuleContext(0, TransCoordinatorContext);
    }
    defaultConstructor() {
        return this.tryGetRuleContext(0, DefaultConstructorContext);
    }
    indexTypeDecor() {
        return this.tryGetRuleContext(0, IndexTypeDecorContext);
    }
    uniqueIndexTypeDecor() {
        return this.tryGetRuleContext(0, UniqueIndexTypeDecorContext);
    }
    queryPartitionDecor() {
        return this.tryGetRuleContext(0, QueryPartitionDecorContext);
    }
    crossPartitionDecor() {
        return this.tryGetRuleContext(0, CrossPartitionDecorContext);
    }
    umlGroupDecor() {
        return this.tryGetRuleContext(0, UmlGroupDecorContext);
    }
    webValueString() {
        return this.tryGetRuleContext(0, WebValueStringContext);
    }
    hashLookupDecor() {
        return this.tryGetRuleContext(0, HashLookupDecorContext);
    }
    beforeEventPersist() {
        return this.tryGetRuleContext(0, BeforeEventPersistContext);
    }
    authPolicyDecor() {
        return this.tryGetRuleContext(0, AuthPolicyDecorContext);
    }
    styleDecor() {
        return this.tryGetRuleContext(0, StyleDecorContext);
    }
    discriminatedBy() {
        return this.tryGetRuleContext(0, DiscriminatedByContext);
    }
    pluralNameDecor() {
        return this.tryGetRuleContext(0, PluralNameDecorContext);
    }
    tableNameDecor() {
        return this.tryGetRuleContext(0, TableNameDecorContext);
    }
    graphQLNameDecor() {
        return this.tryGetRuleContext(0, GraphQLNameDecorContext);
    }
    graphQLPluralNameDecor() {
        return this.tryGetRuleContext(0, GraphQLPluralNameDecorContext);
    }
    graphQLCamelCaseDecor() {
        return this.tryGetRuleContext(0, GraphQLCamelCaseDecorContext);
    }
    graphQLPluralCamelCaseDecor() {
        return this.tryGetRuleContext(0, GraphQLPluralCamelCaseDecorContext);
    }
    base64RefIdsDecor() {
        return this.tryGetRuleContext(0, Base64RefIdsDecorContext);
    }
    interfaceDecor() {
        return this.tryGetRuleContext(0, InterfaceDecorContext);
    }
    camelCaseDecor() {
        return this.tryGetRuleContext(0, CamelCaseDecorContext);
    }
    generateIfDecor() {
        return this.tryGetRuleContext(0, GenerateIfDecorContext);
    }
    attributeDecor() {
        return this.tryGetRuleContext(0, AttributeDecorContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_entityTypeDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterEntityTypeDecor) {
            listener.enterEntityTypeDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEntityTypeDecor) {
            listener.exitEntityTypeDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEntityTypeDecor) {
            return visitor.visitEntityTypeDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EntityTypeDecorContext = EntityTypeDecorContext;
class PartialCommandContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_partialCommand; }
    // @Override
    enterRule(listener) {
        if (listener.enterPartialCommand) {
            listener.enterPartialCommand(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitPartialCommand) {
            listener.exitPartialCommand(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitPartialCommand) {
            return visitor.visitPartialCommand(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.PartialCommandContext = PartialCommandContext;
class TransCoordinatorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_transCoordinator; }
    // @Override
    enterRule(listener) {
        if (listener.enterTransCoordinator) {
            listener.enterTransCoordinator(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitTransCoordinator) {
            listener.exitTransCoordinator(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitTransCoordinator) {
            return visitor.visitTransCoordinator(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.TransCoordinatorContext = TransCoordinatorContext;
class DefaultConstructorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_defaultConstructor; }
    // @Override
    enterRule(listener) {
        if (listener.enterDefaultConstructor) {
            listener.enterDefaultConstructor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDefaultConstructor) {
            listener.exitDefaultConstructor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDefaultConstructor) {
            return visitor.visitDefaultConstructor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DefaultConstructorContext = DefaultConstructorContext;
class IndexTypeDecorContext extends ParserRuleContext_1.ParserRuleContext {
    indexTypeAttrs() {
        return this.getRuleContext(0, IndexTypeAttrsContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_indexTypeDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterIndexTypeDecor) {
            listener.enterIndexTypeDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitIndexTypeDecor) {
            listener.exitIndexTypeDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitIndexTypeDecor) {
            return visitor.visitIndexTypeDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.IndexTypeDecorContext = IndexTypeDecorContext;
class UniqueIndexTypeDecorContext extends ParserRuleContext_1.ParserRuleContext {
    indexTypeAttrs() {
        return this.getRuleContext(0, IndexTypeAttrsContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_uniqueIndexTypeDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterUniqueIndexTypeDecor) {
            listener.enterUniqueIndexTypeDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitUniqueIndexTypeDecor) {
            listener.exitUniqueIndexTypeDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitUniqueIndexTypeDecor) {
            return visitor.visitUniqueIndexTypeDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.UniqueIndexTypeDecorContext = UniqueIndexTypeDecorContext;
class IndexTypeAttrsContext extends ParserRuleContext_1.ParserRuleContext {
    name() {
        return this.getRuleContext(0, NameContext);
    }
    indexedField(i) {
        if (i === undefined) {
            return this.getRuleContexts(IndexedFieldContext);
        }
        else {
            return this.getRuleContext(i, IndexedFieldContext);
        }
    }
    indexAttrDecors() {
        return this.tryGetRuleContext(0, IndexAttrDecorsContext);
    }
    indexKeyAttrs(i) {
        if (i === undefined) {
            return this.getRuleContexts(IndexKeyAttrsContext);
        }
        else {
            return this.getRuleContext(i, IndexKeyAttrsContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_indexTypeAttrs; }
    // @Override
    enterRule(listener) {
        if (listener.enterIndexTypeAttrs) {
            listener.enterIndexTypeAttrs(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitIndexTypeAttrs) {
            listener.exitIndexTypeAttrs(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitIndexTypeAttrs) {
            return visitor.visitIndexTypeAttrs(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.IndexTypeAttrsContext = IndexTypeAttrsContext;
class IndexKeyAttrsContext extends ParserRuleContext_1.ParserRuleContext {
    indexKeyAttr(i) {
        if (i === undefined) {
            return this.getRuleContexts(IndexKeyAttrContext);
        }
        else {
            return this.getRuleContext(i, IndexKeyAttrContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_indexKeyAttrs; }
    // @Override
    enterRule(listener) {
        if (listener.enterIndexKeyAttrs) {
            listener.enterIndexKeyAttrs(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitIndexKeyAttrs) {
            listener.exitIndexKeyAttrs(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitIndexKeyAttrs) {
            return visitor.visitIndexKeyAttrs(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.IndexKeyAttrsContext = IndexKeyAttrsContext;
class IndexKeyAttrContext extends ParserRuleContext_1.ParserRuleContext {
    caseInsensitive() {
        return this.tryGetRuleContext(0, CaseInsensitiveContext);
    }
    nullHandling() {
        return this.tryGetRuleContext(0, NullHandlingContext);
    }
    dimensionNameDecor() {
        return this.tryGetRuleContext(0, DimensionNameDecorContext);
    }
    dimensionKeyDecor() {
        return this.tryGetRuleContext(0, DimensionKeyDecorContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_indexKeyAttr; }
    // @Override
    enterRule(listener) {
        if (listener.enterIndexKeyAttr) {
            listener.enterIndexKeyAttr(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitIndexKeyAttr) {
            listener.exitIndexKeyAttr(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitIndexKeyAttr) {
            return visitor.visitIndexKeyAttr(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.IndexKeyAttrContext = IndexKeyAttrContext;
class IndexedFieldContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_indexedField; }
    // @Override
    enterRule(listener) {
        if (listener.enterIndexedField) {
            listener.enterIndexedField(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitIndexedField) {
            listener.exitIndexedField(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitIndexedField) {
            return visitor.visitIndexedField(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.IndexedFieldContext = IndexedFieldContext;
class QueryPartitionDecorContext extends ParserRuleContext_1.ParserRuleContext {
    queryPartitionAttrs() {
        return this.getRuleContext(0, QueryPartitionAttrsContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_queryPartitionDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterQueryPartitionDecor) {
            listener.enterQueryPartitionDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitQueryPartitionDecor) {
            listener.exitQueryPartitionDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitQueryPartitionDecor) {
            return visitor.visitQueryPartitionDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.QueryPartitionDecorContext = QueryPartitionDecorContext;
class QueryPartitionAttrsContext extends ParserRuleContext_1.ParserRuleContext {
    partitionIndex() {
        return this.getRuleContext(0, PartitionIndexContext);
    }
    partitionTypeUniqueIndex() {
        return this.getRuleContext(0, PartitionTypeUniqueIndexContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_queryPartitionAttrs; }
    // @Override
    enterRule(listener) {
        if (listener.enterQueryPartitionAttrs) {
            listener.enterQueryPartitionAttrs(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitQueryPartitionAttrs) {
            listener.exitQueryPartitionAttrs(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitQueryPartitionAttrs) {
            return visitor.visitQueryPartitionAttrs(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.QueryPartitionAttrsContext = QueryPartitionAttrsContext;
class PartitionIndexContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_partitionIndex; }
    // @Override
    enterRule(listener) {
        if (listener.enterPartitionIndex) {
            listener.enterPartitionIndex(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitPartitionIndex) {
            listener.exitPartitionIndex(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitPartitionIndex) {
            return visitor.visitPartitionIndex(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.PartitionIndexContext = PartitionIndexContext;
class PartitionTypeUniqueIndexContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_partitionTypeUniqueIndex; }
    // @Override
    enterRule(listener) {
        if (listener.enterPartitionTypeUniqueIndex) {
            listener.enterPartitionTypeUniqueIndex(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitPartitionTypeUniqueIndex) {
            listener.exitPartitionTypeUniqueIndex(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitPartitionTypeUniqueIndex) {
            return visitor.visitPartitionTypeUniqueIndex(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.PartitionTypeUniqueIndexContext = PartitionTypeUniqueIndexContext;
class CrossPartitionDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_crossPartitionDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterCrossPartitionDecor) {
            listener.enterCrossPartitionDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitCrossPartitionDecor) {
            listener.exitCrossPartitionDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitCrossPartitionDecor) {
            return visitor.visitCrossPartitionDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.CrossPartitionDecorContext = CrossPartitionDecorContext;
class UmlGroupDecorContext extends ParserRuleContext_1.ParserRuleContext {
    umlGroup() {
        return this.getRuleContext(0, UmlGroupContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_umlGroupDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterUmlGroupDecor) {
            listener.enterUmlGroupDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitUmlGroupDecor) {
            listener.exitUmlGroupDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitUmlGroupDecor) {
            return visitor.visitUmlGroupDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.UmlGroupDecorContext = UmlGroupDecorContext;
class UmlGroupContext extends ParserRuleContext_1.ParserRuleContext {
    umlGroupTitle() {
        return this.getRuleContext(0, UmlGroupTitleContext);
    }
    umlSubGroup() {
        return this.tryGetRuleContext(0, UmlSubGroupContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_umlGroup; }
    // @Override
    enterRule(listener) {
        if (listener.enterUmlGroup) {
            listener.enterUmlGroup(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitUmlGroup) {
            listener.exitUmlGroup(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitUmlGroup) {
            return visitor.visitUmlGroup(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.UmlGroupContext = UmlGroupContext;
class UmlGroupTitleContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_umlGroupTitle; }
    // @Override
    enterRule(listener) {
        if (listener.enterUmlGroupTitle) {
            listener.enterUmlGroupTitle(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitUmlGroupTitle) {
            listener.exitUmlGroupTitle(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitUmlGroupTitle) {
            return visitor.visitUmlGroupTitle(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.UmlGroupTitleContext = UmlGroupTitleContext;
class UmlSubGroupContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_umlSubGroup; }
    // @Override
    enterRule(listener) {
        if (listener.enterUmlSubGroup) {
            listener.enterUmlSubGroup(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitUmlSubGroup) {
            listener.exitUmlSubGroup(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitUmlSubGroup) {
            return visitor.visitUmlSubGroup(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.UmlSubGroupContext = UmlSubGroupContext;
class HashLookupDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_hashLookupDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterHashLookupDecor) {
            listener.enterHashLookupDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitHashLookupDecor) {
            listener.exitHashLookupDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitHashLookupDecor) {
            return visitor.visitHashLookupDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.HashLookupDecorContext = HashLookupDecorContext;
class BeforeEventPersistContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_beforeEventPersist; }
    // @Override
    enterRule(listener) {
        if (listener.enterBeforeEventPersist) {
            listener.enterBeforeEventPersist(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitBeforeEventPersist) {
            listener.exitBeforeEventPersist(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitBeforeEventPersist) {
            return visitor.visitBeforeEventPersist(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.BeforeEventPersistContext = BeforeEventPersistContext;
class AuthPolicyNameContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_authPolicyName; }
    // @Override
    enterRule(listener) {
        if (listener.enterAuthPolicyName) {
            listener.enterAuthPolicyName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAuthPolicyName) {
            listener.exitAuthPolicyName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAuthPolicyName) {
            return visitor.visitAuthPolicyName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AuthPolicyNameContext = AuthPolicyNameContext;
class AuthPolicyDecorContext extends ParserRuleContext_1.ParserRuleContext {
    authPolicyNames() {
        return this.getRuleContext(0, AuthPolicyNamesContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_authPolicyDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterAuthPolicyDecor) {
            listener.enterAuthPolicyDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAuthPolicyDecor) {
            listener.exitAuthPolicyDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAuthPolicyDecor) {
            return visitor.visitAuthPolicyDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AuthPolicyDecorContext = AuthPolicyDecorContext;
class PublicDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_publicDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterPublicDecor) {
            listener.enterPublicDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitPublicDecor) {
            listener.exitPublicDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitPublicDecor) {
            return visitor.visitPublicDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.PublicDecorContext = PublicDecorContext;
class PluralNameDecorContext extends ParserRuleContext_1.ParserRuleContext {
    pluralName() {
        return this.getRuleContext(0, PluralNameContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_pluralNameDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterPluralNameDecor) {
            listener.enterPluralNameDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitPluralNameDecor) {
            listener.exitPluralNameDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitPluralNameDecor) {
            return visitor.visitPluralNameDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.PluralNameDecorContext = PluralNameDecorContext;
class PluralNameContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_pluralName; }
    // @Override
    enterRule(listener) {
        if (listener.enterPluralName) {
            listener.enterPluralName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitPluralName) {
            listener.exitPluralName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitPluralName) {
            return visitor.visitPluralName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.PluralNameContext = PluralNameContext;
class TableNameDecorContext extends ParserRuleContext_1.ParserRuleContext {
    tableName() {
        return this.getRuleContext(0, TableNameContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_tableNameDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterTableNameDecor) {
            listener.enterTableNameDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitTableNameDecor) {
            listener.exitTableNameDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitTableNameDecor) {
            return visitor.visitTableNameDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.TableNameDecorContext = TableNameDecorContext;
class TableNameContext extends ParserRuleContext_1.ParserRuleContext {
    dottedId() {
        return this.getRuleContext(0, DottedIdContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_tableName; }
    // @Override
    enterRule(listener) {
        if (listener.enterTableName) {
            listener.enterTableName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitTableName) {
            listener.exitTableName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitTableName) {
            return visitor.visitTableName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.TableNameContext = TableNameContext;
class GraphQLNameDecorContext extends ParserRuleContext_1.ParserRuleContext {
    graphQLName() {
        return this.getRuleContext(0, GraphQLNameContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_graphQLNameDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterGraphQLNameDecor) {
            listener.enterGraphQLNameDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitGraphQLNameDecor) {
            listener.exitGraphQLNameDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitGraphQLNameDecor) {
            return visitor.visitGraphQLNameDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.GraphQLNameDecorContext = GraphQLNameDecorContext;
class GraphQLNameContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_graphQLName; }
    // @Override
    enterRule(listener) {
        if (listener.enterGraphQLName) {
            listener.enterGraphQLName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitGraphQLName) {
            listener.exitGraphQLName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitGraphQLName) {
            return visitor.visitGraphQLName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.GraphQLNameContext = GraphQLNameContext;
class GraphQLPluralNameDecorContext extends ParserRuleContext_1.ParserRuleContext {
    graphQLPluralName() {
        return this.getRuleContext(0, GraphQLPluralNameContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_graphQLPluralNameDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterGraphQLPluralNameDecor) {
            listener.enterGraphQLPluralNameDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitGraphQLPluralNameDecor) {
            listener.exitGraphQLPluralNameDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitGraphQLPluralNameDecor) {
            return visitor.visitGraphQLPluralNameDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.GraphQLPluralNameDecorContext = GraphQLPluralNameDecorContext;
class GraphQLPluralNameContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_graphQLPluralName; }
    // @Override
    enterRule(listener) {
        if (listener.enterGraphQLPluralName) {
            listener.enterGraphQLPluralName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitGraphQLPluralName) {
            listener.exitGraphQLPluralName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitGraphQLPluralName) {
            return visitor.visitGraphQLPluralName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.GraphQLPluralNameContext = GraphQLPluralNameContext;
class GraphQLCamelCaseDecorContext extends ParserRuleContext_1.ParserRuleContext {
    graphQLCamelCase() {
        return this.getRuleContext(0, GraphQLCamelCaseContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_graphQLCamelCaseDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterGraphQLCamelCaseDecor) {
            listener.enterGraphQLCamelCaseDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitGraphQLCamelCaseDecor) {
            listener.exitGraphQLCamelCaseDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitGraphQLCamelCaseDecor) {
            return visitor.visitGraphQLCamelCaseDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.GraphQLCamelCaseDecorContext = GraphQLCamelCaseDecorContext;
class GraphQLCamelCaseContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_graphQLCamelCase; }
    // @Override
    enterRule(listener) {
        if (listener.enterGraphQLCamelCase) {
            listener.enterGraphQLCamelCase(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitGraphQLCamelCase) {
            listener.exitGraphQLCamelCase(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitGraphQLCamelCase) {
            return visitor.visitGraphQLCamelCase(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.GraphQLCamelCaseContext = GraphQLCamelCaseContext;
class GraphQLPluralCamelCaseDecorContext extends ParserRuleContext_1.ParserRuleContext {
    graphQLPluralCamelCase() {
        return this.getRuleContext(0, GraphQLPluralCamelCaseContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_graphQLPluralCamelCaseDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterGraphQLPluralCamelCaseDecor) {
            listener.enterGraphQLPluralCamelCaseDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitGraphQLPluralCamelCaseDecor) {
            listener.exitGraphQLPluralCamelCaseDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitGraphQLPluralCamelCaseDecor) {
            return visitor.visitGraphQLPluralCamelCaseDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.GraphQLPluralCamelCaseDecorContext = GraphQLPluralCamelCaseDecorContext;
class GraphQLPluralCamelCaseContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_graphQLPluralCamelCase; }
    // @Override
    enterRule(listener) {
        if (listener.enterGraphQLPluralCamelCase) {
            listener.enterGraphQLPluralCamelCase(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitGraphQLPluralCamelCase) {
            listener.exitGraphQLPluralCamelCase(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitGraphQLPluralCamelCase) {
            return visitor.visitGraphQLPluralCamelCase(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.GraphQLPluralCamelCaseContext = GraphQLPluralCamelCaseContext;
class Base64RefIdsDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_base64RefIdsDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterBase64RefIdsDecor) {
            listener.enterBase64RefIdsDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitBase64RefIdsDecor) {
            listener.exitBase64RefIdsDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitBase64RefIdsDecor) {
            return visitor.visitBase64RefIdsDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.Base64RefIdsDecorContext = Base64RefIdsDecorContext;
class InterfaceDecorContext extends ParserRuleContext_1.ParserRuleContext {
    interfaceName() {
        return this.getRuleContext(0, InterfaceNameContext);
    }
    extendsList() {
        return this.getRuleContext(0, ExtendsListContext);
    }
    umlGroup() {
        return this.tryGetRuleContext(0, UmlGroupContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_interfaceDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterInterfaceDecor) {
            listener.enterInterfaceDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitInterfaceDecor) {
            listener.exitInterfaceDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitInterfaceDecor) {
            return visitor.visitInterfaceDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.InterfaceDecorContext = InterfaceDecorContext;
class InterfaceNameContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_interfaceName; }
    // @Override
    enterRule(listener) {
        if (listener.enterInterfaceName) {
            listener.enterInterfaceName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitInterfaceName) {
            listener.exitInterfaceName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitInterfaceName) {
            return visitor.visitInterfaceName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.InterfaceNameContext = InterfaceNameContext;
class CamelCaseDecorContext extends ParserRuleContext_1.ParserRuleContext {
    camelCaseName() {
        return this.getRuleContext(0, CamelCaseNameContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_camelCaseDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterCamelCaseDecor) {
            listener.enterCamelCaseDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitCamelCaseDecor) {
            listener.exitCamelCaseDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitCamelCaseDecor) {
            return visitor.visitCamelCaseDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.CamelCaseDecorContext = CamelCaseDecorContext;
class CamelCaseNameContext extends ParserRuleContext_1.ParserRuleContext {
    CAMELCASE_IDENTIFIER() { return this.getToken(ESSL_Parser.CAMELCASE_IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_camelCaseName; }
    // @Override
    enterRule(listener) {
        if (listener.enterCamelCaseName) {
            listener.enterCamelCaseName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitCamelCaseName) {
            listener.exitCamelCaseName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitCamelCaseName) {
            return visitor.visitCamelCaseName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.CamelCaseNameContext = CamelCaseNameContext;
class GenerateIfDecorContext extends ParserRuleContext_1.ParserRuleContext {
    generateIfName() {
        return this.getRuleContext(0, GenerateIfNameContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_generateIfDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterGenerateIfDecor) {
            listener.enterGenerateIfDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitGenerateIfDecor) {
            listener.exitGenerateIfDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitGenerateIfDecor) {
            return visitor.visitGenerateIfDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.GenerateIfDecorContext = GenerateIfDecorContext;
class GenerateIfNameContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.tryGetToken(ESSL_Parser.IDENTIFIER, 0); }
    CAMELCASE_IDENTIFIER() { return this.tryGetToken(ESSL_Parser.CAMELCASE_IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_generateIfName; }
    // @Override
    enterRule(listener) {
        if (listener.enterGenerateIfName) {
            listener.enterGenerateIfName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitGenerateIfName) {
            listener.exitGenerateIfName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitGenerateIfName) {
            return visitor.visitGenerateIfName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.GenerateIfNameContext = GenerateIfNameContext;
class DimensionNameDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_dimensionNameDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterDimensionNameDecor) {
            listener.enterDimensionNameDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDimensionNameDecor) {
            listener.exitDimensionNameDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDimensionNameDecor) {
            return visitor.visitDimensionNameDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DimensionNameDecorContext = DimensionNameDecorContext;
class StyleDecorContext extends ParserRuleContext_1.ParserRuleContext {
    events() {
        return this.tryGetRuleContext(0, EventsContext);
    }
    slowlyChanging() {
        return this.tryGetRuleContext(0, SlowlyChangingContext);
    }
    ledger() {
        return this.tryGetRuleContext(0, LedgerContext);
    }
    factLedger() {
        return this.tryGetRuleContext(0, FactLedgerContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_styleDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterStyleDecor) {
            listener.enterStyleDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitStyleDecor) {
            listener.exitStyleDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitStyleDecor) {
            return visitor.visitStyleDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.StyleDecorContext = StyleDecorContext;
class EventsContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_events; }
    // @Override
    enterRule(listener) {
        if (listener.enterEvents) {
            listener.enterEvents(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEvents) {
            listener.exitEvents(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEvents) {
            return visitor.visitEvents(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EventsContext = EventsContext;
class SlowlyChangingContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_slowlyChanging; }
    // @Override
    enterRule(listener) {
        if (listener.enterSlowlyChanging) {
            listener.enterSlowlyChanging(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitSlowlyChanging) {
            listener.exitSlowlyChanging(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitSlowlyChanging) {
            return visitor.visitSlowlyChanging(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.SlowlyChangingContext = SlowlyChangingContext;
class LedgerContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_ledger; }
    // @Override
    enterRule(listener) {
        if (listener.enterLedger) {
            listener.enterLedger(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitLedger) {
            listener.exitLedger(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitLedger) {
            return visitor.visitLedger(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.LedgerContext = LedgerContext;
class FactLedgerContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_factLedger; }
    // @Override
    enterRule(listener) {
        if (listener.enterFactLedger) {
            listener.enterFactLedger(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitFactLedger) {
            listener.exitFactLedger(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitFactLedger) {
            return visitor.visitFactLedger(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.FactLedgerContext = FactLedgerContext;
class IndexAttrDecorsContext extends ParserRuleContext_1.ParserRuleContext {
    indexAttrDecor(i) {
        if (i === undefined) {
            return this.getRuleContexts(IndexAttrDecorContext);
        }
        else {
            return this.getRuleContext(i, IndexAttrDecorContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_indexAttrDecors; }
    // @Override
    enterRule(listener) {
        if (listener.enterIndexAttrDecors) {
            listener.enterIndexAttrDecors(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitIndexAttrDecors) {
            listener.exitIndexAttrDecors(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitIndexAttrDecors) {
            return visitor.visitIndexAttrDecors(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.IndexAttrDecorsContext = IndexAttrDecorsContext;
class IndexAttrDecorContext extends ParserRuleContext_1.ParserRuleContext {
    cacheModel() {
        return this.tryGetRuleContext(0, CacheModelContext);
    }
    noCacheModel() {
        return this.tryGetRuleContext(0, NoCacheModelContext);
    }
    allowTemporal() {
        return this.tryGetRuleContext(0, AllowTemporalContext);
    }
    indexNameDecor() {
        return this.tryGetRuleContext(0, IndexNameDecorContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_indexAttrDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterIndexAttrDecor) {
            listener.enterIndexAttrDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitIndexAttrDecor) {
            listener.exitIndexAttrDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitIndexAttrDecor) {
            return visitor.visitIndexAttrDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.IndexAttrDecorContext = IndexAttrDecorContext;
class CacheModelContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_cacheModel; }
    // @Override
    enterRule(listener) {
        if (listener.enterCacheModel) {
            listener.enterCacheModel(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitCacheModel) {
            listener.exitCacheModel(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitCacheModel) {
            return visitor.visitCacheModel(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.CacheModelContext = CacheModelContext;
class NoCacheModelContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_noCacheModel; }
    // @Override
    enterRule(listener) {
        if (listener.enterNoCacheModel) {
            listener.enterNoCacheModel(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitNoCacheModel) {
            listener.exitNoCacheModel(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitNoCacheModel) {
            return visitor.visitNoCacheModel(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.NoCacheModelContext = NoCacheModelContext;
class AllowTemporalContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_allowTemporal; }
    // @Override
    enterRule(listener) {
        if (listener.enterAllowTemporal) {
            listener.enterAllowTemporal(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAllowTemporal) {
            listener.exitAllowTemporal(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAllowTemporal) {
            return visitor.visitAllowTemporal(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AllowTemporalContext = AllowTemporalContext;
class IndexNameDecorContext extends ParserRuleContext_1.ParserRuleContext {
    indexName() {
        return this.getRuleContext(0, IndexNameContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_indexNameDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterIndexNameDecor) {
            listener.enterIndexNameDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitIndexNameDecor) {
            listener.exitIndexNameDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitIndexNameDecor) {
            return visitor.visitIndexNameDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.IndexNameDecorContext = IndexNameDecorContext;
class IndexNameContext extends ParserRuleContext_1.ParserRuleContext {
    dottedId() {
        return this.getRuleContext(0, DottedIdContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_indexName; }
    // @Override
    enterRule(listener) {
        if (listener.enterIndexName) {
            listener.enterIndexName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitIndexName) {
            listener.exitIndexName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitIndexName) {
            return visitor.visitIndexName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.IndexNameContext = IndexNameContext;
class ValueTypeDecorsContext extends ParserRuleContext_1.ParserRuleContext {
    valueTypeDecor(i) {
        if (i === undefined) {
            return this.getRuleContexts(ValueTypeDecorContext);
        }
        else {
            return this.getRuleContext(i, ValueTypeDecorContext);
        }
    }
    esslComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslCommentContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_valueTypeDecors; }
    // @Override
    enterRule(listener) {
        if (listener.enterValueTypeDecors) {
            listener.enterValueTypeDecors(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitValueTypeDecors) {
            listener.exitValueTypeDecors(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitValueTypeDecors) {
            return visitor.visitValueTypeDecors(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ValueTypeDecorsContext = ValueTypeDecorsContext;
class ValueTypeDecorContext extends ParserRuleContext_1.ParserRuleContext {
    partial() {
        return this.tryGetRuleContext(0, PartialContext);
    }
    defaultConstructor() {
        return this.tryGetRuleContext(0, DefaultConstructorContext);
    }
    jsonConstructor() {
        return this.tryGetRuleContext(0, JsonConstructorContext);
    }
    webValueString() {
        return this.tryGetRuleContext(0, WebValueStringContext);
    }
    umlGroupDecor() {
        return this.tryGetRuleContext(0, UmlGroupDecorContext);
    }
    suppressDecor() {
        return this.tryGetRuleContext(0, SuppressDecorContext);
    }
    authPolicyDecor() {
        return this.tryGetRuleContext(0, AuthPolicyDecorContext);
    }
    discriminatedBy() {
        return this.tryGetRuleContext(0, DiscriminatedByContext);
    }
    base64RefIdsDecor() {
        return this.tryGetRuleContext(0, Base64RefIdsDecorContext);
    }
    outputType() {
        return this.tryGetRuleContext(0, OutputTypeContext);
    }
    interfaceDecor() {
        return this.tryGetRuleContext(0, InterfaceDecorContext);
    }
    camelCaseDecor() {
        return this.tryGetRuleContext(0, CamelCaseDecorContext);
    }
    generateIfDecor() {
        return this.tryGetRuleContext(0, GenerateIfDecorContext);
    }
    attributeDecor() {
        return this.tryGetRuleContext(0, AttributeDecorContext);
    }
    factDimensionDecor() {
        return this.tryGetRuleContext(0, FactDimensionDecorContext);
    }
    voidPrimaryFact() {
        return this.tryGetRuleContext(0, VoidPrimaryFactContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_valueTypeDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterValueTypeDecor) {
            listener.enterValueTypeDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitValueTypeDecor) {
            listener.exitValueTypeDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitValueTypeDecor) {
            return visitor.visitValueTypeDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ValueTypeDecorContext = ValueTypeDecorContext;
class PartialContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_partial; }
    // @Override
    enterRule(listener) {
        if (listener.enterPartial) {
            listener.enterPartial(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitPartial) {
            listener.exitPartial(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitPartial) {
            return visitor.visitPartial(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.PartialContext = PartialContext;
class JsonConstructorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_jsonConstructor; }
    // @Override
    enterRule(listener) {
        if (listener.enterJsonConstructor) {
            listener.enterJsonConstructor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitJsonConstructor) {
            listener.exitJsonConstructor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitJsonConstructor) {
            return visitor.visitJsonConstructor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.JsonConstructorContext = JsonConstructorContext;
class WebValueStringContext extends ParserRuleContext_1.ParserRuleContext {
    webValueMethod() {
        return this.getRuleContext(0, WebValueMethodContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_webValueString; }
    // @Override
    enterRule(listener) {
        if (listener.enterWebValueString) {
            listener.enterWebValueString(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitWebValueString) {
            listener.exitWebValueString(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitWebValueString) {
            return visitor.visitWebValueString(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.WebValueStringContext = WebValueStringContext;
class WebValueMethodContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_webValueMethod; }
    // @Override
    enterRule(listener) {
        if (listener.enterWebValueMethod) {
            listener.enterWebValueMethod(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitWebValueMethod) {
            listener.exitWebValueMethod(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitWebValueMethod) {
            return visitor.visitWebValueMethod(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.WebValueMethodContext = WebValueMethodContext;
class DiscriminatedByContext extends ParserRuleContext_1.ParserRuleContext {
    discriminatedByValue() {
        return this.getRuleContext(0, DiscriminatedByValueContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_discriminatedBy; }
    // @Override
    enterRule(listener) {
        if (listener.enterDiscriminatedBy) {
            listener.enterDiscriminatedBy(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDiscriminatedBy) {
            listener.exitDiscriminatedBy(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDiscriminatedBy) {
            return visitor.visitDiscriminatedBy(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DiscriminatedByContext = DiscriminatedByContext;
class DiscriminatedByValueContext extends ParserRuleContext_1.ParserRuleContext {
    dottedId() {
        return this.getRuleContext(0, DottedIdContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_discriminatedByValue; }
    // @Override
    enterRule(listener) {
        if (listener.enterDiscriminatedByValue) {
            listener.enterDiscriminatedByValue(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDiscriminatedByValue) {
            listener.exitDiscriminatedByValue(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDiscriminatedByValue) {
            return visitor.visitDiscriminatedByValue(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DiscriminatedByValueContext = DiscriminatedByValueContext;
class OutputTypeContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_outputType; }
    // @Override
    enterRule(listener) {
        if (listener.enterOutputType) {
            listener.enterOutputType(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitOutputType) {
            listener.exitOutputType(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitOutputType) {
            return visitor.visitOutputType(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.OutputTypeContext = OutputTypeContext;
class VoidPrimaryFactContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_voidPrimaryFact; }
    // @Override
    enterRule(listener) {
        if (listener.enterVoidPrimaryFact) {
            listener.enterVoidPrimaryFact(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitVoidPrimaryFact) {
            listener.exitVoidPrimaryFact(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitVoidPrimaryFact) {
            return visitor.visitVoidPrimaryFact(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.VoidPrimaryFactContext = VoidPrimaryFactContext;
class InterfaceTypeDecorsContext extends ParserRuleContext_1.ParserRuleContext {
    interfaceTypeDecor(i) {
        if (i === undefined) {
            return this.getRuleContexts(InterfaceTypeDecorContext);
        }
        else {
            return this.getRuleContext(i, InterfaceTypeDecorContext);
        }
    }
    esslComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslCommentContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_interfaceTypeDecors; }
    // @Override
    enterRule(listener) {
        if (listener.enterInterfaceTypeDecors) {
            listener.enterInterfaceTypeDecors(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitInterfaceTypeDecors) {
            listener.exitInterfaceTypeDecors(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitInterfaceTypeDecors) {
            return visitor.visitInterfaceTypeDecors(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.InterfaceTypeDecorsContext = InterfaceTypeDecorsContext;
class InterfaceTypeDecorContext extends ParserRuleContext_1.ParserRuleContext {
    partial() {
        return this.tryGetRuleContext(0, PartialContext);
    }
    internalDecor() {
        return this.tryGetRuleContext(0, InternalDecorContext);
    }
    umlGroupDecor() {
        return this.tryGetRuleContext(0, UmlGroupDecorContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_interfaceTypeDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterInterfaceTypeDecor) {
            listener.enterInterfaceTypeDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitInterfaceTypeDecor) {
            listener.exitInterfaceTypeDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitInterfaceTypeDecor) {
            return visitor.visitInterfaceTypeDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.InterfaceTypeDecorContext = InterfaceTypeDecorContext;
class EnumDeclContext extends ParserRuleContext_1.ParserRuleContext {
    beforeComment() {
        return this.getRuleContext(0, BeforeCommentContext);
    }
    name() {
        return this.getRuleContext(0, NameContext);
    }
    enumBlock() {
        return this.getRuleContext(0, EnumBlockContext);
    }
    enumDecors() {
        return this.getRuleContext(0, EnumDecorsContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_enumDecl; }
    // @Override
    enterRule(listener) {
        if (listener.enterEnumDecl) {
            listener.enterEnumDecl(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEnumDecl) {
            listener.exitEnumDecl(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEnumDecl) {
            return visitor.visitEnumDecl(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EnumDeclContext = EnumDeclContext;
class EnumBlockContext extends ParserRuleContext_1.ParserRuleContext {
    enumItem(i) {
        if (i === undefined) {
            return this.getRuleContexts(EnumItemContext);
        }
        else {
            return this.getRuleContext(i, EnumItemContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_enumBlock; }
    // @Override
    enterRule(listener) {
        if (listener.enterEnumBlock) {
            listener.enterEnumBlock(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEnumBlock) {
            listener.exitEnumBlock(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEnumBlock) {
            return visitor.visitEnumBlock(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EnumBlockContext = EnumBlockContext;
class EnumItemContext extends ParserRuleContext_1.ParserRuleContext {
    beforeComment() {
        return this.getRuleContext(0, BeforeCommentContext);
    }
    enumValue() {
        return this.getRuleContext(0, EnumValueContext);
    }
    comment() {
        return this.getRuleContext(0, CommentContext);
    }
    enumExplicitValue() {
        return this.tryGetRuleContext(0, EnumExplicitValueContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_enumItem; }
    // @Override
    enterRule(listener) {
        if (listener.enterEnumItem) {
            listener.enterEnumItem(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEnumItem) {
            listener.exitEnumItem(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEnumItem) {
            return visitor.visitEnumItem(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EnumItemContext = EnumItemContext;
class EnumValueContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_enumValue; }
    // @Override
    enterRule(listener) {
        if (listener.enterEnumValue) {
            listener.enterEnumValue(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEnumValue) {
            listener.exitEnumValue(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEnumValue) {
            return visitor.visitEnumValue(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EnumValueContext = EnumValueContext;
class EnumExplicitValueContext extends ParserRuleContext_1.ParserRuleContext {
    integer() {
        return this.getRuleContext(0, IntegerContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_enumExplicitValue; }
    // @Override
    enterRule(listener) {
        if (listener.enterEnumExplicitValue) {
            listener.enterEnumExplicitValue(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEnumExplicitValue) {
            listener.exitEnumExplicitValue(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEnumExplicitValue) {
            return visitor.visitEnumExplicitValue(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EnumExplicitValueContext = EnumExplicitValueContext;
class EnumDecorsContext extends ParserRuleContext_1.ParserRuleContext {
    enumDecor(i) {
        if (i === undefined) {
            return this.getRuleContexts(EnumDecorContext);
        }
        else {
            return this.getRuleContext(i, EnumDecorContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_enumDecors; }
    // @Override
    enterRule(listener) {
        if (listener.enterEnumDecors) {
            listener.enterEnumDecors(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEnumDecors) {
            listener.exitEnumDecors(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEnumDecors) {
            return visitor.visitEnumDecors(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EnumDecorsContext = EnumDecorsContext;
class EnumDecorContext extends ParserRuleContext_1.ParserRuleContext {
    umlGroupDecor() {
        return this.tryGetRuleContext(0, UmlGroupDecorContext);
    }
    suppressDecor() {
        return this.tryGetRuleContext(0, SuppressDecorContext);
    }
    flagsDecor() {
        return this.tryGetRuleContext(0, FlagsDecorContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_enumDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterEnumDecor) {
            listener.enterEnumDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEnumDecor) {
            listener.exitEnumDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEnumDecor) {
            return visitor.visitEnumDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EnumDecorContext = EnumDecorContext;
class SuppressDecorContext extends ParserRuleContext_1.ParserRuleContext {
    suppressOption(i) {
        if (i === undefined) {
            return this.getRuleContexts(SuppressOptionContext);
        }
        else {
            return this.getRuleContext(i, SuppressOptionContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_suppressDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterSuppressDecor) {
            listener.enterSuppressDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitSuppressDecor) {
            listener.exitSuppressDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitSuppressDecor) {
            return visitor.visitSuppressDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.SuppressDecorContext = SuppressDecorContext;
class SuppressOptionContext extends ParserRuleContext_1.ParserRuleContext {
    modelsuppressOption() {
        return this.tryGetRuleContext(0, ModelsuppressOptionContext);
    }
    graphQlsuppressOption() {
        return this.tryGetRuleContext(0, GraphQlsuppressOptionContext);
    }
    inputTypeOption() {
        return this.tryGetRuleContext(0, InputTypeOptionContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_suppressOption; }
    // @Override
    enterRule(listener) {
        if (listener.enterSuppressOption) {
            listener.enterSuppressOption(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitSuppressOption) {
            listener.exitSuppressOption(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitSuppressOption) {
            return visitor.visitSuppressOption(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.SuppressOptionContext = SuppressOptionContext;
class ModelsuppressOptionContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_modelsuppressOption; }
    // @Override
    enterRule(listener) {
        if (listener.enterModelsuppressOption) {
            listener.enterModelsuppressOption(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitModelsuppressOption) {
            listener.exitModelsuppressOption(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitModelsuppressOption) {
            return visitor.visitModelsuppressOption(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ModelsuppressOptionContext = ModelsuppressOptionContext;
class GraphQlsuppressOptionContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_graphQlsuppressOption; }
    // @Override
    enterRule(listener) {
        if (listener.enterGraphQlsuppressOption) {
            listener.enterGraphQlsuppressOption(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitGraphQlsuppressOption) {
            listener.exitGraphQlsuppressOption(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitGraphQlsuppressOption) {
            return visitor.visitGraphQlsuppressOption(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.GraphQlsuppressOptionContext = GraphQlsuppressOptionContext;
class InputTypeOptionContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_inputTypeOption; }
    // @Override
    enterRule(listener) {
        if (listener.enterInputTypeOption) {
            listener.enterInputTypeOption(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitInputTypeOption) {
            listener.exitInputTypeOption(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitInputTypeOption) {
            return visitor.visitInputTypeOption(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.InputTypeOptionContext = InputTypeOptionContext;
class FlagsDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_flagsDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterFlagsDecor) {
            listener.enterFlagsDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitFlagsDecor) {
            listener.exitFlagsDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitFlagsDecor) {
            return visitor.visitFlagsDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.FlagsDecorContext = FlagsDecorContext;
class DictionaryContext extends ParserRuleContext_1.ParserRuleContext {
    beforeComment() {
        return this.getRuleContext(0, BeforeCommentContext);
    }
    name() {
        return this.getRuleContext(0, NameContext);
    }
    dictionaryBlock() {
        return this.getRuleContext(0, DictionaryBlockContext);
    }
    dictionaryDecors() {
        return this.getRuleContext(0, DictionaryDecorsContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_dictionary; }
    // @Override
    enterRule(listener) {
        if (listener.enterDictionary) {
            listener.enterDictionary(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDictionary) {
            listener.exitDictionary(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDictionary) {
            return visitor.visitDictionary(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DictionaryContext = DictionaryContext;
class DictionaryBlockContext extends ParserRuleContext_1.ParserRuleContext {
    caseInsensitive(i) {
        if (i === undefined) {
            return this.getRuleContexts(CaseInsensitiveContext);
        }
        else {
            return this.getRuleContext(i, CaseInsensitiveContext);
        }
    }
    dictPartitioned(i) {
        if (i === undefined) {
            return this.getRuleContexts(DictPartitionedContext);
        }
        else {
            return this.getRuleContext(i, DictPartitionedContext);
        }
    }
    dictValueTypeDecl(i) {
        if (i === undefined) {
            return this.getRuleContexts(DictValueTypeDeclContext);
        }
        else {
            return this.getRuleContext(i, DictValueTypeDeclContext);
        }
    }
    dictDefaultDecl(i) {
        if (i === undefined) {
            return this.getRuleContexts(DictDefaultDeclContext);
        }
        else {
            return this.getRuleContext(i, DictDefaultDeclContext);
        }
    }
    dictEntries() {
        return this.tryGetRuleContext(0, DictEntriesContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_dictionaryBlock; }
    // @Override
    enterRule(listener) {
        if (listener.enterDictionaryBlock) {
            listener.enterDictionaryBlock(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDictionaryBlock) {
            listener.exitDictionaryBlock(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDictionaryBlock) {
            return visitor.visitDictionaryBlock(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DictionaryBlockContext = DictionaryBlockContext;
class DictPartitionedContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_dictPartitioned; }
    // @Override
    enterRule(listener) {
        if (listener.enterDictPartitioned) {
            listener.enterDictPartitioned(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDictPartitioned) {
            listener.exitDictPartitioned(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDictPartitioned) {
            return visitor.visitDictPartitioned(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DictPartitionedContext = DictPartitionedContext;
class DictValueTypeDeclContext extends ParserRuleContext_1.ParserRuleContext {
    dictValueType() {
        return this.getRuleContext(0, DictValueTypeContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_dictValueTypeDecl; }
    // @Override
    enterRule(listener) {
        if (listener.enterDictValueTypeDecl) {
            listener.enterDictValueTypeDecl(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDictValueTypeDecl) {
            listener.exitDictValueTypeDecl(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDictValueTypeDecl) {
            return visitor.visitDictValueTypeDecl(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DictValueTypeDeclContext = DictValueTypeDeclContext;
class DictValueTypeContext extends ParserRuleContext_1.ParserRuleContext {
    typeRef() {
        return this.getRuleContext(0, TypeRefContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_dictValueType; }
    // @Override
    enterRule(listener) {
        if (listener.enterDictValueType) {
            listener.enterDictValueType(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDictValueType) {
            listener.exitDictValueType(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDictValueType) {
            return visitor.visitDictValueType(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DictValueTypeContext = DictValueTypeContext;
class DictDefaultDeclContext extends ParserRuleContext_1.ParserRuleContext {
    dictDefault() {
        return this.getRuleContext(0, DictDefaultContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_dictDefaultDecl; }
    // @Override
    enterRule(listener) {
        if (listener.enterDictDefaultDecl) {
            listener.enterDictDefaultDecl(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDictDefaultDecl) {
            listener.exitDictDefaultDecl(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDictDefaultDecl) {
            return visitor.visitDictDefaultDecl(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DictDefaultDeclContext = DictDefaultDeclContext;
class DictDefaultContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.tryGetToken(ESSL_Parser.IDENTIFIER, 0); }
    CAMELCASE_IDENTIFIER() { return this.tryGetToken(ESSL_Parser.CAMELCASE_IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_dictDefault; }
    // @Override
    enterRule(listener) {
        if (listener.enterDictDefault) {
            listener.enterDictDefault(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDictDefault) {
            listener.exitDictDefault(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDictDefault) {
            return visitor.visitDictDefault(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DictDefaultContext = DictDefaultContext;
class DictEntriesContext extends ParserRuleContext_1.ParserRuleContext {
    DICTENTRIES() { return this.getToken(ESSL_Parser.DICTENTRIES, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_dictEntries; }
    // @Override
    enterRule(listener) {
        if (listener.enterDictEntries) {
            listener.enterDictEntries(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDictEntries) {
            listener.exitDictEntries(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDictEntries) {
            return visitor.visitDictEntries(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DictEntriesContext = DictEntriesContext;
class DictionaryDecorsContext extends ParserRuleContext_1.ParserRuleContext {
    dictionaryDecor(i) {
        if (i === undefined) {
            return this.getRuleContexts(DictionaryDecorContext);
        }
        else {
            return this.getRuleContext(i, DictionaryDecorContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_dictionaryDecors; }
    // @Override
    enterRule(listener) {
        if (listener.enterDictionaryDecors) {
            listener.enterDictionaryDecors(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDictionaryDecors) {
            listener.exitDictionaryDecors(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDictionaryDecors) {
            return visitor.visitDictionaryDecors(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DictionaryDecorsContext = DictionaryDecorsContext;
class DictionaryDecorContext extends ParserRuleContext_1.ParserRuleContext {
    umlGroupDecor() {
        return this.tryGetRuleContext(0, UmlGroupDecorContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_dictionaryDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterDictionaryDecor) {
            listener.enterDictionaryDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDictionaryDecor) {
            listener.exitDictionaryDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDictionaryDecor) {
            return visitor.visitDictionaryDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DictionaryDecorContext = DictionaryDecorContext;
class FieldDefContext extends ParserRuleContext_1.ParserRuleContext {
    beforeComment() {
        return this.getRuleContext(0, BeforeCommentContext);
    }
    typeRef() {
        return this.getRuleContext(0, TypeRefContext);
    }
    name() {
        return this.getRuleContext(0, NameContext);
    }
    fieldInitializer() {
        return this.getRuleContext(0, FieldInitializerContext);
    }
    fieldDecorators() {
        return this.getRuleContext(0, FieldDecoratorsContext);
    }
    comment() {
        return this.getRuleContext(0, CommentContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_fieldDef; }
    // @Override
    enterRule(listener) {
        if (listener.enterFieldDef) {
            listener.enterFieldDef(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitFieldDef) {
            listener.exitFieldDef(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitFieldDef) {
            return visitor.visitFieldDef(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.FieldDefContext = FieldDefContext;
class TypeRefContext extends ParserRuleContext_1.ParserRuleContext {
    typeRefName() {
        return this.getRuleContext(0, TypeRefNameContext);
    }
    listIndicator() {
        return this.tryGetRuleContext(0, ListIndicatorContext);
    }
    nullableIndicator() {
        return this.tryGetRuleContext(0, NullableIndicatorContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_typeRef; }
    // @Override
    enterRule(listener) {
        if (listener.enterTypeRef) {
            listener.enterTypeRef(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitTypeRef) {
            listener.exitTypeRef(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitTypeRef) {
            return visitor.visitTypeRef(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.TypeRefContext = TypeRefContext;
class TypeRefNameContext extends ParserRuleContext_1.ParserRuleContext {
    primitiveType() {
        return this.tryGetRuleContext(0, PrimitiveTypeContext);
    }
    valueTypeRef() {
        return this.tryGetRuleContext(0, ValueTypeRefContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_typeRefName; }
    // @Override
    enterRule(listener) {
        if (listener.enterTypeRefName) {
            listener.enterTypeRefName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitTypeRefName) {
            listener.exitTypeRefName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitTypeRefName) {
            return visitor.visitTypeRefName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.TypeRefNameContext = TypeRefNameContext;
class PrimitiveTypeContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_primitiveType; }
    // @Override
    enterRule(listener) {
        if (listener.enterPrimitiveType) {
            listener.enterPrimitiveType(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitPrimitiveType) {
            listener.exitPrimitiveType(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitPrimitiveType) {
            return visitor.visitPrimitiveType(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.PrimitiveTypeContext = PrimitiveTypeContext;
class ValueTypeRefContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_valueTypeRef; }
    // @Override
    enterRule(listener) {
        if (listener.enterValueTypeRef) {
            listener.enterValueTypeRef(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitValueTypeRef) {
            listener.exitValueTypeRef(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitValueTypeRef) {
            return visitor.visitValueTypeRef(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ValueTypeRefContext = ValueTypeRefContext;
class ListIndicatorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_listIndicator; }
    // @Override
    enterRule(listener) {
        if (listener.enterListIndicator) {
            listener.enterListIndicator(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitListIndicator) {
            listener.exitListIndicator(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitListIndicator) {
            return visitor.visitListIndicator(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ListIndicatorContext = ListIndicatorContext;
class NullableIndicatorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_nullableIndicator; }
    // @Override
    enterRule(listener) {
        if (listener.enterNullableIndicator) {
            listener.enterNullableIndicator(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitNullableIndicator) {
            listener.exitNullableIndicator(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitNullableIndicator) {
            return visitor.visitNullableIndicator(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.NullableIndicatorContext = NullableIndicatorContext;
class FieldInitializerContext extends ParserRuleContext_1.ParserRuleContext {
    nullValue() {
        return this.tryGetRuleContext(0, NullValueContext);
    }
    initialEnumVal() {
        return this.tryGetRuleContext(0, InitialEnumValContext);
    }
    boolValue() {
        return this.tryGetRuleContext(0, BoolValueContext);
    }
    stringValue() {
        return this.tryGetRuleContext(0, StringValueContext);
    }
    numValue() {
        return this.tryGetRuleContext(0, NumValueContext);
    }
    newInstance() {
        return this.tryGetRuleContext(0, NewInstanceContext);
    }
    flagsEnumSet() {
        return this.tryGetRuleContext(0, FlagsEnumSetContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_fieldInitializer; }
    // @Override
    enterRule(listener) {
        if (listener.enterFieldInitializer) {
            listener.enterFieldInitializer(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitFieldInitializer) {
            listener.exitFieldInitializer(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitFieldInitializer) {
            return visitor.visitFieldInitializer(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.FieldInitializerContext = FieldInitializerContext;
class NullValueContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_nullValue; }
    // @Override
    enterRule(listener) {
        if (listener.enterNullValue) {
            listener.enterNullValue(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitNullValue) {
            listener.exitNullValue(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitNullValue) {
            return visitor.visitNullValue(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.NullValueContext = NullValueContext;
class BoolValueContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_boolValue; }
    // @Override
    enterRule(listener) {
        if (listener.enterBoolValue) {
            listener.enterBoolValue(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitBoolValue) {
            listener.exitBoolValue(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitBoolValue) {
            return visitor.visitBoolValue(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.BoolValueContext = BoolValueContext;
class NewInstanceContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_newInstance; }
    // @Override
    enterRule(listener) {
        if (listener.enterNewInstance) {
            listener.enterNewInstance(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitNewInstance) {
            listener.exitNewInstance(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitNewInstance) {
            return visitor.visitNewInstance(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.NewInstanceContext = NewInstanceContext;
class InitialEnumValContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_initialEnumVal; }
    // @Override
    enterRule(listener) {
        if (listener.enterInitialEnumVal) {
            listener.enterInitialEnumVal(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitInitialEnumVal) {
            listener.exitInitialEnumVal(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitInitialEnumVal) {
            return visitor.visitInitialEnumVal(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.InitialEnumValContext = InitialEnumValContext;
class NumValueContext extends ParserRuleContext_1.ParserRuleContext {
    integer() {
        return this.tryGetRuleContext(0, IntegerContext);
    }
    decimal() {
        return this.tryGetRuleContext(0, DecimalContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_numValue; }
    // @Override
    enterRule(listener) {
        if (listener.enterNumValue) {
            listener.enterNumValue(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitNumValue) {
            listener.exitNumValue(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitNumValue) {
            return visitor.visitNumValue(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.NumValueContext = NumValueContext;
class IntegerContext extends ParserRuleContext_1.ParserRuleContext {
    POS_INT() { return this.getToken(ESSL_Parser.POS_INT, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_integer; }
    // @Override
    enterRule(listener) {
        if (listener.enterInteger) {
            listener.enterInteger(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitInteger) {
            listener.exitInteger(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitInteger) {
            return visitor.visitInteger(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.IntegerContext = IntegerContext;
class DecimalContext extends ParserRuleContext_1.ParserRuleContext {
    POS_DEC() { return this.getToken(ESSL_Parser.POS_DEC, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_decimal; }
    // @Override
    enterRule(listener) {
        if (listener.enterDecimal) {
            listener.enterDecimal(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDecimal) {
            listener.exitDecimal(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDecimal) {
            return visitor.visitDecimal(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DecimalContext = DecimalContext;
class StringValueContext extends ParserRuleContext_1.ParserRuleContext {
    QUOTED_STR() { return this.getToken(ESSL_Parser.QUOTED_STR, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_stringValue; }
    // @Override
    enterRule(listener) {
        if (listener.enterStringValue) {
            listener.enterStringValue(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitStringValue) {
            listener.exitStringValue(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitStringValue) {
            return visitor.visitStringValue(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.StringValueContext = StringValueContext;
class FlagsEnumSetContext extends ParserRuleContext_1.ParserRuleContext {
    flagsEnumValue(i) {
        if (i === undefined) {
            return this.getRuleContexts(FlagsEnumValueContext);
        }
        else {
            return this.getRuleContext(i, FlagsEnumValueContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_flagsEnumSet; }
    // @Override
    enterRule(listener) {
        if (listener.enterFlagsEnumSet) {
            listener.enterFlagsEnumSet(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitFlagsEnumSet) {
            listener.exitFlagsEnumSet(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitFlagsEnumSet) {
            return visitor.visitFlagsEnumSet(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.FlagsEnumSetContext = FlagsEnumSetContext;
class FlagsEnumValueContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_flagsEnumValue; }
    // @Override
    enterRule(listener) {
        if (listener.enterFlagsEnumValue) {
            listener.enterFlagsEnumValue(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitFlagsEnumValue) {
            listener.exitFlagsEnumValue(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitFlagsEnumValue) {
            return visitor.visitFlagsEnumValue(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.FlagsEnumValueContext = FlagsEnumValueContext;
class FieldDecoratorsContext extends ParserRuleContext_1.ParserRuleContext {
    fieldDecorator(i) {
        if (i === undefined) {
            return this.getRuleContexts(FieldDecoratorContext);
        }
        else {
            return this.getRuleContext(i, FieldDecoratorContext);
        }
    }
    esslComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslCommentContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_fieldDecorators; }
    // @Override
    enterRule(listener) {
        if (listener.enterFieldDecorators) {
            listener.enterFieldDecorators(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitFieldDecorators) {
            listener.exitFieldDecorators(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitFieldDecorators) {
            return visitor.visitFieldDecorators(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.FieldDecoratorsContext = FieldDecoratorsContext;
class FieldDecoratorContext extends ParserRuleContext_1.ParserRuleContext {
    indexDecor() {
        return this.tryGetRuleContext(0, IndexDecorContext);
    }
    uniqueIndexDecor() {
        return this.tryGetRuleContext(0, UniqueIndexDecorContext);
    }
    inlineEnumDecor() {
        return this.tryGetRuleContext(0, InlineEnumDecorContext);
    }
    composedDecor() {
        return this.tryGetRuleContext(0, ComposedDecorContext);
    }
    requiredDecor() {
        return this.tryGetRuleContext(0, RequiredDecorContext);
    }
    readOnlyDecor() {
        return this.tryGetRuleContext(0, ReadOnlyDecorContext);
    }
    hiddenDecor() {
        return this.tryGetRuleContext(0, HiddenDecorContext);
    }
    calculatedDecor() {
        return this.tryGetRuleContext(0, CalculatedDecorContext);
    }
    constantDecor() {
        return this.tryGetRuleContext(0, ConstantDecorContext);
    }
    notPersistedDecor() {
        return this.tryGetRuleContext(0, NotPersistedDecorContext);
    }
    immutableDecor() {
        return this.tryGetRuleContext(0, ImmutableDecorContext);
    }
    clonePartitionerDecor() {
        return this.tryGetRuleContext(0, ClonePartitionerDecorContext);
    }
    cloneIdAsIsDecor() {
        return this.tryGetRuleContext(0, CloneIdAsIsDecorContext);
    }
    fieldUmlDecor() {
        return this.tryGetRuleContext(0, FieldUmlDecorContext);
    }
    sectionDecor() {
        return this.tryGetRuleContext(0, SectionDecorContext);
    }
    asValueTypeDecor() {
        return this.tryGetRuleContext(0, AsValueTypeDecorContext);
    }
    authPolicyDecor() {
        return this.tryGetRuleContext(0, AuthPolicyDecorContext);
    }
    idDecor() {
        return this.tryGetRuleContext(0, IdDecorContext);
    }
    sameAsDecor() {
        return this.tryGetRuleContext(0, SameAsDecorContext);
    }
    handCodedDecor() {
        return this.tryGetRuleContext(0, HandCodedDecorContext);
    }
    dictDecor() {
        return this.tryGetRuleContext(0, DictDecorContext);
    }
    typeDiscriminator() {
        return this.tryGetRuleContext(0, TypeDiscriminatorContext);
    }
    multiLineDecor() {
        return this.tryGetRuleContext(0, MultiLineDecorContext);
    }
    questionDecor() {
        return this.tryGetRuleContext(0, QuestionDecorContext);
    }
    camelCaseDecor() {
        return this.tryGetRuleContext(0, CamelCaseDecorContext);
    }
    labelDecor() {
        return this.tryGetRuleContext(0, LabelDecorContext);
    }
    autoFillDecor() {
        return this.tryGetRuleContext(0, AutoFillDecorContext);
    }
    attributeDecor() {
        return this.tryGetRuleContext(0, AttributeDecorContext);
    }
    factDimensionDecor() {
        return this.tryGetRuleContext(0, FactDimensionDecorContext);
    }
    dimensionKeyDecor() {
        return this.tryGetRuleContext(0, DimensionKeyDecorContext);
    }
    journalEntriesDecor() {
        return this.tryGetRuleContext(0, JournalEntriesDecorContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_fieldDecorator; }
    // @Override
    enterRule(listener) {
        if (listener.enterFieldDecorator) {
            listener.enterFieldDecorator(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitFieldDecorator) {
            listener.exitFieldDecorator(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitFieldDecorator) {
            return visitor.visitFieldDecorator(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.FieldDecoratorContext = FieldDecoratorContext;
class IndexDecorContext extends ParserRuleContext_1.ParserRuleContext {
    indexAttrs() {
        return this.getRuleContext(0, IndexAttrsContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_indexDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterIndexDecor) {
            listener.enterIndexDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitIndexDecor) {
            listener.exitIndexDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitIndexDecor) {
            return visitor.visitIndexDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.IndexDecorContext = IndexDecorContext;
class UniqueIndexDecorContext extends ParserRuleContext_1.ParserRuleContext {
    indexAttrs() {
        return this.getRuleContext(0, IndexAttrsContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_uniqueIndexDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterUniqueIndexDecor) {
            listener.enterUniqueIndexDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitUniqueIndexDecor) {
            listener.exitUniqueIndexDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitUniqueIndexDecor) {
            return visitor.visitUniqueIndexDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.UniqueIndexDecorContext = UniqueIndexDecorContext;
class IndexAttrsContext extends ParserRuleContext_1.ParserRuleContext {
    name() {
        return this.getRuleContext(0, NameContext);
    }
    caseInsensitive() {
        return this.tryGetRuleContext(0, CaseInsensitiveContext);
    }
    nullHandling() {
        return this.tryGetRuleContext(0, NullHandlingContext);
    }
    indexAttrDecors() {
        return this.tryGetRuleContext(0, IndexAttrDecorsContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_indexAttrs; }
    // @Override
    enterRule(listener) {
        if (listener.enterIndexAttrs) {
            listener.enterIndexAttrs(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitIndexAttrs) {
            listener.exitIndexAttrs(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitIndexAttrs) {
            return visitor.visitIndexAttrs(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.IndexAttrsContext = IndexAttrsContext;
class CaseInsensitiveContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_caseInsensitive; }
    // @Override
    enterRule(listener) {
        if (listener.enterCaseInsensitive) {
            listener.enterCaseInsensitive(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitCaseInsensitive) {
            listener.exitCaseInsensitive(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitCaseInsensitive) {
            return visitor.visitCaseInsensitive(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.CaseInsensitiveContext = CaseInsensitiveContext;
class NullHandlingContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_nullHandling; }
    // @Override
    enterRule(listener) {
        if (listener.enterNullHandling) {
            listener.enterNullHandling(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitNullHandling) {
            listener.exitNullHandling(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitNullHandling) {
            return visitor.visitNullHandling(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.NullHandlingContext = NullHandlingContext;
class InlineEnumDecorContext extends ParserRuleContext_1.ParserRuleContext {
    inlineEnumValue(i) {
        if (i === undefined) {
            return this.getRuleContexts(InlineEnumValueContext);
        }
        else {
            return this.getRuleContext(i, InlineEnumValueContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_inlineEnumDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterInlineEnumDecor) {
            listener.enterInlineEnumDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitInlineEnumDecor) {
            listener.exitInlineEnumDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitInlineEnumDecor) {
            return visitor.visitInlineEnumDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.InlineEnumDecorContext = InlineEnumDecorContext;
class InlineEnumValueContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_inlineEnumValue; }
    // @Override
    enterRule(listener) {
        if (listener.enterInlineEnumValue) {
            listener.enterInlineEnumValue(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitInlineEnumValue) {
            listener.exitInlineEnumValue(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitInlineEnumValue) {
            return visitor.visitInlineEnumValue(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.InlineEnumValueContext = InlineEnumValueContext;
class ComposedDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_composedDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterComposedDecor) {
            listener.enterComposedDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitComposedDecor) {
            listener.exitComposedDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitComposedDecor) {
            return visitor.visitComposedDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ComposedDecorContext = ComposedDecorContext;
class RequiredDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_requiredDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterRequiredDecor) {
            listener.enterRequiredDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitRequiredDecor) {
            listener.exitRequiredDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitRequiredDecor) {
            return visitor.visitRequiredDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.RequiredDecorContext = RequiredDecorContext;
class ReadOnlyDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_readOnlyDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterReadOnlyDecor) {
            listener.enterReadOnlyDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitReadOnlyDecor) {
            listener.exitReadOnlyDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitReadOnlyDecor) {
            return visitor.visitReadOnlyDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ReadOnlyDecorContext = ReadOnlyDecorContext;
class HiddenDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_hiddenDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterHiddenDecor) {
            listener.enterHiddenDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitHiddenDecor) {
            listener.exitHiddenDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitHiddenDecor) {
            return visitor.visitHiddenDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.HiddenDecorContext = HiddenDecorContext;
class CalculatedDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_calculatedDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterCalculatedDecor) {
            listener.enterCalculatedDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitCalculatedDecor) {
            listener.exitCalculatedDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitCalculatedDecor) {
            return visitor.visitCalculatedDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.CalculatedDecorContext = CalculatedDecorContext;
class ImmutableDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_immutableDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterImmutableDecor) {
            listener.enterImmutableDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitImmutableDecor) {
            listener.exitImmutableDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitImmutableDecor) {
            return visitor.visitImmutableDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ImmutableDecorContext = ImmutableDecorContext;
class ClonePartitionerDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_clonePartitionerDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterClonePartitionerDecor) {
            listener.enterClonePartitionerDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitClonePartitionerDecor) {
            listener.exitClonePartitionerDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitClonePartitionerDecor) {
            return visitor.visitClonePartitionerDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ClonePartitionerDecorContext = ClonePartitionerDecorContext;
class CloneIdAsIsDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_cloneIdAsIsDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterCloneIdAsIsDecor) {
            listener.enterCloneIdAsIsDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitCloneIdAsIsDecor) {
            listener.exitCloneIdAsIsDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitCloneIdAsIsDecor) {
            return visitor.visitCloneIdAsIsDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.CloneIdAsIsDecorContext = CloneIdAsIsDecorContext;
class FieldUmlDecorContext extends ParserRuleContext_1.ParserRuleContext {
    fieldUmlGroupTitle() {
        return this.getRuleContext(0, FieldUmlGroupTitleContext);
    }
    direction(i) {
        if (i === undefined) {
            return this.getRuleContexts(DirectionContext);
        }
        else {
            return this.getRuleContext(i, DirectionContext);
        }
    }
    horizontalVertical(i) {
        if (i === undefined) {
            return this.getRuleContexts(HorizontalVerticalContext);
        }
        else {
            return this.getRuleContext(i, HorizontalVerticalContext);
        }
    }
    lineLength(i) {
        if (i === undefined) {
            return this.getRuleContexts(LineLengthContext);
        }
        else {
            return this.getRuleContext(i, LineLengthContext);
        }
    }
    hideRelationship(i) {
        if (i === undefined) {
            return this.getRuleContexts(HideRelationshipContext);
        }
        else {
            return this.getRuleContext(i, HideRelationshipContext);
        }
    }
    lineLabel(i) {
        if (i === undefined) {
            return this.getRuleContexts(LineLabelContext);
        }
        else {
            return this.getRuleContext(i, LineLabelContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_fieldUmlDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterFieldUmlDecor) {
            listener.enterFieldUmlDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitFieldUmlDecor) {
            listener.exitFieldUmlDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitFieldUmlDecor) {
            return visitor.visitFieldUmlDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.FieldUmlDecorContext = FieldUmlDecorContext;
class FieldUmlGroupTitleContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_fieldUmlGroupTitle; }
    // @Override
    enterRule(listener) {
        if (listener.enterFieldUmlGroupTitle) {
            listener.enterFieldUmlGroupTitle(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitFieldUmlGroupTitle) {
            listener.exitFieldUmlGroupTitle(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitFieldUmlGroupTitle) {
            return visitor.visitFieldUmlGroupTitle(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.FieldUmlGroupTitleContext = FieldUmlGroupTitleContext;
class DirectionContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_direction; }
    // @Override
    enterRule(listener) {
        if (listener.enterDirection) {
            listener.enterDirection(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDirection) {
            listener.exitDirection(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDirection) {
            return visitor.visitDirection(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DirectionContext = DirectionContext;
class HorizontalVerticalContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_horizontalVertical; }
    // @Override
    enterRule(listener) {
        if (listener.enterHorizontalVertical) {
            listener.enterHorizontalVertical(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitHorizontalVertical) {
            listener.exitHorizontalVertical(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitHorizontalVertical) {
            return visitor.visitHorizontalVertical(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.HorizontalVerticalContext = HorizontalVerticalContext;
class LineLengthContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_lineLength; }
    // @Override
    enterRule(listener) {
        if (listener.enterLineLength) {
            listener.enterLineLength(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitLineLength) {
            listener.exitLineLength(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitLineLength) {
            return visitor.visitLineLength(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.LineLengthContext = LineLengthContext;
class HideRelationshipContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_hideRelationship; }
    // @Override
    enterRule(listener) {
        if (listener.enterHideRelationship) {
            listener.enterHideRelationship(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitHideRelationship) {
            listener.exitHideRelationship(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitHideRelationship) {
            return visitor.visitHideRelationship(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.HideRelationshipContext = HideRelationshipContext;
class LineLabelContext extends ParserRuleContext_1.ParserRuleContext {
    QUOTED_STR() { return this.getToken(ESSL_Parser.QUOTED_STR, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_lineLabel; }
    // @Override
    enterRule(listener) {
        if (listener.enterLineLabel) {
            listener.enterLineLabel(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitLineLabel) {
            listener.exitLineLabel(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitLineLabel) {
            return visitor.visitLineLabel(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.LineLabelContext = LineLabelContext;
class SectionDecorContext extends ParserRuleContext_1.ParserRuleContext {
    breakType() {
        return this.getRuleContext(0, BreakTypeContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_sectionDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterSectionDecor) {
            listener.enterSectionDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitSectionDecor) {
            listener.exitSectionDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitSectionDecor) {
            return visitor.visitSectionDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.SectionDecorContext = SectionDecorContext;
class BreakTypeContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_breakType; }
    // @Override
    enterRule(listener) {
        if (listener.enterBreakType) {
            listener.enterBreakType(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitBreakType) {
            listener.exitBreakType(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitBreakType) {
            return visitor.visitBreakType(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.BreakTypeContext = BreakTypeContext;
class AsValueTypeDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_asValueTypeDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterAsValueTypeDecor) {
            listener.enterAsValueTypeDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAsValueTypeDecor) {
            listener.exitAsValueTypeDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAsValueTypeDecor) {
            return visitor.visitAsValueTypeDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AsValueTypeDecorContext = AsValueTypeDecorContext;
class SameAsDecorContext extends ParserRuleContext_1.ParserRuleContext {
    dottedId() {
        return this.getRuleContext(0, DottedIdContext);
    }
    sameAsPersist() {
        return this.tryGetRuleContext(0, SameAsPersistContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_sameAsDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterSameAsDecor) {
            listener.enterSameAsDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitSameAsDecor) {
            listener.exitSameAsDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitSameAsDecor) {
            return visitor.visitSameAsDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.SameAsDecorContext = SameAsDecorContext;
class SameAsPersistContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_sameAsPersist; }
    // @Override
    enterRule(listener) {
        if (listener.enterSameAsPersist) {
            listener.enterSameAsPersist(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitSameAsPersist) {
            listener.exitSameAsPersist(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitSameAsPersist) {
            return visitor.visitSameAsPersist(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.SameAsPersistContext = SameAsPersistContext;
class ConstantDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_constantDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterConstantDecor) {
            listener.enterConstantDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitConstantDecor) {
            listener.exitConstantDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitConstantDecor) {
            return visitor.visitConstantDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ConstantDecorContext = ConstantDecorContext;
class NotPersistedDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_notPersistedDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterNotPersistedDecor) {
            listener.enterNotPersistedDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitNotPersistedDecor) {
            listener.exitNotPersistedDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitNotPersistedDecor) {
            return visitor.visitNotPersistedDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.NotPersistedDecorContext = NotPersistedDecorContext;
class HandCodedDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_handCodedDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterHandCodedDecor) {
            listener.enterHandCodedDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitHandCodedDecor) {
            listener.exitHandCodedDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitHandCodedDecor) {
            return visitor.visitHandCodedDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.HandCodedDecorContext = HandCodedDecorContext;
class DictDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_dictDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterDictDecor) {
            listener.enterDictDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDictDecor) {
            listener.exitDictDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDictDecor) {
            return visitor.visitDictDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DictDecorContext = DictDecorContext;
class TypeDiscriminatorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_typeDiscriminator; }
    // @Override
    enterRule(listener) {
        if (listener.enterTypeDiscriminator) {
            listener.enterTypeDiscriminator(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitTypeDiscriminator) {
            listener.exitTypeDiscriminator(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitTypeDiscriminator) {
            return visitor.visitTypeDiscriminator(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.TypeDiscriminatorContext = TypeDiscriminatorContext;
class MultiLineDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_multiLineDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterMultiLineDecor) {
            listener.enterMultiLineDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitMultiLineDecor) {
            listener.exitMultiLineDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitMultiLineDecor) {
            return visitor.visitMultiLineDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.MultiLineDecorContext = MultiLineDecorContext;
class QuestionDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_questionDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterQuestionDecor) {
            listener.enterQuestionDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitQuestionDecor) {
            listener.exitQuestionDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitQuestionDecor) {
            return visitor.visitQuestionDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.QuestionDecorContext = QuestionDecorContext;
class LabelDecorContext extends ParserRuleContext_1.ParserRuleContext {
    label() {
        return this.getRuleContext(0, LabelContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_labelDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterLabelDecor) {
            listener.enterLabelDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitLabelDecor) {
            listener.exitLabelDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitLabelDecor) {
            return visitor.visitLabelDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.LabelDecorContext = LabelDecorContext;
class LabelContext extends ParserRuleContext_1.ParserRuleContext {
    QUOTED_STR() { return this.getToken(ESSL_Parser.QUOTED_STR, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_label; }
    // @Override
    enterRule(listener) {
        if (listener.enterLabel) {
            listener.enterLabel(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitLabel) {
            listener.exitLabel(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitLabel) {
            return visitor.visitLabel(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.LabelContext = LabelContext;
class AutoFillDecorContext extends ParserRuleContext_1.ParserRuleContext {
    autoFillType() {
        return this.getRuleContext(0, AutoFillTypeContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_autoFillDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterAutoFillDecor) {
            listener.enterAutoFillDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAutoFillDecor) {
            listener.exitAutoFillDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAutoFillDecor) {
            return visitor.visitAutoFillDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AutoFillDecorContext = AutoFillDecorContext;
class AutoFillTypeContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_autoFillType; }
    // @Override
    enterRule(listener) {
        if (listener.enterAutoFillType) {
            listener.enterAutoFillType(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAutoFillType) {
            listener.exitAutoFillType(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAutoFillType) {
            return visitor.visitAutoFillType(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AutoFillTypeContext = AutoFillTypeContext;
class AttributeDecorContext extends ParserRuleContext_1.ParserRuleContext {
    attributePair(i) {
        if (i === undefined) {
            return this.getRuleContexts(AttributePairContext);
        }
        else {
            return this.getRuleContext(i, AttributePairContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_attributeDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterAttributeDecor) {
            listener.enterAttributeDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAttributeDecor) {
            listener.exitAttributeDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAttributeDecor) {
            return visitor.visitAttributeDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AttributeDecorContext = AttributeDecorContext;
class AttributePairContext extends ParserRuleContext_1.ParserRuleContext {
    attributeKey() {
        return this.getRuleContext(0, AttributeKeyContext);
    }
    attributeValue() {
        return this.getRuleContext(0, AttributeValueContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_attributePair; }
    // @Override
    enterRule(listener) {
        if (listener.enterAttributePair) {
            listener.enterAttributePair(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAttributePair) {
            listener.exitAttributePair(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAttributePair) {
            return visitor.visitAttributePair(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AttributePairContext = AttributePairContext;
class AttributeKeyContext extends ParserRuleContext_1.ParserRuleContext {
    QUOTED_STR() { return this.getToken(ESSL_Parser.QUOTED_STR, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_attributeKey; }
    // @Override
    enterRule(listener) {
        if (listener.enterAttributeKey) {
            listener.enterAttributeKey(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAttributeKey) {
            listener.exitAttributeKey(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAttributeKey) {
            return visitor.visitAttributeKey(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AttributeKeyContext = AttributeKeyContext;
class AttributeValueContext extends ParserRuleContext_1.ParserRuleContext {
    QUOTED_STR() { return this.getToken(ESSL_Parser.QUOTED_STR, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_attributeValue; }
    // @Override
    enterRule(listener) {
        if (listener.enterAttributeValue) {
            listener.enterAttributeValue(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAttributeValue) {
            listener.exitAttributeValue(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAttributeValue) {
            return visitor.visitAttributeValue(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AttributeValueContext = AttributeValueContext;
class FactDimensionDecorContext extends ParserRuleContext_1.ParserRuleContext {
    dimPrimary() {
        return this.tryGetRuleContext(0, DimPrimaryContext);
    }
    dimInx() {
        return this.tryGetRuleContext(0, DimInxContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_factDimensionDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterFactDimensionDecor) {
            listener.enterFactDimensionDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitFactDimensionDecor) {
            listener.exitFactDimensionDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitFactDimensionDecor) {
            return visitor.visitFactDimensionDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.FactDimensionDecorContext = FactDimensionDecorContext;
class DimPrimaryContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_dimPrimary; }
    // @Override
    enterRule(listener) {
        if (listener.enterDimPrimary) {
            listener.enterDimPrimary(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDimPrimary) {
            listener.exitDimPrimary(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDimPrimary) {
            return visitor.visitDimPrimary(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DimPrimaryContext = DimPrimaryContext;
class DimInxContext extends ParserRuleContext_1.ParserRuleContext {
    POS_INT() { return this.tryGetToken(ESSL_Parser.POS_INT, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_dimInx; }
    // @Override
    enterRule(listener) {
        if (listener.enterDimInx) {
            listener.enterDimInx(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDimInx) {
            listener.exitDimInx(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDimInx) {
            return visitor.visitDimInx(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DimInxContext = DimInxContext;
class DimensionKeyDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_dimensionKeyDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterDimensionKeyDecor) {
            listener.enterDimensionKeyDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDimensionKeyDecor) {
            listener.exitDimensionKeyDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDimensionKeyDecor) {
            return visitor.visitDimensionKeyDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DimensionKeyDecorContext = DimensionKeyDecorContext;
class JournalEntriesDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_journalEntriesDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterJournalEntriesDecor) {
            listener.enterJournalEntriesDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitJournalEntriesDecor) {
            listener.exitJournalEntriesDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitJournalEntriesDecor) {
            return visitor.visitJournalEntriesDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.JournalEntriesDecorContext = JournalEntriesDecorContext;
class EntityEventsContext extends ParserRuleContext_1.ParserRuleContext {
    beforeComment() {
        return this.getRuleContext(0, BeforeCommentContext);
    }
    eventsBlock() {
        return this.getRuleContext(0, EventsBlockContext);
    }
    eventsDecors() {
        return this.getRuleContext(0, EventsDecorsContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_entityEvents; }
    // @Override
    enterRule(listener) {
        if (listener.enterEntityEvents) {
            listener.enterEntityEvents(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEntityEvents) {
            listener.exitEntityEvents(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEntityEvents) {
            return visitor.visitEntityEvents(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EntityEventsContext = EntityEventsContext;
class EventsBlockContext extends ParserRuleContext_1.ParserRuleContext {
    createdEventDef() {
        return this.getRuleContext(0, CreatedEventDefContext);
    }
    eventDef(i) {
        if (i === undefined) {
            return this.getRuleContexts(EventDefContext);
        }
        else {
            return this.getRuleContext(i, EventDefContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_eventsBlock; }
    // @Override
    enterRule(listener) {
        if (listener.enterEventsBlock) {
            listener.enterEventsBlock(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEventsBlock) {
            listener.exitEventsBlock(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEventsBlock) {
            return visitor.visitEventsBlock(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EventsBlockContext = EventsBlockContext;
class CreatedEventDefContext extends ParserRuleContext_1.ParserRuleContext {
    beforeComment() {
        return this.getRuleContext(0, BeforeCommentContext);
    }
    createdEvent() {
        return this.getRuleContext(0, CreatedEventContext);
    }
    eventNameDecors() {
        return this.getRuleContext(0, EventNameDecorsContext);
    }
    comment() {
        return this.getRuleContext(0, CommentContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_createdEventDef; }
    // @Override
    enterRule(listener) {
        if (listener.enterCreatedEventDef) {
            listener.enterCreatedEventDef(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitCreatedEventDef) {
            listener.exitCreatedEventDef(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitCreatedEventDef) {
            return visitor.visitCreatedEventDef(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.CreatedEventDefContext = CreatedEventDefContext;
class CreatedEventContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_createdEvent; }
    // @Override
    enterRule(listener) {
        if (listener.enterCreatedEvent) {
            listener.enterCreatedEvent(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitCreatedEvent) {
            listener.exitCreatedEvent(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitCreatedEvent) {
            return visitor.visitCreatedEvent(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.CreatedEventContext = CreatedEventContext;
class EventDefContext extends ParserRuleContext_1.ParserRuleContext {
    beforeComment() {
        return this.getRuleContext(0, BeforeCommentContext);
    }
    eventName() {
        return this.getRuleContext(0, EventNameContext);
    }
    eventNameDecors() {
        return this.getRuleContext(0, EventNameDecorsContext);
    }
    comment() {
        return this.getRuleContext(0, CommentContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_eventDef; }
    // @Override
    enterRule(listener) {
        if (listener.enterEventDef) {
            listener.enterEventDef(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEventDef) {
            listener.exitEventDef(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEventDef) {
            return visitor.visitEventDef(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EventDefContext = EventDefContext;
class EventNameContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_eventName; }
    // @Override
    enterRule(listener) {
        if (listener.enterEventName) {
            listener.enterEventName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEventName) {
            listener.exitEventName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEventName) {
            return visitor.visitEventName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EventNameContext = EventNameContext;
class EventsDecorsContext extends ParserRuleContext_1.ParserRuleContext {
    eventsDecor(i) {
        if (i === undefined) {
            return this.getRuleContexts(EventsDecorContext);
        }
        else {
            return this.getRuleContext(i, EventsDecorContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_eventsDecors; }
    // @Override
    enterRule(listener) {
        if (listener.enterEventsDecors) {
            listener.enterEventsDecors(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEventsDecors) {
            listener.exitEventsDecors(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEventsDecors) {
            return visitor.visitEventsDecors(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EventsDecorsContext = EventsDecorsContext;
class EventsDecorContext extends ParserRuleContext_1.ParserRuleContext {
    umlGroupDecor() {
        return this.tryGetRuleContext(0, UmlGroupDecorContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_eventsDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterEventsDecor) {
            listener.enterEventsDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEventsDecor) {
            listener.exitEventsDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEventsDecor) {
            return visitor.visitEventsDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EventsDecorContext = EventsDecorContext;
class EventNameDecorsContext extends ParserRuleContext_1.ParserRuleContext {
    eventNameDecor(i) {
        if (i === undefined) {
            return this.getRuleContexts(EventNameDecorContext);
        }
        else {
            return this.getRuleContext(i, EventNameDecorContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_eventNameDecors; }
    // @Override
    enterRule(listener) {
        if (listener.enterEventNameDecors) {
            listener.enterEventNameDecors(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEventNameDecors) {
            listener.exitEventNameDecors(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEventNameDecors) {
            return visitor.visitEventNameDecors(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EventNameDecorsContext = EventNameDecorsContext;
class EventNameDecorContext extends ParserRuleContext_1.ParserRuleContext {
    hashLookupDecor() {
        return this.tryGetRuleContext(0, HashLookupDecorContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_eventNameDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterEventNameDecor) {
            listener.enterEventNameDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEventNameDecor) {
            listener.exitEventNameDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEventNameDecor) {
            return visitor.visitEventNameDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EventNameDecorContext = EventNameDecorContext;
class EntityCommandsContext extends ParserRuleContext_1.ParserRuleContext {
    beforeComment() {
        return this.getRuleContext(0, BeforeCommentContext);
    }
    commandsBlock() {
        return this.getRuleContext(0, CommandsBlockContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_entityCommands; }
    // @Override
    enterRule(listener) {
        if (listener.enterEntityCommands) {
            listener.enterEntityCommands(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEntityCommands) {
            listener.exitEntityCommands(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEntityCommands) {
            return visitor.visitEntityCommands(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EntityCommandsContext = EntityCommandsContext;
class CommandsBlockContext extends ParserRuleContext_1.ParserRuleContext {
    commandsBlockDecorators() {
        return this.getRuleContext(0, CommandsBlockDecoratorsContext);
    }
    command(i) {
        if (i === undefined) {
            return this.getRuleContexts(CommandContext);
        }
        else {
            return this.getRuleContext(i, CommandContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_commandsBlock; }
    // @Override
    enterRule(listener) {
        if (listener.enterCommandsBlock) {
            listener.enterCommandsBlock(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitCommandsBlock) {
            listener.exitCommandsBlock(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitCommandsBlock) {
            return visitor.visitCommandsBlock(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.CommandsBlockContext = CommandsBlockContext;
class CommandsBlockDecoratorContext extends ParserRuleContext_1.ParserRuleContext {
    authPolicyDecor() {
        return this.tryGetRuleContext(0, AuthPolicyDecorContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_commandsBlockDecorator; }
    // @Override
    enterRule(listener) {
        if (listener.enterCommandsBlockDecorator) {
            listener.enterCommandsBlockDecorator(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitCommandsBlockDecorator) {
            listener.exitCommandsBlockDecorator(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitCommandsBlockDecorator) {
            return visitor.visitCommandsBlockDecorator(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.CommandsBlockDecoratorContext = CommandsBlockDecoratorContext;
class CommandsBlockDecoratorsContext extends ParserRuleContext_1.ParserRuleContext {
    commandsBlockDecorator(i) {
        if (i === undefined) {
            return this.getRuleContexts(CommandsBlockDecoratorContext);
        }
        else {
            return this.getRuleContext(i, CommandsBlockDecoratorContext);
        }
    }
    esslComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslCommentContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_commandsBlockDecorators; }
    // @Override
    enterRule(listener) {
        if (listener.enterCommandsBlockDecorators) {
            listener.enterCommandsBlockDecorators(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitCommandsBlockDecorators) {
            listener.exitCommandsBlockDecorators(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitCommandsBlockDecorators) {
            return visitor.visitCommandsBlockDecorators(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.CommandsBlockDecoratorsContext = CommandsBlockDecoratorsContext;
class CommandContext extends ParserRuleContext_1.ParserRuleContext {
    beforeComment() {
        return this.getRuleContext(0, BeforeCommentContext);
    }
    name() {
        return this.getRuleContext(0, NameContext);
    }
    commandParams() {
        return this.getRuleContext(0, CommandParamsContext);
    }
    commandDecorators() {
        return this.getRuleContext(0, CommandDecoratorsContext);
    }
    commandResultsIn() {
        return this.getRuleContext(0, CommandResultsInContext);
    }
    esslComment() {
        return this.getRuleContext(0, EsslCommentContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_command; }
    // @Override
    enterRule(listener) {
        if (listener.enterCommand) {
            listener.enterCommand(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitCommand) {
            listener.exitCommand(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitCommand) {
            return visitor.visitCommand(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.CommandContext = CommandContext;
class CommandParamsContext extends ParserRuleContext_1.ParserRuleContext {
    param(i) {
        if (i === undefined) {
            return this.getRuleContexts(ParamContext);
        }
        else {
            return this.getRuleContext(i, ParamContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_commandParams; }
    // @Override
    enterRule(listener) {
        if (listener.enterCommandParams) {
            listener.enterCommandParams(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitCommandParams) {
            listener.exitCommandParams(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitCommandParams) {
            return visitor.visitCommandParams(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.CommandParamsContext = CommandParamsContext;
class ParamContext extends ParserRuleContext_1.ParserRuleContext {
    beforeComment() {
        return this.getRuleContext(0, BeforeCommentContext);
    }
    paramDecl() {
        return this.getRuleContext(0, ParamDeclContext);
    }
    paramInitializer() {
        return this.getRuleContext(0, ParamInitializerContext);
    }
    paramDecorators() {
        return this.tryGetRuleContext(0, ParamDecoratorsContext);
    }
    esslComment() {
        return this.tryGetRuleContext(0, EsslCommentContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_param; }
    // @Override
    enterRule(listener) {
        if (listener.enterParam) {
            listener.enterParam(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitParam) {
            listener.exitParam(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitParam) {
            return visitor.visitParam(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ParamContext = ParamContext;
class ParamDeclContext extends ParserRuleContext_1.ParserRuleContext {
    optionalTypeRef() {
        return this.getRuleContext(0, OptionalTypeRefContext);
    }
    name() {
        return this.getRuleContext(0, NameContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_paramDecl; }
    // @Override
    enterRule(listener) {
        if (listener.enterParamDecl) {
            listener.enterParamDecl(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitParamDecl) {
            listener.exitParamDecl(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitParamDecl) {
            return visitor.visitParamDecl(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ParamDeclContext = ParamDeclContext;
class OptionalTypeRefContext extends ParserRuleContext_1.ParserRuleContext {
    typeRefName() {
        return this.tryGetRuleContext(0, TypeRefNameContext);
    }
    paramNameModifiers() {
        return this.tryGetRuleContext(0, ParamNameModifiersContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_optionalTypeRef; }
    // @Override
    enterRule(listener) {
        if (listener.enterOptionalTypeRef) {
            listener.enterOptionalTypeRef(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitOptionalTypeRef) {
            listener.exitOptionalTypeRef(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitOptionalTypeRef) {
            return visitor.visitOptionalTypeRef(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.OptionalTypeRefContext = OptionalTypeRefContext;
class ParamNameModifiersContext extends ParserRuleContext_1.ParserRuleContext {
    listIndicator() {
        return this.tryGetRuleContext(0, ListIndicatorContext);
    }
    nullableIndicator() {
        return this.tryGetRuleContext(0, NullableIndicatorContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_paramNameModifiers; }
    // @Override
    enterRule(listener) {
        if (listener.enterParamNameModifiers) {
            listener.enterParamNameModifiers(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitParamNameModifiers) {
            listener.exitParamNameModifiers(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitParamNameModifiers) {
            return visitor.visitParamNameModifiers(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ParamNameModifiersContext = ParamNameModifiersContext;
class ParamInitializerContext extends ParserRuleContext_1.ParserRuleContext {
    nullValue() {
        return this.tryGetRuleContext(0, NullValueContext);
    }
    initialEnumVal() {
        return this.tryGetRuleContext(0, InitialEnumValContext);
    }
    boolValue() {
        return this.tryGetRuleContext(0, BoolValueContext);
    }
    numValue() {
        return this.tryGetRuleContext(0, NumValueContext);
    }
    stringValue() {
        return this.tryGetRuleContext(0, StringValueContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_paramInitializer; }
    // @Override
    enterRule(listener) {
        if (listener.enterParamInitializer) {
            listener.enterParamInitializer(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitParamInitializer) {
            listener.exitParamInitializer(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitParamInitializer) {
            return visitor.visitParamInitializer(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ParamInitializerContext = ParamInitializerContext;
class CommandResultsInContext extends ParserRuleContext_1.ParserRuleContext {
    yields() {
        return this.tryGetRuleContext(0, YieldsContext);
    }
    returnsType() {
        return this.tryGetRuleContext(0, ReturnsTypeContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_commandResultsIn; }
    // @Override
    enterRule(listener) {
        if (listener.enterCommandResultsIn) {
            listener.enterCommandResultsIn(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitCommandResultsIn) {
            listener.exitCommandResultsIn(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitCommandResultsIn) {
            return visitor.visitCommandResultsIn(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.CommandResultsInContext = CommandResultsInContext;
class YieldsContext extends ParserRuleContext_1.ParserRuleContext {
    eventList() {
        return this.getRuleContext(0, EventListContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_yields; }
    // @Override
    enterRule(listener) {
        if (listener.enterYields) {
            listener.enterYields(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitYields) {
            listener.exitYields(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitYields) {
            return visitor.visitYields(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.YieldsContext = YieldsContext;
class EventListContext extends ParserRuleContext_1.ParserRuleContext {
    eventRef(i) {
        if (i === undefined) {
            return this.getRuleContexts(EventRefContext);
        }
        else {
            return this.getRuleContext(i, EventRefContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_eventList; }
    // @Override
    enterRule(listener) {
        if (listener.enterEventList) {
            listener.enterEventList(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEventList) {
            listener.exitEventList(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEventList) {
            return visitor.visitEventList(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EventListContext = EventListContext;
class EventRefContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_eventRef; }
    // @Override
    enterRule(listener) {
        if (listener.enterEventRef) {
            listener.enterEventRef(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEventRef) {
            listener.exitEventRef(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEventRef) {
            return visitor.visitEventRef(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EventRefContext = EventRefContext;
class ReturnsTypeContext extends ParserRuleContext_1.ParserRuleContext {
    returnTypeRef() {
        return this.getRuleContext(0, ReturnTypeRefContext);
    }
    paramNameModifiers() {
        return this.getRuleContext(0, ParamNameModifiersContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_returnsType; }
    // @Override
    enterRule(listener) {
        if (listener.enterReturnsType) {
            listener.enterReturnsType(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitReturnsType) {
            listener.exitReturnsType(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitReturnsType) {
            return visitor.visitReturnsType(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ReturnsTypeContext = ReturnsTypeContext;
class ReturnTypeRefContext extends ParserRuleContext_1.ParserRuleContext {
    typeRefName() {
        return this.getRuleContext(0, TypeRefNameContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_returnTypeRef; }
    // @Override
    enterRule(listener) {
        if (listener.enterReturnTypeRef) {
            listener.enterReturnTypeRef(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitReturnTypeRef) {
            listener.exitReturnTypeRef(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitReturnTypeRef) {
            return visitor.visitReturnTypeRef(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ReturnTypeRefContext = ReturnTypeRefContext;
class ParamDecoratorContext extends ParserRuleContext_1.ParserRuleContext {
    typeQualifierDecor() {
        return this.tryGetRuleContext(0, TypeQualifierDecorContext);
    }
    byValueDecor() {
        return this.tryGetRuleContext(0, ByValueDecorContext);
    }
    idDecor() {
        return this.tryGetRuleContext(0, IdDecorContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_paramDecorator; }
    // @Override
    enterRule(listener) {
        if (listener.enterParamDecorator) {
            listener.enterParamDecorator(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitParamDecorator) {
            listener.exitParamDecorator(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitParamDecorator) {
            return visitor.visitParamDecorator(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ParamDecoratorContext = ParamDecoratorContext;
class TypeQualifierDecorContext extends ParserRuleContext_1.ParserRuleContext {
    typeQualifierName() {
        return this.getRuleContext(0, TypeQualifierNameContext);
    }
    indexQualifier() {
        return this.tryGetRuleContext(0, IndexQualifierContext);
    }
    filterExpr() {
        return this.tryGetRuleContext(0, FilterExprContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_typeQualifierDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterTypeQualifierDecor) {
            listener.enterTypeQualifierDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitTypeQualifierDecor) {
            listener.exitTypeQualifierDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitTypeQualifierDecor) {
            return visitor.visitTypeQualifierDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.TypeQualifierDecorContext = TypeQualifierDecorContext;
class ParamDecoratorsContext extends ParserRuleContext_1.ParserRuleContext {
    paramDecorator(i) {
        if (i === undefined) {
            return this.getRuleContexts(ParamDecoratorContext);
        }
        else {
            return this.getRuleContext(i, ParamDecoratorContext);
        }
    }
    esslComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslCommentContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_paramDecorators; }
    // @Override
    enterRule(listener) {
        if (listener.enterParamDecorators) {
            listener.enterParamDecorators(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitParamDecorators) {
            listener.exitParamDecorators(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitParamDecorators) {
            return visitor.visitParamDecorators(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ParamDecoratorsContext = ParamDecoratorsContext;
class TypeQualifierNameContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_typeQualifierName; }
    // @Override
    enterRule(listener) {
        if (listener.enterTypeQualifierName) {
            listener.enterTypeQualifierName(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitTypeQualifierName) {
            listener.exitTypeQualifierName(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitTypeQualifierName) {
            return visitor.visitTypeQualifierName(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.TypeQualifierNameContext = TypeQualifierNameContext;
class IndexQualifierContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_indexQualifier; }
    // @Override
    enterRule(listener) {
        if (listener.enterIndexQualifier) {
            listener.enterIndexQualifier(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitIndexQualifier) {
            listener.exitIndexQualifier(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitIndexQualifier) {
            return visitor.visitIndexQualifier(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.IndexQualifierContext = IndexQualifierContext;
class FilterExprContext extends ParserRuleContext_1.ParserRuleContext {
    FILTEREXPR() { return this.getToken(ESSL_Parser.FILTEREXPR, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_filterExpr; }
    // @Override
    enterRule(listener) {
        if (listener.enterFilterExpr) {
            listener.enterFilterExpr(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitFilterExpr) {
            listener.exitFilterExpr(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitFilterExpr) {
            return visitor.visitFilterExpr(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.FilterExprContext = FilterExprContext;
class ByValueDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_byValueDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterByValueDecor) {
            listener.enterByValueDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitByValueDecor) {
            listener.exitByValueDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitByValueDecor) {
            return visitor.visitByValueDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ByValueDecorContext = ByValueDecorContext;
class IdDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_idDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterIdDecor) {
            listener.enterIdDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitIdDecor) {
            listener.exitIdDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitIdDecor) {
            return visitor.visitIdDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.IdDecorContext = IdDecorContext;
class CommandDecoratorContext extends ParserRuleContext_1.ParserRuleContext {
    modelDecor() {
        return this.tryGetRuleContext(0, ModelDecorContext);
    }
    asyncDecor() {
        return this.tryGetRuleContext(0, AsyncDecorContext);
    }
    syncDecor() {
        return this.tryGetRuleContext(0, SyncDecorContext);
    }
    transDecor() {
        return this.tryGetRuleContext(0, TransDecorContext);
    }
    continuationDecor() {
        return this.tryGetRuleContext(0, ContinuationDecorContext);
    }
    effectiveDateDecor() {
        return this.tryGetRuleContext(0, EffectiveDateDecorContext);
    }
    explicitDecor() {
        return this.tryGetRuleContext(0, ExplicitDecorContext);
    }
    internalDecor() {
        return this.tryGetRuleContext(0, InternalDecorContext);
    }
    messageSourceDecor() {
        return this.tryGetRuleContext(0, MessageSourceDecorContext);
    }
    createDecor() {
        return this.tryGetRuleContext(0, CreateDecorContext);
    }
    deleteDecor() {
        return this.tryGetRuleContext(0, DeleteDecorContext);
    }
    partialErrorDecor() {
        return this.tryGetRuleContext(0, PartialErrorDecorContext);
    }
    generateDecor() {
        return this.tryGetRuleContext(0, GenerateDecorContext);
    }
    authPolicyDecor() {
        return this.tryGetRuleContext(0, AuthPolicyDecorContext);
    }
    publicDecor() {
        return this.tryGetRuleContext(0, PublicDecorContext);
    }
    provideGraphQLSchemaDecor() {
        return this.tryGetRuleContext(0, ProvideGraphQLSchemaDecorContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_commandDecorator; }
    // @Override
    enterRule(listener) {
        if (listener.enterCommandDecorator) {
            listener.enterCommandDecorator(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitCommandDecorator) {
            listener.exitCommandDecorator(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitCommandDecorator) {
            return visitor.visitCommandDecorator(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.CommandDecoratorContext = CommandDecoratorContext;
class CommandDecoratorsContext extends ParserRuleContext_1.ParserRuleContext {
    commandDecorator(i) {
        if (i === undefined) {
            return this.getRuleContexts(CommandDecoratorContext);
        }
        else {
            return this.getRuleContext(i, CommandDecoratorContext);
        }
    }
    esslComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslCommentContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_commandDecorators; }
    // @Override
    enterRule(listener) {
        if (listener.enterCommandDecorators) {
            listener.enterCommandDecorators(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitCommandDecorators) {
            listener.exitCommandDecorators(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitCommandDecorators) {
            return visitor.visitCommandDecorators(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.CommandDecoratorsContext = CommandDecoratorsContext;
class ModelDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_modelDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterModelDecor) {
            listener.enterModelDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitModelDecor) {
            listener.exitModelDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitModelDecor) {
            return visitor.visitModelDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ModelDecorContext = ModelDecorContext;
class AsyncDecorContext extends ParserRuleContext_1.ParserRuleContext {
    asyncSpec(i) {
        if (i === undefined) {
            return this.getRuleContexts(AsyncSpecContext);
        }
        else {
            return this.getRuleContext(i, AsyncSpecContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_asyncDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterAsyncDecor) {
            listener.enterAsyncDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAsyncDecor) {
            listener.exitAsyncDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAsyncDecor) {
            return visitor.visitAsyncDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AsyncDecorContext = AsyncDecorContext;
class AsyncSpecContext extends ParserRuleContext_1.ParserRuleContext {
    asyncParamValidation() {
        return this.tryGetRuleContext(0, AsyncParamValidationContext);
    }
    asyncModelValidation() {
        return this.tryGetRuleContext(0, AsyncModelValidationContext);
    }
    asyncBusinessLogic() {
        return this.tryGetRuleContext(0, AsyncBusinessLogicContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_asyncSpec; }
    // @Override
    enterRule(listener) {
        if (listener.enterAsyncSpec) {
            listener.enterAsyncSpec(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAsyncSpec) {
            listener.exitAsyncSpec(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAsyncSpec) {
            return visitor.visitAsyncSpec(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AsyncSpecContext = AsyncSpecContext;
class AsyncParamValidationContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_asyncParamValidation; }
    // @Override
    enterRule(listener) {
        if (listener.enterAsyncParamValidation) {
            listener.enterAsyncParamValidation(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAsyncParamValidation) {
            listener.exitAsyncParamValidation(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAsyncParamValidation) {
            return visitor.visitAsyncParamValidation(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AsyncParamValidationContext = AsyncParamValidationContext;
class AsyncModelValidationContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_asyncModelValidation; }
    // @Override
    enterRule(listener) {
        if (listener.enterAsyncModelValidation) {
            listener.enterAsyncModelValidation(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAsyncModelValidation) {
            listener.exitAsyncModelValidation(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAsyncModelValidation) {
            return visitor.visitAsyncModelValidation(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AsyncModelValidationContext = AsyncModelValidationContext;
class AsyncBusinessLogicContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_asyncBusinessLogic; }
    // @Override
    enterRule(listener) {
        if (listener.enterAsyncBusinessLogic) {
            listener.enterAsyncBusinessLogic(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAsyncBusinessLogic) {
            listener.exitAsyncBusinessLogic(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAsyncBusinessLogic) {
            return visitor.visitAsyncBusinessLogic(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AsyncBusinessLogicContext = AsyncBusinessLogicContext;
class SyncDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_syncDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterSyncDecor) {
            listener.enterSyncDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitSyncDecor) {
            listener.exitSyncDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitSyncDecor) {
            return visitor.visitSyncDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.SyncDecorContext = SyncDecorContext;
class TransDecorContext extends ParserRuleContext_1.ParserRuleContext {
    transAttr() {
        return this.getRuleContext(0, TransAttrContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_transDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterTransDecor) {
            listener.enterTransDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitTransDecor) {
            listener.exitTransDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitTransDecor) {
            return visitor.visitTransDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.TransDecorContext = TransDecorContext;
class TransAttrContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_transAttr; }
    // @Override
    enterRule(listener) {
        if (listener.enterTransAttr) {
            listener.enterTransAttr(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitTransAttr) {
            listener.exitTransAttr(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitTransAttr) {
            return visitor.visitTransAttr(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.TransAttrContext = TransAttrContext;
class ContinuationDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_continuationDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterContinuationDecor) {
            listener.enterContinuationDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitContinuationDecor) {
            listener.exitContinuationDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitContinuationDecor) {
            return visitor.visitContinuationDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ContinuationDecorContext = ContinuationDecorContext;
class EffectiveDateDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_effectiveDateDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterEffectiveDateDecor) {
            listener.enterEffectiveDateDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEffectiveDateDecor) {
            listener.exitEffectiveDateDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEffectiveDateDecor) {
            return visitor.visitEffectiveDateDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EffectiveDateDecorContext = EffectiveDateDecorContext;
class ExplicitDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_explicitDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterExplicitDecor) {
            listener.enterExplicitDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitExplicitDecor) {
            listener.exitExplicitDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitExplicitDecor) {
            return visitor.visitExplicitDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ExplicitDecorContext = ExplicitDecorContext;
class InternalDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_internalDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterInternalDecor) {
            listener.enterInternalDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitInternalDecor) {
            listener.exitInternalDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitInternalDecor) {
            return visitor.visitInternalDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.InternalDecorContext = InternalDecorContext;
class MessageSourceDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_messageSourceDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterMessageSourceDecor) {
            listener.enterMessageSourceDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitMessageSourceDecor) {
            listener.exitMessageSourceDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitMessageSourceDecor) {
            return visitor.visitMessageSourceDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.MessageSourceDecorContext = MessageSourceDecorContext;
class CreateDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_createDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterCreateDecor) {
            listener.enterCreateDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitCreateDecor) {
            listener.exitCreateDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitCreateDecor) {
            return visitor.visitCreateDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.CreateDecorContext = CreateDecorContext;
class DeleteDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_deleteDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterDeleteDecor) {
            listener.enterDeleteDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDeleteDecor) {
            listener.exitDeleteDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDeleteDecor) {
            return visitor.visitDeleteDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DeleteDecorContext = DeleteDecorContext;
class PartialErrorDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_partialErrorDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterPartialErrorDecor) {
            listener.enterPartialErrorDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitPartialErrorDecor) {
            listener.exitPartialErrorDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitPartialErrorDecor) {
            return visitor.visitPartialErrorDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.PartialErrorDecorContext = PartialErrorDecorContext;
class GenerateDecorContext extends ParserRuleContext_1.ParserRuleContext {
    generatorOption() {
        return this.getRuleContext(0, GeneratorOptionContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_generateDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterGenerateDecor) {
            listener.enterGenerateDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitGenerateDecor) {
            listener.exitGenerateDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitGenerateDecor) {
            return visitor.visitGenerateDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.GenerateDecorContext = GenerateDecorContext;
class GeneratorOptionContext extends ParserRuleContext_1.ParserRuleContext {
    genSetField() {
        return this.tryGetRuleContext(0, GenSetFieldContext);
    }
    genAssignments() {
        return this.tryGetRuleContext(0, GenAssignmentsContext);
    }
    genAddToSet() {
        return this.tryGetRuleContext(0, GenAddToSetContext);
    }
    genRemoveFromSet() {
        return this.tryGetRuleContext(0, GenRemoveFromSetContext);
    }
    genClearSet() {
        return this.tryGetRuleContext(0, GenClearSetContext);
    }
    genUpdate() {
        return this.tryGetRuleContext(0, GenUpdateContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_generatorOption; }
    // @Override
    enterRule(listener) {
        if (listener.enterGeneratorOption) {
            listener.enterGeneratorOption(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitGeneratorOption) {
            listener.exitGeneratorOption(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitGeneratorOption) {
            return visitor.visitGeneratorOption(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.GeneratorOptionContext = GeneratorOptionContext;
class GenSetFieldContext extends ParserRuleContext_1.ParserRuleContext {
    refField() {
        return this.tryGetRuleContext(0, RefFieldContext);
    }
    jsonKey() {
        return this.tryGetRuleContext(0, JsonKeyContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_genSetField; }
    // @Override
    enterRule(listener) {
        if (listener.enterGenSetField) {
            listener.enterGenSetField(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitGenSetField) {
            listener.exitGenSetField(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitGenSetField) {
            return visitor.visitGenSetField(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.GenSetFieldContext = GenSetFieldContext;
class GenAssignmentsContext extends ParserRuleContext_1.ParserRuleContext {
    assignmentList() {
        return this.tryGetRuleContext(0, AssignmentListContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_genAssignments; }
    // @Override
    enterRule(listener) {
        if (listener.enterGenAssignments) {
            listener.enterGenAssignments(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitGenAssignments) {
            listener.exitGenAssignments(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitGenAssignments) {
            return visitor.visitGenAssignments(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.GenAssignmentsContext = GenAssignmentsContext;
class AssignmentListContext extends ParserRuleContext_1.ParserRuleContext {
    assignment(i) {
        if (i === undefined) {
            return this.getRuleContexts(AssignmentContext);
        }
        else {
            return this.getRuleContext(i, AssignmentContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_assignmentList; }
    // @Override
    enterRule(listener) {
        if (listener.enterAssignmentList) {
            listener.enterAssignmentList(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAssignmentList) {
            listener.exitAssignmentList(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAssignmentList) {
            return visitor.visitAssignmentList(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AssignmentListContext = AssignmentListContext;
class AssignmentContext extends ParserRuleContext_1.ParserRuleContext {
    refField() {
        return this.getRuleContext(0, RefFieldContext);
    }
    jsonKey() {
        return this.tryGetRuleContext(0, JsonKeyContext);
    }
    valueExpression() {
        return this.tryGetRuleContext(0, ValueExpressionContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_assignment; }
    // @Override
    enterRule(listener) {
        if (listener.enterAssignment) {
            listener.enterAssignment(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAssignment) {
            listener.exitAssignment(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAssignment) {
            return visitor.visitAssignment(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AssignmentContext = AssignmentContext;
class ValueExpressionContext extends ParserRuleContext_1.ParserRuleContext {
    DELIMITED_EXPRESSION() { return this.getToken(ESSL_Parser.DELIMITED_EXPRESSION, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_valueExpression; }
    // @Override
    enterRule(listener) {
        if (listener.enterValueExpression) {
            listener.enterValueExpression(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitValueExpression) {
            listener.exitValueExpression(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitValueExpression) {
            return visitor.visitValueExpression(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ValueExpressionContext = ValueExpressionContext;
class GenAddToSetContext extends ParserRuleContext_1.ParserRuleContext {
    refField() {
        return this.tryGetRuleContext(0, RefFieldContext);
    }
    jsonKey() {
        return this.tryGetRuleContext(0, JsonKeyContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_genAddToSet; }
    // @Override
    enterRule(listener) {
        if (listener.enterGenAddToSet) {
            listener.enterGenAddToSet(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitGenAddToSet) {
            listener.exitGenAddToSet(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitGenAddToSet) {
            return visitor.visitGenAddToSet(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.GenAddToSetContext = GenAddToSetContext;
class GenRemoveFromSetContext extends ParserRuleContext_1.ParserRuleContext {
    refField() {
        return this.tryGetRuleContext(0, RefFieldContext);
    }
    jsonKey() {
        return this.tryGetRuleContext(0, JsonKeyContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_genRemoveFromSet; }
    // @Override
    enterRule(listener) {
        if (listener.enterGenRemoveFromSet) {
            listener.enterGenRemoveFromSet(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitGenRemoveFromSet) {
            listener.exitGenRemoveFromSet(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitGenRemoveFromSet) {
            return visitor.visitGenRemoveFromSet(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.GenRemoveFromSetContext = GenRemoveFromSetContext;
class GenClearSetContext extends ParserRuleContext_1.ParserRuleContext {
    refField() {
        return this.tryGetRuleContext(0, RefFieldContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_genClearSet; }
    // @Override
    enterRule(listener) {
        if (listener.enterGenClearSet) {
            listener.enterGenClearSet(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitGenClearSet) {
            listener.exitGenClearSet(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitGenClearSet) {
            return visitor.visitGenClearSet(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.GenClearSetContext = GenClearSetContext;
class GenUpdateContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_genUpdate; }
    // @Override
    enterRule(listener) {
        if (listener.enterGenUpdate) {
            listener.enterGenUpdate(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitGenUpdate) {
            listener.exitGenUpdate(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitGenUpdate) {
            return visitor.visitGenUpdate(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.GenUpdateContext = GenUpdateContext;
class JsonKeyContext extends ParserRuleContext_1.ParserRuleContext {
    QUOTED_STR() { return this.getToken(ESSL_Parser.QUOTED_STR, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_jsonKey; }
    // @Override
    enterRule(listener) {
        if (listener.enterJsonKey) {
            listener.enterJsonKey(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitJsonKey) {
            listener.exitJsonKey(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitJsonKey) {
            return visitor.visitJsonKey(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.JsonKeyContext = JsonKeyContext;
class RefFieldContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER() { return this.getToken(ESSL_Parser.IDENTIFIER, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_refField; }
    // @Override
    enterRule(listener) {
        if (listener.enterRefField) {
            listener.enterRefField(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitRefField) {
            listener.exitRefField(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitRefField) {
            return visitor.visitRefField(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.RefFieldContext = RefFieldContext;
class EntityQueriesContext extends ParserRuleContext_1.ParserRuleContext {
    beforeComment() {
        return this.getRuleContext(0, BeforeCommentContext);
    }
    queriesBlock() {
        return this.getRuleContext(0, QueriesBlockContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_entityQueries; }
    // @Override
    enterRule(listener) {
        if (listener.enterEntityQueries) {
            listener.enterEntityQueries(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEntityQueries) {
            listener.exitEntityQueries(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEntityQueries) {
            return visitor.visitEntityQueries(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EntityQueriesContext = EntityQueriesContext;
class QueriesBlockContext extends ParserRuleContext_1.ParserRuleContext {
    comment() {
        return this.getRuleContext(0, CommentContext);
    }
    queriesBlockDecorators() {
        return this.getRuleContext(0, QueriesBlockDecoratorsContext);
    }
    query(i) {
        if (i === undefined) {
            return this.getRuleContexts(QueryContext);
        }
        else {
            return this.getRuleContext(i, QueryContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_queriesBlock; }
    // @Override
    enterRule(listener) {
        if (listener.enterQueriesBlock) {
            listener.enterQueriesBlock(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitQueriesBlock) {
            listener.exitQueriesBlock(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitQueriesBlock) {
            return visitor.visitQueriesBlock(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.QueriesBlockContext = QueriesBlockContext;
class QueriesBlockDecoratorContext extends ParserRuleContext_1.ParserRuleContext {
    authPolicyDecor() {
        return this.tryGetRuleContext(0, AuthPolicyDecorContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_queriesBlockDecorator; }
    // @Override
    enterRule(listener) {
        if (listener.enterQueriesBlockDecorator) {
            listener.enterQueriesBlockDecorator(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitQueriesBlockDecorator) {
            listener.exitQueriesBlockDecorator(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitQueriesBlockDecorator) {
            return visitor.visitQueriesBlockDecorator(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.QueriesBlockDecoratorContext = QueriesBlockDecoratorContext;
class QueriesBlockDecoratorsContext extends ParserRuleContext_1.ParserRuleContext {
    queriesBlockDecorator(i) {
        if (i === undefined) {
            return this.getRuleContexts(QueriesBlockDecoratorContext);
        }
        else {
            return this.getRuleContext(i, QueriesBlockDecoratorContext);
        }
    }
    esslComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslCommentContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_queriesBlockDecorators; }
    // @Override
    enterRule(listener) {
        if (listener.enterQueriesBlockDecorators) {
            listener.enterQueriesBlockDecorators(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitQueriesBlockDecorators) {
            listener.exitQueriesBlockDecorators(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitQueriesBlockDecorators) {
            return visitor.visitQueriesBlockDecorators(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.QueriesBlockDecoratorsContext = QueriesBlockDecoratorsContext;
class QueryContext extends ParserRuleContext_1.ParserRuleContext {
    beforeComment() {
        return this.getRuleContext(0, BeforeCommentContext);
    }
    name() {
        return this.getRuleContext(0, NameContext);
    }
    commandParams() {
        return this.getRuleContext(0, CommandParamsContext);
    }
    queryDecorators() {
        return this.getRuleContext(0, QueryDecoratorsContext);
    }
    returnsType() {
        return this.getRuleContext(0, ReturnsTypeContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_query; }
    // @Override
    enterRule(listener) {
        if (listener.enterQuery) {
            listener.enterQuery(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitQuery) {
            listener.exitQuery(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitQuery) {
            return visitor.visitQuery(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.QueryContext = QueryContext;
class QueryDecoratorContext extends ParserRuleContext_1.ParserRuleContext {
    temporalDecor() {
        return this.tryGetRuleContext(0, TemporalDecorContext);
    }
    provideUserContextDecor() {
        return this.tryGetRuleContext(0, ProvideUserContextDecorContext);
    }
    internalDecor() {
        return this.tryGetRuleContext(0, InternalDecorContext);
    }
    syncDecor() {
        return this.tryGetRuleContext(0, SyncDecorContext);
    }
    authPolicyDecor() {
        return this.tryGetRuleContext(0, AuthPolicyDecorContext);
    }
    publicDecor() {
        return this.tryGetRuleContext(0, PublicDecorContext);
    }
    provideGraphQLSchemaDecor() {
        return this.tryGetRuleContext(0, ProvideGraphQLSchemaDecorContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_queryDecorator; }
    // @Override
    enterRule(listener) {
        if (listener.enterQueryDecorator) {
            listener.enterQueryDecorator(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitQueryDecorator) {
            listener.exitQueryDecorator(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitQueryDecorator) {
            return visitor.visitQueryDecorator(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.QueryDecoratorContext = QueryDecoratorContext;
class QueryDecoratorsContext extends ParserRuleContext_1.ParserRuleContext {
    queryDecorator(i) {
        if (i === undefined) {
            return this.getRuleContexts(QueryDecoratorContext);
        }
        else {
            return this.getRuleContext(i, QueryDecoratorContext);
        }
    }
    esslComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslCommentContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_queryDecorators; }
    // @Override
    enterRule(listener) {
        if (listener.enterQueryDecorators) {
            listener.enterQueryDecorators(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitQueryDecorators) {
            listener.exitQueryDecorators(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitQueryDecorators) {
            return visitor.visitQueryDecorators(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.QueryDecoratorsContext = QueryDecoratorsContext;
class TemporalDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_temporalDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterTemporalDecor) {
            listener.enterTemporalDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitTemporalDecor) {
            listener.exitTemporalDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitTemporalDecor) {
            return visitor.visitTemporalDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.TemporalDecorContext = TemporalDecorContext;
class ProvideUserContextDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_provideUserContextDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterProvideUserContextDecor) {
            listener.enterProvideUserContextDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitProvideUserContextDecor) {
            listener.exitProvideUserContextDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitProvideUserContextDecor) {
            return visitor.visitProvideUserContextDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ProvideUserContextDecorContext = ProvideUserContextDecorContext;
class ProvideGraphQLSchemaDecorContext extends ParserRuleContext_1.ParserRuleContext {
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_provideGraphQLSchemaDecor; }
    // @Override
    enterRule(listener) {
        if (listener.enterProvideGraphQLSchemaDecor) {
            listener.enterProvideGraphQLSchemaDecor(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitProvideGraphQLSchemaDecor) {
            listener.exitProvideGraphQLSchemaDecor(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitProvideGraphQLSchemaDecor) {
            return visitor.visitProvideGraphQLSchemaDecor(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.ProvideGraphQLSchemaDecorContext = ProvideGraphQLSchemaDecorContext;
class EntitySubscriptionsContext extends ParserRuleContext_1.ParserRuleContext {
    beforeComment() {
        return this.getRuleContext(0, BeforeCommentContext);
    }
    subscriptionsBlock() {
        return this.getRuleContext(0, SubscriptionsBlockContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_entitySubscriptions; }
    // @Override
    enterRule(listener) {
        if (listener.enterEntitySubscriptions) {
            listener.enterEntitySubscriptions(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEntitySubscriptions) {
            listener.exitEntitySubscriptions(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEntitySubscriptions) {
            return visitor.visitEntitySubscriptions(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EntitySubscriptionsContext = EntitySubscriptionsContext;
class SubscriptionsBlockContext extends ParserRuleContext_1.ParserRuleContext {
    comment() {
        return this.getRuleContext(0, CommentContext);
    }
    subscriptionsBlockDecorators() {
        return this.getRuleContext(0, SubscriptionsBlockDecoratorsContext);
    }
    subscription(i) {
        if (i === undefined) {
            return this.getRuleContexts(SubscriptionContext);
        }
        else {
            return this.getRuleContext(i, SubscriptionContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_subscriptionsBlock; }
    // @Override
    enterRule(listener) {
        if (listener.enterSubscriptionsBlock) {
            listener.enterSubscriptionsBlock(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitSubscriptionsBlock) {
            listener.exitSubscriptionsBlock(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitSubscriptionsBlock) {
            return visitor.visitSubscriptionsBlock(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.SubscriptionsBlockContext = SubscriptionsBlockContext;
class SubscriptionsBlockDecoratorContext extends ParserRuleContext_1.ParserRuleContext {
    authPolicyDecor() {
        return this.tryGetRuleContext(0, AuthPolicyDecorContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_subscriptionsBlockDecorator; }
    // @Override
    enterRule(listener) {
        if (listener.enterSubscriptionsBlockDecorator) {
            listener.enterSubscriptionsBlockDecorator(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitSubscriptionsBlockDecorator) {
            listener.exitSubscriptionsBlockDecorator(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitSubscriptionsBlockDecorator) {
            return visitor.visitSubscriptionsBlockDecorator(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.SubscriptionsBlockDecoratorContext = SubscriptionsBlockDecoratorContext;
class SubscriptionsBlockDecoratorsContext extends ParserRuleContext_1.ParserRuleContext {
    subscriptionsBlockDecorator(i) {
        if (i === undefined) {
            return this.getRuleContexts(SubscriptionsBlockDecoratorContext);
        }
        else {
            return this.getRuleContext(i, SubscriptionsBlockDecoratorContext);
        }
    }
    esslComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslCommentContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_subscriptionsBlockDecorators; }
    // @Override
    enterRule(listener) {
        if (listener.enterSubscriptionsBlockDecorators) {
            listener.enterSubscriptionsBlockDecorators(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitSubscriptionsBlockDecorators) {
            listener.exitSubscriptionsBlockDecorators(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitSubscriptionsBlockDecorators) {
            return visitor.visitSubscriptionsBlockDecorators(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.SubscriptionsBlockDecoratorsContext = SubscriptionsBlockDecoratorsContext;
class SubscriptionContext extends ParserRuleContext_1.ParserRuleContext {
    beforeComment() {
        return this.getRuleContext(0, BeforeCommentContext);
    }
    name() {
        return this.getRuleContext(0, NameContext);
    }
    commandParams() {
        return this.getRuleContext(0, CommandParamsContext);
    }
    subscriptionDecorators() {
        return this.getRuleContext(0, SubscriptionDecoratorsContext);
    }
    returnsType() {
        return this.getRuleContext(0, ReturnsTypeContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_subscription; }
    // @Override
    enterRule(listener) {
        if (listener.enterSubscription) {
            listener.enterSubscription(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitSubscription) {
            listener.exitSubscription(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitSubscription) {
            return visitor.visitSubscription(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.SubscriptionContext = SubscriptionContext;
class SubscriptionDecoratorContext extends ParserRuleContext_1.ParserRuleContext {
    authPolicyDecor() {
        return this.tryGetRuleContext(0, AuthPolicyDecorContext);
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_subscriptionDecorator; }
    // @Override
    enterRule(listener) {
        if (listener.enterSubscriptionDecorator) {
            listener.enterSubscriptionDecorator(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitSubscriptionDecorator) {
            listener.exitSubscriptionDecorator(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitSubscriptionDecorator) {
            return visitor.visitSubscriptionDecorator(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.SubscriptionDecoratorContext = SubscriptionDecoratorContext;
class SubscriptionDecoratorsContext extends ParserRuleContext_1.ParserRuleContext {
    subscriptionDecorator(i) {
        if (i === undefined) {
            return this.getRuleContexts(SubscriptionDecoratorContext);
        }
        else {
            return this.getRuleContext(i, SubscriptionDecoratorContext);
        }
    }
    esslComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslCommentContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_subscriptionDecorators; }
    // @Override
    enterRule(listener) {
        if (listener.enterSubscriptionDecorators) {
            listener.enterSubscriptionDecorators(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitSubscriptionDecorators) {
            listener.exitSubscriptionDecorators(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitSubscriptionDecorators) {
            return visitor.visitSubscriptionDecorators(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.SubscriptionDecoratorsContext = SubscriptionDecoratorsContext;
class DottedIdContext extends ParserRuleContext_1.ParserRuleContext {
    IDENTIFIER(i) {
        if (i === undefined) {
            return this.getTokens(ESSL_Parser.IDENTIFIER);
        }
        else {
            return this.getToken(ESSL_Parser.IDENTIFIER, i);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_dottedId; }
    // @Override
    enterRule(listener) {
        if (listener.enterDottedId) {
            listener.enterDottedId(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDottedId) {
            listener.exitDottedId(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDottedId) {
            return visitor.visitDottedId(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DottedIdContext = DottedIdContext;
class CommentContext extends ParserRuleContext_1.ParserRuleContext {
    domBlockComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(DomBlockCommentContext);
        }
        else {
            return this.getRuleContext(i, DomBlockCommentContext);
        }
    }
    esslBlockComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslBlockCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslBlockCommentContext);
        }
    }
    esslLineComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslLineCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslLineCommentContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_comment; }
    // @Override
    enterRule(listener) {
        if (listener.enterComment) {
            listener.enterComment(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitComment) {
            listener.exitComment(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitComment) {
            return visitor.visitComment(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.CommentContext = CommentContext;
class BeforeCommentContext extends ParserRuleContext_1.ParserRuleContext {
    domBeforeLineComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(DomBeforeLineCommentContext);
        }
        else {
            return this.getRuleContext(i, DomBeforeLineCommentContext);
        }
    }
    esslBlockComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslBlockCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslBlockCommentContext);
        }
    }
    esslLineComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslLineCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslLineCommentContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_beforeComment; }
    // @Override
    enterRule(listener) {
        if (listener.enterBeforeComment) {
            listener.enterBeforeComment(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitBeforeComment) {
            listener.exitBeforeComment(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitBeforeComment) {
            return visitor.visitBeforeComment(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.BeforeCommentContext = BeforeCommentContext;
class AfterCommentContext extends ParserRuleContext_1.ParserRuleContext {
    domBeforeLineComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(DomBeforeLineCommentContext);
        }
        else {
            return this.getRuleContext(i, DomBeforeLineCommentContext);
        }
    }
    esslBlockComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslBlockCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslBlockCommentContext);
        }
    }
    esslLineComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslLineCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslLineCommentContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_afterComment; }
    // @Override
    enterRule(listener) {
        if (listener.enterAfterComment) {
            listener.enterAfterComment(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitAfterComment) {
            listener.exitAfterComment(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitAfterComment) {
            return visitor.visitAfterComment(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.AfterCommentContext = AfterCommentContext;
class EsslCommentContext extends ParserRuleContext_1.ParserRuleContext {
    esslBlockComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslBlockCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslBlockCommentContext);
        }
    }
    esslLineComment(i) {
        if (i === undefined) {
            return this.getRuleContexts(EsslLineCommentContext);
        }
        else {
            return this.getRuleContext(i, EsslLineCommentContext);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_esslComment; }
    // @Override
    enterRule(listener) {
        if (listener.enterEsslComment) {
            listener.enterEsslComment(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEsslComment) {
            listener.exitEsslComment(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEsslComment) {
            return visitor.visitEsslComment(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EsslCommentContext = EsslCommentContext;
class DomBeforeLineCommentContext extends ParserRuleContext_1.ParserRuleContext {
    BEFORE_COMMENT_LINE() { return this.getToken(ESSL_Parser.BEFORE_COMMENT_LINE, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_domBeforeLineComment; }
    // @Override
    enterRule(listener) {
        if (listener.enterDomBeforeLineComment) {
            listener.enterDomBeforeLineComment(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDomBeforeLineComment) {
            listener.exitDomBeforeLineComment(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDomBeforeLineComment) {
            return visitor.visitDomBeforeLineComment(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DomBeforeLineCommentContext = DomBeforeLineCommentContext;
class DomBlockCommentContext extends ParserRuleContext_1.ParserRuleContext {
    COMMENT_BLOCK() { return this.getToken(ESSL_Parser.COMMENT_BLOCK, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_domBlockComment; }
    // @Override
    enterRule(listener) {
        if (listener.enterDomBlockComment) {
            listener.enterDomBlockComment(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitDomBlockComment) {
            listener.exitDomBlockComment(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitDomBlockComment) {
            return visitor.visitDomBlockComment(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.DomBlockCommentContext = DomBlockCommentContext;
class EsslBlockCommentContext extends ParserRuleContext_1.ParserRuleContext {
    ESSL_COMMENT_BLOCK() { return this.getToken(ESSL_Parser.ESSL_COMMENT_BLOCK, 0); }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_esslBlockComment; }
    // @Override
    enterRule(listener) {
        if (listener.enterEsslBlockComment) {
            listener.enterEsslBlockComment(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEsslBlockComment) {
            listener.exitEsslBlockComment(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEsslBlockComment) {
            return visitor.visitEsslBlockComment(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EsslBlockCommentContext = EsslBlockCommentContext;
class EsslLineCommentContext extends ParserRuleContext_1.ParserRuleContext {
    ESSL_COMMENT_LINE(i) {
        if (i === undefined) {
            return this.getTokens(ESSL_Parser.ESSL_COMMENT_LINE);
        }
        else {
            return this.getToken(ESSL_Parser.ESSL_COMMENT_LINE, i);
        }
    }
    constructor(parent, invokingState) {
        super(parent, invokingState);
    }
    // @Override
    get ruleIndex() { return ESSL_Parser.RULE_esslLineComment; }
    // @Override
    enterRule(listener) {
        if (listener.enterEsslLineComment) {
            listener.enterEsslLineComment(this);
        }
    }
    // @Override
    exitRule(listener) {
        if (listener.exitEsslLineComment) {
            listener.exitEsslLineComment(this);
        }
    }
    // @Override
    accept(visitor) {
        if (visitor.visitEsslLineComment) {
            return visitor.visitEsslLineComment(this);
        }
        else {
            return visitor.visitChildren(this);
        }
    }
}
exports.EsslLineCommentContext = EsslLineCommentContext;
//# sourceMappingURL=ESSL_Parser.js.map