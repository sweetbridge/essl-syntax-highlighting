"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getEsslFiles = exports.buildCompletion = exports.EsslCompletionItemProvider = exports.declarations = void 0;
const vscode = require("vscode");
const vscode_1 = require("vscode");
const path = require("path");
const fileSystem = require("fs");
const semantics = require("../semantics");
const g4type = require("./type");
const decl = require("./declaration");
const reg = require("./regions");
const utility = require("../utility");
exports.declarations = [];
class EsslCompletionItemProvider {
    // This method is called whenever the user types a char in the list specified in extension.ts
    // It returns a list of CompletionItems based on the input, context, etc.
    provideCompletionItems(document, position, token, context) {
        // Reload the regions
        // Needed because they will have incorrect indices for Range
        semantics.EsslSemanticTokensProvider.updateRegions(document.getText());
        // Get the config for whether to provide completion
        const config = vscode.workspace.getConfiguration('ESSL');
        var enable = config.get("EnableAutocomplete");
        if (!enable) {
            return;
        }
        // Pull the current line out of the editor
        // Does not include a newline at the end
        var currentLine = document.getText().split("\n")[position.line];
        // Code if . is typed (Path)
        if (context.triggerCharacter === ".") {
            // Search for available namespaces
        }
        // Code if @ is typed (Decorators)
        else if (context.triggerCharacter === "@") {
            // Check all decorators in the json file, with hardcoded completion values
            return this.getCompletionItemsFromJson("decorators.json", position);
        }
        // Code if anything else is pressed (normal keys, return, etc.)
        else {
            // Code for type property and parameters, because they use the same autocompletion list of var type
            // Check if input is in a valid type/param region
            if (this.positionInValidRegion(position, "type", "param")) {
                // Check if the current line is built correctly, with the current type/param being typed
                var match = currentLine.match(/^\s*[A-Za-z]*[0-9A-Za-z_\[\]\?]*(?![ \[\]\?0-9A-Za-z_]+)/m);
                if (match && position.character <= match[0].length) {
                    // Return all stored declarations and (optionally) all primitive types (string, etc.)
                    return decl.Declaration.toCompletionList(exports.declarations, true);
                }
            }
        }
        return new vscode_1.CompletionList();
    }
    // Selects a regionType based on the given valid ones (validTypes)
    // Then loops through each corresponding region to see if the specified position is within that region
    // If any region contains the position, return true.
    positionInValidRegion(position, ...validTypes) {
        for (var i = 0; i < reg.RegionType.regionTypes.length; i++) {
            var regionType = reg.RegionType.regionTypes[i];
            if (validTypes.indexOf(regionType.type) === -1) {
                continue;
            }
            for (var j = 0; j < regionType.regions.length; j++) {
                var region = regionType.regions[j];
                if (region.range.contains(position)) {
                    return true;
                }
            }
        }
        return false;
    }
    // Parse the given json file (stored in ./src/completion/, format of 'xyz.json') for the list of keys
    // For every key, Checks if the given position is within the corresponding region for that key
    // If it is, return a CompletionList of those keys
    // If it is invalid, return an empty CompletionList
    getCompletionItemsFromJson(jsonFile, position) {
        var extensionPath = vscode.extensions.getExtension("Sweetbridge.essl-extension").extensionUri.fsPath;
        var jsonPath = path.join(extensionPath, "src", "completion", jsonFile);
        if (fileSystem.existsSync(jsonPath)) {
            var content = fileSystem.readFileSync(jsonPath, "utf8");
            var data = JSON.parse(content);
            var keys = Object.keys(data);
            var list = new vscode_1.CompletionList();
            for (var a = 0; a < keys.length; a++) {
                var objArr = data[keys[a]];
                if (!this.positionInValidRegion(position, keys[a])) {
                    continue;
                }
                for (var i = 0; i < objArr.length; i++) {
                    var label = objArr[i]["name"];
                    var insertText = objArr[i]["insertText"];
                    var item = new vscode.CompletionItem(label, vscode.CompletionItemKind.Property);
                    item.insertText = insertText;
                    list.items.push(item);
                }
                return list;
            }
        }
        return new vscode_1.CompletionList();
    }
}
exports.EsslCompletionItemProvider = EsslCompletionItemProvider;
// Generic function to generate resources to use for code completion
function buildCompletion() {
    // Get currently opened workspaces. Return if none are open.
    var openWorkspaceFolders = vscode.workspace.workspaceFolders;
    if (openWorkspaceFolders === undefined) {
        return;
    }
    // Loop through all open workspaces, get the essl files recursively in those workspaces
    // Work of adding declarations to the list is done in getEsslFiles below
    for (var i = 0; i < openWorkspaceFolders.length; i++) {
        var workspace = openWorkspaceFolders[i];
        getEsslFiles(workspace.uri.fsPath);
    }
}
exports.buildCompletion = buildCompletion;
// Recursively gets all essl files in the children of a given directory path
function getEsslFiles(dirPath) {
    let esslPaths = [];
    fileSystem.readdirSync(dirPath).forEach(file => {
        var absPath = path.join(dirPath, file);
        // If the new path is a directory, loop through that directory
        if (fileSystem.lstatSync(absPath).isDirectory()) {
            var recEsslPaths = getEsslFiles(absPath);
            for (var i = 0; i < recEsslPaths.length; i++) {
                esslPaths.push(recEsslPaths[i]);
            }
        }
        // If the new path is a .essl file, generate that file's declarations for code completion
        else if (path.extname(absPath) === ".essl") {
            getDeclarations(absPath);
            esslPaths.push(absPath);
        }
    });
    return esslPaths;
}
exports.getEsslFiles = getEsslFiles;
// Generates the Declarations of a given essl file and adds them to the list
// Each declaration has the type, the name, the file name, the project name, the index of the declaration in the file,
// and the documentation given about that Declaration in the lines before (specified with /// in .essl).
const declarationRegex = /^(?!\n)(([\s]*(?:\/\/\/).+\n)*)([\s]*)\b(entity|type|enum|dictionary|interface)(\s*:*\s*)([a-zA-Z0-9_]*)/gm;
function getDeclarations(filePath) {
    // Get the content of the .essl file
    var content = fileSystem.readFileSync(filePath, "utf8");
    var matches = [];
    var lastName = "";
    var lastPosition = new vscode.Position(0, 0);
    // For each match...
    // Using this .exec syntax, matches[0] is the whole match, matches[1] is the first group, etc.
    while (matches = declarationRegex.exec(content)) {
        var type = g4type.getTypeFromStr(matches[4]);
        // Check if in type {
        var name = "";
        var position = new vscode.Position(0, 0);
        // Matches the case of type { and type : itype {
        // Saves to the location of the entity X {, the name of the entity, but uses the documentation of the type
        if (matches[5].indexOf(":") !== -1 || matches[6] === "") {
            name = lastName;
            position = lastPosition;
        }
        // Normal case
        else {
            name = matches[6];
            position = getPositionFromIndex(content, matches.index + matches[0].length);
            lastName = name;
            lastPosition = position;
        }
        // In the case of 'type {', the name used is "lastName", which for this would be xyz in 'entity xyz'
        // with 'xyz' saved as lastName.
        if (type === g4type.Type.entity) {
            continue;
        }
        // Pull the documentation out of the regex, and strip it of /// and excess spaces
        var documentation = stripDocSyntax(matches[1]);
        var projPath = filePath.replace(path.basename(filePath), "");
        var relProjPath = path.relative(path.dirname(utility.savedSolutionPath), projPath).replace(path.sep, "/");
        // Declare and push the new declaration to the list if it does not already exist
        var declaration = new decl.Declaration(type, name, filePath, path.basename(filePath), relProjPath, position, documentation);
        var valid = true;
        for (var i = 0; i < exports.declarations.length; i++) {
            var dec = exports.declarations[i];
            if (declaration.isEqual(dec)) {
                valid = false;
                break;
            }
        }
        if (valid) {
            exports.declarations.push(declaration);
        }
    }
}
// Get position in file from index
function getPositionFromIndex(content, index) {
    // Get line number
    let cutContent = content.substr(0, index);
    let splitCutContent = cutContent.split("\n");
    let lineNumber = splitCutContent.length - 1;
    // Get char number
    let charNumber = splitCutContent[splitCutContent.length - 1].length;
    return new vscode.Position(lineNumber, charNumber);
}
// Strips and cleans up the declaration content found in the declarationRegex.
// Strips '///', excess spaces, and new lines
function stripDocSyntax(content) {
    content = content.replace(/[ ]{2,}/g, " ");
    content = content.replace(/([ ]+\/\/\/\s)/g, "");
    content = content.replace(/(\n)/g, "");
    return content;
}
//# sourceMappingURL=completion.js.map