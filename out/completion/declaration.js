"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeclarationUsage = exports.Declaration = exports.primitives = void 0;
const vscode = require("vscode");
const completion_1 = require("./completion");
const g4type = require("./type");
const path = require("path");
exports.primitives = ["bool", "date", "datetime", "decimal", "float", "int", "string", "uuid"];
// Instance of a 'type xyz {' saved
// Vars:
// type = type, enum, dictionary, etc
// name = 'xyz' for above example
// fileName = name of the essl file
// projectName = name of the project the essl file is in
// position = where in the file the declaration occurs (used for go to definition)
// documentation = the document provided in the .essl file, in the lines before (documentation is ///)
// usages = instances of this declaration being used elsewhere
class Declaration {
    constructor(_type, _name, _fsPath, _fileName, _projectName, position, _documentation) {
        this.type = _type;
        this.name = _name;
        this.fileName = _fileName;
        this.projectName = _projectName;
        this.position = position;
        this.documentation = _documentation;
        this.fsPath = vscode.Uri.file(_fsPath);
        this.usages = [];
    }
    isEqual(dec) {
        return this.name === dec.name &&
            this.fsPath.toString() === dec.fsPath.toString();
    }
    // Generates the CompletionList using the given list of declarations, and optionally includes the primitive types (string, etc.)
    static toCompletionList(declarations, includePrimitives) {
        // Declare new CompletionList
        var list = new vscode.CompletionList();
        // Loop through every given declaration, and create a new CompletionItem for each
        for (var i = 0; i < declarations.length; i++) {
            var dec = declarations[i];
            var item = new vscode.CompletionItem(dec.name);
            item.kind = Declaration.getCompletionItemKind(dec.type);
            item.detail = ` ${g4type.Type[dec.type]}\n ${dec.projectName}\n ${dec.fileName}`;
            if (dec.documentation !== "") {
                item.documentation = dec.documentation;
            }
            list.items.push(item);
        }
        // Optionally includes the primitive types in the CompletionList
        if (includePrimitives) {
            for (var i = 0; i < exports.primitives.length; i++) {
                var primitive = exports.primitives[i];
                var item = new vscode.CompletionItem(primitive);
                item.kind = vscode.CompletionItemKind.Keyword;
                item.detail = "Primitive";
                list.items.push(item);
            }
        }
        return list;
    }
    // Gets the corresponding CompletionItemKind (icon in completion list) for types
    static getCompletionItemKind(type) {
        switch (type) {
            case g4type.Type.type:
                return vscode.CompletionItemKind.TypeParameter;
            case g4type.Type.enum:
                return vscode.CompletionItemKind.Enum;
            case g4type.Type.dictionary:
                return vscode.CompletionItemKind.Class;
        }
        return vscode.CompletionItemKind.Field;
    }
    static getDeclarationFromName(text) {
        for (var i = 0; i < completion_1.declarations.length; i++) {
            if (completion_1.declarations[i].name === text) {
                return completion_1.declarations[i];
            }
        }
        return undefined;
    }
    // Gets the corresponding declaration from a given name
    static getDeclaration(text, uri, projPath) {
        // Get possible declarations
        var possibleDecs = [];
        for (var i = 0; i < completion_1.declarations.length; i++) {
            if (completion_1.declarations[i].name === text) {
                possibleDecs.push(completion_1.declarations[i]);
            }
        }
        // If just one, return that one
        if (possibleDecs.length === 1) {
            return possibleDecs[0];
        }
        // Get the most likely declaration via file name
        for (var i = 0; i < possibleDecs.length; i++) {
            var declaration = possibleDecs[i];
            if (declaration.fsPath === uri) {
                return declaration;
            }
        }
        var splitProjPath = projPath.split(path.sep);
        // If that doesn't work, get it via project name
        for (var i = 0; i < possibleDecs.length; i++) {
            var declaration = possibleDecs[i];
            if (splitProjPath.indexOf(declaration.projectName.split("/")[0]) !== -1) {
                return declaration;
            }
        }
        if (possibleDecs.length >= 1) {
            return possibleDecs[0];
        }
        return undefined;
    }
}
exports.Declaration = Declaration;
// Stores the usages of the Declarations
class DeclarationUsage {
    constructor(_position, _fileName, _projectName) {
        this.fileName = _fileName;
        this.projectName = _projectName;
        this.position = _position;
    }
}
exports.DeclarationUsage = DeclarationUsage;
//# sourceMappingURL=declaration.js.map