"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getTypeFromStr = exports.Type = void 0;
var Type;
(function (Type) {
    Type[Type["type"] = 0] = "type";
    Type[Type["enum"] = 1] = "enum";
    Type[Type["dictionary"] = 2] = "dictionary";
    Type[Type["default"] = 3] = "default";
    Type[Type["entity"] = 4] = "entity";
})(Type = exports.Type || (exports.Type = {}));
function getTypeFromStr(str) {
    switch (str) {
        case "entity":
            return Type.entity;
        case "type":
            return Type.type;
        case "enum":
            return Type.enum;
        case "dictionary":
            return Type.dictionary;
    }
    return Type.default;
}
exports.getTypeFromStr = getTypeFromStr;
//# sourceMappingURL=type.js.map