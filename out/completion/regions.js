"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Region = exports.RegionType = void 0;
const vscode = require("vscode");
class RegionType {
    constructor(_regions, _type) {
        this.regions = _regions;
        this.type = _type;
    }
    static getRegionTypeFromType(type) {
        for (var i = 0; i < this.regionTypes.length; i++) {
            if (RegionType.regionTypes[i].type === type) {
                return RegionType.regionTypes[i];
            }
        }
        var newRegionType = new RegionType([], type);
        RegionType.regionTypes.push(newRegionType);
        return newRegionType;
    }
}
exports.RegionType = RegionType;
RegionType.regionTypes = [];
class Region {
    constructor(_range) {
        this.range = _range;
    }
    static genRegion(context, _type) {
        var range = new vscode.Range(new vscode.Position(context._start.line - 1, context._start.charPositionInLine), new vscode.Position(context._stop.line - 1, context._stop.charPositionInLine + context.text.length));
        var region = new Region(range);
        var regionType = RegionType.getRegionTypeFromType(_type);
        regionType.regions.push(region);
    }
}
exports.Region = Region;
//# sourceMappingURL=regions.js.map