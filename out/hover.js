"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getPositionOfIdentifierStart = exports.EsslHoverProvider = void 0;
const vscode = require("vscode");
const completion_1 = require("./completion/completion");
class EsslHoverProvider {
    provideHover(document, position, token) {
        // Get the config for whether to provide hover dialog
        const config = vscode.workspace.getConfiguration('ESSL');
        var enable = config.get("EnableHoveringPreview");
        if (!enable) {
            return;
        }
        // Adjust the given position to be the start of the identifier.
        // If undefined, user is not hovering on an identifier, so return.
        var adjustedChar = getPositionOfIdentifierStart(document, position);
        if (adjustedChar === undefined) {
            return;
        }
        // Declare the new position of the hovered location
        var adjustedPosition = new vscode.Position(position.line, adjustedChar);
        // Loop through all predefined declarations to find the corresponding one
        for (var i = 0; i < completion_1.declarations.length; i++) {
            var declaration = completion_1.declarations[i];
            if (declaration.usages.length > 0) {
                for (var j = 0; j < declaration.usages.length; j++) {
                    var usage = declaration.usages[j];
                    if (usage.fileName === document.uri.fsPath && usage.position.isEqual(adjustedPosition)) {
                        // Declare the text to be shown in a tooltip above the cursor
                        var mkString = new vscode.MarkdownString(`**${declaration.name}** *${declaration.projectName}/${declaration.fileName}*\n\n${declaration.documentation}`);
                        return new vscode.Hover(mkString);
                    }
                }
            }
        }
        return undefined;
    }
}
exports.EsslHoverProvider = EsslHoverProvider;
// Adjust the given position to be the start of the identifier.
// Return undefined if the index is not an identifier.
function getPositionOfIdentifierStart(document, position) {
    var line = document.getText().split("\n")[position.line];
    var index = position.character;
    while (index >= 0) {
        if (!line.charAt(index).match(/[a-zA-Z0-9_]/)) {
            return index === position.character ? undefined : index + 1;
        }
        index -= 1;
    }
    return undefined;
}
exports.getPositionOfIdentifierStart = getPositionOfIdentifierStart;
//# sourceMappingURL=hover.js.map